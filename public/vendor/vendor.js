/*! jQuery v1.11.3 | (c) 2005, 2015 jQuery Foundation, Inc. | jquery.org/license */
!function(a,b){"object"==typeof module&&"object"==typeof module.exports?module.exports=a.document?b(a,!0):function(a){if(!a.document)throw new Error("jQuery requires a window with a document");return b(a)}:b(a)}("undefined"!=typeof window?window:this,function(a,b){var c=[],d=c.slice,e=c.concat,f=c.push,g=c.indexOf,h={},i=h.toString,j=h.hasOwnProperty,k={},l="1.11.3",m=function(a,b){return new m.fn.init(a,b)},n=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,o=/^-ms-/,p=/-([\da-z])/gi,q=function(a,b){return b.toUpperCase()};m.fn=m.prototype={jquery:l,constructor:m,selector:"",length:0,toArray:function(){return d.call(this)},get:function(a){return null!=a?0>a?this[a+this.length]:this[a]:d.call(this)},pushStack:function(a){var b=m.merge(this.constructor(),a);return b.prevObject=this,b.context=this.context,b},each:function(a,b){return m.each(this,a,b)},map:function(a){return this.pushStack(m.map(this,function(b,c){return a.call(b,c,b)}))},slice:function(){return this.pushStack(d.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},eq:function(a){var b=this.length,c=+a+(0>a?b:0);return this.pushStack(c>=0&&b>c?[this[c]]:[])},end:function(){return this.prevObject||this.constructor(null)},push:f,sort:c.sort,splice:c.splice},m.extend=m.fn.extend=function(){var a,b,c,d,e,f,g=arguments[0]||{},h=1,i=arguments.length,j=!1;for("boolean"==typeof g&&(j=g,g=arguments[h]||{},h++),"object"==typeof g||m.isFunction(g)||(g={}),h===i&&(g=this,h--);i>h;h++)if(null!=(e=arguments[h]))for(d in e)a=g[d],c=e[d],g!==c&&(j&&c&&(m.isPlainObject(c)||(b=m.isArray(c)))?(b?(b=!1,f=a&&m.isArray(a)?a:[]):f=a&&m.isPlainObject(a)?a:{},g[d]=m.extend(j,f,c)):void 0!==c&&(g[d]=c));return g},m.extend({expando:"jQuery"+(l+Math.random()).replace(/\D/g,""),isReady:!0,error:function(a){throw new Error(a)},noop:function(){},isFunction:function(a){return"function"===m.type(a)},isArray:Array.isArray||function(a){return"array"===m.type(a)},isWindow:function(a){return null!=a&&a==a.window},isNumeric:function(a){return!m.isArray(a)&&a-parseFloat(a)+1>=0},isEmptyObject:function(a){var b;for(b in a)return!1;return!0},isPlainObject:function(a){var b;if(!a||"object"!==m.type(a)||a.nodeType||m.isWindow(a))return!1;try{if(a.constructor&&!j.call(a,"constructor")&&!j.call(a.constructor.prototype,"isPrototypeOf"))return!1}catch(c){return!1}if(k.ownLast)for(b in a)return j.call(a,b);for(b in a);return void 0===b||j.call(a,b)},type:function(a){return null==a?a+"":"object"==typeof a||"function"==typeof a?h[i.call(a)]||"object":typeof a},globalEval:function(b){b&&m.trim(b)&&(a.execScript||function(b){a.eval.call(a,b)})(b)},camelCase:function(a){return a.replace(o,"ms-").replace(p,q)},nodeName:function(a,b){return a.nodeName&&a.nodeName.toLowerCase()===b.toLowerCase()},each:function(a,b,c){var d,e=0,f=a.length,g=r(a);if(c){if(g){for(;f>e;e++)if(d=b.apply(a[e],c),d===!1)break}else for(e in a)if(d=b.apply(a[e],c),d===!1)break}else if(g){for(;f>e;e++)if(d=b.call(a[e],e,a[e]),d===!1)break}else for(e in a)if(d=b.call(a[e],e,a[e]),d===!1)break;return a},trim:function(a){return null==a?"":(a+"").replace(n,"")},makeArray:function(a,b){var c=b||[];return null!=a&&(r(Object(a))?m.merge(c,"string"==typeof a?[a]:a):f.call(c,a)),c},inArray:function(a,b,c){var d;if(b){if(g)return g.call(b,a,c);for(d=b.length,c=c?0>c?Math.max(0,d+c):c:0;d>c;c++)if(c in b&&b[c]===a)return c}return-1},merge:function(a,b){var c=+b.length,d=0,e=a.length;while(c>d)a[e++]=b[d++];if(c!==c)while(void 0!==b[d])a[e++]=b[d++];return a.length=e,a},grep:function(a,b,c){for(var d,e=[],f=0,g=a.length,h=!c;g>f;f++)d=!b(a[f],f),d!==h&&e.push(a[f]);return e},map:function(a,b,c){var d,f=0,g=a.length,h=r(a),i=[];if(h)for(;g>f;f++)d=b(a[f],f,c),null!=d&&i.push(d);else for(f in a)d=b(a[f],f,c),null!=d&&i.push(d);return e.apply([],i)},guid:1,proxy:function(a,b){var c,e,f;return"string"==typeof b&&(f=a[b],b=a,a=f),m.isFunction(a)?(c=d.call(arguments,2),e=function(){return a.apply(b||this,c.concat(d.call(arguments)))},e.guid=a.guid=a.guid||m.guid++,e):void 0},now:function(){return+new Date},support:k}),m.each("Boolean Number String Function Array Date RegExp Object Error".split(" "),function(a,b){h["[object "+b+"]"]=b.toLowerCase()});function r(a){var b="length"in a&&a.length,c=m.type(a);return"function"===c||m.isWindow(a)?!1:1===a.nodeType&&b?!0:"array"===c||0===b||"number"==typeof b&&b>0&&b-1 in a}var s=function(a){var b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u="sizzle"+1*new Date,v=a.document,w=0,x=0,y=ha(),z=ha(),A=ha(),B=function(a,b){return a===b&&(l=!0),0},C=1<<31,D={}.hasOwnProperty,E=[],F=E.pop,G=E.push,H=E.push,I=E.slice,J=function(a,b){for(var c=0,d=a.length;d>c;c++)if(a[c]===b)return c;return-1},K="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",L="[\\x20\\t\\r\\n\\f]",M="(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",N=M.replace("w","w#"),O="\\["+L+"*("+M+")(?:"+L+"*([*^$|!~]?=)"+L+"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+N+"))|)"+L+"*\\]",P=":("+M+")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|"+O+")*)|.*)\\)|)",Q=new RegExp(L+"+","g"),R=new RegExp("^"+L+"+|((?:^|[^\\\\])(?:\\\\.)*)"+L+"+$","g"),S=new RegExp("^"+L+"*,"+L+"*"),T=new RegExp("^"+L+"*([>+~]|"+L+")"+L+"*"),U=new RegExp("="+L+"*([^\\]'\"]*?)"+L+"*\\]","g"),V=new RegExp(P),W=new RegExp("^"+N+"$"),X={ID:new RegExp("^#("+M+")"),CLASS:new RegExp("^\\.("+M+")"),TAG:new RegExp("^("+M.replace("w","w*")+")"),ATTR:new RegExp("^"+O),PSEUDO:new RegExp("^"+P),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+L+"*(even|odd|(([+-]|)(\\d*)n|)"+L+"*(?:([+-]|)"+L+"*(\\d+)|))"+L+"*\\)|)","i"),bool:new RegExp("^(?:"+K+")$","i"),needsContext:new RegExp("^"+L+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+L+"*((?:-\\d)?\\d*)"+L+"*\\)|)(?=[^-]|$)","i")},Y=/^(?:input|select|textarea|button)$/i,Z=/^h\d$/i,$=/^[^{]+\{\s*\[native \w/,_=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,aa=/[+~]/,ba=/'|\\/g,ca=new RegExp("\\\\([\\da-f]{1,6}"+L+"?|("+L+")|.)","ig"),da=function(a,b,c){var d="0x"+b-65536;return d!==d||c?b:0>d?String.fromCharCode(d+65536):String.fromCharCode(d>>10|55296,1023&d|56320)},ea=function(){m()};try{H.apply(E=I.call(v.childNodes),v.childNodes),E[v.childNodes.length].nodeType}catch(fa){H={apply:E.length?function(a,b){G.apply(a,I.call(b))}:function(a,b){var c=a.length,d=0;while(a[c++]=b[d++]);a.length=c-1}}}function ga(a,b,d,e){var f,h,j,k,l,o,r,s,w,x;if((b?b.ownerDocument||b:v)!==n&&m(b),b=b||n,d=d||[],k=b.nodeType,"string"!=typeof a||!a||1!==k&&9!==k&&11!==k)return d;if(!e&&p){if(11!==k&&(f=_.exec(a)))if(j=f[1]){if(9===k){if(h=b.getElementById(j),!h||!h.parentNode)return d;if(h.id===j)return d.push(h),d}else if(b.ownerDocument&&(h=b.ownerDocument.getElementById(j))&&t(b,h)&&h.id===j)return d.push(h),d}else{if(f[2])return H.apply(d,b.getElementsByTagName(a)),d;if((j=f[3])&&c.getElementsByClassName)return H.apply(d,b.getElementsByClassName(j)),d}if(c.qsa&&(!q||!q.test(a))){if(s=r=u,w=b,x=1!==k&&a,1===k&&"object"!==b.nodeName.toLowerCase()){o=g(a),(r=b.getAttribute("id"))?s=r.replace(ba,"\\$&"):b.setAttribute("id",s),s="[id='"+s+"'] ",l=o.length;while(l--)o[l]=s+ra(o[l]);w=aa.test(a)&&pa(b.parentNode)||b,x=o.join(",")}if(x)try{return H.apply(d,w.querySelectorAll(x)),d}catch(y){}finally{r||b.removeAttribute("id")}}}return i(a.replace(R,"$1"),b,d,e)}function ha(){var a=[];function b(c,e){return a.push(c+" ")>d.cacheLength&&delete b[a.shift()],b[c+" "]=e}return b}function ia(a){return a[u]=!0,a}function ja(a){var b=n.createElement("div");try{return!!a(b)}catch(c){return!1}finally{b.parentNode&&b.parentNode.removeChild(b),b=null}}function ka(a,b){var c=a.split("|"),e=a.length;while(e--)d.attrHandle[c[e]]=b}function la(a,b){var c=b&&a,d=c&&1===a.nodeType&&1===b.nodeType&&(~b.sourceIndex||C)-(~a.sourceIndex||C);if(d)return d;if(c)while(c=c.nextSibling)if(c===b)return-1;return a?1:-1}function ma(a){return function(b){var c=b.nodeName.toLowerCase();return"input"===c&&b.type===a}}function na(a){return function(b){var c=b.nodeName.toLowerCase();return("input"===c||"button"===c)&&b.type===a}}function oa(a){return ia(function(b){return b=+b,ia(function(c,d){var e,f=a([],c.length,b),g=f.length;while(g--)c[e=f[g]]&&(c[e]=!(d[e]=c[e]))})})}function pa(a){return a&&"undefined"!=typeof a.getElementsByTagName&&a}c=ga.support={},f=ga.isXML=function(a){var b=a&&(a.ownerDocument||a).documentElement;return b?"HTML"!==b.nodeName:!1},m=ga.setDocument=function(a){var b,e,g=a?a.ownerDocument||a:v;return g!==n&&9===g.nodeType&&g.documentElement?(n=g,o=g.documentElement,e=g.defaultView,e&&e!==e.top&&(e.addEventListener?e.addEventListener("unload",ea,!1):e.attachEvent&&e.attachEvent("onunload",ea)),p=!f(g),c.attributes=ja(function(a){return a.className="i",!a.getAttribute("className")}),c.getElementsByTagName=ja(function(a){return a.appendChild(g.createComment("")),!a.getElementsByTagName("*").length}),c.getElementsByClassName=$.test(g.getElementsByClassName),c.getById=ja(function(a){return o.appendChild(a).id=u,!g.getElementsByName||!g.getElementsByName(u).length}),c.getById?(d.find.ID=function(a,b){if("undefined"!=typeof b.getElementById&&p){var c=b.getElementById(a);return c&&c.parentNode?[c]:[]}},d.filter.ID=function(a){var b=a.replace(ca,da);return function(a){return a.getAttribute("id")===b}}):(delete d.find.ID,d.filter.ID=function(a){var b=a.replace(ca,da);return function(a){var c="undefined"!=typeof a.getAttributeNode&&a.getAttributeNode("id");return c&&c.value===b}}),d.find.TAG=c.getElementsByTagName?function(a,b){return"undefined"!=typeof b.getElementsByTagName?b.getElementsByTagName(a):c.qsa?b.querySelectorAll(a):void 0}:function(a,b){var c,d=[],e=0,f=b.getElementsByTagName(a);if("*"===a){while(c=f[e++])1===c.nodeType&&d.push(c);return d}return f},d.find.CLASS=c.getElementsByClassName&&function(a,b){return p?b.getElementsByClassName(a):void 0},r=[],q=[],(c.qsa=$.test(g.querySelectorAll))&&(ja(function(a){o.appendChild(a).innerHTML="<a id='"+u+"'></a><select id='"+u+"-\f]' msallowcapture=''><option selected=''></option></select>",a.querySelectorAll("[msallowcapture^='']").length&&q.push("[*^$]="+L+"*(?:''|\"\")"),a.querySelectorAll("[selected]").length||q.push("\\["+L+"*(?:value|"+K+")"),a.querySelectorAll("[id~="+u+"-]").length||q.push("~="),a.querySelectorAll(":checked").length||q.push(":checked"),a.querySelectorAll("a#"+u+"+*").length||q.push(".#.+[+~]")}),ja(function(a){var b=g.createElement("input");b.setAttribute("type","hidden"),a.appendChild(b).setAttribute("name","D"),a.querySelectorAll("[name=d]").length&&q.push("name"+L+"*[*^$|!~]?="),a.querySelectorAll(":enabled").length||q.push(":enabled",":disabled"),a.querySelectorAll("*,:x"),q.push(",.*:")})),(c.matchesSelector=$.test(s=o.matches||o.webkitMatchesSelector||o.mozMatchesSelector||o.oMatchesSelector||o.msMatchesSelector))&&ja(function(a){c.disconnectedMatch=s.call(a,"div"),s.call(a,"[s!='']:x"),r.push("!=",P)}),q=q.length&&new RegExp(q.join("|")),r=r.length&&new RegExp(r.join("|")),b=$.test(o.compareDocumentPosition),t=b||$.test(o.contains)?function(a,b){var c=9===a.nodeType?a.documentElement:a,d=b&&b.parentNode;return a===d||!(!d||1!==d.nodeType||!(c.contains?c.contains(d):a.compareDocumentPosition&&16&a.compareDocumentPosition(d)))}:function(a,b){if(b)while(b=b.parentNode)if(b===a)return!0;return!1},B=b?function(a,b){if(a===b)return l=!0,0;var d=!a.compareDocumentPosition-!b.compareDocumentPosition;return d?d:(d=(a.ownerDocument||a)===(b.ownerDocument||b)?a.compareDocumentPosition(b):1,1&d||!c.sortDetached&&b.compareDocumentPosition(a)===d?a===g||a.ownerDocument===v&&t(v,a)?-1:b===g||b.ownerDocument===v&&t(v,b)?1:k?J(k,a)-J(k,b):0:4&d?-1:1)}:function(a,b){if(a===b)return l=!0,0;var c,d=0,e=a.parentNode,f=b.parentNode,h=[a],i=[b];if(!e||!f)return a===g?-1:b===g?1:e?-1:f?1:k?J(k,a)-J(k,b):0;if(e===f)return la(a,b);c=a;while(c=c.parentNode)h.unshift(c);c=b;while(c=c.parentNode)i.unshift(c);while(h[d]===i[d])d++;return d?la(h[d],i[d]):h[d]===v?-1:i[d]===v?1:0},g):n},ga.matches=function(a,b){return ga(a,null,null,b)},ga.matchesSelector=function(a,b){if((a.ownerDocument||a)!==n&&m(a),b=b.replace(U,"='$1']"),!(!c.matchesSelector||!p||r&&r.test(b)||q&&q.test(b)))try{var d=s.call(a,b);if(d||c.disconnectedMatch||a.document&&11!==a.document.nodeType)return d}catch(e){}return ga(b,n,null,[a]).length>0},ga.contains=function(a,b){return(a.ownerDocument||a)!==n&&m(a),t(a,b)},ga.attr=function(a,b){(a.ownerDocument||a)!==n&&m(a);var e=d.attrHandle[b.toLowerCase()],f=e&&D.call(d.attrHandle,b.toLowerCase())?e(a,b,!p):void 0;return void 0!==f?f:c.attributes||!p?a.getAttribute(b):(f=a.getAttributeNode(b))&&f.specified?f.value:null},ga.error=function(a){throw new Error("Syntax error, unrecognized expression: "+a)},ga.uniqueSort=function(a){var b,d=[],e=0,f=0;if(l=!c.detectDuplicates,k=!c.sortStable&&a.slice(0),a.sort(B),l){while(b=a[f++])b===a[f]&&(e=d.push(f));while(e--)a.splice(d[e],1)}return k=null,a},e=ga.getText=function(a){var b,c="",d=0,f=a.nodeType;if(f){if(1===f||9===f||11===f){if("string"==typeof a.textContent)return a.textContent;for(a=a.firstChild;a;a=a.nextSibling)c+=e(a)}else if(3===f||4===f)return a.nodeValue}else while(b=a[d++])c+=e(b);return c},d=ga.selectors={cacheLength:50,createPseudo:ia,match:X,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(a){return a[1]=a[1].replace(ca,da),a[3]=(a[3]||a[4]||a[5]||"").replace(ca,da),"~="===a[2]&&(a[3]=" "+a[3]+" "),a.slice(0,4)},CHILD:function(a){return a[1]=a[1].toLowerCase(),"nth"===a[1].slice(0,3)?(a[3]||ga.error(a[0]),a[4]=+(a[4]?a[5]+(a[6]||1):2*("even"===a[3]||"odd"===a[3])),a[5]=+(a[7]+a[8]||"odd"===a[3])):a[3]&&ga.error(a[0]),a},PSEUDO:function(a){var b,c=!a[6]&&a[2];return X.CHILD.test(a[0])?null:(a[3]?a[2]=a[4]||a[5]||"":c&&V.test(c)&&(b=g(c,!0))&&(b=c.indexOf(")",c.length-b)-c.length)&&(a[0]=a[0].slice(0,b),a[2]=c.slice(0,b)),a.slice(0,3))}},filter:{TAG:function(a){var b=a.replace(ca,da).toLowerCase();return"*"===a?function(){return!0}:function(a){return a.nodeName&&a.nodeName.toLowerCase()===b}},CLASS:function(a){var b=y[a+" "];return b||(b=new RegExp("(^|"+L+")"+a+"("+L+"|$)"))&&y(a,function(a){return b.test("string"==typeof a.className&&a.className||"undefined"!=typeof a.getAttribute&&a.getAttribute("class")||"")})},ATTR:function(a,b,c){return function(d){var e=ga.attr(d,a);return null==e?"!="===b:b?(e+="","="===b?e===c:"!="===b?e!==c:"^="===b?c&&0===e.indexOf(c):"*="===b?c&&e.indexOf(c)>-1:"$="===b?c&&e.slice(-c.length)===c:"~="===b?(" "+e.replace(Q," ")+" ").indexOf(c)>-1:"|="===b?e===c||e.slice(0,c.length+1)===c+"-":!1):!0}},CHILD:function(a,b,c,d,e){var f="nth"!==a.slice(0,3),g="last"!==a.slice(-4),h="of-type"===b;return 1===d&&0===e?function(a){return!!a.parentNode}:function(b,c,i){var j,k,l,m,n,o,p=f!==g?"nextSibling":"previousSibling",q=b.parentNode,r=h&&b.nodeName.toLowerCase(),s=!i&&!h;if(q){if(f){while(p){l=b;while(l=l[p])if(h?l.nodeName.toLowerCase()===r:1===l.nodeType)return!1;o=p="only"===a&&!o&&"nextSibling"}return!0}if(o=[g?q.firstChild:q.lastChild],g&&s){k=q[u]||(q[u]={}),j=k[a]||[],n=j[0]===w&&j[1],m=j[0]===w&&j[2],l=n&&q.childNodes[n];while(l=++n&&l&&l[p]||(m=n=0)||o.pop())if(1===l.nodeType&&++m&&l===b){k[a]=[w,n,m];break}}else if(s&&(j=(b[u]||(b[u]={}))[a])&&j[0]===w)m=j[1];else while(l=++n&&l&&l[p]||(m=n=0)||o.pop())if((h?l.nodeName.toLowerCase()===r:1===l.nodeType)&&++m&&(s&&((l[u]||(l[u]={}))[a]=[w,m]),l===b))break;return m-=e,m===d||m%d===0&&m/d>=0}}},PSEUDO:function(a,b){var c,e=d.pseudos[a]||d.setFilters[a.toLowerCase()]||ga.error("unsupported pseudo: "+a);return e[u]?e(b):e.length>1?(c=[a,a,"",b],d.setFilters.hasOwnProperty(a.toLowerCase())?ia(function(a,c){var d,f=e(a,b),g=f.length;while(g--)d=J(a,f[g]),a[d]=!(c[d]=f[g])}):function(a){return e(a,0,c)}):e}},pseudos:{not:ia(function(a){var b=[],c=[],d=h(a.replace(R,"$1"));return d[u]?ia(function(a,b,c,e){var f,g=d(a,null,e,[]),h=a.length;while(h--)(f=g[h])&&(a[h]=!(b[h]=f))}):function(a,e,f){return b[0]=a,d(b,null,f,c),b[0]=null,!c.pop()}}),has:ia(function(a){return function(b){return ga(a,b).length>0}}),contains:ia(function(a){return a=a.replace(ca,da),function(b){return(b.textContent||b.innerText||e(b)).indexOf(a)>-1}}),lang:ia(function(a){return W.test(a||"")||ga.error("unsupported lang: "+a),a=a.replace(ca,da).toLowerCase(),function(b){var c;do if(c=p?b.lang:b.getAttribute("xml:lang")||b.getAttribute("lang"))return c=c.toLowerCase(),c===a||0===c.indexOf(a+"-");while((b=b.parentNode)&&1===b.nodeType);return!1}}),target:function(b){var c=a.location&&a.location.hash;return c&&c.slice(1)===b.id},root:function(a){return a===o},focus:function(a){return a===n.activeElement&&(!n.hasFocus||n.hasFocus())&&!!(a.type||a.href||~a.tabIndex)},enabled:function(a){return a.disabled===!1},disabled:function(a){return a.disabled===!0},checked:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&!!a.checked||"option"===b&&!!a.selected},selected:function(a){return a.parentNode&&a.parentNode.selectedIndex,a.selected===!0},empty:function(a){for(a=a.firstChild;a;a=a.nextSibling)if(a.nodeType<6)return!1;return!0},parent:function(a){return!d.pseudos.empty(a)},header:function(a){return Z.test(a.nodeName)},input:function(a){return Y.test(a.nodeName)},button:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&"button"===a.type||"button"===b},text:function(a){var b;return"input"===a.nodeName.toLowerCase()&&"text"===a.type&&(null==(b=a.getAttribute("type"))||"text"===b.toLowerCase())},first:oa(function(){return[0]}),last:oa(function(a,b){return[b-1]}),eq:oa(function(a,b,c){return[0>c?c+b:c]}),even:oa(function(a,b){for(var c=0;b>c;c+=2)a.push(c);return a}),odd:oa(function(a,b){for(var c=1;b>c;c+=2)a.push(c);return a}),lt:oa(function(a,b,c){for(var d=0>c?c+b:c;--d>=0;)a.push(d);return a}),gt:oa(function(a,b,c){for(var d=0>c?c+b:c;++d<b;)a.push(d);return a})}},d.pseudos.nth=d.pseudos.eq;for(b in{radio:!0,checkbox:!0,file:!0,password:!0,image:!0})d.pseudos[b]=ma(b);for(b in{submit:!0,reset:!0})d.pseudos[b]=na(b);function qa(){}qa.prototype=d.filters=d.pseudos,d.setFilters=new qa,g=ga.tokenize=function(a,b){var c,e,f,g,h,i,j,k=z[a+" "];if(k)return b?0:k.slice(0);h=a,i=[],j=d.preFilter;while(h){(!c||(e=S.exec(h)))&&(e&&(h=h.slice(e[0].length)||h),i.push(f=[])),c=!1,(e=T.exec(h))&&(c=e.shift(),f.push({value:c,type:e[0].replace(R," ")}),h=h.slice(c.length));for(g in d.filter)!(e=X[g].exec(h))||j[g]&&!(e=j[g](e))||(c=e.shift(),f.push({value:c,type:g,matches:e}),h=h.slice(c.length));if(!c)break}return b?h.length:h?ga.error(a):z(a,i).slice(0)};function ra(a){for(var b=0,c=a.length,d="";c>b;b++)d+=a[b].value;return d}function sa(a,b,c){var d=b.dir,e=c&&"parentNode"===d,f=x++;return b.first?function(b,c,f){while(b=b[d])if(1===b.nodeType||e)return a(b,c,f)}:function(b,c,g){var h,i,j=[w,f];if(g){while(b=b[d])if((1===b.nodeType||e)&&a(b,c,g))return!0}else while(b=b[d])if(1===b.nodeType||e){if(i=b[u]||(b[u]={}),(h=i[d])&&h[0]===w&&h[1]===f)return j[2]=h[2];if(i[d]=j,j[2]=a(b,c,g))return!0}}}function ta(a){return a.length>1?function(b,c,d){var e=a.length;while(e--)if(!a[e](b,c,d))return!1;return!0}:a[0]}function ua(a,b,c){for(var d=0,e=b.length;e>d;d++)ga(a,b[d],c);return c}function va(a,b,c,d,e){for(var f,g=[],h=0,i=a.length,j=null!=b;i>h;h++)(f=a[h])&&(!c||c(f,d,e))&&(g.push(f),j&&b.push(h));return g}function wa(a,b,c,d,e,f){return d&&!d[u]&&(d=wa(d)),e&&!e[u]&&(e=wa(e,f)),ia(function(f,g,h,i){var j,k,l,m=[],n=[],o=g.length,p=f||ua(b||"*",h.nodeType?[h]:h,[]),q=!a||!f&&b?p:va(p,m,a,h,i),r=c?e||(f?a:o||d)?[]:g:q;if(c&&c(q,r,h,i),d){j=va(r,n),d(j,[],h,i),k=j.length;while(k--)(l=j[k])&&(r[n[k]]=!(q[n[k]]=l))}if(f){if(e||a){if(e){j=[],k=r.length;while(k--)(l=r[k])&&j.push(q[k]=l);e(null,r=[],j,i)}k=r.length;while(k--)(l=r[k])&&(j=e?J(f,l):m[k])>-1&&(f[j]=!(g[j]=l))}}else r=va(r===g?r.splice(o,r.length):r),e?e(null,g,r,i):H.apply(g,r)})}function xa(a){for(var b,c,e,f=a.length,g=d.relative[a[0].type],h=g||d.relative[" "],i=g?1:0,k=sa(function(a){return a===b},h,!0),l=sa(function(a){return J(b,a)>-1},h,!0),m=[function(a,c,d){var e=!g&&(d||c!==j)||((b=c).nodeType?k(a,c,d):l(a,c,d));return b=null,e}];f>i;i++)if(c=d.relative[a[i].type])m=[sa(ta(m),c)];else{if(c=d.filter[a[i].type].apply(null,a[i].matches),c[u]){for(e=++i;f>e;e++)if(d.relative[a[e].type])break;return wa(i>1&&ta(m),i>1&&ra(a.slice(0,i-1).concat({value:" "===a[i-2].type?"*":""})).replace(R,"$1"),c,e>i&&xa(a.slice(i,e)),f>e&&xa(a=a.slice(e)),f>e&&ra(a))}m.push(c)}return ta(m)}function ya(a,b){var c=b.length>0,e=a.length>0,f=function(f,g,h,i,k){var l,m,o,p=0,q="0",r=f&&[],s=[],t=j,u=f||e&&d.find.TAG("*",k),v=w+=null==t?1:Math.random()||.1,x=u.length;for(k&&(j=g!==n&&g);q!==x&&null!=(l=u[q]);q++){if(e&&l){m=0;while(o=a[m++])if(o(l,g,h)){i.push(l);break}k&&(w=v)}c&&((l=!o&&l)&&p--,f&&r.push(l))}if(p+=q,c&&q!==p){m=0;while(o=b[m++])o(r,s,g,h);if(f){if(p>0)while(q--)r[q]||s[q]||(s[q]=F.call(i));s=va(s)}H.apply(i,s),k&&!f&&s.length>0&&p+b.length>1&&ga.uniqueSort(i)}return k&&(w=v,j=t),r};return c?ia(f):f}return h=ga.compile=function(a,b){var c,d=[],e=[],f=A[a+" "];if(!f){b||(b=g(a)),c=b.length;while(c--)f=xa(b[c]),f[u]?d.push(f):e.push(f);f=A(a,ya(e,d)),f.selector=a}return f},i=ga.select=function(a,b,e,f){var i,j,k,l,m,n="function"==typeof a&&a,o=!f&&g(a=n.selector||a);if(e=e||[],1===o.length){if(j=o[0]=o[0].slice(0),j.length>2&&"ID"===(k=j[0]).type&&c.getById&&9===b.nodeType&&p&&d.relative[j[1].type]){if(b=(d.find.ID(k.matches[0].replace(ca,da),b)||[])[0],!b)return e;n&&(b=b.parentNode),a=a.slice(j.shift().value.length)}i=X.needsContext.test(a)?0:j.length;while(i--){if(k=j[i],d.relative[l=k.type])break;if((m=d.find[l])&&(f=m(k.matches[0].replace(ca,da),aa.test(j[0].type)&&pa(b.parentNode)||b))){if(j.splice(i,1),a=f.length&&ra(j),!a)return H.apply(e,f),e;break}}}return(n||h(a,o))(f,b,!p,e,aa.test(a)&&pa(b.parentNode)||b),e},c.sortStable=u.split("").sort(B).join("")===u,c.detectDuplicates=!!l,m(),c.sortDetached=ja(function(a){return 1&a.compareDocumentPosition(n.createElement("div"))}),ja(function(a){return a.innerHTML="<a href='#'></a>","#"===a.firstChild.getAttribute("href")})||ka("type|href|height|width",function(a,b,c){return c?void 0:a.getAttribute(b,"type"===b.toLowerCase()?1:2)}),c.attributes&&ja(function(a){return a.innerHTML="<input/>",a.firstChild.setAttribute("value",""),""===a.firstChild.getAttribute("value")})||ka("value",function(a,b,c){return c||"input"!==a.nodeName.toLowerCase()?void 0:a.defaultValue}),ja(function(a){return null==a.getAttribute("disabled")})||ka(K,function(a,b,c){var d;return c?void 0:a[b]===!0?b.toLowerCase():(d=a.getAttributeNode(b))&&d.specified?d.value:null}),ga}(a);m.find=s,m.expr=s.selectors,m.expr[":"]=m.expr.pseudos,m.unique=s.uniqueSort,m.text=s.getText,m.isXMLDoc=s.isXML,m.contains=s.contains;var t=m.expr.match.needsContext,u=/^<(\w+)\s*\/?>(?:<\/\1>|)$/,v=/^.[^:#\[\.,]*$/;function w(a,b,c){if(m.isFunction(b))return m.grep(a,function(a,d){return!!b.call(a,d,a)!==c});if(b.nodeType)return m.grep(a,function(a){return a===b!==c});if("string"==typeof b){if(v.test(b))return m.filter(b,a,c);b=m.filter(b,a)}return m.grep(a,function(a){return m.inArray(a,b)>=0!==c})}m.filter=function(a,b,c){var d=b[0];return c&&(a=":not("+a+")"),1===b.length&&1===d.nodeType?m.find.matchesSelector(d,a)?[d]:[]:m.find.matches(a,m.grep(b,function(a){return 1===a.nodeType}))},m.fn.extend({find:function(a){var b,c=[],d=this,e=d.length;if("string"!=typeof a)return this.pushStack(m(a).filter(function(){for(b=0;e>b;b++)if(m.contains(d[b],this))return!0}));for(b=0;e>b;b++)m.find(a,d[b],c);return c=this.pushStack(e>1?m.unique(c):c),c.selector=this.selector?this.selector+" "+a:a,c},filter:function(a){return this.pushStack(w(this,a||[],!1))},not:function(a){return this.pushStack(w(this,a||[],!0))},is:function(a){return!!w(this,"string"==typeof a&&t.test(a)?m(a):a||[],!1).length}});var x,y=a.document,z=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,A=m.fn.init=function(a,b){var c,d;if(!a)return this;if("string"==typeof a){if(c="<"===a.charAt(0)&&">"===a.charAt(a.length-1)&&a.length>=3?[null,a,null]:z.exec(a),!c||!c[1]&&b)return!b||b.jquery?(b||x).find(a):this.constructor(b).find(a);if(c[1]){if(b=b instanceof m?b[0]:b,m.merge(this,m.parseHTML(c[1],b&&b.nodeType?b.ownerDocument||b:y,!0)),u.test(c[1])&&m.isPlainObject(b))for(c in b)m.isFunction(this[c])?this[c](b[c]):this.attr(c,b[c]);return this}if(d=y.getElementById(c[2]),d&&d.parentNode){if(d.id!==c[2])return x.find(a);this.length=1,this[0]=d}return this.context=y,this.selector=a,this}return a.nodeType?(this.context=this[0]=a,this.length=1,this):m.isFunction(a)?"undefined"!=typeof x.ready?x.ready(a):a(m):(void 0!==a.selector&&(this.selector=a.selector,this.context=a.context),m.makeArray(a,this))};A.prototype=m.fn,x=m(y);var B=/^(?:parents|prev(?:Until|All))/,C={children:!0,contents:!0,next:!0,prev:!0};m.extend({dir:function(a,b,c){var d=[],e=a[b];while(e&&9!==e.nodeType&&(void 0===c||1!==e.nodeType||!m(e).is(c)))1===e.nodeType&&d.push(e),e=e[b];return d},sibling:function(a,b){for(var c=[];a;a=a.nextSibling)1===a.nodeType&&a!==b&&c.push(a);return c}}),m.fn.extend({has:function(a){var b,c=m(a,this),d=c.length;return this.filter(function(){for(b=0;d>b;b++)if(m.contains(this,c[b]))return!0})},closest:function(a,b){for(var c,d=0,e=this.length,f=[],g=t.test(a)||"string"!=typeof a?m(a,b||this.context):0;e>d;d++)for(c=this[d];c&&c!==b;c=c.parentNode)if(c.nodeType<11&&(g?g.index(c)>-1:1===c.nodeType&&m.find.matchesSelector(c,a))){f.push(c);break}return this.pushStack(f.length>1?m.unique(f):f)},index:function(a){return a?"string"==typeof a?m.inArray(this[0],m(a)):m.inArray(a.jquery?a[0]:a,this):this[0]&&this[0].parentNode?this.first().prevAll().length:-1},add:function(a,b){return this.pushStack(m.unique(m.merge(this.get(),m(a,b))))},addBack:function(a){return this.add(null==a?this.prevObject:this.prevObject.filter(a))}});function D(a,b){do a=a[b];while(a&&1!==a.nodeType);return a}m.each({parent:function(a){var b=a.parentNode;return b&&11!==b.nodeType?b:null},parents:function(a){return m.dir(a,"parentNode")},parentsUntil:function(a,b,c){return m.dir(a,"parentNode",c)},next:function(a){return D(a,"nextSibling")},prev:function(a){return D(a,"previousSibling")},nextAll:function(a){return m.dir(a,"nextSibling")},prevAll:function(a){return m.dir(a,"previousSibling")},nextUntil:function(a,b,c){return m.dir(a,"nextSibling",c)},prevUntil:function(a,b,c){return m.dir(a,"previousSibling",c)},siblings:function(a){return m.sibling((a.parentNode||{}).firstChild,a)},children:function(a){return m.sibling(a.firstChild)},contents:function(a){return m.nodeName(a,"iframe")?a.contentDocument||a.contentWindow.document:m.merge([],a.childNodes)}},function(a,b){m.fn[a]=function(c,d){var e=m.map(this,b,c);return"Until"!==a.slice(-5)&&(d=c),d&&"string"==typeof d&&(e=m.filter(d,e)),this.length>1&&(C[a]||(e=m.unique(e)),B.test(a)&&(e=e.reverse())),this.pushStack(e)}});var E=/\S+/g,F={};function G(a){var b=F[a]={};return m.each(a.match(E)||[],function(a,c){b[c]=!0}),b}m.Callbacks=function(a){a="string"==typeof a?F[a]||G(a):m.extend({},a);var b,c,d,e,f,g,h=[],i=!a.once&&[],j=function(l){for(c=a.memory&&l,d=!0,f=g||0,g=0,e=h.length,b=!0;h&&e>f;f++)if(h[f].apply(l[0],l[1])===!1&&a.stopOnFalse){c=!1;break}b=!1,h&&(i?i.length&&j(i.shift()):c?h=[]:k.disable())},k={add:function(){if(h){var d=h.length;!function f(b){m.each(b,function(b,c){var d=m.type(c);"function"===d?a.unique&&k.has(c)||h.push(c):c&&c.length&&"string"!==d&&f(c)})}(arguments),b?e=h.length:c&&(g=d,j(c))}return this},remove:function(){return h&&m.each(arguments,function(a,c){var d;while((d=m.inArray(c,h,d))>-1)h.splice(d,1),b&&(e>=d&&e--,f>=d&&f--)}),this},has:function(a){return a?m.inArray(a,h)>-1:!(!h||!h.length)},empty:function(){return h=[],e=0,this},disable:function(){return h=i=c=void 0,this},disabled:function(){return!h},lock:function(){return i=void 0,c||k.disable(),this},locked:function(){return!i},fireWith:function(a,c){return!h||d&&!i||(c=c||[],c=[a,c.slice?c.slice():c],b?i.push(c):j(c)),this},fire:function(){return k.fireWith(this,arguments),this},fired:function(){return!!d}};return k},m.extend({Deferred:function(a){var b=[["resolve","done",m.Callbacks("once memory"),"resolved"],["reject","fail",m.Callbacks("once memory"),"rejected"],["notify","progress",m.Callbacks("memory")]],c="pending",d={state:function(){return c},always:function(){return e.done(arguments).fail(arguments),this},then:function(){var a=arguments;return m.Deferred(function(c){m.each(b,function(b,f){var g=m.isFunction(a[b])&&a[b];e[f[1]](function(){var a=g&&g.apply(this,arguments);a&&m.isFunction(a.promise)?a.promise().done(c.resolve).fail(c.reject).progress(c.notify):c[f[0]+"With"](this===d?c.promise():this,g?[a]:arguments)})}),a=null}).promise()},promise:function(a){return null!=a?m.extend(a,d):d}},e={};return d.pipe=d.then,m.each(b,function(a,f){var g=f[2],h=f[3];d[f[1]]=g.add,h&&g.add(function(){c=h},b[1^a][2].disable,b[2][2].lock),e[f[0]]=function(){return e[f[0]+"With"](this===e?d:this,arguments),this},e[f[0]+"With"]=g.fireWith}),d.promise(e),a&&a.call(e,e),e},when:function(a){var b=0,c=d.call(arguments),e=c.length,f=1!==e||a&&m.isFunction(a.promise)?e:0,g=1===f?a:m.Deferred(),h=function(a,b,c){return function(e){b[a]=this,c[a]=arguments.length>1?d.call(arguments):e,c===i?g.notifyWith(b,c):--f||g.resolveWith(b,c)}},i,j,k;if(e>1)for(i=new Array(e),j=new Array(e),k=new Array(e);e>b;b++)c[b]&&m.isFunction(c[b].promise)?c[b].promise().done(h(b,k,c)).fail(g.reject).progress(h(b,j,i)):--f;return f||g.resolveWith(k,c),g.promise()}});var H;m.fn.ready=function(a){return m.ready.promise().done(a),this},m.extend({isReady:!1,readyWait:1,holdReady:function(a){a?m.readyWait++:m.ready(!0)},ready:function(a){if(a===!0?!--m.readyWait:!m.isReady){if(!y.body)return setTimeout(m.ready);m.isReady=!0,a!==!0&&--m.readyWait>0||(H.resolveWith(y,[m]),m.fn.triggerHandler&&(m(y).triggerHandler("ready"),m(y).off("ready")))}}});function I(){y.addEventListener?(y.removeEventListener("DOMContentLoaded",J,!1),a.removeEventListener("load",J,!1)):(y.detachEvent("onreadystatechange",J),a.detachEvent("onload",J))}function J(){(y.addEventListener||"load"===event.type||"complete"===y.readyState)&&(I(),m.ready())}m.ready.promise=function(b){if(!H)if(H=m.Deferred(),"complete"===y.readyState)setTimeout(m.ready);else if(y.addEventListener)y.addEventListener("DOMContentLoaded",J,!1),a.addEventListener("load",J,!1);else{y.attachEvent("onreadystatechange",J),a.attachEvent("onload",J);var c=!1;try{c=null==a.frameElement&&y.documentElement}catch(d){}c&&c.doScroll&&!function e(){if(!m.isReady){try{c.doScroll("left")}catch(a){return setTimeout(e,50)}I(),m.ready()}}()}return H.promise(b)};var K="undefined",L;for(L in m(k))break;k.ownLast="0"!==L,k.inlineBlockNeedsLayout=!1,m(function(){var a,b,c,d;c=y.getElementsByTagName("body")[0],c&&c.style&&(b=y.createElement("div"),d=y.createElement("div"),d.style.cssText="position:absolute;border:0;width:0;height:0;top:0;left:-9999px",c.appendChild(d).appendChild(b),typeof b.style.zoom!==K&&(b.style.cssText="display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1",k.inlineBlockNeedsLayout=a=3===b.offsetWidth,a&&(c.style.zoom=1)),c.removeChild(d))}),function(){var a=y.createElement("div");if(null==k.deleteExpando){k.deleteExpando=!0;try{delete a.test}catch(b){k.deleteExpando=!1}}a=null}(),m.acceptData=function(a){var b=m.noData[(a.nodeName+" ").toLowerCase()],c=+a.nodeType||1;return 1!==c&&9!==c?!1:!b||b!==!0&&a.getAttribute("classid")===b};var M=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,N=/([A-Z])/g;function O(a,b,c){if(void 0===c&&1===a.nodeType){var d="data-"+b.replace(N,"-$1").toLowerCase();if(c=a.getAttribute(d),"string"==typeof c){try{c="true"===c?!0:"false"===c?!1:"null"===c?null:+c+""===c?+c:M.test(c)?m.parseJSON(c):c}catch(e){}m.data(a,b,c)}else c=void 0}return c}function P(a){var b;for(b in a)if(("data"!==b||!m.isEmptyObject(a[b]))&&"toJSON"!==b)return!1;

  return!0}function Q(a,b,d,e){if(m.acceptData(a)){var f,g,h=m.expando,i=a.nodeType,j=i?m.cache:a,k=i?a[h]:a[h]&&h;if(k&&j[k]&&(e||j[k].data)||void 0!==d||"string"!=typeof b)return k||(k=i?a[h]=c.pop()||m.guid++:h),j[k]||(j[k]=i?{}:{toJSON:m.noop}),("object"==typeof b||"function"==typeof b)&&(e?j[k]=m.extend(j[k],b):j[k].data=m.extend(j[k].data,b)),g=j[k],e||(g.data||(g.data={}),g=g.data),void 0!==d&&(g[m.camelCase(b)]=d),"string"==typeof b?(f=g[b],null==f&&(f=g[m.camelCase(b)])):f=g,f}}function R(a,b,c){if(m.acceptData(a)){var d,e,f=a.nodeType,g=f?m.cache:a,h=f?a[m.expando]:m.expando;if(g[h]){if(b&&(d=c?g[h]:g[h].data)){m.isArray(b)?b=b.concat(m.map(b,m.camelCase)):b in d?b=[b]:(b=m.camelCase(b),b=b in d?[b]:b.split(" ")),e=b.length;while(e--)delete d[b[e]];if(c?!P(d):!m.isEmptyObject(d))return}(c||(delete g[h].data,P(g[h])))&&(f?m.cleanData([a],!0):k.deleteExpando||g!=g.window?delete g[h]:g[h]=null)}}}m.extend({cache:{},noData:{"applet ":!0,"embed ":!0,"object ":"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"},hasData:function(a){return a=a.nodeType?m.cache[a[m.expando]]:a[m.expando],!!a&&!P(a)},data:function(a,b,c){return Q(a,b,c)},removeData:function(a,b){return R(a,b)},_data:function(a,b,c){return Q(a,b,c,!0)},_removeData:function(a,b){return R(a,b,!0)}}),m.fn.extend({data:function(a,b){var c,d,e,f=this[0],g=f&&f.attributes;if(void 0===a){if(this.length&&(e=m.data(f),1===f.nodeType&&!m._data(f,"parsedAttrs"))){c=g.length;while(c--)g[c]&&(d=g[c].name,0===d.indexOf("data-")&&(d=m.camelCase(d.slice(5)),O(f,d,e[d])));m._data(f,"parsedAttrs",!0)}return e}return"object"==typeof a?this.each(function(){m.data(this,a)}):arguments.length>1?this.each(function(){m.data(this,a,b)}):f?O(f,a,m.data(f,a)):void 0},removeData:function(a){return this.each(function(){m.removeData(this,a)})}}),m.extend({queue:function(a,b,c){var d;return a?(b=(b||"fx")+"queue",d=m._data(a,b),c&&(!d||m.isArray(c)?d=m._data(a,b,m.makeArray(c)):d.push(c)),d||[]):void 0},dequeue:function(a,b){b=b||"fx";var c=m.queue(a,b),d=c.length,e=c.shift(),f=m._queueHooks(a,b),g=function(){m.dequeue(a,b)};"inprogress"===e&&(e=c.shift(),d--),e&&("fx"===b&&c.unshift("inprogress"),delete f.stop,e.call(a,g,f)),!d&&f&&f.empty.fire()},_queueHooks:function(a,b){var c=b+"queueHooks";return m._data(a,c)||m._data(a,c,{empty:m.Callbacks("once memory").add(function(){m._removeData(a,b+"queue"),m._removeData(a,c)})})}}),m.fn.extend({queue:function(a,b){var c=2;return"string"!=typeof a&&(b=a,a="fx",c--),arguments.length<c?m.queue(this[0],a):void 0===b?this:this.each(function(){var c=m.queue(this,a,b);m._queueHooks(this,a),"fx"===a&&"inprogress"!==c[0]&&m.dequeue(this,a)})},dequeue:function(a){return this.each(function(){m.dequeue(this,a)})},clearQueue:function(a){return this.queue(a||"fx",[])},promise:function(a,b){var c,d=1,e=m.Deferred(),f=this,g=this.length,h=function(){--d||e.resolveWith(f,[f])};"string"!=typeof a&&(b=a,a=void 0),a=a||"fx";while(g--)c=m._data(f[g],a+"queueHooks"),c&&c.empty&&(d++,c.empty.add(h));return h(),e.promise(b)}});var S=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,T=["Top","Right","Bottom","Left"],U=function(a,b){return a=b||a,"none"===m.css(a,"display")||!m.contains(a.ownerDocument,a)},V=m.access=function(a,b,c,d,e,f,g){var h=0,i=a.length,j=null==c;if("object"===m.type(c)){e=!0;for(h in c)m.access(a,b,h,c[h],!0,f,g)}else if(void 0!==d&&(e=!0,m.isFunction(d)||(g=!0),j&&(g?(b.call(a,d),b=null):(j=b,b=function(a,b,c){return j.call(m(a),c)})),b))for(;i>h;h++)b(a[h],c,g?d:d.call(a[h],h,b(a[h],c)));return e?a:j?b.call(a):i?b(a[0],c):f},W=/^(?:checkbox|radio)$/i;!function(){var a=y.createElement("input"),b=y.createElement("div"),c=y.createDocumentFragment();if(b.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",k.leadingWhitespace=3===b.firstChild.nodeType,k.tbody=!b.getElementsByTagName("tbody").length,k.htmlSerialize=!!b.getElementsByTagName("link").length,k.html5Clone="<:nav></:nav>"!==y.createElement("nav").cloneNode(!0).outerHTML,a.type="checkbox",a.checked=!0,c.appendChild(a),k.appendChecked=a.checked,b.innerHTML="<textarea>x</textarea>",k.noCloneChecked=!!b.cloneNode(!0).lastChild.defaultValue,c.appendChild(b),b.innerHTML="<input type='radio' checked='checked' name='t'/>",k.checkClone=b.cloneNode(!0).cloneNode(!0).lastChild.checked,k.noCloneEvent=!0,b.attachEvent&&(b.attachEvent("onclick",function(){k.noCloneEvent=!1}),b.cloneNode(!0).click()),null==k.deleteExpando){k.deleteExpando=!0;try{delete b.test}catch(d){k.deleteExpando=!1}}}(),function(){var b,c,d=y.createElement("div");for(b in{submit:!0,change:!0,focusin:!0})c="on"+b,(k[b+"Bubbles"]=c in a)||(d.setAttribute(c,"t"),k[b+"Bubbles"]=d.attributes[c].expando===!1);d=null}();var X=/^(?:input|select|textarea)$/i,Y=/^key/,Z=/^(?:mouse|pointer|contextmenu)|click/,$=/^(?:focusinfocus|focusoutblur)$/,_=/^([^.]*)(?:\.(.+)|)$/;function aa(){return!0}function ba(){return!1}function ca(){try{return y.activeElement}catch(a){}}m.event={global:{},add:function(a,b,c,d,e){var f,g,h,i,j,k,l,n,o,p,q,r=m._data(a);if(r){c.handler&&(i=c,c=i.handler,e=i.selector),c.guid||(c.guid=m.guid++),(g=r.events)||(g=r.events={}),(k=r.handle)||(k=r.handle=function(a){return typeof m===K||a&&m.event.triggered===a.type?void 0:m.event.dispatch.apply(k.elem,arguments)},k.elem=a),b=(b||"").match(E)||[""],h=b.length;while(h--)f=_.exec(b[h])||[],o=q=f[1],p=(f[2]||"").split(".").sort(),o&&(j=m.event.special[o]||{},o=(e?j.delegateType:j.bindType)||o,j=m.event.special[o]||{},l=m.extend({type:o,origType:q,data:d,handler:c,guid:c.guid,selector:e,needsContext:e&&m.expr.match.needsContext.test(e),namespace:p.join(".")},i),(n=g[o])||(n=g[o]=[],n.delegateCount=0,j.setup&&j.setup.call(a,d,p,k)!==!1||(a.addEventListener?a.addEventListener(o,k,!1):a.attachEvent&&a.attachEvent("on"+o,k))),j.add&&(j.add.call(a,l),l.handler.guid||(l.handler.guid=c.guid)),e?n.splice(n.delegateCount++,0,l):n.push(l),m.event.global[o]=!0);a=null}},remove:function(a,b,c,d,e){var f,g,h,i,j,k,l,n,o,p,q,r=m.hasData(a)&&m._data(a);if(r&&(k=r.events)){b=(b||"").match(E)||[""],j=b.length;while(j--)if(h=_.exec(b[j])||[],o=q=h[1],p=(h[2]||"").split(".").sort(),o){l=m.event.special[o]||{},o=(d?l.delegateType:l.bindType)||o,n=k[o]||[],h=h[2]&&new RegExp("(^|\\.)"+p.join("\\.(?:.*\\.|)")+"(\\.|$)"),i=f=n.length;while(f--)g=n[f],!e&&q!==g.origType||c&&c.guid!==g.guid||h&&!h.test(g.namespace)||d&&d!==g.selector&&("**"!==d||!g.selector)||(n.splice(f,1),g.selector&&n.delegateCount--,l.remove&&l.remove.call(a,g));i&&!n.length&&(l.teardown&&l.teardown.call(a,p,r.handle)!==!1||m.removeEvent(a,o,r.handle),delete k[o])}else for(o in k)m.event.remove(a,o+b[j],c,d,!0);m.isEmptyObject(k)&&(delete r.handle,m._removeData(a,"events"))}},trigger:function(b,c,d,e){var f,g,h,i,k,l,n,o=[d||y],p=j.call(b,"type")?b.type:b,q=j.call(b,"namespace")?b.namespace.split("."):[];if(h=l=d=d||y,3!==d.nodeType&&8!==d.nodeType&&!$.test(p+m.event.triggered)&&(p.indexOf(".")>=0&&(q=p.split("."),p=q.shift(),q.sort()),g=p.indexOf(":")<0&&"on"+p,b=b[m.expando]?b:new m.Event(p,"object"==typeof b&&b),b.isTrigger=e?2:3,b.namespace=q.join("."),b.namespace_re=b.namespace?new RegExp("(^|\\.)"+q.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,b.result=void 0,b.target||(b.target=d),c=null==c?[b]:m.makeArray(c,[b]),k=m.event.special[p]||{},e||!k.trigger||k.trigger.apply(d,c)!==!1)){if(!e&&!k.noBubble&&!m.isWindow(d)){for(i=k.delegateType||p,$.test(i+p)||(h=h.parentNode);h;h=h.parentNode)o.push(h),l=h;l===(d.ownerDocument||y)&&o.push(l.defaultView||l.parentWindow||a)}n=0;while((h=o[n++])&&!b.isPropagationStopped())b.type=n>1?i:k.bindType||p,f=(m._data(h,"events")||{})[b.type]&&m._data(h,"handle"),f&&f.apply(h,c),f=g&&h[g],f&&f.apply&&m.acceptData(h)&&(b.result=f.apply(h,c),b.result===!1&&b.preventDefault());if(b.type=p,!e&&!b.isDefaultPrevented()&&(!k._default||k._default.apply(o.pop(),c)===!1)&&m.acceptData(d)&&g&&d[p]&&!m.isWindow(d)){l=d[g],l&&(d[g]=null),m.event.triggered=p;try{d[p]()}catch(r){}m.event.triggered=void 0,l&&(d[g]=l)}return b.result}},dispatch:function(a){a=m.event.fix(a);var b,c,e,f,g,h=[],i=d.call(arguments),j=(m._data(this,"events")||{})[a.type]||[],k=m.event.special[a.type]||{};if(i[0]=a,a.delegateTarget=this,!k.preDispatch||k.preDispatch.call(this,a)!==!1){h=m.event.handlers.call(this,a,j),b=0;while((f=h[b++])&&!a.isPropagationStopped()){a.currentTarget=f.elem,g=0;while((e=f.handlers[g++])&&!a.isImmediatePropagationStopped())(!a.namespace_re||a.namespace_re.test(e.namespace))&&(a.handleObj=e,a.data=e.data,c=((m.event.special[e.origType]||{}).handle||e.handler).apply(f.elem,i),void 0!==c&&(a.result=c)===!1&&(a.preventDefault(),a.stopPropagation()))}return k.postDispatch&&k.postDispatch.call(this,a),a.result}},handlers:function(a,b){var c,d,e,f,g=[],h=b.delegateCount,i=a.target;if(h&&i.nodeType&&(!a.button||"click"!==a.type))for(;i!=this;i=i.parentNode||this)if(1===i.nodeType&&(i.disabled!==!0||"click"!==a.type)){for(e=[],f=0;h>f;f++)d=b[f],c=d.selector+" ",void 0===e[c]&&(e[c]=d.needsContext?m(c,this).index(i)>=0:m.find(c,this,null,[i]).length),e[c]&&e.push(d);e.length&&g.push({elem:i,handlers:e})}return h<b.length&&g.push({elem:this,handlers:b.slice(h)}),g},fix:function(a){if(a[m.expando])return a;var b,c,d,e=a.type,f=a,g=this.fixHooks[e];g||(this.fixHooks[e]=g=Z.test(e)?this.mouseHooks:Y.test(e)?this.keyHooks:{}),d=g.props?this.props.concat(g.props):this.props,a=new m.Event(f),b=d.length;while(b--)c=d[b],a[c]=f[c];return a.target||(a.target=f.srcElement||y),3===a.target.nodeType&&(a.target=a.target.parentNode),a.metaKey=!!a.metaKey,g.filter?g.filter(a,f):a},props:"altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),fixHooks:{},keyHooks:{props:"char charCode key keyCode".split(" "),filter:function(a,b){return null==a.which&&(a.which=null!=b.charCode?b.charCode:b.keyCode),a}},mouseHooks:{props:"button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),filter:function(a,b){var c,d,e,f=b.button,g=b.fromElement;return null==a.pageX&&null!=b.clientX&&(d=a.target.ownerDocument||y,e=d.documentElement,c=d.body,a.pageX=b.clientX+(e&&e.scrollLeft||c&&c.scrollLeft||0)-(e&&e.clientLeft||c&&c.clientLeft||0),a.pageY=b.clientY+(e&&e.scrollTop||c&&c.scrollTop||0)-(e&&e.clientTop||c&&c.clientTop||0)),!a.relatedTarget&&g&&(a.relatedTarget=g===a.target?b.toElement:g),a.which||void 0===f||(a.which=1&f?1:2&f?3:4&f?2:0),a}},special:{load:{noBubble:!0},focus:{trigger:function(){if(this!==ca()&&this.focus)try{return this.focus(),!1}catch(a){}},delegateType:"focusin"},blur:{trigger:function(){return this===ca()&&this.blur?(this.blur(),!1):void 0},delegateType:"focusout"},click:{trigger:function(){return m.nodeName(this,"input")&&"checkbox"===this.type&&this.click?(this.click(),!1):void 0},_default:function(a){return m.nodeName(a.target,"a")}},beforeunload:{postDispatch:function(a){void 0!==a.result&&a.originalEvent&&(a.originalEvent.returnValue=a.result)}}},simulate:function(a,b,c,d){var e=m.extend(new m.Event,c,{type:a,isSimulated:!0,originalEvent:{}});d?m.event.trigger(e,null,b):m.event.dispatch.call(b,e),e.isDefaultPrevented()&&c.preventDefault()}},m.removeEvent=y.removeEventListener?function(a,b,c){a.removeEventListener&&a.removeEventListener(b,c,!1)}:function(a,b,c){var d="on"+b;a.detachEvent&&(typeof a[d]===K&&(a[d]=null),a.detachEvent(d,c))},m.Event=function(a,b){return this instanceof m.Event?(a&&a.type?(this.originalEvent=a,this.type=a.type,this.isDefaultPrevented=a.defaultPrevented||void 0===a.defaultPrevented&&a.returnValue===!1?aa:ba):this.type=a,b&&m.extend(this,b),this.timeStamp=a&&a.timeStamp||m.now(),void(this[m.expando]=!0)):new m.Event(a,b)},m.Event.prototype={isDefaultPrevented:ba,isPropagationStopped:ba,isImmediatePropagationStopped:ba,preventDefault:function(){var a=this.originalEvent;this.isDefaultPrevented=aa,a&&(a.preventDefault?a.preventDefault():a.returnValue=!1)},stopPropagation:function(){var a=this.originalEvent;this.isPropagationStopped=aa,a&&(a.stopPropagation&&a.stopPropagation(),a.cancelBubble=!0)},stopImmediatePropagation:function(){var a=this.originalEvent;this.isImmediatePropagationStopped=aa,a&&a.stopImmediatePropagation&&a.stopImmediatePropagation(),this.stopPropagation()}},m.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(a,b){m.event.special[a]={delegateType:b,bindType:b,handle:function(a){var c,d=this,e=a.relatedTarget,f=a.handleObj;return(!e||e!==d&&!m.contains(d,e))&&(a.type=f.origType,c=f.handler.apply(this,arguments),a.type=b),c}}}),k.submitBubbles||(m.event.special.submit={setup:function(){return m.nodeName(this,"form")?!1:void m.event.add(this,"click._submit keypress._submit",function(a){var b=a.target,c=m.nodeName(b,"input")||m.nodeName(b,"button")?b.form:void 0;c&&!m._data(c,"submitBubbles")&&(m.event.add(c,"submit._submit",function(a){a._submit_bubble=!0}),m._data(c,"submitBubbles",!0))})},postDispatch:function(a){a._submit_bubble&&(delete a._submit_bubble,this.parentNode&&!a.isTrigger&&m.event.simulate("submit",this.parentNode,a,!0))},teardown:function(){return m.nodeName(this,"form")?!1:void m.event.remove(this,"._submit")}}),k.changeBubbles||(m.event.special.change={setup:function(){return X.test(this.nodeName)?(("checkbox"===this.type||"radio"===this.type)&&(m.event.add(this,"propertychange._change",function(a){"checked"===a.originalEvent.propertyName&&(this._just_changed=!0)}),m.event.add(this,"click._change",function(a){this._just_changed&&!a.isTrigger&&(this._just_changed=!1),m.event.simulate("change",this,a,!0)})),!1):void m.event.add(this,"beforeactivate._change",function(a){var b=a.target;X.test(b.nodeName)&&!m._data(b,"changeBubbles")&&(m.event.add(b,"change._change",function(a){!this.parentNode||a.isSimulated||a.isTrigger||m.event.simulate("change",this.parentNode,a,!0)}),m._data(b,"changeBubbles",!0))})},handle:function(a){var b=a.target;return this!==b||a.isSimulated||a.isTrigger||"radio"!==b.type&&"checkbox"!==b.type?a.handleObj.handler.apply(this,arguments):void 0},teardown:function(){return m.event.remove(this,"._change"),!X.test(this.nodeName)}}),k.focusinBubbles||m.each({focus:"focusin",blur:"focusout"},function(a,b){var c=function(a){m.event.simulate(b,a.target,m.event.fix(a),!0)};m.event.special[b]={setup:function(){var d=this.ownerDocument||this,e=m._data(d,b);e||d.addEventListener(a,c,!0),m._data(d,b,(e||0)+1)},teardown:function(){var d=this.ownerDocument||this,e=m._data(d,b)-1;e?m._data(d,b,e):(d.removeEventListener(a,c,!0),m._removeData(d,b))}}}),m.fn.extend({on:function(a,b,c,d,e){var f,g;if("object"==typeof a){"string"!=typeof b&&(c=c||b,b=void 0);for(f in a)this.on(f,b,c,a[f],e);return this}if(null==c&&null==d?(d=b,c=b=void 0):null==d&&("string"==typeof b?(d=c,c=void 0):(d=c,c=b,b=void 0)),d===!1)d=ba;else if(!d)return this;return 1===e&&(g=d,d=function(a){return m().off(a),g.apply(this,arguments)},d.guid=g.guid||(g.guid=m.guid++)),this.each(function(){m.event.add(this,a,d,c,b)})},one:function(a,b,c,d){return this.on(a,b,c,d,1)},off:function(a,b,c){var d,e;if(a&&a.preventDefault&&a.handleObj)return d=a.handleObj,m(a.delegateTarget).off(d.namespace?d.origType+"."+d.namespace:d.origType,d.selector,d.handler),this;if("object"==typeof a){for(e in a)this.off(e,b,a[e]);return this}return(b===!1||"function"==typeof b)&&(c=b,b=void 0),c===!1&&(c=ba),this.each(function(){m.event.remove(this,a,c,b)})},trigger:function(a,b){return this.each(function(){m.event.trigger(a,b,this)})},triggerHandler:function(a,b){var c=this[0];return c?m.event.trigger(a,b,c,!0):void 0}});function da(a){var b=ea.split("|"),c=a.createDocumentFragment();if(c.createElement)while(b.length)c.createElement(b.pop());return c}var ea="abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",fa=/ jQuery\d+="(?:null|\d+)"/g,ga=new RegExp("<(?:"+ea+")[\\s/>]","i"),ha=/^\s+/,ia=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,ja=/<([\w:]+)/,ka=/<tbody/i,la=/<|&#?\w+;/,ma=/<(?:script|style|link)/i,na=/checked\s*(?:[^=]|=\s*.checked.)/i,oa=/^$|\/(?:java|ecma)script/i,pa=/^true\/(.*)/,qa=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,ra={option:[1,"<select multiple='multiple'>","</select>"],legend:[1,"<fieldset>","</fieldset>"],area:[1,"<map>","</map>"],param:[1,"<object>","</object>"],thead:[1,"<table>","</table>"],tr:[2,"<table><tbody>","</tbody></table>"],col:[2,"<table><tbody></tbody><colgroup>","</colgroup></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:k.htmlSerialize?[0,"",""]:[1,"X<div>","</div>"]},sa=da(y),ta=sa.appendChild(y.createElement("div"));ra.optgroup=ra.option,ra.tbody=ra.tfoot=ra.colgroup=ra.caption=ra.thead,ra.th=ra.td;function ua(a,b){var c,d,e=0,f=typeof a.getElementsByTagName!==K?a.getElementsByTagName(b||"*"):typeof a.querySelectorAll!==K?a.querySelectorAll(b||"*"):void 0;if(!f)for(f=[],c=a.childNodes||a;null!=(d=c[e]);e++)!b||m.nodeName(d,b)?f.push(d):m.merge(f,ua(d,b));return void 0===b||b&&m.nodeName(a,b)?m.merge([a],f):f}function va(a){W.test(a.type)&&(a.defaultChecked=a.checked)}function wa(a,b){return m.nodeName(a,"table")&&m.nodeName(11!==b.nodeType?b:b.firstChild,"tr")?a.getElementsByTagName("tbody")[0]||a.appendChild(a.ownerDocument.createElement("tbody")):a}function xa(a){return a.type=(null!==m.find.attr(a,"type"))+"/"+a.type,a}function ya(a){var b=pa.exec(a.type);return b?a.type=b[1]:a.removeAttribute("type"),a}function za(a,b){for(var c,d=0;null!=(c=a[d]);d++)m._data(c,"globalEval",!b||m._data(b[d],"globalEval"))}function Aa(a,b){if(1===b.nodeType&&m.hasData(a)){var c,d,e,f=m._data(a),g=m._data(b,f),h=f.events;if(h){delete g.handle,g.events={};for(c in h)for(d=0,e=h[c].length;e>d;d++)m.event.add(b,c,h[c][d])}g.data&&(g.data=m.extend({},g.data))}}function Ba(a,b){var c,d,e;if(1===b.nodeType){if(c=b.nodeName.toLowerCase(),!k.noCloneEvent&&b[m.expando]){e=m._data(b);for(d in e.events)m.removeEvent(b,d,e.handle);b.removeAttribute(m.expando)}"script"===c&&b.text!==a.text?(xa(b).text=a.text,ya(b)):"object"===c?(b.parentNode&&(b.outerHTML=a.outerHTML),k.html5Clone&&a.innerHTML&&!m.trim(b.innerHTML)&&(b.innerHTML=a.innerHTML)):"input"===c&&W.test(a.type)?(b.defaultChecked=b.checked=a.checked,b.value!==a.value&&(b.value=a.value)):"option"===c?b.defaultSelected=b.selected=a.defaultSelected:("input"===c||"textarea"===c)&&(b.defaultValue=a.defaultValue)}}m.extend({clone:function(a,b,c){var d,e,f,g,h,i=m.contains(a.ownerDocument,a);if(k.html5Clone||m.isXMLDoc(a)||!ga.test("<"+a.nodeName+">")?f=a.cloneNode(!0):(ta.innerHTML=a.outerHTML,ta.removeChild(f=ta.firstChild)),!(k.noCloneEvent&&k.noCloneChecked||1!==a.nodeType&&11!==a.nodeType||m.isXMLDoc(a)))for(d=ua(f),h=ua(a),g=0;null!=(e=h[g]);++g)d[g]&&Ba(e,d[g]);if(b)if(c)for(h=h||ua(a),d=d||ua(f),g=0;null!=(e=h[g]);g++)Aa(e,d[g]);else Aa(a,f);return d=ua(f,"script"),d.length>0&&za(d,!i&&ua(a,"script")),d=h=e=null,f},buildFragment:function(a,b,c,d){for(var e,f,g,h,i,j,l,n=a.length,o=da(b),p=[],q=0;n>q;q++)if(f=a[q],f||0===f)if("object"===m.type(f))m.merge(p,f.nodeType?[f]:f);else if(la.test(f)){h=h||o.appendChild(b.createElement("div")),i=(ja.exec(f)||["",""])[1].toLowerCase(),l=ra[i]||ra._default,h.innerHTML=l[1]+f.replace(ia,"<$1></$2>")+l[2],e=l[0];while(e--)h=h.lastChild;if(!k.leadingWhitespace&&ha.test(f)&&p.push(b.createTextNode(ha.exec(f)[0])),!k.tbody){f="table"!==i||ka.test(f)?"<table>"!==l[1]||ka.test(f)?0:h:h.firstChild,e=f&&f.childNodes.length;while(e--)m.nodeName(j=f.childNodes[e],"tbody")&&!j.childNodes.length&&f.removeChild(j)}m.merge(p,h.childNodes),h.textContent="";while(h.firstChild)h.removeChild(h.firstChild);h=o.lastChild}else p.push(b.createTextNode(f));h&&o.removeChild(h),k.appendChecked||m.grep(ua(p,"input"),va),q=0;while(f=p[q++])if((!d||-1===m.inArray(f,d))&&(g=m.contains(f.ownerDocument,f),h=ua(o.appendChild(f),"script"),g&&za(h),c)){e=0;while(f=h[e++])oa.test(f.type||"")&&c.push(f)}return h=null,o},cleanData:function(a,b){for(var d,e,f,g,h=0,i=m.expando,j=m.cache,l=k.deleteExpando,n=m.event.special;null!=(d=a[h]);h++)if((b||m.acceptData(d))&&(f=d[i],g=f&&j[f])){if(g.events)for(e in g.events)n[e]?m.event.remove(d,e):m.removeEvent(d,e,g.handle);j[f]&&(delete j[f],l?delete d[i]:typeof d.removeAttribute!==K?d.removeAttribute(i):d[i]=null,c.push(f))}}}),m.fn.extend({text:function(a){return V(this,function(a){return void 0===a?m.text(this):this.empty().append((this[0]&&this[0].ownerDocument||y).createTextNode(a))},null,a,arguments.length)},append:function(){return this.domManip(arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=wa(this,a);b.appendChild(a)}})},prepend:function(){return this.domManip(arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=wa(this,a);b.insertBefore(a,b.firstChild)}})},before:function(){return this.domManip(arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this)})},after:function(){return this.domManip(arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this.nextSibling)})},remove:function(a,b){for(var c,d=a?m.filter(a,this):this,e=0;null!=(c=d[e]);e++)b||1!==c.nodeType||m.cleanData(ua(c)),c.parentNode&&(b&&m.contains(c.ownerDocument,c)&&za(ua(c,"script")),c.parentNode.removeChild(c));return this},empty:function(){for(var a,b=0;null!=(a=this[b]);b++){1===a.nodeType&&m.cleanData(ua(a,!1));while(a.firstChild)a.removeChild(a.firstChild);a.options&&m.nodeName(a,"select")&&(a.options.length=0)}return this},clone:function(a,b){return a=null==a?!1:a,b=null==b?a:b,this.map(function(){return m.clone(this,a,b)})},html:function(a){return V(this,function(a){var b=this[0]||{},c=0,d=this.length;if(void 0===a)return 1===b.nodeType?b.innerHTML.replace(fa,""):void 0;if(!("string"!=typeof a||ma.test(a)||!k.htmlSerialize&&ga.test(a)||!k.leadingWhitespace&&ha.test(a)||ra[(ja.exec(a)||["",""])[1].toLowerCase()])){a=a.replace(ia,"<$1></$2>");try{for(;d>c;c++)b=this[c]||{},1===b.nodeType&&(m.cleanData(ua(b,!1)),b.innerHTML=a);b=0}catch(e){}}b&&this.empty().append(a)},null,a,arguments.length)},replaceWith:function(){var a=arguments[0];return this.domManip(arguments,function(b){a=this.parentNode,m.cleanData(ua(this)),a&&a.replaceChild(b,this)}),a&&(a.length||a.nodeType)?this:this.remove()},detach:function(a){return this.remove(a,!0)},domManip:function(a,b){a=e.apply([],a);var c,d,f,g,h,i,j=0,l=this.length,n=this,o=l-1,p=a[0],q=m.isFunction(p);if(q||l>1&&"string"==typeof p&&!k.checkClone&&na.test(p))return this.each(function(c){var d=n.eq(c);q&&(a[0]=p.call(this,c,d.html())),d.domManip(a,b)});if(l&&(i=m.buildFragment(a,this[0].ownerDocument,!1,this),c=i.firstChild,1===i.childNodes.length&&(i=c),c)){for(g=m.map(ua(i,"script"),xa),f=g.length;l>j;j++)d=i,j!==o&&(d=m.clone(d,!0,!0),f&&m.merge(g,ua(d,"script"))),b.call(this[j],d,j);if(f)for(h=g[g.length-1].ownerDocument,m.map(g,ya),j=0;f>j;j++)d=g[j],oa.test(d.type||"")&&!m._data(d,"globalEval")&&m.contains(h,d)&&(d.src?m._evalUrl&&m._evalUrl(d.src):m.globalEval((d.text||d.textContent||d.innerHTML||"").replace(qa,"")));i=c=null}return this}}),m.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(a,b){m.fn[a]=function(a){for(var c,d=0,e=[],g=m(a),h=g.length-1;h>=d;d++)c=d===h?this:this.clone(!0),m(g[d])[b](c),f.apply(e,c.get());return this.pushStack(e)}});var Ca,Da={};function Ea(b,c){var d,e=m(c.createElement(b)).appendTo(c.body),f=a.getDefaultComputedStyle&&(d=a.getDefaultComputedStyle(e[0]))?d.display:m.css(e[0],"display");return e.detach(),f}function Fa(a){var b=y,c=Da[a];return c||(c=Ea(a,b),"none"!==c&&c||(Ca=(Ca||m("<iframe frameborder='0' width='0' height='0'/>")).appendTo(b.documentElement),b=(Ca[0].contentWindow||Ca[0].contentDocument).document,b.write(),b.close(),c=Ea(a,b),Ca.detach()),Da[a]=c),c}!function(){var a;k.shrinkWrapBlocks=function(){if(null!=a)return a;a=!1;var b,c,d;return c=y.getElementsByTagName("body")[0],c&&c.style?(b=y.createElement("div"),d=y.createElement("div"),d.style.cssText="position:absolute;border:0;width:0;height:0;top:0;left:-9999px",c.appendChild(d).appendChild(b),typeof b.style.zoom!==K&&(b.style.cssText="-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1",b.appendChild(y.createElement("div")).style.width="5px",a=3!==b.offsetWidth),c.removeChild(d),a):void 0}}();var Ga=/^margin/,Ha=new RegExp("^("+S+")(?!px)[a-z%]+$","i"),Ia,Ja,Ka=/^(top|right|bottom|left)$/;a.getComputedStyle?(Ia=function(b){return b.ownerDocument.defaultView.opener?b.ownerDocument.defaultView.getComputedStyle(b,null):a.getComputedStyle(b,null)},Ja=function(a,b,c){var d,e,f,g,h=a.style;return c=c||Ia(a),g=c?c.getPropertyValue(b)||c[b]:void 0,c&&(""!==g||m.contains(a.ownerDocument,a)||(g=m.style(a,b)),Ha.test(g)&&Ga.test(b)&&(d=h.width,e=h.minWidth,f=h.maxWidth,h.minWidth=h.maxWidth=h.width=g,g=c.width,h.width=d,h.minWidth=e,h.maxWidth=f)),void 0===g?g:g+""}):y.documentElement.currentStyle&&(Ia=function(a){return a.currentStyle},Ja=function(a,b,c){var d,e,f,g,h=a.style;return c=c||Ia(a),g=c?c[b]:void 0,null==g&&h&&h[b]&&(g=h[b]),Ha.test(g)&&!Ka.test(b)&&(d=h.left,e=a.runtimeStyle,f=e&&e.left,f&&(e.left=a.currentStyle.left),h.left="fontSize"===b?"1em":g,g=h.pixelLeft+"px",h.left=d,f&&(e.left=f)),void 0===g?g:g+""||"auto"});function La(a,b){return{get:function(){var c=a();if(null!=c)return c?void delete this.get:(this.get=b).apply(this,arguments)}}}!function(){var b,c,d,e,f,g,h;if(b=y.createElement("div"),b.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",d=b.getElementsByTagName("a")[0],c=d&&d.style){c.cssText="float:left;opacity:.5",k.opacity="0.5"===c.opacity,k.cssFloat=!!c.cssFloat,b.style.backgroundClip="content-box",b.cloneNode(!0).style.backgroundClip="",k.clearCloneStyle="content-box"===b.style.backgroundClip,k.boxSizing=""===c.boxSizing||""===c.MozBoxSizing||""===c.WebkitBoxSizing,m.extend(k,{reliableHiddenOffsets:function(){return null==g&&i(),g},boxSizingReliable:function(){return null==f&&i(),f},pixelPosition:function(){return null==e&&i(),e},reliableMarginRight:function(){return null==h&&i(),h}});function i(){var b,c,d,i;c=y.getElementsByTagName("body")[0],c&&c.style&&(b=y.createElement("div"),d=y.createElement("div"),d.style.cssText="position:absolute;border:0;width:0;height:0;top:0;left:-9999px",c.appendChild(d).appendChild(b),b.style.cssText="-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute",e=f=!1,h=!0,a.getComputedStyle&&(e="1%"!==(a.getComputedStyle(b,null)||{}).top,f="4px"===(a.getComputedStyle(b,null)||{width:"4px"}).width,i=b.appendChild(y.createElement("div")),i.style.cssText=b.style.cssText="-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0",i.style.marginRight=i.style.width="0",b.style.width="1px",h=!parseFloat((a.getComputedStyle(i,null)||{}).marginRight),b.removeChild(i)),b.innerHTML="<table><tr><td></td><td>t</td></tr></table>",i=b.getElementsByTagName("td"),i[0].style.cssText="margin:0;border:0;padding:0;display:none",g=0===i[0].offsetHeight,g&&(i[0].style.display="",i[1].style.display="none",g=0===i[0].offsetHeight),c.removeChild(d))}}}(),m.swap=function(a,b,c,d){var e,f,g={};for(f in b)g[f]=a.style[f],a.style[f]=b[f];e=c.apply(a,d||[]);for(f in b)a.style[f]=g[f];return e};var Ma=/alpha\([^)]*\)/i,Na=/opacity\s*=\s*([^)]*)/,Oa=/^(none|table(?!-c[ea]).+)/,Pa=new RegExp("^("+S+")(.*)$","i"),Qa=new RegExp("^([+-])=("+S+")","i"),Ra={position:"absolute",visibility:"hidden",display:"block"},Sa={letterSpacing:"0",fontWeight:"400"},Ta=["Webkit","O","Moz","ms"];function Ua(a,b){if(b in a)return b;var c=b.charAt(0).toUpperCase()+b.slice(1),d=b,e=Ta.length;while(e--)if(b=Ta[e]+c,b in a)return b;return d}function Va(a,b){for(var c,d,e,f=[],g=0,h=a.length;h>g;g++)d=a[g],d.style&&(f[g]=m._data(d,"olddisplay"),c=d.style.display,b?(f[g]||"none"!==c||(d.style.display=""),""===d.style.display&&U(d)&&(f[g]=m._data(d,"olddisplay",Fa(d.nodeName)))):(e=U(d),(c&&"none"!==c||!e)&&m._data(d,"olddisplay",e?c:m.css(d,"display"))));for(g=0;h>g;g++)d=a[g],d.style&&(b&&"none"!==d.style.display&&""!==d.style.display||(d.style.display=b?f[g]||"":"none"));return a}function Wa(a,b,c){var d=Pa.exec(b);return d?Math.max(0,d[1]-(c||0))+(d[2]||"px"):b}function Xa(a,b,c,d,e){for(var f=c===(d?"border":"content")?4:"width"===b?1:0,g=0;4>f;f+=2)"margin"===c&&(g+=m.css(a,c+T[f],!0,e)),d?("content"===c&&(g-=m.css(a,"padding"+T[f],!0,e)),"margin"!==c&&(g-=m.css(a,"border"+T[f]+"Width",!0,e))):(g+=m.css(a,"padding"+T[f],!0,e),"padding"!==c&&(g+=m.css(a,"border"+T[f]+"Width",!0,e)));return g}function Ya(a,b,c){var d=!0,e="width"===b?a.offsetWidth:a.offsetHeight,f=Ia(a),g=k.boxSizing&&"border-box"===m.css(a,"boxSizing",!1,f);if(0>=e||null==e){if(e=Ja(a,b,f),(0>e||null==e)&&(e=a.style[b]),Ha.test(e))return e;d=g&&(k.boxSizingReliable()||e===a.style[b]),e=parseFloat(e)||0}return e+Xa(a,b,c||(g?"border":"content"),d,f)+"px"}m.extend({cssHooks:{opacity:{get:function(a,b){if(b){var c=Ja(a,"opacity");return""===c?"1":c}}}},cssNumber:{columnCount:!0,fillOpacity:!0,flexGrow:!0,flexShrink:!0,fontWeight:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{"float":k.cssFloat?"cssFloat":"styleFloat"},style:function(a,b,c,d){if(a&&3!==a.nodeType&&8!==a.nodeType&&a.style){var e,f,g,h=m.camelCase(b),i=a.style;if(b=m.cssProps[h]||(m.cssProps[h]=Ua(i,h)),g=m.cssHooks[b]||m.cssHooks[h],void 0===c)return g&&"get"in g&&void 0!==(e=g.get(a,!1,d))?e:i[b];if(f=typeof c,"string"===f&&(e=Qa.exec(c))&&(c=(e[1]+1)*e[2]+parseFloat(m.css(a,b)),f="number"),null!=c&&c===c&&("number"!==f||m.cssNumber[h]||(c+="px"),k.clearCloneStyle||""!==c||0!==b.indexOf("background")||(i[b]="inherit"),!(g&&"set"in g&&void 0===(c=g.set(a,c,d)))))try{i[b]=c}catch(j){}}},css:function(a,b,c,d){var e,f,g,h=m.camelCase(b);return b=m.cssProps[h]||(m.cssProps[h]=Ua(a.style,h)),g=m.cssHooks[b]||m.cssHooks[h],g&&"get"in g&&(f=g.get(a,!0,c)),void 0===f&&(f=Ja(a,b,d)),"normal"===f&&b in Sa&&(f=Sa[b]),""===c||c?(e=parseFloat(f),c===!0||m.isNumeric(e)?e||0:f):f}}),m.each(["height","width"],function(a,b){m.cssHooks[b]={get:function(a,c,d){return c?Oa.test(m.css(a,"display"))&&0===a.offsetWidth?m.swap(a,Ra,function(){return Ya(a,b,d)}):Ya(a,b,d):void 0},set:function(a,c,d){var e=d&&Ia(a);return Wa(a,c,d?Xa(a,b,d,k.boxSizing&&"border-box"===m.css(a,"boxSizing",!1,e),e):0)}}}),k.opacity||(m.cssHooks.opacity={get:function(a,b){return Na.test((b&&a.currentStyle?a.currentStyle.filter:a.style.filter)||"")?.01*parseFloat(RegExp.$1)+"":b?"1":""},set:function(a,b){var c=a.style,d=a.currentStyle,e=m.isNumeric(b)?"alpha(opacity="+100*b+")":"",f=d&&d.filter||c.filter||"";c.zoom=1,(b>=1||""===b)&&""===m.trim(f.replace(Ma,""))&&c.removeAttribute&&(c.removeAttribute("filter"),""===b||d&&!d.filter)||(c.filter=Ma.test(f)?f.replace(Ma,e):f+" "+e)}}),m.cssHooks.marginRight=La(k.reliableMarginRight,function(a,b){return b?m.swap(a,{display:"inline-block"},Ja,[a,"marginRight"]):void 0}),m.each({margin:"",padding:"",border:"Width"},function(a,b){m.cssHooks[a+b]={expand:function(c){for(var d=0,e={},f="string"==typeof c?c.split(" "):[c];4>d;d++)e[a+T[d]+b]=f[d]||f[d-2]||f[0];return e}},Ga.test(a)||(m.cssHooks[a+b].set=Wa)}),m.fn.extend({css:function(a,b){return V(this,function(a,b,c){var d,e,f={},g=0;if(m.isArray(b)){for(d=Ia(a),e=b.length;e>g;g++)f[b[g]]=m.css(a,b[g],!1,d);return f}return void 0!==c?m.style(a,b,c):m.css(a,b)},a,b,arguments.length>1)},show:function(){return Va(this,!0)},hide:function(){return Va(this)},toggle:function(a){return"boolean"==typeof a?a?this.show():this.hide():this.each(function(){U(this)?m(this).show():m(this).hide()})}});function Za(a,b,c,d,e){
  return new Za.prototype.init(a,b,c,d,e)}m.Tween=Za,Za.prototype={constructor:Za,init:function(a,b,c,d,e,f){this.elem=a,this.prop=c,this.easing=e||"swing",this.options=b,this.start=this.now=this.cur(),this.end=d,this.unit=f||(m.cssNumber[c]?"":"px")},cur:function(){var a=Za.propHooks[this.prop];return a&&a.get?a.get(this):Za.propHooks._default.get(this)},run:function(a){var b,c=Za.propHooks[this.prop];return this.options.duration?this.pos=b=m.easing[this.easing](a,this.options.duration*a,0,1,this.options.duration):this.pos=b=a,this.now=(this.end-this.start)*b+this.start,this.options.step&&this.options.step.call(this.elem,this.now,this),c&&c.set?c.set(this):Za.propHooks._default.set(this),this}},Za.prototype.init.prototype=Za.prototype,Za.propHooks={_default:{get:function(a){var b;return null==a.elem[a.prop]||a.elem.style&&null!=a.elem.style[a.prop]?(b=m.css(a.elem,a.prop,""),b&&"auto"!==b?b:0):a.elem[a.prop]},set:function(a){m.fx.step[a.prop]?m.fx.step[a.prop](a):a.elem.style&&(null!=a.elem.style[m.cssProps[a.prop]]||m.cssHooks[a.prop])?m.style(a.elem,a.prop,a.now+a.unit):a.elem[a.prop]=a.now}}},Za.propHooks.scrollTop=Za.propHooks.scrollLeft={set:function(a){a.elem.nodeType&&a.elem.parentNode&&(a.elem[a.prop]=a.now)}},m.easing={linear:function(a){return a},swing:function(a){return.5-Math.cos(a*Math.PI)/2}},m.fx=Za.prototype.init,m.fx.step={};var $a,_a,ab=/^(?:toggle|show|hide)$/,bb=new RegExp("^(?:([+-])=|)("+S+")([a-z%]*)$","i"),cb=/queueHooks$/,db=[ib],eb={"*":[function(a,b){var c=this.createTween(a,b),d=c.cur(),e=bb.exec(b),f=e&&e[3]||(m.cssNumber[a]?"":"px"),g=(m.cssNumber[a]||"px"!==f&&+d)&&bb.exec(m.css(c.elem,a)),h=1,i=20;if(g&&g[3]!==f){f=f||g[3],e=e||[],g=+d||1;do h=h||".5",g/=h,m.style(c.elem,a,g+f);while(h!==(h=c.cur()/d)&&1!==h&&--i)}return e&&(g=c.start=+g||+d||0,c.unit=f,c.end=e[1]?g+(e[1]+1)*e[2]:+e[2]),c}]};function fb(){return setTimeout(function(){$a=void 0}),$a=m.now()}function gb(a,b){var c,d={height:a},e=0;for(b=b?1:0;4>e;e+=2-b)c=T[e],d["margin"+c]=d["padding"+c]=a;return b&&(d.opacity=d.width=a),d}function hb(a,b,c){for(var d,e=(eb[b]||[]).concat(eb["*"]),f=0,g=e.length;g>f;f++)if(d=e[f].call(c,b,a))return d}function ib(a,b,c){var d,e,f,g,h,i,j,l,n=this,o={},p=a.style,q=a.nodeType&&U(a),r=m._data(a,"fxshow");c.queue||(h=m._queueHooks(a,"fx"),null==h.unqueued&&(h.unqueued=0,i=h.empty.fire,h.empty.fire=function(){h.unqueued||i()}),h.unqueued++,n.always(function(){n.always(function(){h.unqueued--,m.queue(a,"fx").length||h.empty.fire()})})),1===a.nodeType&&("height"in b||"width"in b)&&(c.overflow=[p.overflow,p.overflowX,p.overflowY],j=m.css(a,"display"),l="none"===j?m._data(a,"olddisplay")||Fa(a.nodeName):j,"inline"===l&&"none"===m.css(a,"float")&&(k.inlineBlockNeedsLayout&&"inline"!==Fa(a.nodeName)?p.zoom=1:p.display="inline-block")),c.overflow&&(p.overflow="hidden",k.shrinkWrapBlocks()||n.always(function(){p.overflow=c.overflow[0],p.overflowX=c.overflow[1],p.overflowY=c.overflow[2]}));for(d in b)if(e=b[d],ab.exec(e)){if(delete b[d],f=f||"toggle"===e,e===(q?"hide":"show")){if("show"!==e||!r||void 0===r[d])continue;q=!0}o[d]=r&&r[d]||m.style(a,d)}else j=void 0;if(m.isEmptyObject(o))"inline"===("none"===j?Fa(a.nodeName):j)&&(p.display=j);else{r?"hidden"in r&&(q=r.hidden):r=m._data(a,"fxshow",{}),f&&(r.hidden=!q),q?m(a).show():n.done(function(){m(a).hide()}),n.done(function(){var b;m._removeData(a,"fxshow");for(b in o)m.style(a,b,o[b])});for(d in o)g=hb(q?r[d]:0,d,n),d in r||(r[d]=g.start,q&&(g.end=g.start,g.start="width"===d||"height"===d?1:0))}}function jb(a,b){var c,d,e,f,g;for(c in a)if(d=m.camelCase(c),e=b[d],f=a[c],m.isArray(f)&&(e=f[1],f=a[c]=f[0]),c!==d&&(a[d]=f,delete a[c]),g=m.cssHooks[d],g&&"expand"in g){f=g.expand(f),delete a[d];for(c in f)c in a||(a[c]=f[c],b[c]=e)}else b[d]=e}function kb(a,b,c){var d,e,f=0,g=db.length,h=m.Deferred().always(function(){delete i.elem}),i=function(){if(e)return!1;for(var b=$a||fb(),c=Math.max(0,j.startTime+j.duration-b),d=c/j.duration||0,f=1-d,g=0,i=j.tweens.length;i>g;g++)j.tweens[g].run(f);return h.notifyWith(a,[j,f,c]),1>f&&i?c:(h.resolveWith(a,[j]),!1)},j=h.promise({elem:a,props:m.extend({},b),opts:m.extend(!0,{specialEasing:{}},c),originalProperties:b,originalOptions:c,startTime:$a||fb(),duration:c.duration,tweens:[],createTween:function(b,c){var d=m.Tween(a,j.opts,b,c,j.opts.specialEasing[b]||j.opts.easing);return j.tweens.push(d),d},stop:function(b){var c=0,d=b?j.tweens.length:0;if(e)return this;for(e=!0;d>c;c++)j.tweens[c].run(1);return b?h.resolveWith(a,[j,b]):h.rejectWith(a,[j,b]),this}}),k=j.props;for(jb(k,j.opts.specialEasing);g>f;f++)if(d=db[f].call(j,a,k,j.opts))return d;return m.map(k,hb,j),m.isFunction(j.opts.start)&&j.opts.start.call(a,j),m.fx.timer(m.extend(i,{elem:a,anim:j,queue:j.opts.queue})),j.progress(j.opts.progress).done(j.opts.done,j.opts.complete).fail(j.opts.fail).always(j.opts.always)}m.Animation=m.extend(kb,{tweener:function(a,b){m.isFunction(a)?(b=a,a=["*"]):a=a.split(" ");for(var c,d=0,e=a.length;e>d;d++)c=a[d],eb[c]=eb[c]||[],eb[c].unshift(b)},prefilter:function(a,b){b?db.unshift(a):db.push(a)}}),m.speed=function(a,b,c){var d=a&&"object"==typeof a?m.extend({},a):{complete:c||!c&&b||m.isFunction(a)&&a,duration:a,easing:c&&b||b&&!m.isFunction(b)&&b};return d.duration=m.fx.off?0:"number"==typeof d.duration?d.duration:d.duration in m.fx.speeds?m.fx.speeds[d.duration]:m.fx.speeds._default,(null==d.queue||d.queue===!0)&&(d.queue="fx"),d.old=d.complete,d.complete=function(){m.isFunction(d.old)&&d.old.call(this),d.queue&&m.dequeue(this,d.queue)},d},m.fn.extend({fadeTo:function(a,b,c,d){return this.filter(U).css("opacity",0).show().end().animate({opacity:b},a,c,d)},animate:function(a,b,c,d){var e=m.isEmptyObject(a),f=m.speed(b,c,d),g=function(){var b=kb(this,m.extend({},a),f);(e||m._data(this,"finish"))&&b.stop(!0)};return g.finish=g,e||f.queue===!1?this.each(g):this.queue(f.queue,g)},stop:function(a,b,c){var d=function(a){var b=a.stop;delete a.stop,b(c)};return"string"!=typeof a&&(c=b,b=a,a=void 0),b&&a!==!1&&this.queue(a||"fx",[]),this.each(function(){var b=!0,e=null!=a&&a+"queueHooks",f=m.timers,g=m._data(this);if(e)g[e]&&g[e].stop&&d(g[e]);else for(e in g)g[e]&&g[e].stop&&cb.test(e)&&d(g[e]);for(e=f.length;e--;)f[e].elem!==this||null!=a&&f[e].queue!==a||(f[e].anim.stop(c),b=!1,f.splice(e,1));(b||!c)&&m.dequeue(this,a)})},finish:function(a){return a!==!1&&(a=a||"fx"),this.each(function(){var b,c=m._data(this),d=c[a+"queue"],e=c[a+"queueHooks"],f=m.timers,g=d?d.length:0;for(c.finish=!0,m.queue(this,a,[]),e&&e.stop&&e.stop.call(this,!0),b=f.length;b--;)f[b].elem===this&&f[b].queue===a&&(f[b].anim.stop(!0),f.splice(b,1));for(b=0;g>b;b++)d[b]&&d[b].finish&&d[b].finish.call(this);delete c.finish})}}),m.each(["toggle","show","hide"],function(a,b){var c=m.fn[b];m.fn[b]=function(a,d,e){return null==a||"boolean"==typeof a?c.apply(this,arguments):this.animate(gb(b,!0),a,d,e)}}),m.each({slideDown:gb("show"),slideUp:gb("hide"),slideToggle:gb("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(a,b){m.fn[a]=function(a,c,d){return this.animate(b,a,c,d)}}),m.timers=[],m.fx.tick=function(){var a,b=m.timers,c=0;for($a=m.now();c<b.length;c++)a=b[c],a()||b[c]!==a||b.splice(c--,1);b.length||m.fx.stop(),$a=void 0},m.fx.timer=function(a){m.timers.push(a),a()?m.fx.start():m.timers.pop()},m.fx.interval=13,m.fx.start=function(){_a||(_a=setInterval(m.fx.tick,m.fx.interval))},m.fx.stop=function(){clearInterval(_a),_a=null},m.fx.speeds={slow:600,fast:200,_default:400},m.fn.delay=function(a,b){return a=m.fx?m.fx.speeds[a]||a:a,b=b||"fx",this.queue(b,function(b,c){var d=setTimeout(b,a);c.stop=function(){clearTimeout(d)}})},function(){var a,b,c,d,e;b=y.createElement("div"),b.setAttribute("className","t"),b.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",d=b.getElementsByTagName("a")[0],c=y.createElement("select"),e=c.appendChild(y.createElement("option")),a=b.getElementsByTagName("input")[0],d.style.cssText="top:1px",k.getSetAttribute="t"!==b.className,k.style=/top/.test(d.getAttribute("style")),k.hrefNormalized="/a"===d.getAttribute("href"),k.checkOn=!!a.value,k.optSelected=e.selected,k.enctype=!!y.createElement("form").enctype,c.disabled=!0,k.optDisabled=!e.disabled,a=y.createElement("input"),a.setAttribute("value",""),k.input=""===a.getAttribute("value"),a.value="t",a.setAttribute("type","radio"),k.radioValue="t"===a.value}();var lb=/\r/g;m.fn.extend({val:function(a){var b,c,d,e=this[0];{if(arguments.length)return d=m.isFunction(a),this.each(function(c){var e;1===this.nodeType&&(e=d?a.call(this,c,m(this).val()):a,null==e?e="":"number"==typeof e?e+="":m.isArray(e)&&(e=m.map(e,function(a){return null==a?"":a+""})),b=m.valHooks[this.type]||m.valHooks[this.nodeName.toLowerCase()],b&&"set"in b&&void 0!==b.set(this,e,"value")||(this.value=e))});if(e)return b=m.valHooks[e.type]||m.valHooks[e.nodeName.toLowerCase()],b&&"get"in b&&void 0!==(c=b.get(e,"value"))?c:(c=e.value,"string"==typeof c?c.replace(lb,""):null==c?"":c)}}}),m.extend({valHooks:{option:{get:function(a){var b=m.find.attr(a,"value");return null!=b?b:m.trim(m.text(a))}},select:{get:function(a){for(var b,c,d=a.options,e=a.selectedIndex,f="select-one"===a.type||0>e,g=f?null:[],h=f?e+1:d.length,i=0>e?h:f?e:0;h>i;i++)if(c=d[i],!(!c.selected&&i!==e||(k.optDisabled?c.disabled:null!==c.getAttribute("disabled"))||c.parentNode.disabled&&m.nodeName(c.parentNode,"optgroup"))){if(b=m(c).val(),f)return b;g.push(b)}return g},set:function(a,b){var c,d,e=a.options,f=m.makeArray(b),g=e.length;while(g--)if(d=e[g],m.inArray(m.valHooks.option.get(d),f)>=0)try{d.selected=c=!0}catch(h){d.scrollHeight}else d.selected=!1;return c||(a.selectedIndex=-1),e}}}}),m.each(["radio","checkbox"],function(){m.valHooks[this]={set:function(a,b){return m.isArray(b)?a.checked=m.inArray(m(a).val(),b)>=0:void 0}},k.checkOn||(m.valHooks[this].get=function(a){return null===a.getAttribute("value")?"on":a.value})});var mb,nb,ob=m.expr.attrHandle,pb=/^(?:checked|selected)$/i,qb=k.getSetAttribute,rb=k.input;m.fn.extend({attr:function(a,b){return V(this,m.attr,a,b,arguments.length>1)},removeAttr:function(a){return this.each(function(){m.removeAttr(this,a)})}}),m.extend({attr:function(a,b,c){var d,e,f=a.nodeType;if(a&&3!==f&&8!==f&&2!==f)return typeof a.getAttribute===K?m.prop(a,b,c):(1===f&&m.isXMLDoc(a)||(b=b.toLowerCase(),d=m.attrHooks[b]||(m.expr.match.bool.test(b)?nb:mb)),void 0===c?d&&"get"in d&&null!==(e=d.get(a,b))?e:(e=m.find.attr(a,b),null==e?void 0:e):null!==c?d&&"set"in d&&void 0!==(e=d.set(a,c,b))?e:(a.setAttribute(b,c+""),c):void m.removeAttr(a,b))},removeAttr:function(a,b){var c,d,e=0,f=b&&b.match(E);if(f&&1===a.nodeType)while(c=f[e++])d=m.propFix[c]||c,m.expr.match.bool.test(c)?rb&&qb||!pb.test(c)?a[d]=!1:a[m.camelCase("default-"+c)]=a[d]=!1:m.attr(a,c,""),a.removeAttribute(qb?c:d)},attrHooks:{type:{set:function(a,b){if(!k.radioValue&&"radio"===b&&m.nodeName(a,"input")){var c=a.value;return a.setAttribute("type",b),c&&(a.value=c),b}}}}}),nb={set:function(a,b,c){return b===!1?m.removeAttr(a,c):rb&&qb||!pb.test(c)?a.setAttribute(!qb&&m.propFix[c]||c,c):a[m.camelCase("default-"+c)]=a[c]=!0,c}},m.each(m.expr.match.bool.source.match(/\w+/g),function(a,b){var c=ob[b]||m.find.attr;ob[b]=rb&&qb||!pb.test(b)?function(a,b,d){var e,f;return d||(f=ob[b],ob[b]=e,e=null!=c(a,b,d)?b.toLowerCase():null,ob[b]=f),e}:function(a,b,c){return c?void 0:a[m.camelCase("default-"+b)]?b.toLowerCase():null}}),rb&&qb||(m.attrHooks.value={set:function(a,b,c){return m.nodeName(a,"input")?void(a.defaultValue=b):mb&&mb.set(a,b,c)}}),qb||(mb={set:function(a,b,c){var d=a.getAttributeNode(c);return d||a.setAttributeNode(d=a.ownerDocument.createAttribute(c)),d.value=b+="","value"===c||b===a.getAttribute(c)?b:void 0}},ob.id=ob.name=ob.coords=function(a,b,c){var d;return c?void 0:(d=a.getAttributeNode(b))&&""!==d.value?d.value:null},m.valHooks.button={get:function(a,b){var c=a.getAttributeNode(b);return c&&c.specified?c.value:void 0},set:mb.set},m.attrHooks.contenteditable={set:function(a,b,c){mb.set(a,""===b?!1:b,c)}},m.each(["width","height"],function(a,b){m.attrHooks[b]={set:function(a,c){return""===c?(a.setAttribute(b,"auto"),c):void 0}}})),k.style||(m.attrHooks.style={get:function(a){return a.style.cssText||void 0},set:function(a,b){return a.style.cssText=b+""}});var sb=/^(?:input|select|textarea|button|object)$/i,tb=/^(?:a|area)$/i;m.fn.extend({prop:function(a,b){return V(this,m.prop,a,b,arguments.length>1)},removeProp:function(a){return a=m.propFix[a]||a,this.each(function(){try{this[a]=void 0,delete this[a]}catch(b){}})}}),m.extend({propFix:{"for":"htmlFor","class":"className"},prop:function(a,b,c){var d,e,f,g=a.nodeType;if(a&&3!==g&&8!==g&&2!==g)return f=1!==g||!m.isXMLDoc(a),f&&(b=m.propFix[b]||b,e=m.propHooks[b]),void 0!==c?e&&"set"in e&&void 0!==(d=e.set(a,c,b))?d:a[b]=c:e&&"get"in e&&null!==(d=e.get(a,b))?d:a[b]},propHooks:{tabIndex:{get:function(a){var b=m.find.attr(a,"tabindex");return b?parseInt(b,10):sb.test(a.nodeName)||tb.test(a.nodeName)&&a.href?0:-1}}}}),k.hrefNormalized||m.each(["href","src"],function(a,b){m.propHooks[b]={get:function(a){return a.getAttribute(b,4)}}}),k.optSelected||(m.propHooks.selected={get:function(a){var b=a.parentNode;return b&&(b.selectedIndex,b.parentNode&&b.parentNode.selectedIndex),null}}),m.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){m.propFix[this.toLowerCase()]=this}),k.enctype||(m.propFix.enctype="encoding");var ub=/[\t\r\n\f]/g;m.fn.extend({addClass:function(a){var b,c,d,e,f,g,h=0,i=this.length,j="string"==typeof a&&a;if(m.isFunction(a))return this.each(function(b){m(this).addClass(a.call(this,b,this.className))});if(j)for(b=(a||"").match(E)||[];i>h;h++)if(c=this[h],d=1===c.nodeType&&(c.className?(" "+c.className+" ").replace(ub," "):" ")){f=0;while(e=b[f++])d.indexOf(" "+e+" ")<0&&(d+=e+" ");g=m.trim(d),c.className!==g&&(c.className=g)}return this},removeClass:function(a){var b,c,d,e,f,g,h=0,i=this.length,j=0===arguments.length||"string"==typeof a&&a;if(m.isFunction(a))return this.each(function(b){m(this).removeClass(a.call(this,b,this.className))});if(j)for(b=(a||"").match(E)||[];i>h;h++)if(c=this[h],d=1===c.nodeType&&(c.className?(" "+c.className+" ").replace(ub," "):"")){f=0;while(e=b[f++])while(d.indexOf(" "+e+" ")>=0)d=d.replace(" "+e+" "," ");g=a?m.trim(d):"",c.className!==g&&(c.className=g)}return this},toggleClass:function(a,b){var c=typeof a;return"boolean"==typeof b&&"string"===c?b?this.addClass(a):this.removeClass(a):this.each(m.isFunction(a)?function(c){m(this).toggleClass(a.call(this,c,this.className,b),b)}:function(){if("string"===c){var b,d=0,e=m(this),f=a.match(E)||[];while(b=f[d++])e.hasClass(b)?e.removeClass(b):e.addClass(b)}else(c===K||"boolean"===c)&&(this.className&&m._data(this,"__className__",this.className),this.className=this.className||a===!1?"":m._data(this,"__className__")||"")})},hasClass:function(a){for(var b=" "+a+" ",c=0,d=this.length;d>c;c++)if(1===this[c].nodeType&&(" "+this[c].className+" ").replace(ub," ").indexOf(b)>=0)return!0;return!1}}),m.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "),function(a,b){m.fn[b]=function(a,c){return arguments.length>0?this.on(b,null,a,c):this.trigger(b)}}),m.fn.extend({hover:function(a,b){return this.mouseenter(a).mouseleave(b||a)},bind:function(a,b,c){return this.on(a,null,b,c)},unbind:function(a,b){return this.off(a,null,b)},delegate:function(a,b,c,d){return this.on(b,a,c,d)},undelegate:function(a,b,c){return 1===arguments.length?this.off(a,"**"):this.off(b,a||"**",c)}});var vb=m.now(),wb=/\?/,xb=/(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;m.parseJSON=function(b){if(a.JSON&&a.JSON.parse)return a.JSON.parse(b+"");var c,d=null,e=m.trim(b+"");return e&&!m.trim(e.replace(xb,function(a,b,e,f){return c&&b&&(d=0),0===d?a:(c=e||b,d+=!f-!e,"")}))?Function("return "+e)():m.error("Invalid JSON: "+b)},m.parseXML=function(b){var c,d;if(!b||"string"!=typeof b)return null;try{a.DOMParser?(d=new DOMParser,c=d.parseFromString(b,"text/xml")):(c=new ActiveXObject("Microsoft.XMLDOM"),c.async="false",c.loadXML(b))}catch(e){c=void 0}return c&&c.documentElement&&!c.getElementsByTagName("parsererror").length||m.error("Invalid XML: "+b),c};var yb,zb,Ab=/#.*$/,Bb=/([?&])_=[^&]*/,Cb=/^(.*?):[ \t]*([^\r\n]*)\r?$/gm,Db=/^(?:about|app|app-storage|.+-extension|file|res|widget):$/,Eb=/^(?:GET|HEAD)$/,Fb=/^\/\//,Gb=/^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,Hb={},Ib={},Jb="*/".concat("*");try{zb=location.href}catch(Kb){zb=y.createElement("a"),zb.href="",zb=zb.href}yb=Gb.exec(zb.toLowerCase())||[];function Lb(a){return function(b,c){"string"!=typeof b&&(c=b,b="*");var d,e=0,f=b.toLowerCase().match(E)||[];if(m.isFunction(c))while(d=f[e++])"+"===d.charAt(0)?(d=d.slice(1)||"*",(a[d]=a[d]||[]).unshift(c)):(a[d]=a[d]||[]).push(c)}}function Mb(a,b,c,d){var e={},f=a===Ib;function g(h){var i;return e[h]=!0,m.each(a[h]||[],function(a,h){var j=h(b,c,d);return"string"!=typeof j||f||e[j]?f?!(i=j):void 0:(b.dataTypes.unshift(j),g(j),!1)}),i}return g(b.dataTypes[0])||!e["*"]&&g("*")}function Nb(a,b){var c,d,e=m.ajaxSettings.flatOptions||{};for(d in b)void 0!==b[d]&&((e[d]?a:c||(c={}))[d]=b[d]);return c&&m.extend(!0,a,c),a}function Ob(a,b,c){var d,e,f,g,h=a.contents,i=a.dataTypes;while("*"===i[0])i.shift(),void 0===e&&(e=a.mimeType||b.getResponseHeader("Content-Type"));if(e)for(g in h)if(h[g]&&h[g].test(e)){i.unshift(g);break}if(i[0]in c)f=i[0];else{for(g in c){if(!i[0]||a.converters[g+" "+i[0]]){f=g;break}d||(d=g)}f=f||d}return f?(f!==i[0]&&i.unshift(f),c[f]):void 0}function Pb(a,b,c,d){var e,f,g,h,i,j={},k=a.dataTypes.slice();if(k[1])for(g in a.converters)j[g.toLowerCase()]=a.converters[g];f=k.shift();while(f)if(a.responseFields[f]&&(c[a.responseFields[f]]=b),!i&&d&&a.dataFilter&&(b=a.dataFilter(b,a.dataType)),i=f,f=k.shift())if("*"===f)f=i;else if("*"!==i&&i!==f){if(g=j[i+" "+f]||j["* "+f],!g)for(e in j)if(h=e.split(" "),h[1]===f&&(g=j[i+" "+h[0]]||j["* "+h[0]])){g===!0?g=j[e]:j[e]!==!0&&(f=h[0],k.unshift(h[1]));break}if(g!==!0)if(g&&a["throws"])b=g(b);else try{b=g(b)}catch(l){return{state:"parsererror",error:g?l:"No conversion from "+i+" to "+f}}}return{state:"success",data:b}}m.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:zb,type:"GET",isLocal:Db.test(yb[1]),global:!0,processData:!0,async:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":Jb,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/xml/,html:/html/,json:/json/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},converters:{"* text":String,"text html":!0,"text json":m.parseJSON,"text xml":m.parseXML},flatOptions:{url:!0,context:!0}},ajaxSetup:function(a,b){return b?Nb(Nb(a,m.ajaxSettings),b):Nb(m.ajaxSettings,a)},ajaxPrefilter:Lb(Hb),ajaxTransport:Lb(Ib),ajax:function(a,b){"object"==typeof a&&(b=a,a=void 0),b=b||{};var c,d,e,f,g,h,i,j,k=m.ajaxSetup({},b),l=k.context||k,n=k.context&&(l.nodeType||l.jquery)?m(l):m.event,o=m.Deferred(),p=m.Callbacks("once memory"),q=k.statusCode||{},r={},s={},t=0,u="canceled",v={readyState:0,getResponseHeader:function(a){var b;if(2===t){if(!j){j={};while(b=Cb.exec(f))j[b[1].toLowerCase()]=b[2]}b=j[a.toLowerCase()]}return null==b?null:b},getAllResponseHeaders:function(){return 2===t?f:null},setRequestHeader:function(a,b){var c=a.toLowerCase();return t||(a=s[c]=s[c]||a,r[a]=b),this},overrideMimeType:function(a){return t||(k.mimeType=a),this},statusCode:function(a){var b;if(a)if(2>t)for(b in a)q[b]=[q[b],a[b]];else v.always(a[v.status]);return this},abort:function(a){var b=a||u;return i&&i.abort(b),x(0,b),this}};if(o.promise(v).complete=p.add,v.success=v.done,v.error=v.fail,k.url=((a||k.url||zb)+"").replace(Ab,"").replace(Fb,yb[1]+"//"),k.type=b.method||b.type||k.method||k.type,k.dataTypes=m.trim(k.dataType||"*").toLowerCase().match(E)||[""],null==k.crossDomain&&(c=Gb.exec(k.url.toLowerCase()),k.crossDomain=!(!c||c[1]===yb[1]&&c[2]===yb[2]&&(c[3]||("http:"===c[1]?"80":"443"))===(yb[3]||("http:"===yb[1]?"80":"443")))),k.data&&k.processData&&"string"!=typeof k.data&&(k.data=m.param(k.data,k.traditional)),Mb(Hb,k,b,v),2===t)return v;h=m.event&&k.global,h&&0===m.active++&&m.event.trigger("ajaxStart"),k.type=k.type.toUpperCase(),k.hasContent=!Eb.test(k.type),e=k.url,k.hasContent||(k.data&&(e=k.url+=(wb.test(e)?"&":"?")+k.data,delete k.data),k.cache===!1&&(k.url=Bb.test(e)?e.replace(Bb,"$1_="+vb++):e+(wb.test(e)?"&":"?")+"_="+vb++)),k.ifModified&&(m.lastModified[e]&&v.setRequestHeader("If-Modified-Since",m.lastModified[e]),m.etag[e]&&v.setRequestHeader("If-None-Match",m.etag[e])),(k.data&&k.hasContent&&k.contentType!==!1||b.contentType)&&v.setRequestHeader("Content-Type",k.contentType),v.setRequestHeader("Accept",k.dataTypes[0]&&k.accepts[k.dataTypes[0]]?k.accepts[k.dataTypes[0]]+("*"!==k.dataTypes[0]?", "+Jb+"; q=0.01":""):k.accepts["*"]);for(d in k.headers)v.setRequestHeader(d,k.headers[d]);if(k.beforeSend&&(k.beforeSend.call(l,v,k)===!1||2===t))return v.abort();u="abort";for(d in{success:1,error:1,complete:1})v[d](k[d]);if(i=Mb(Ib,k,b,v)){v.readyState=1,h&&n.trigger("ajaxSend",[v,k]),k.async&&k.timeout>0&&(g=setTimeout(function(){v.abort("timeout")},k.timeout));try{t=1,i.send(r,x)}catch(w){if(!(2>t))throw w;x(-1,w)}}else x(-1,"No Transport");function x(a,b,c,d){var j,r,s,u,w,x=b;2!==t&&(t=2,g&&clearTimeout(g),i=void 0,f=d||"",v.readyState=a>0?4:0,j=a>=200&&300>a||304===a,c&&(u=Ob(k,v,c)),u=Pb(k,u,v,j),j?(k.ifModified&&(w=v.getResponseHeader("Last-Modified"),w&&(m.lastModified[e]=w),w=v.getResponseHeader("etag"),w&&(m.etag[e]=w)),204===a||"HEAD"===k.type?x="nocontent":304===a?x="notmodified":(x=u.state,r=u.data,s=u.error,j=!s)):(s=x,(a||!x)&&(x="error",0>a&&(a=0))),v.status=a,v.statusText=(b||x)+"",j?o.resolveWith(l,[r,x,v]):o.rejectWith(l,[v,x,s]),v.statusCode(q),q=void 0,h&&n.trigger(j?"ajaxSuccess":"ajaxError",[v,k,j?r:s]),p.fireWith(l,[v,x]),h&&(n.trigger("ajaxComplete",[v,k]),--m.active||m.event.trigger("ajaxStop")))}return v},getJSON:function(a,b,c){return m.get(a,b,c,"json")},getScript:function(a,b){return m.get(a,void 0,b,"script")}}),m.each(["get","post"],function(a,b){m[b]=function(a,c,d,e){return m.isFunction(c)&&(e=e||d,d=c,c=void 0),m.ajax({url:a,type:b,dataType:e,data:c,success:d})}}),m._evalUrl=function(a){return m.ajax({url:a,type:"GET",dataType:"script",async:!1,global:!1,"throws":!0})},m.fn.extend({wrapAll:function(a){if(m.isFunction(a))return this.each(function(b){m(this).wrapAll(a.call(this,b))});if(this[0]){var b=m(a,this[0].ownerDocument).eq(0).clone(!0);this[0].parentNode&&b.insertBefore(this[0]),b.map(function(){var a=this;while(a.firstChild&&1===a.firstChild.nodeType)a=a.firstChild;return a}).append(this)}return this},wrapInner:function(a){return this.each(m.isFunction(a)?function(b){m(this).wrapInner(a.call(this,b))}:function(){var b=m(this),c=b.contents();c.length?c.wrapAll(a):b.append(a)})},wrap:function(a){var b=m.isFunction(a);return this.each(function(c){m(this).wrapAll(b?a.call(this,c):a)})},unwrap:function(){return this.parent().each(function(){m.nodeName(this,"body")||m(this).replaceWith(this.childNodes)}).end()}}),m.expr.filters.hidden=function(a){return a.offsetWidth<=0&&a.offsetHeight<=0||!k.reliableHiddenOffsets()&&"none"===(a.style&&a.style.display||m.css(a,"display"))},m.expr.filters.visible=function(a){return!m.expr.filters.hidden(a)};var Qb=/%20/g,Rb=/\[\]$/,Sb=/\r?\n/g,Tb=/^(?:submit|button|image|reset|file)$/i,Ub=/^(?:input|select|textarea|keygen)/i;function Vb(a,b,c,d){var e;if(m.isArray(b))m.each(b,function(b,e){c||Rb.test(a)?d(a,e):Vb(a+"["+("object"==typeof e?b:"")+"]",e,c,d)});else if(c||"object"!==m.type(b))d(a,b);else for(e in b)Vb(a+"["+e+"]",b[e],c,d)}m.param=function(a,b){var c,d=[],e=function(a,b){b=m.isFunction(b)?b():null==b?"":b,d[d.length]=encodeURIComponent(a)+"="+encodeURIComponent(b)};if(void 0===b&&(b=m.ajaxSettings&&m.ajaxSettings.traditional),m.isArray(a)||a.jquery&&!m.isPlainObject(a))m.each(a,function(){e(this.name,this.value)});else for(c in a)Vb(c,a[c],b,e);return d.join("&").replace(Qb,"+")},m.fn.extend({serialize:function(){return m.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var a=m.prop(this,"elements");return a?m.makeArray(a):this}).filter(function(){var a=this.type;return this.name&&!m(this).is(":disabled")&&Ub.test(this.nodeName)&&!Tb.test(a)&&(this.checked||!W.test(a))}).map(function(a,b){var c=m(this).val();return null==c?null:m.isArray(c)?m.map(c,function(a){return{name:b.name,value:a.replace(Sb,"\r\n")}}):{name:b.name,value:c.replace(Sb,"\r\n")}}).get()}}),m.ajaxSettings.xhr=void 0!==a.ActiveXObject?function(){return!this.isLocal&&/^(get|post|head|put|delete|options)$/i.test(this.type)&&Zb()||$b()}:Zb;var Wb=0,Xb={},Yb=m.ajaxSettings.xhr();a.attachEvent&&a.attachEvent("onunload",function(){for(var a in Xb)Xb[a](void 0,!0)}),k.cors=!!Yb&&"withCredentials"in Yb,Yb=k.ajax=!!Yb,Yb&&m.ajaxTransport(function(a){if(!a.crossDomain||k.cors){var b;return{send:function(c,d){var e,f=a.xhr(),g=++Wb;if(f.open(a.type,a.url,a.async,a.username,a.password),a.xhrFields)for(e in a.xhrFields)f[e]=a.xhrFields[e];a.mimeType&&f.overrideMimeType&&f.overrideMimeType(a.mimeType),a.crossDomain||c["X-Requested-With"]||(c["X-Requested-With"]="XMLHttpRequest");for(e in c)void 0!==c[e]&&f.setRequestHeader(e,c[e]+"");f.send(a.hasContent&&a.data||null),b=function(c,e){var h,i,j;if(b&&(e||4===f.readyState))if(delete Xb[g],b=void 0,f.onreadystatechange=m.noop,e)4!==f.readyState&&f.abort();else{j={},h=f.status,"string"==typeof f.responseText&&(j.text=f.responseText);try{i=f.statusText}catch(k){i=""}h||!a.isLocal||a.crossDomain?1223===h&&(h=204):h=j.text?200:404}j&&d(h,i,j,f.getAllResponseHeaders())},a.async?4===f.readyState?setTimeout(b):f.onreadystatechange=Xb[g]=b:b()},abort:function(){b&&b(void 0,!0)}}}});function Zb(){try{return new a.XMLHttpRequest}catch(b){}}function $b(){try{return new a.ActiveXObject("Microsoft.XMLHTTP")}catch(b){}}m.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/(?:java|ecma)script/},converters:{"text script":function(a){return m.globalEval(a),a}}}),m.ajaxPrefilter("script",function(a){void 0===a.cache&&(a.cache=!1),a.crossDomain&&(a.type="GET",a.global=!1)}),m.ajaxTransport("script",function(a){if(a.crossDomain){var b,c=y.head||m("head")[0]||y.documentElement;return{send:function(d,e){b=y.createElement("script"),b.async=!0,a.scriptCharset&&(b.charset=a.scriptCharset),b.src=a.url,b.onload=b.onreadystatechange=function(a,c){(c||!b.readyState||/loaded|complete/.test(b.readyState))&&(b.onload=b.onreadystatechange=null,b.parentNode&&b.parentNode.removeChild(b),b=null,c||e(200,"success"))},c.insertBefore(b,c.firstChild)},abort:function(){b&&b.onload(void 0,!0)}}}});var _b=[],ac=/(=)\?(?=&|$)|\?\?/;m.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var a=_b.pop()||m.expando+"_"+vb++;return this[a]=!0,a}}),m.ajaxPrefilter("json jsonp",function(b,c,d){var e,f,g,h=b.jsonp!==!1&&(ac.test(b.url)?"url":"string"==typeof b.data&&!(b.contentType||"").indexOf("application/x-www-form-urlencoded")&&ac.test(b.data)&&"data");return h||"jsonp"===b.dataTypes[0]?(e=b.jsonpCallback=m.isFunction(b.jsonpCallback)?b.jsonpCallback():b.jsonpCallback,h?b[h]=b[h].replace(ac,"$1"+e):b.jsonp!==!1&&(b.url+=(wb.test(b.url)?"&":"?")+b.jsonp+"="+e),b.converters["script json"]=function(){return g||m.error(e+" was not called"),g[0]},b.dataTypes[0]="json",f=a[e],a[e]=function(){g=arguments},d.always(function(){a[e]=f,b[e]&&(b.jsonpCallback=c.jsonpCallback,_b.push(e)),g&&m.isFunction(f)&&f(g[0]),g=f=void 0}),"script"):void 0}),m.parseHTML=function(a,b,c){if(!a||"string"!=typeof a)return null;"boolean"==typeof b&&(c=b,b=!1),b=b||y;var d=u.exec(a),e=!c&&[];return d?[b.createElement(d[1])]:(d=m.buildFragment([a],b,e),e&&e.length&&m(e).remove(),m.merge([],d.childNodes))};var bc=m.fn.load;m.fn.load=function(a,b,c){if("string"!=typeof a&&bc)return bc.apply(this,arguments);var d,e,f,g=this,h=a.indexOf(" ");return h>=0&&(d=m.trim(a.slice(h,a.length)),a=a.slice(0,h)),m.isFunction(b)?(c=b,b=void 0):b&&"object"==typeof b&&(f="POST"),g.length>0&&m.ajax({url:a,type:f,dataType:"html",data:b}).done(function(a){e=arguments,g.html(d?m("<div>").append(m.parseHTML(a)).find(d):a)}).complete(c&&function(a,b){g.each(c,e||[a.responseText,b,a])}),this},m.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(a,b){m.fn[b]=function(a){return this.on(b,a)}}),m.expr.filters.animated=function(a){return m.grep(m.timers,function(b){return a===b.elem}).length};var cc=a.document.documentElement;function dc(a){return m.isWindow(a)?a:9===a.nodeType?a.defaultView||a.parentWindow:!1}m.offset={setOffset:function(a,b,c){var d,e,f,g,h,i,j,k=m.css(a,"position"),l=m(a),n={};"static"===k&&(a.style.position="relative"),h=l.offset(),f=m.css(a,"top"),i=m.css(a,"left"),j=("absolute"===k||"fixed"===k)&&m.inArray("auto",[f,i])>-1,j?(d=l.position(),g=d.top,e=d.left):(g=parseFloat(f)||0,e=parseFloat(i)||0),m.isFunction(b)&&(b=b.call(a,c,h)),null!=b.top&&(n.top=b.top-h.top+g),null!=b.left&&(n.left=b.left-h.left+e),"using"in b?b.using.call(a,n):l.css(n)}},m.fn.extend({offset:function(a){if(arguments.length)return void 0===a?this:this.each(function(b){m.offset.setOffset(this,a,b)});var b,c,d={top:0,left:0},e=this[0],f=e&&e.ownerDocument;if(f)return b=f.documentElement,m.contains(b,e)?(typeof e.getBoundingClientRect!==K&&(d=e.getBoundingClientRect()),c=dc(f),{top:d.top+(c.pageYOffset||b.scrollTop)-(b.clientTop||0),left:d.left+(c.pageXOffset||b.scrollLeft)-(b.clientLeft||0)}):d},position:function(){if(this[0]){var a,b,c={top:0,left:0},d=this[0];return"fixed"===m.css(d,"position")?b=d.getBoundingClientRect():(a=this.offsetParent(),b=this.offset(),m.nodeName(a[0],"html")||(c=a.offset()),c.top+=m.css(a[0],"borderTopWidth",!0),c.left+=m.css(a[0],"borderLeftWidth",!0)),{top:b.top-c.top-m.css(d,"marginTop",!0),left:b.left-c.left-m.css(d,"marginLeft",!0)}}},offsetParent:function(){return this.map(function(){var a=this.offsetParent||cc;while(a&&!m.nodeName(a,"html")&&"static"===m.css(a,"position"))a=a.offsetParent;return a||cc})}}),m.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(a,b){var c=/Y/.test(b);m.fn[a]=function(d){return V(this,function(a,d,e){var f=dc(a);return void 0===e?f?b in f?f[b]:f.document.documentElement[d]:a[d]:void(f?f.scrollTo(c?m(f).scrollLeft():e,c?e:m(f).scrollTop()):a[d]=e)},a,d,arguments.length,null)}}),m.each(["top","left"],function(a,b){m.cssHooks[b]=La(k.pixelPosition,function(a,c){return c?(c=Ja(a,b),Ha.test(c)?m(a).position()[b]+"px":c):void 0})}),m.each({Height:"height",Width:"width"},function(a,b){m.each({padding:"inner"+a,content:b,"":"outer"+a},function(c,d){m.fn[d]=function(d,e){var f=arguments.length&&(c||"boolean"!=typeof d),g=c||(d===!0||e===!0?"margin":"border");return V(this,function(b,c,d){var e;return m.isWindow(b)?b.document.documentElement["client"+a]:9===b.nodeType?(e=b.documentElement,Math.max(b.body["scroll"+a],e["scroll"+a],b.body["offset"+a],e["offset"+a],e["client"+a])):void 0===d?m.css(b,c,g):m.style(b,c,d,g)},b,f?d:void 0,f,null)}})}),m.fn.size=function(){return this.length},m.fn.andSelf=m.fn.addBack,"function"==typeof define&&define.amd&&define("jquery",[],function(){return m});var ec=a.jQuery,fc=a.$;return m.noConflict=function(b){return a.$===m&&(a.$=fc),b&&a.jQuery===m&&(a.jQuery=ec),m},typeof b===K&&(a.jQuery=a.$=m),m});
//# sourceMappingURL=jquery.min.map

/*! jQuery UI - v1.11.4+CommonJS - 2015-08-28
* http://jqueryui.com
* Includes: widget.js
* Copyright 2015 jQuery Foundation and other contributors; Licensed MIT */

(function( factory ) {
  if ( typeof define === "function" && define.amd ) {

    // AMD. Register as an anonymous module.
    define([ "jquery" ], factory );

  } else if ( typeof exports === "object" ) {

    // Node/CommonJS
    factory( require( "jquery" ) );

  } else {

    // Browser globals
    factory( jQuery );
  }
}(function( $ ) {
  /*!
   * jQuery UI Widget 1.11.4
   * http://jqueryui.com
   *
   * Copyright jQuery Foundation and other contributors
   * Released under the MIT license.
   * http://jquery.org/license
   *
   * http://api.jqueryui.com/jQuery.widget/
   */


  var widget_uuid = 0,
    widget_slice = Array.prototype.slice;

  $.cleanData = (function( orig ) {
    return function( elems ) {
      var events, elem, i;
      for ( i = 0; (elem = elems[i]) != null; i++ ) {
        try {

          // Only trigger remove when necessary to save time
          events = $._data( elem, "events" );
          if ( events && events.remove ) {
            $( elem ).triggerHandler( "remove" );
          }

          // http://bugs.jquery.com/ticket/8235
        } catch ( e ) {}
      }
      orig( elems );
    };
  })( $.cleanData );

  $.widget = function( name, base, prototype ) {
    var fullName, existingConstructor, constructor, basePrototype,
      // proxiedPrototype allows the provided prototype to remain unmodified
      // so that it can be used as a mixin for multiple widgets (#8876)
      proxiedPrototype = {},
      namespace = name.split( "." )[ 0 ];

    name = name.split( "." )[ 1 ];
    fullName = namespace + "-" + name;

    if ( !prototype ) {
      prototype = base;
      base = $.Widget;
    }

    // create selector for plugin
    $.expr[ ":" ][ fullName.toLowerCase() ] = function( elem ) {
      return !!$.data( elem, fullName );
    };

    $[ namespace ] = $[ namespace ] || {};
    existingConstructor = $[ namespace ][ name ];
    constructor = $[ namespace ][ name ] = function( options, element ) {
      // allow instantiation without "new" keyword
      if ( !this._createWidget ) {
        return new constructor( options, element );
      }

      // allow instantiation without initializing for simple inheritance
      // must use "new" keyword (the code above always passes args)
      if ( arguments.length ) {
        this._createWidget( options, element );
      }
    };
    // extend with the existing constructor to carry over any static properties
    $.extend( constructor, existingConstructor, {
      version: prototype.version,
      // copy the object used to create the prototype in case we need to
      // redefine the widget later
      _proto: $.extend( {}, prototype ),
      // track widgets that inherit from this widget in case this widget is
      // redefined after a widget inherits from it
      _childConstructors: []
    });

    basePrototype = new base();
    // we need to make the options hash a property directly on the new instance
    // otherwise we'll modify the options hash on the prototype that we're
    // inheriting from
    basePrototype.options = $.widget.extend( {}, basePrototype.options );
    $.each( prototype, function( prop, value ) {
      if ( !$.isFunction( value ) ) {
        proxiedPrototype[ prop ] = value;
        return;
      }
      proxiedPrototype[ prop ] = (function() {
        var _super = function() {
            return base.prototype[ prop ].apply( this, arguments );
          },
          _superApply = function( args ) {
            return base.prototype[ prop ].apply( this, args );
          };
        return function() {
          var __super = this._super,
            __superApply = this._superApply,
            returnValue;

          this._super = _super;
          this._superApply = _superApply;

          returnValue = value.apply( this, arguments );

          this._super = __super;
          this._superApply = __superApply;

          return returnValue;
        };
      })();
    });
    constructor.prototype = $.widget.extend( basePrototype, {
      // TODO: remove support for widgetEventPrefix
      // always use the name + a colon as the prefix, e.g., draggable:start
      // don't prefix for widgets that aren't DOM-based
      widgetEventPrefix: existingConstructor ? (basePrototype.widgetEventPrefix || name) : name
    }, proxiedPrototype, {
      constructor: constructor,
      namespace: namespace,
      widgetName: name,
      widgetFullName: fullName
    });

    // If this widget is being redefined then we need to find all widgets that
    // are inheriting from it and redefine all of them so that they inherit from
    // the new version of this widget. We're essentially trying to replace one
    // level in the prototype chain.
    if ( existingConstructor ) {
      $.each( existingConstructor._childConstructors, function( i, child ) {
        var childPrototype = child.prototype;

        // redefine the child widget using the same prototype that was
        // originally used, but inherit from the new version of the base
        $.widget( childPrototype.namespace + "." + childPrototype.widgetName, constructor, child._proto );
      });
      // remove the list of existing child constructors from the old constructor
      // so the old child constructors can be garbage collected
      delete existingConstructor._childConstructors;
    } else {
      base._childConstructors.push( constructor );
    }

    $.widget.bridge( name, constructor );

    return constructor;
  };

  $.widget.extend = function( target ) {
    var input = widget_slice.call( arguments, 1 ),
      inputIndex = 0,
      inputLength = input.length,
      key,
      value;
    for ( ; inputIndex < inputLength; inputIndex++ ) {
      for ( key in input[ inputIndex ] ) {
        value = input[ inputIndex ][ key ];
        if ( input[ inputIndex ].hasOwnProperty( key ) && value !== undefined ) {
          // Clone objects
          if ( $.isPlainObject( value ) ) {
            target[ key ] = $.isPlainObject( target[ key ] ) ?
              $.widget.extend( {}, target[ key ], value ) :
              // Don't extend strings, arrays, etc. with objects
              $.widget.extend( {}, value );
            // Copy everything else by reference
          } else {
            target[ key ] = value;
          }
        }
      }
    }
    return target;
  };

  $.widget.bridge = function( name, object ) {
    var fullName = object.prototype.widgetFullName || name;
    $.fn[ name ] = function( options ) {
      var isMethodCall = typeof options === "string",
        args = widget_slice.call( arguments, 1 ),
        returnValue = this;

      if ( isMethodCall ) {
        this.each(function() {
          var methodValue,
            instance = $.data( this, fullName );
          if ( options === "instance" ) {
            returnValue = instance;
            return false;
          }
          if ( !instance ) {
            return $.error( "cannot call methods on " + name + " prior to initialization; " +
              "attempted to call method '" + options + "'" );
          }
          if ( !$.isFunction( instance[options] ) || options.charAt( 0 ) === "_" ) {
            return $.error( "no such method '" + options + "' for " + name + " widget instance" );
          }
          methodValue = instance[ options ].apply( instance, args );
          if ( methodValue !== instance && methodValue !== undefined ) {
            returnValue = methodValue && methodValue.jquery ?
              returnValue.pushStack( methodValue.get() ) :
              methodValue;
            return false;
          }
        });
      } else {

        // Allow multiple hashes to be passed on init
        if ( args.length ) {
          options = $.widget.extend.apply( null, [ options ].concat(args) );
        }

        this.each(function() {
          var instance = $.data( this, fullName );
          if ( instance ) {
            instance.option( options || {} );
            if ( instance._init ) {
              instance._init();
            }
          } else {
            $.data( this, fullName, new object( options, this ) );
          }
        });
      }

      return returnValue;
    };
  };

  $.Widget = function( /* options, element */ ) {};
  $.Widget._childConstructors = [];

  $.Widget.prototype = {
    widgetName: "widget",
    widgetEventPrefix: "",
    defaultElement: "<div>",
    options: {
      disabled: false,

      // callbacks
      create: null
    },
    _createWidget: function( options, element ) {
      element = $( element || this.defaultElement || this )[ 0 ];
      this.element = $( element );
      this.uuid = widget_uuid++;
      this.eventNamespace = "." + this.widgetName + this.uuid;

      this.bindings = $();
      this.hoverable = $();
      this.focusable = $();

      if ( element !== this ) {
        $.data( element, this.widgetFullName, this );
        this._on( true, this.element, {
          remove: function( event ) {
            if ( event.target === element ) {
              this.destroy();
            }
          }
        });
        this.document = $( element.style ?
          // element within the document
          element.ownerDocument :
          // element is window or document
          element.document || element );
        this.window = $( this.document[0].defaultView || this.document[0].parentWindow );
      }

      this.options = $.widget.extend( {},
        this.options,
        this._getCreateOptions(),
        options );

      this._create();
      this._trigger( "create", null, this._getCreateEventData() );
      this._init();
    },
    _getCreateOptions: $.noop,
    _getCreateEventData: $.noop,
    _create: $.noop,
    _init: $.noop,

    destroy: function() {
      this._destroy();
      // we can probably remove the unbind calls in 2.0
      // all event bindings should go through this._on()
      this.element
        .unbind( this.eventNamespace )
        .removeData( this.widgetFullName )
        // support: jquery <1.6.3
        // http://bugs.jquery.com/ticket/9413
        .removeData( $.camelCase( this.widgetFullName ) );
      this.widget()
        .unbind( this.eventNamespace )
        .removeAttr( "aria-disabled" )
        .removeClass(
          this.widgetFullName + "-disabled " +
          "ui-state-disabled" );

      // clean up events and states
      this.bindings.unbind( this.eventNamespace );
      this.hoverable.removeClass( "ui-state-hover" );
      this.focusable.removeClass( "ui-state-focus" );
    },
    _destroy: $.noop,

    widget: function() {
      return this.element;
    },

    option: function( key, value ) {
      var options = key,
        parts,
        curOption,
        i;

      if ( arguments.length === 0 ) {
        // don't return a reference to the internal hash
        return $.widget.extend( {}, this.options );
      }

      if ( typeof key === "string" ) {
        // handle nested keys, e.g., "foo.bar" => { foo: { bar: ___ } }
        options = {};
        parts = key.split( "." );
        key = parts.shift();
        if ( parts.length ) {
          curOption = options[ key ] = $.widget.extend( {}, this.options[ key ] );
          for ( i = 0; i < parts.length - 1; i++ ) {
            curOption[ parts[ i ] ] = curOption[ parts[ i ] ] || {};
            curOption = curOption[ parts[ i ] ];
          }
          key = parts.pop();
          if ( arguments.length === 1 ) {
            return curOption[ key ] === undefined ? null : curOption[ key ];
          }
          curOption[ key ] = value;
        } else {
          if ( arguments.length === 1 ) {
            return this.options[ key ] === undefined ? null : this.options[ key ];
          }
          options[ key ] = value;
        }
      }

      this._setOptions( options );

      return this;
    },
    _setOptions: function( options ) {
      var key;

      for ( key in options ) {
        this._setOption( key, options[ key ] );
      }

      return this;
    },
    _setOption: function( key, value ) {
      this.options[ key ] = value;

      if ( key === "disabled" ) {
        this.widget()
          .toggleClass( this.widgetFullName + "-disabled", !!value );

        // If the widget is becoming disabled, then nothing is interactive
        if ( value ) {
          this.hoverable.removeClass( "ui-state-hover" );
          this.focusable.removeClass( "ui-state-focus" );
        }
      }

      return this;
    },

    enable: function() {
      return this._setOptions({ disabled: false });
    },
    disable: function() {
      return this._setOptions({ disabled: true });
    },

    _on: function( suppressDisabledCheck, element, handlers ) {
      var delegateElement,
        instance = this;

      // no suppressDisabledCheck flag, shuffle arguments
      if ( typeof suppressDisabledCheck !== "boolean" ) {
        handlers = element;
        element = suppressDisabledCheck;
        suppressDisabledCheck = false;
      }

      // no element argument, shuffle and use this.element
      if ( !handlers ) {
        handlers = element;
        element = this.element;
        delegateElement = this.widget();
      } else {
        element = delegateElement = $( element );
        this.bindings = this.bindings.add( element );
      }

      $.each( handlers, function( event, handler ) {
        function handlerProxy() {
          // allow widgets to customize the disabled handling
          // - disabled as an array instead of boolean
          // - disabled class as method for disabling individual parts
          if ( !suppressDisabledCheck &&
            ( instance.options.disabled === true ||
              $( this ).hasClass( "ui-state-disabled" ) ) ) {
            return;
          }
          return ( typeof handler === "string" ? instance[ handler ] : handler )
            .apply( instance, arguments );
        }

        // copy the guid so direct unbinding works
        if ( typeof handler !== "string" ) {
          handlerProxy.guid = handler.guid =
            handler.guid || handlerProxy.guid || $.guid++;
        }

        var match = event.match( /^([\w:-]*)\s*(.*)$/ ),
          eventName = match[1] + instance.eventNamespace,
          selector = match[2];
        if ( selector ) {
          delegateElement.delegate( selector, eventName, handlerProxy );
        } else {
          element.bind( eventName, handlerProxy );
        }
      });
    },

    _off: function( element, eventName ) {
      eventName = (eventName || "").split( " " ).join( this.eventNamespace + " " ) +
        this.eventNamespace;
      element.unbind( eventName ).undelegate( eventName );

      // Clear the stack to avoid memory leaks (#10056)
      this.bindings = $( this.bindings.not( element ).get() );
      this.focusable = $( this.focusable.not( element ).get() );
      this.hoverable = $( this.hoverable.not( element ).get() );
    },

    _delay: function( handler, delay ) {
      function handlerProxy() {
        return ( typeof handler === "string" ? instance[ handler ] : handler )
          .apply( instance, arguments );
      }
      var instance = this;
      return setTimeout( handlerProxy, delay || 0 );
    },

    _hoverable: function( element ) {
      this.hoverable = this.hoverable.add( element );
      this._on( element, {
        mouseenter: function( event ) {
          $( event.currentTarget ).addClass( "ui-state-hover" );
        },
        mouseleave: function( event ) {
          $( event.currentTarget ).removeClass( "ui-state-hover" );
        }
      });
    },

    _focusable: function( element ) {
      this.focusable = this.focusable.add( element );
      this._on( element, {
        focusin: function( event ) {
          $( event.currentTarget ).addClass( "ui-state-focus" );
        },
        focusout: function( event ) {
          $( event.currentTarget ).removeClass( "ui-state-focus" );
        }
      });
    },

    _trigger: function( type, event, data ) {
      var prop, orig,
        callback = this.options[ type ];

      data = data || {};
      event = $.Event( event );
      event.type = ( type === this.widgetEventPrefix ?
        type :
        this.widgetEventPrefix + type ).toLowerCase();
      // the original event may come from any element
      // so we need to reset the target on the new event
      event.target = this.element[ 0 ];

      // copy original event properties over to the new event
      orig = event.originalEvent;
      if ( orig ) {
        for ( prop in orig ) {
          if ( !( prop in event ) ) {
            event[ prop ] = orig[ prop ];
          }
        }
      }

      this.element.trigger( event, data );
      return !( $.isFunction( callback ) &&
        callback.apply( this.element[0], [ event ].concat( data ) ) === false ||
        event.isDefaultPrevented() );
    }
  };

  $.each( { show: "fadeIn", hide: "fadeOut" }, function( method, defaultEffect ) {
    $.Widget.prototype[ "_" + method ] = function( element, options, callback ) {
      if ( typeof options === "string" ) {
        options = { effect: options };
      }
      var hasOptions,
        effectName = !options ?
          method :
          options === true || typeof options === "number" ?
            defaultEffect :
            options.effect || defaultEffect;
      options = options || {};
      if ( typeof options === "number" ) {
        options = { duration: options };
      }
      hasOptions = !$.isEmptyObject( options );
      options.complete = callback;
      if ( options.delay ) {
        element.delay( options.delay );
      }
      if ( hasOptions && $.effects && $.effects.effect[ effectName ] ) {
        element[ method ]( options );
      } else if ( effectName !== method && element[ effectName ] ) {
        element[ effectName ]( options.duration, options.easing, callback );
      } else {
        element.queue(function( next ) {
          $( this )[ method ]();
          if ( callback ) {
            callback.call( element[ 0 ] );
          }
          next();
        });
      }
    };
  });

  var widget = $.widget;



}));


/*
 * jQuery Iframe Transport Plugin
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2011, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global define, require, window, document */

(function (factory) {
  'use strict';
  if (typeof define === 'function' && define.amd) {
    // Register as an anonymous AMD module:
    define(['jquery'], factory);
  } else if (typeof exports === 'object') {
    // Node/CommonJS:
    factory(require('jquery'));
  } else {
    // Browser globals:
    factory(window.jQuery);
  }
}(function ($) {
  'use strict';

  // Helper variable to create unique names for the transport iframes:
  var counter = 0;

  // The iframe transport accepts four additional options:
  // options.fileInput: a jQuery collection of file input fields
  // options.paramName: the parameter name for the file form data,
  //  overrides the name property of the file input field(s),
  //  can be a string or an array of strings.
  // options.formData: an array of objects with name and value properties,
  //  equivalent to the return data of .serializeArray(), e.g.:
  //  [{name: 'a', value: 1}, {name: 'b', value: 2}]
  // options.initialIframeSrc: the URL of the initial iframe src,
  //  by default set to "javascript:false;"
  $.ajaxTransport('iframe', function (options) {
    if (options.async) {
      // javascript:false as initial iframe src
      // prevents warning popups on HTTPS in IE6:
      /*jshint scripturl: true */
      var initialIframeSrc = options.initialIframeSrc || 'javascript:false;',
        /*jshint scripturl: false */
        form,
        iframe,
        addParamChar;
      return {
        send: function (_, completeCallback) {
          form = $('<form style="display:none;"></form>');
          form.attr('accept-charset', options.formAcceptCharset);
          addParamChar = /\?/.test(options.url) ? '&' : '?';
          // XDomainRequest only supports GET and POST:
          if (options.type === 'DELETE') {
            options.url = options.url + addParamChar + '_method=DELETE';
            options.type = 'POST';
          } else if (options.type === 'PUT') {
            options.url = options.url + addParamChar + '_method=PUT';
            options.type = 'POST';
          } else if (options.type === 'PATCH') {
            options.url = options.url + addParamChar + '_method=PATCH';
            options.type = 'POST';
          }
          // IE versions below IE8 cannot set the name property of
          // elements that have already been added to the DOM,
          // so we set the name along with the iframe HTML markup:
          counter += 1;
          iframe = $(
            '<iframe src="' + initialIframeSrc +
            '" name="iframe-transport-' + counter + '"></iframe>'
          ).bind('load', function () {
            var fileInputClones,
              paramNames = $.isArray(options.paramName) ?
                options.paramName : [options.paramName];
            iframe
              .unbind('load')
              .bind('load', function () {
                var response;
                // Wrap in a try/catch block to catch exceptions thrown
                // when trying to access cross-domain iframe contents:
                try {
                  response = iframe.contents();
                  // Google Chrome and Firefox do not throw an
                  // exception when calling iframe.contents() on
                  // cross-domain requests, so we unify the response:
                  if (!response.length || !response[0].firstChild) {
                    throw new Error();
                  }
                } catch (e) {
                  response = undefined;
                }
                // The complete callback returns the
                // iframe content document as response object:
                completeCallback(
                  200,
                  'success',
                  {'iframe': response}
                );
                // Fix for IE endless progress bar activity bug
                // (happens on form submits to iframe targets):
                $('<iframe src="' + initialIframeSrc + '"></iframe>')
                  .appendTo(form);
                window.setTimeout(function () {
                  // Removing the form in a setTimeout call
                  // allows Chrome's developer tools to display
                  // the response result
                  form.remove();
                }, 0);
              });
            form
              .prop('target', iframe.prop('name'))
              .prop('action', options.url)
              .prop('method', options.type);
            if (options.formData) {
              $.each(options.formData, function (index, field) {
                $('<input type="hidden"/>')
                  .prop('name', field.name)
                  .val(field.value)
                  .appendTo(form);
              });
            }
            if (options.fileInput && options.fileInput.length &&
              options.type === 'POST') {
              fileInputClones = options.fileInput.clone();
              // Insert a clone for each file input field:
              options.fileInput.after(function (index) {
                return fileInputClones[index];
              });
              if (options.paramName) {
                options.fileInput.each(function (index) {
                  $(this).prop(
                    'name',
                    paramNames[index] || options.paramName
                  );
                });
              }
              // Appending the file input fields to the hidden form
              // removes them from their original location:
              form
                .append(options.fileInput)
                .prop('enctype', 'multipart/form-data')
                // enctype must be set as encoding for IE:
                .prop('encoding', 'multipart/form-data');
              // Remove the HTML5 form attribute from the input(s):
              options.fileInput.removeAttr('form');
            }
            form.submit();
            // Insert the file input fields at their original location
            // by replacing the clones with the originals:
            if (fileInputClones && fileInputClones.length) {
              options.fileInput.each(function (index, input) {
                var clone = $(fileInputClones[index]);
                // Restore the original name and form properties:
                $(input)
                  .prop('name', clone.prop('name'))
                  .attr('form', clone.attr('form'));
                clone.replaceWith(input);
              });
            }
          });
          form.append(iframe).appendTo(document.body);
        },
        abort: function () {
          if (iframe) {
            // javascript:false as iframe src aborts the request
            // and prevents warning popups on HTTPS in IE6.
            // concat is used to avoid the "Script URL" JSLint error:
            iframe
              .unbind('load')
              .prop('src', initialIframeSrc);
          }
          if (form) {
            form.remove();
          }
        }
      };
    }
  });

  // The iframe transport returns the iframe content document as response.
  // The following adds converters from iframe to text, json, html, xml
  // and script.
  // Please note that the Content-Type for JSON responses has to be text/plain
  // or text/html, if the browser doesn't include application/json in the
  // Accept header, else IE will show a download dialog.
  // The Content-Type for XML responses on the other hand has to be always
  // application/xml or text/xml, so IE properly parses the XML response.
  // See also
  // https://github.com/blueimp/jQuery-File-Upload/wiki/Setup#content-type-negotiation
  $.ajaxSetup({
    converters: {
      'iframe text': function (iframe) {
        return iframe && $(iframe[0].body).text();
      },
      'iframe json': function (iframe) {
        return iframe && $.parseJSON($(iframe[0].body).text());
      },
      'iframe html': function (iframe) {
        return iframe && $(iframe[0].body).html();
      },
      'iframe xml': function (iframe) {
        var xmlDoc = iframe && iframe[0];
        return xmlDoc && $.isXMLDoc(xmlDoc) ? xmlDoc :
          $.parseXML((xmlDoc.XMLDocument && xmlDoc.XMLDocument.xml) ||
            $(xmlDoc.body).html());
      },
      'iframe script': function (iframe) {
        return iframe && $.globalEval($(iframe[0].body).text());
      }
    }
  });

}));


/*
 * jQuery File Upload Plugin
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* jshint nomen:false */
/* global define, require, window, document, location, Blob, FormData */

(function (factory) {
  'use strict';
  if (typeof define === 'function' && define.amd) {
    // Register as an anonymous AMD module:
    define([
      'jquery',
      'jquery.ui.widget'
    ], factory);
  } else if (typeof exports === 'object') {
    // Node/CommonJS:
    factory(
      require('jquery'),
      require('./vendor/jquery.ui.widget')
    );
  } else {
    // Browser globals:
    factory(window.jQuery);
  }
}(function ($) {
  'use strict';

  // Detect file input support, based on
  // http://viljamis.com/blog/2012/file-upload-support-on-mobile/
  $.support.fileInput = !(new RegExp(
    // Handle devices which give false positives for the feature detection:
    '(Android (1\\.[0156]|2\\.[01]))' +
    '|(Windows Phone (OS 7|8\\.0))|(XBLWP)|(ZuneWP)|(WPDesktop)' +
    '|(w(eb)?OSBrowser)|(webOS)' +
    '|(Kindle/(1\\.0|2\\.[05]|3\\.0))'
    ).test(window.navigator.userAgent) ||
    // Feature detection for all other devices:
    $('<input type="file">').prop('disabled'));

  // The FileReader API is not actually used, but works as feature detection,
  // as some Safari versions (5?) support XHR file uploads via the FormData API,
  // but not non-multipart XHR file uploads.
  // window.XMLHttpRequestUpload is not available on IE10, so we check for
  // window.ProgressEvent instead to detect XHR2 file upload capability:
  $.support.xhrFileUpload = !!(window.ProgressEvent && window.FileReader);
  $.support.xhrFormDataFileUpload = !!window.FormData;

  // Detect support for Blob slicing (required for chunked uploads):
  $.support.blobSlice = window.Blob && (Blob.prototype.slice ||
    Blob.prototype.webkitSlice || Blob.prototype.mozSlice);

  // Helper function to create drag handlers for dragover/dragenter/dragleave:
  function getDragHandler(type) {
    var isDragOver = type === 'dragover';
    return function (e) {
      e.dataTransfer = e.originalEvent && e.originalEvent.dataTransfer;
      var dataTransfer = e.dataTransfer;
      if (dataTransfer && $.inArray('Files', dataTransfer.types) !== -1 &&
        this._trigger(
          type,
          $.Event(type, {delegatedEvent: e})
        ) !== false) {
        e.preventDefault();
        if (isDragOver) {
          dataTransfer.dropEffect = 'copy';
        }
      }
    };
  }

  // The fileupload widget listens for change events on file input fields defined
  // via fileInput setting and paste or drop events of the given dropZone.
  // In addition to the default jQuery Widget methods, the fileupload widget
  // exposes the "add" and "send" methods, to add or directly send files using
  // the fileupload API.
  // By default, files added via file input selection, paste, drag & drop or
  // "add" method are uploaded immediately, but it is possible to override
  // the "add" callback option to queue file uploads.
  $.widget('blueimp.fileupload', {

    options: {
      // The drop target element(s), by the default the complete document.
      // Set to null to disable drag & drop support:
      dropZone: $(document),
      // The paste target element(s), by the default undefined.
      // Set to a DOM node or jQuery object to enable file pasting:
      pasteZone: undefined,
      // The file input field(s), that are listened to for change events.
      // If undefined, it is set to the file input fields inside
      // of the widget element on plugin initialization.
      // Set to null to disable the change listener.
      fileInput: undefined,
      // By default, the file input field is replaced with a clone after
      // each input field change event. This is required for iframe transport
      // queues and allows change events to be fired for the same file
      // selection, but can be disabled by setting the following option to false:
      replaceFileInput: true,
      // The parameter name for the file form data (the request argument name).
      // If undefined or empty, the name property of the file input field is
      // used, or "files[]" if the file input name property is also empty,
      // can be a string or an array of strings:
      paramName: undefined,
      // By default, each file of a selection is uploaded using an individual
      // request for XHR type uploads. Set to false to upload file
      // selections in one request each:
      singleFileUploads: true,
      // To limit the number of files uploaded with one XHR request,
      // set the following option to an integer greater than 0:
      limitMultiFileUploads: undefined,
      // The following option limits the number of files uploaded with one
      // XHR request to keep the request size under or equal to the defined
      // limit in bytes:
      limitMultiFileUploadSize: undefined,
      // Multipart file uploads add a number of bytes to each uploaded file,
      // therefore the following option adds an overhead for each file used
      // in the limitMultiFileUploadSize configuration:
      limitMultiFileUploadSizeOverhead: 512,
      // Set the following option to true to issue all file upload requests
      // in a sequential order:
      sequentialUploads: false,
      // To limit the number of concurrent uploads,
      // set the following option to an integer greater than 0:
      limitConcurrentUploads: undefined,
      // Set the following option to true to force iframe transport uploads:
      forceIframeTransport: false,
      // Set the following option to the location of a redirect url on the
      // origin server, for cross-domain iframe transport uploads:
      redirect: undefined,
      // The parameter name for the redirect url, sent as part of the form
      // data and set to 'redirect' if this option is empty:
      redirectParamName: undefined,
      // Set the following option to the location of a postMessage window,
      // to enable postMessage transport uploads:
      postMessage: undefined,
      // By default, XHR file uploads are sent as multipart/form-data.
      // The iframe transport is always using multipart/form-data.
      // Set to false to enable non-multipart XHR uploads:
      multipart: true,
      // To upload large files in smaller chunks, set the following option
      // to a preferred maximum chunk size. If set to 0, null or undefined,
      // or the browser does not support the required Blob API, files will
      // be uploaded as a whole.
      maxChunkSize: undefined,
      // When a non-multipart upload or a chunked multipart upload has been
      // aborted, this option can be used to resume the upload by setting
      // it to the size of the already uploaded bytes. This option is most
      // useful when modifying the options object inside of the "add" or
      // "send" callbacks, as the options are cloned for each file upload.
      uploadedBytes: undefined,
      // By default, failed (abort or error) file uploads are removed from the
      // global progress calculation. Set the following option to false to
      // prevent recalculating the global progress data:
      recalculateProgress: true,
      // Interval in milliseconds to calculate and trigger progress events:
      progressInterval: 100,
      // Interval in milliseconds to calculate progress bitrate:
      bitrateInterval: 500,
      // By default, uploads are started automatically when adding files:
      autoUpload: true,

      // Error and info messages:
      messages: {
        uploadedBytes: 'Uploaded bytes exceed file size'
      },

      // Translation function, gets the message key to be translated
      // and an object with context specific data as arguments:
      i18n: function (message, context) {
        message = this.messages[message] || message.toString();
        if (context) {
          $.each(context, function (key, value) {
            message = message.replace('{' + key + '}', value);
          });
        }
        return message;
      },

      // Additional form data to be sent along with the file uploads can be set
      // using this option, which accepts an array of objects with name and
      // value properties, a function returning such an array, a FormData
      // object (for XHR file uploads), or a simple object.
      // The form of the first fileInput is given as parameter to the function:
      formData: function (form) {
        return form.serializeArray();
      },

      // The add callback is invoked as soon as files are added to the fileupload
      // widget (via file input selection, drag & drop, paste or add API call).
      // If the singleFileUploads option is enabled, this callback will be
      // called once for each file in the selection for XHR file uploads, else
      // once for each file selection.
      //
      // The upload starts when the submit method is invoked on the data parameter.
      // The data object contains a files property holding the added files
      // and allows you to override plugin options as well as define ajax settings.
      //
      // Listeners for this callback can also be bound the following way:
      // .bind('fileuploadadd', func);
      //
      // data.submit() returns a Promise object and allows to attach additional
      // handlers using jQuery's Deferred callbacks:
      // data.submit().done(func).fail(func).always(func);
      add: function (e, data) {
        if (e.isDefaultPrevented()) {
          return false;
        }
        if (data.autoUpload || (data.autoUpload !== false &&
            $(this).fileupload('option', 'autoUpload'))) {
          data.process().done(function () {
            data.submit();
          });
        }
      },

      // Other callbacks:

      // Callback for the submit event of each file upload:
      // submit: function (e, data) {}, // .bind('fileuploadsubmit', func);

      // Callback for the start of each file upload request:
      // send: function (e, data) {}, // .bind('fileuploadsend', func);

      // Callback for successful uploads:
      // done: function (e, data) {}, // .bind('fileuploaddone', func);

      // Callback for failed (abort or error) uploads:
      // fail: function (e, data) {}, // .bind('fileuploadfail', func);

      // Callback for completed (success, abort or error) requests:
      // always: function (e, data) {}, // .bind('fileuploadalways', func);

      // Callback for upload progress events:
      // progress: function (e, data) {}, // .bind('fileuploadprogress', func);

      // Callback for global upload progress events:
      // progressall: function (e, data) {}, // .bind('fileuploadprogressall', func);

      // Callback for uploads start, equivalent to the global ajaxStart event:
      // start: function (e) {}, // .bind('fileuploadstart', func);

      // Callback for uploads stop, equivalent to the global ajaxStop event:
      // stop: function (e) {}, // .bind('fileuploadstop', func);

      // Callback for change events of the fileInput(s):
      // change: function (e, data) {}, // .bind('fileuploadchange', func);

      // Callback for paste events to the pasteZone(s):
      // paste: function (e, data) {}, // .bind('fileuploadpaste', func);

      // Callback for drop events of the dropZone(s):
      // drop: function (e, data) {}, // .bind('fileuploaddrop', func);

      // Callback for dragover events of the dropZone(s):
      // dragover: function (e) {}, // .bind('fileuploaddragover', func);

      // Callback for the start of each chunk upload request:
      // chunksend: function (e, data) {}, // .bind('fileuploadchunksend', func);

      // Callback for successful chunk uploads:
      // chunkdone: function (e, data) {}, // .bind('fileuploadchunkdone', func);

      // Callback for failed (abort or error) chunk uploads:
      // chunkfail: function (e, data) {}, // .bind('fileuploadchunkfail', func);

      // Callback for completed (success, abort or error) chunk upload requests:
      // chunkalways: function (e, data) {}, // .bind('fileuploadchunkalways', func);

      // The plugin options are used as settings object for the ajax calls.
      // The following are jQuery ajax settings required for the file uploads:
      processData: false,
      contentType: false,
      cache: false,
      timeout: 0
    },

    // A list of options that require reinitializing event listeners and/or
    // special initialization code:
    _specialOptions: [
      'fileInput',
      'dropZone',
      'pasteZone',
      'multipart',
      'forceIframeTransport'
    ],

    _blobSlice: $.support.blobSlice && function () {
      var slice = this.slice || this.webkitSlice || this.mozSlice;
      return slice.apply(this, arguments);
    },

    _BitrateTimer: function () {
      this.timestamp = ((Date.now) ? Date.now() : (new Date()).getTime());
      this.loaded = 0;
      this.bitrate = 0;
      this.getBitrate = function (now, loaded, interval) {
        var timeDiff = now - this.timestamp;
        if (!this.bitrate || !interval || timeDiff > interval) {
          this.bitrate = (loaded - this.loaded) * (1000 / timeDiff) * 8;
          this.loaded = loaded;
          this.timestamp = now;
        }
        return this.bitrate;
      };
    },

    _isXHRUpload: function (options) {
      return !options.forceIframeTransport &&
        ((!options.multipart && $.support.xhrFileUpload) ||
          $.support.xhrFormDataFileUpload);
    },

    _getFormData: function (options) {
      var formData;
      if ($.type(options.formData) === 'function') {
        return options.formData(options.form);
      }
      if ($.isArray(options.formData)) {
        return options.formData;
      }
      if ($.type(options.formData) === 'object') {
        formData = [];
        $.each(options.formData, function (name, value) {
          formData.push({name: name, value: value});
        });
        return formData;
      }
      return [];
    },

    _getTotal: function (files) {
      var total = 0;
      $.each(files, function (index, file) {
        total += file.size || 1;
      });
      return total;
    },

    _initProgressObject: function (obj) {
      var progress = {
        loaded: 0,
        total: 0,
        bitrate: 0
      };
      if (obj._progress) {
        $.extend(obj._progress, progress);
      } else {
        obj._progress = progress;
      }
    },

    _initResponseObject: function (obj) {
      var prop;
      if (obj._response) {
        for (prop in obj._response) {
          if (obj._response.hasOwnProperty(prop)) {
            delete obj._response[prop];
          }
        }
      } else {
        obj._response = {};
      }
    },

    _onProgress: function (e, data) {
      if (e.lengthComputable) {
        var now = ((Date.now) ? Date.now() : (new Date()).getTime()),
          loaded;
        if (data._time && data.progressInterval &&
          (now - data._time < data.progressInterval) &&
          e.loaded !== e.total) {
          return;
        }
        data._time = now;
        loaded = Math.floor(
          e.loaded / e.total * (data.chunkSize || data._progress.total)
        ) + (data.uploadedBytes || 0);
        // Add the difference from the previously loaded state
        // to the global loaded counter:
        this._progress.loaded += (loaded - data._progress.loaded);
        this._progress.bitrate = this._bitrateTimer.getBitrate(
          now,
          this._progress.loaded,
          data.bitrateInterval
        );
        data._progress.loaded = data.loaded = loaded;
        data._progress.bitrate = data.bitrate = data._bitrateTimer.getBitrate(
          now,
          loaded,
          data.bitrateInterval
        );
        // Trigger a custom progress event with a total data property set
        // to the file size(s) of the current upload and a loaded data
        // property calculated accordingly:
        this._trigger(
          'progress',
          $.Event('progress', {delegatedEvent: e}),
          data
        );
        // Trigger a global progress event for all current file uploads,
        // including ajax calls queued for sequential file uploads:
        this._trigger(
          'progressall',
          $.Event('progressall', {delegatedEvent: e}),
          this._progress
        );
      }
    },

    _initProgressListener: function (options) {
      var that = this,
        xhr = options.xhr ? options.xhr() : $.ajaxSettings.xhr();
      // Accesss to the native XHR object is required to add event listeners
      // for the upload progress event:
      if (xhr.upload) {
        $(xhr.upload).bind('progress', function (e) {
          var oe = e.originalEvent;
          // Make sure the progress event properties get copied over:
          e.lengthComputable = oe.lengthComputable;
          e.loaded = oe.loaded;
          e.total = oe.total;
          that._onProgress(e, options);
        });
        options.xhr = function () {
          return xhr;
        };
      }
    },

    _isInstanceOf: function (type, obj) {
      // Cross-frame instanceof check
      return Object.prototype.toString.call(obj) === '[object ' + type + ']';
    },

    _initXHRData: function (options) {
      var that = this,
        formData,
        file = options.files[0],
        // Ignore non-multipart setting if not supported:
        multipart = options.multipart || !$.support.xhrFileUpload,
        paramName = $.type(options.paramName) === 'array' ?
          options.paramName[0] : options.paramName;
      options.headers = $.extend({}, options.headers);
      if (options.contentRange) {
        options.headers['Content-Range'] = options.contentRange;
      }
      if (!multipart || options.blob || !this._isInstanceOf('File', file)) {
        options.headers['Content-Disposition'] = 'attachment; filename="' +
          encodeURI(file.name) + '"';
      }
      if (!multipart) {
        options.contentType = file.type || 'application/octet-stream';
        options.data = options.blob || file;
      } else if ($.support.xhrFormDataFileUpload) {
        if (options.postMessage) {
          // window.postMessage does not allow sending FormData
          // objects, so we just add the File/Blob objects to
          // the formData array and let the postMessage window
          // create the FormData object out of this array:
          formData = this._getFormData(options);
          if (options.blob) {
            formData.push({
              name: paramName,
              value: options.blob
            });
          } else {
            $.each(options.files, function (index, file) {
              formData.push({
                name: ($.type(options.paramName) === 'array' &&
                  options.paramName[index]) || paramName,
                value: file
              });
            });
          }
        } else {
          if (that._isInstanceOf('FormData', options.formData)) {
            formData = options.formData;
          } else {
            formData = new FormData();
            $.each(this._getFormData(options), function (index, field) {
              formData.append(field.name, field.value);
            });
          }
          if (options.blob) {
            formData.append(paramName, options.blob, file.name);
          } else {
            $.each(options.files, function (index, file) {
              // This check allows the tests to run with
              // dummy objects:
              if (that._isInstanceOf('File', file) ||
                that._isInstanceOf('Blob', file)) {
                formData.append(
                  ($.type(options.paramName) === 'array' &&
                    options.paramName[index]) || paramName,
                  file,
                  file.uploadName || file.name
                );
              }
            });
          }
        }
        options.data = formData;
      }
      // Blob reference is not needed anymore, free memory:
      options.blob = null;
    },

    _initIframeSettings: function (options) {
      var targetHost = $('<a></a>').prop('href', options.url).prop('host');
      // Setting the dataType to iframe enables the iframe transport:
      options.dataType = 'iframe ' + (options.dataType || '');
      // The iframe transport accepts a serialized array as form data:
      options.formData = this._getFormData(options);
      // Add redirect url to form data on cross-domain uploads:
      if (options.redirect && targetHost && targetHost !== location.host) {
        options.formData.push({
          name: options.redirectParamName || 'redirect',
          value: options.redirect
        });
      }
    },

    _initDataSettings: function (options) {
      if (this._isXHRUpload(options)) {
        if (!this._chunkedUpload(options, true)) {
          if (!options.data) {
            this._initXHRData(options);
          }
          this._initProgressListener(options);
        }
        if (options.postMessage) {
          // Setting the dataType to postmessage enables the
          // postMessage transport:
          options.dataType = 'postmessage ' + (options.dataType || '');
        }
      } else {
        this._initIframeSettings(options);
      }
    },

    _getParamName: function (options) {
      var fileInput = $(options.fileInput),
        paramName = options.paramName;
      if (!paramName) {
        paramName = [];
        fileInput.each(function () {
          var input = $(this),
            name = input.prop('name') || 'files[]',
            i = (input.prop('files') || [1]).length;
          while (i) {
            paramName.push(name);
            i -= 1;
          }
        });
        if (!paramName.length) {
          paramName = [fileInput.prop('name') || 'files[]'];
        }
      } else if (!$.isArray(paramName)) {
        paramName = [paramName];
      }
      return paramName;
    },

    _initFormSettings: function (options) {
      // Retrieve missing options from the input field and the
      // associated form, if available:
      if (!options.form || !options.form.length) {
        options.form = $(options.fileInput.prop('form'));
        // If the given file input doesn't have an associated form,
        // use the default widget file input's form:
        if (!options.form.length) {
          options.form = $(this.options.fileInput.prop('form'));
        }
      }
      options.paramName = this._getParamName(options);
      if (!options.url) {
        options.url = options.form.prop('action') || location.href;
      }
      // The HTTP request method must be "POST" or "PUT":
      options.type = (options.type ||
        ($.type(options.form.prop('method')) === 'string' &&
          options.form.prop('method')) || ''
      ).toUpperCase();
      if (options.type !== 'POST' && options.type !== 'PUT' &&
        options.type !== 'PATCH') {
        options.type = 'POST';
      }
      if (!options.formAcceptCharset) {
        options.formAcceptCharset = options.form.attr('accept-charset');
      }
    },

    _getAJAXSettings: function (data) {
      var options = $.extend({}, this.options, data);
      this._initFormSettings(options);
      this._initDataSettings(options);
      return options;
    },

    // jQuery 1.6 doesn't provide .state(),
    // while jQuery 1.8+ removed .isRejected() and .isResolved():
    _getDeferredState: function (deferred) {
      if (deferred.state) {
        return deferred.state();
      }
      if (deferred.isResolved()) {
        return 'resolved';
      }
      if (deferred.isRejected()) {
        return 'rejected';
      }
      return 'pending';
    },

    // Maps jqXHR callbacks to the equivalent
    // methods of the given Promise object:
    _enhancePromise: function (promise) {
      promise.success = promise.done;
      promise.error = promise.fail;
      promise.complete = promise.always;
      return promise;
    },

    // Creates and returns a Promise object enhanced with
    // the jqXHR methods abort, success, error and complete:
    _getXHRPromise: function (resolveOrReject, context, args) {
      var dfd = $.Deferred(),
        promise = dfd.promise();
      context = context || this.options.context || promise;
      if (resolveOrReject === true) {
        dfd.resolveWith(context, args);
      } else if (resolveOrReject === false) {
        dfd.rejectWith(context, args);
      }
      promise.abort = dfd.promise;
      return this._enhancePromise(promise);
    },

    // Adds convenience methods to the data callback argument:
    _addConvenienceMethods: function (e, data) {
      var that = this,
        getPromise = function (args) {
          return $.Deferred().resolveWith(that, args).promise();
        };
      data.process = function (resolveFunc, rejectFunc) {
        if (resolveFunc || rejectFunc) {
          data._processQueue = this._processQueue =
            (this._processQueue || getPromise([this])).pipe(
              function () {
                if (data.errorThrown) {
                  return $.Deferred()
                    .rejectWith(that, [data]).promise();
                }
                return getPromise(arguments);
              }
            ).pipe(resolveFunc, rejectFunc);
        }
        return this._processQueue || getPromise([this]);
      };
      data.submit = function () {
        if (this.state() !== 'pending') {
          data.jqXHR = this.jqXHR =
            (that._trigger(
              'submit',
              $.Event('submit', {delegatedEvent: e}),
              this
            ) !== false) && that._onSend(e, this);
        }
        return this.jqXHR || that._getXHRPromise();
      };
      data.abort = function () {
        if (this.jqXHR) {
          return this.jqXHR.abort();
        }
        this.errorThrown = 'abort';
        that._trigger('fail', null, this);
        return that._getXHRPromise(false);
      };
      data.state = function () {
        if (this.jqXHR) {
          return that._getDeferredState(this.jqXHR);
        }
        if (this._processQueue) {
          return that._getDeferredState(this._processQueue);
        }
      };
      data.processing = function () {
        return !this.jqXHR && this._processQueue && that
          ._getDeferredState(this._processQueue) === 'pending';
      };
      data.progress = function () {
        return this._progress;
      };
      data.response = function () {
        return this._response;
      };
    },

    // Parses the Range header from the server response
    // and returns the uploaded bytes:
    _getUploadedBytes: function (jqXHR) {
      var range = jqXHR.getResponseHeader('Range'),
        parts = range && range.split('-'),
        upperBytesPos = parts && parts.length > 1 &&
          parseInt(parts[1], 10);
      return upperBytesPos && upperBytesPos + 1;
    },

    // Uploads a file in multiple, sequential requests
    // by splitting the file up in multiple blob chunks.
    // If the second parameter is true, only tests if the file
    // should be uploaded in chunks, but does not invoke any
    // upload requests:
    _chunkedUpload: function (options, testOnly) {
      options.uploadedBytes = options.uploadedBytes || 0;
      var that = this,
        file = options.files[0],
        fs = file.size,
        ub = options.uploadedBytes,
        mcs = options.maxChunkSize || fs,
        slice = this._blobSlice,
        dfd = $.Deferred(),
        promise = dfd.promise(),
        jqXHR,
        upload;
      if (!(this._isXHRUpload(options) && slice && (ub || mcs < fs)) ||
        options.data) {
        return false;
      }
      if (testOnly) {
        return true;
      }
      if (ub >= fs) {
        file.error = options.i18n('uploadedBytes');
        return this._getXHRPromise(
          false,
          options.context,
          [null, 'error', file.error]
        );
      }
      // The chunk upload method:
      upload = function () {
        // Clone the options object for each chunk upload:
        var o = $.extend({}, options),
          currentLoaded = o._progress.loaded;
        o.blob = slice.call(
          file,
          ub,
          ub + mcs,
          file.type
        );
        // Store the current chunk size, as the blob itself
        // will be dereferenced after data processing:
        o.chunkSize = o.blob.size;
        // Expose the chunk bytes position range:
        o.contentRange = 'bytes ' + ub + '-' +
          (ub + o.chunkSize - 1) + '/' + fs;
        // Process the upload data (the blob and potential form data):
        that._initXHRData(o);
        // Add progress listeners for this chunk upload:
        that._initProgressListener(o);
        jqXHR = ((that._trigger('chunksend', null, o) !== false && $.ajax(o)) ||
          that._getXHRPromise(false, o.context))
          .done(function (result, textStatus, jqXHR) {
            ub = that._getUploadedBytes(jqXHR) ||
              (ub + o.chunkSize);
            // Create a progress event if no final progress event
            // with loaded equaling total has been triggered
            // for this chunk:
            if (currentLoaded + o.chunkSize - o._progress.loaded) {
              that._onProgress($.Event('progress', {
                lengthComputable: true,
                loaded: ub - o.uploadedBytes,
                total: ub - o.uploadedBytes
              }), o);
            }
            options.uploadedBytes = o.uploadedBytes = ub;
            o.result = result;
            o.textStatus = textStatus;
            o.jqXHR = jqXHR;
            that._trigger('chunkdone', null, o);
            that._trigger('chunkalways', null, o);
            if (ub < fs) {
              // File upload not yet complete,
              // continue with the next chunk:
              upload();
            } else {
              dfd.resolveWith(
                o.context,
                [result, textStatus, jqXHR]
              );
            }
          })
          .fail(function (jqXHR, textStatus, errorThrown) {
            o.jqXHR = jqXHR;
            o.textStatus = textStatus;
            o.errorThrown = errorThrown;
            that._trigger('chunkfail', null, o);
            that._trigger('chunkalways', null, o);
            dfd.rejectWith(
              o.context,
              [jqXHR, textStatus, errorThrown]
            );
          });
      };
      this._enhancePromise(promise);
      promise.abort = function () {
        return jqXHR.abort();
      };
      upload();
      return promise;
    },

    _beforeSend: function (e, data) {
      if (this._active === 0) {
        // the start callback is triggered when an upload starts
        // and no other uploads are currently running,
        // equivalent to the global ajaxStart event:
        this._trigger('start');
        // Set timer for global bitrate progress calculation:
        this._bitrateTimer = new this._BitrateTimer();
        // Reset the global progress values:
        this._progress.loaded = this._progress.total = 0;
        this._progress.bitrate = 0;
      }
      // Make sure the container objects for the .response() and
      // .progress() methods on the data object are available
      // and reset to their initial state:
      this._initResponseObject(data);
      this._initProgressObject(data);
      data._progress.loaded = data.loaded = data.uploadedBytes || 0;
      data._progress.total = data.total = this._getTotal(data.files) || 1;
      data._progress.bitrate = data.bitrate = 0;
      this._active += 1;
      // Initialize the global progress values:
      this._progress.loaded += data.loaded;
      this._progress.total += data.total;
    },

    _onDone: function (result, textStatus, jqXHR, options) {
      var total = options._progress.total,
        response = options._response;
      if (options._progress.loaded < total) {
        // Create a progress event if no final progress event
        // with loaded equaling total has been triggered:
        this._onProgress($.Event('progress', {
          lengthComputable: true,
          loaded: total,
          total: total
        }), options);
      }
      response.result = options.result = result;
      response.textStatus = options.textStatus = textStatus;
      response.jqXHR = options.jqXHR = jqXHR;
      this._trigger('done', null, options);
    },

    _onFail: function (jqXHR, textStatus, errorThrown, options) {
      var response = options._response;
      if (options.recalculateProgress) {
        // Remove the failed (error or abort) file upload from
        // the global progress calculation:
        this._progress.loaded -= options._progress.loaded;
        this._progress.total -= options._progress.total;
      }
      response.jqXHR = options.jqXHR = jqXHR;
      response.textStatus = options.textStatus = textStatus;
      response.errorThrown = options.errorThrown = errorThrown;
      this._trigger('fail', null, options);
    },

    _onAlways: function (jqXHRorResult, textStatus, jqXHRorError, options) {
      // jqXHRorResult, textStatus and jqXHRorError are added to the
      // options object via done and fail callbacks
      this._trigger('always', null, options);
    },

    _onSend: function (e, data) {
      if (!data.submit) {
        this._addConvenienceMethods(e, data);
      }
      var that = this,
        jqXHR,
        aborted,
        slot,
        pipe,
        options = that._getAJAXSettings(data),
        send = function () {
          that._sending += 1;
          // Set timer for bitrate progress calculation:
          options._bitrateTimer = new that._BitrateTimer();
          jqXHR = jqXHR || (
            ((aborted || that._trigger(
              'send',
              $.Event('send', {delegatedEvent: e}),
              options
              ) === false) &&
              that._getXHRPromise(false, options.context, aborted)) ||
            that._chunkedUpload(options) || $.ajax(options)
          ).done(function (result, textStatus, jqXHR) {
            that._onDone(result, textStatus, jqXHR, options);
          }).fail(function (jqXHR, textStatus, errorThrown) {
            that._onFail(jqXHR, textStatus, errorThrown, options);
          }).always(function (jqXHRorResult, textStatus, jqXHRorError) {
            that._onAlways(
              jqXHRorResult,
              textStatus,
              jqXHRorError,
              options
            );
            that._sending -= 1;
            that._active -= 1;
            if (options.limitConcurrentUploads &&
              options.limitConcurrentUploads > that._sending) {
              // Start the next queued upload,
              // that has not been aborted:
              var nextSlot = that._slots.shift();
              while (nextSlot) {
                if (that._getDeferredState(nextSlot) === 'pending') {
                  nextSlot.resolve();
                  break;
                }
                nextSlot = that._slots.shift();
              }
            }
            if (that._active === 0) {
              // The stop callback is triggered when all uploads have
              // been completed, equivalent to the global ajaxStop event:
              that._trigger('stop');
            }
          });
          return jqXHR;
        };
      this._beforeSend(e, options);
      if (this.options.sequentialUploads ||
        (this.options.limitConcurrentUploads &&
          this.options.limitConcurrentUploads <= this._sending)) {
        if (this.options.limitConcurrentUploads > 1) {
          slot = $.Deferred();
          this._slots.push(slot);
          pipe = slot.pipe(send);
        } else {
          this._sequence = this._sequence.pipe(send, send);
          pipe = this._sequence;
        }
        // Return the piped Promise object, enhanced with an abort method,
        // which is delegated to the jqXHR object of the current upload,
        // and jqXHR callbacks mapped to the equivalent Promise methods:
        pipe.abort = function () {
          aborted = [undefined, 'abort', 'abort'];
          if (!jqXHR) {
            if (slot) {
              slot.rejectWith(options.context, aborted);
            }
            return send();
          }
          return jqXHR.abort();
        };
        return this._enhancePromise(pipe);
      }
      return send();
    },

    _onAdd: function (e, data) {
      var that = this,
        result = true,
        options = $.extend({}, this.options, data),
        files = data.files,
        filesLength = files.length,
        limit = options.limitMultiFileUploads,
        limitSize = options.limitMultiFileUploadSize,
        overhead = options.limitMultiFileUploadSizeOverhead,
        batchSize = 0,
        paramName = this._getParamName(options),
        paramNameSet,
        paramNameSlice,
        fileSet,
        i,
        j = 0;
      if (!filesLength) {
        return false;
      }
      if (limitSize && files[0].size === undefined) {
        limitSize = undefined;
      }
      if (!(options.singleFileUploads || limit || limitSize) ||
        !this._isXHRUpload(options)) {
        fileSet = [files];
        paramNameSet = [paramName];
      } else if (!(options.singleFileUploads || limitSize) && limit) {
        fileSet = [];
        paramNameSet = [];
        for (i = 0; i < filesLength; i += limit) {
          fileSet.push(files.slice(i, i + limit));
          paramNameSlice = paramName.slice(i, i + limit);
          if (!paramNameSlice.length) {
            paramNameSlice = paramName;
          }
          paramNameSet.push(paramNameSlice);
        }
      } else if (!options.singleFileUploads && limitSize) {
        fileSet = [];
        paramNameSet = [];
        for (i = 0; i < filesLength; i = i + 1) {
          batchSize += files[i].size + overhead;
          if (i + 1 === filesLength ||
            ((batchSize + files[i + 1].size + overhead) > limitSize) ||
            (limit && i + 1 - j >= limit)) {
            fileSet.push(files.slice(j, i + 1));
            paramNameSlice = paramName.slice(j, i + 1);
            if (!paramNameSlice.length) {
              paramNameSlice = paramName;
            }
            paramNameSet.push(paramNameSlice);
            j = i + 1;
            batchSize = 0;
          }
        }
      } else {
        paramNameSet = paramName;
      }
      data.originalFiles = files;
      $.each(fileSet || files, function (index, element) {
        var newData = $.extend({}, data);
        newData.files = fileSet ? element : [element];
        newData.paramName = paramNameSet[index];
        that._initResponseObject(newData);
        that._initProgressObject(newData);
        that._addConvenienceMethods(e, newData);
        result = that._trigger(
          'add',
          $.Event('add', {delegatedEvent: e}),
          newData
        );
        return result;
      });
      return result;
    },

    _replaceFileInput: function (data) {
      var input = data.fileInput,
        inputClone = input.clone(true),
        restoreFocus = input.is(document.activeElement);
      // Add a reference for the new cloned file input to the data argument:
      data.fileInputClone = inputClone;
      $('<form></form>').append(inputClone)[0].reset();
      // Detaching allows to insert the fileInput on another form
      // without loosing the file input value:
      input.after(inputClone).detach();
      // If the fileInput had focus before it was detached,
      // restore focus to the inputClone.
      if (restoreFocus) {
        inputClone.focus();
      }
      // Avoid memory leaks with the detached file input:
      $.cleanData(input.unbind('remove'));
      // Replace the original file input element in the fileInput
      // elements set with the clone, which has been copied including
      // event handlers:
      this.options.fileInput = this.options.fileInput.map(function (i, el) {
        if (el === input[0]) {
          return inputClone[0];
        }
        return el;
      });
      // If the widget has been initialized on the file input itself,
      // override this.element with the file input clone:
      if (input[0] === this.element[0]) {
        this.element = inputClone;
      }
    },

    _handleFileTreeEntry: function (entry, path) {
      var that = this,
        dfd = $.Deferred(),
        errorHandler = function (e) {
          if (e && !e.entry) {
            e.entry = entry;
          }
          // Since $.when returns immediately if one
          // Deferred is rejected, we use resolve instead.
          // This allows valid files and invalid items
          // to be returned together in one set:
          dfd.resolve([e]);
        },
        successHandler = function (entries) {
          that._handleFileTreeEntries(
            entries,
            path + entry.name + '/'
          ).done(function (files) {
            dfd.resolve(files);
          }).fail(errorHandler);
        },
        readEntries = function () {
          dirReader.readEntries(function (results) {
            if (!results.length) {
              successHandler(entries);
            } else {
              entries = entries.concat(results);
              readEntries();
            }
          }, errorHandler);
        },
        dirReader, entries = [];
      path = path || '';
      if (entry.isFile) {
        if (entry._file) {
          // Workaround for Chrome bug #149735
          entry._file.relativePath = path;
          dfd.resolve(entry._file);
        } else {
          entry.file(function (file) {
            file.relativePath = path;
            dfd.resolve(file);
          }, errorHandler);
        }
      } else if (entry.isDirectory) {
        dirReader = entry.createReader();
        readEntries();
      } else {
        // Return an empy list for file system items
        // other than files or directories:
        dfd.resolve([]);
      }
      return dfd.promise();
    },

    _handleFileTreeEntries: function (entries, path) {
      var that = this;
      return $.when.apply(
        $,
        $.map(entries, function (entry) {
          return that._handleFileTreeEntry(entry, path);
        })
      ).pipe(function () {
        return Array.prototype.concat.apply(
          [],
          arguments
        );
      });
    },

    _getDroppedFiles: function (dataTransfer) {
      dataTransfer = dataTransfer || {};
      var items = dataTransfer.items;
      if (items && items.length && (items[0].webkitGetAsEntry ||
          items[0].getAsEntry)) {
        return this._handleFileTreeEntries(
          $.map(items, function (item) {
            var entry;
            if (item.webkitGetAsEntry) {
              entry = item.webkitGetAsEntry();
              if (entry) {
                // Workaround for Chrome bug #149735:
                entry._file = item.getAsFile();
              }
              return entry;
            }
            return item.getAsEntry();
          })
        );
      }
      return $.Deferred().resolve(
        $.makeArray(dataTransfer.files)
      ).promise();
    },

    _getSingleFileInputFiles: function (fileInput) {
      fileInput = $(fileInput);
      var entries = fileInput.prop('webkitEntries') ||
        fileInput.prop('entries'),
        files,
        value;
      if (entries && entries.length) {
        return this._handleFileTreeEntries(entries);
      }
      files = $.makeArray(fileInput.prop('files'));
      if (!files.length) {
        value = fileInput.prop('value');
        if (!value) {
          return $.Deferred().resolve([]).promise();
        }
        // If the files property is not available, the browser does not
        // support the File API and we add a pseudo File object with
        // the input value as name with path information removed:
        files = [{name: value.replace(/^.*\\/, '')}];
      } else if (files[0].name === undefined && files[0].fileName) {
        // File normalization for Safari 4 and Firefox 3:
        $.each(files, function (index, file) {
          file.name = file.fileName;
          file.size = file.fileSize;
        });
      }
      return $.Deferred().resolve(files).promise();
    },

    _getFileInputFiles: function (fileInput) {
      if (!(fileInput instanceof $) || fileInput.length === 1) {
        return this._getSingleFileInputFiles(fileInput);
      }
      return $.when.apply(
        $,
        $.map(fileInput, this._getSingleFileInputFiles)
      ).pipe(function () {
        return Array.prototype.concat.apply(
          [],
          arguments
        );
      });
    },

    _onChange: function (e) {
      var that = this,
        data = {
          fileInput: $(e.target),
          form: $(e.target.form)
        };
      this._getFileInputFiles(data.fileInput).always(function (files) {
        data.files = files;
        if (that.options.replaceFileInput) {
          that._replaceFileInput(data);
        }
        if (that._trigger(
            'change',
            $.Event('change', {delegatedEvent: e}),
            data
          ) !== false) {
          that._onAdd(e, data);
        }
      });
    },

    _onPaste: function (e) {
      var items = e.originalEvent && e.originalEvent.clipboardData &&
        e.originalEvent.clipboardData.items,
        data = {files: []};
      if (items && items.length) {
        $.each(items, function (index, item) {
          var file = item.getAsFile && item.getAsFile();
          if (file) {
            data.files.push(file);
          }
        });
        if (this._trigger(
            'paste',
            $.Event('paste', {delegatedEvent: e}),
            data
          ) !== false) {
          this._onAdd(e, data);
        }
      }
    },

    _onDrop: function (e) {
      e.dataTransfer = e.originalEvent && e.originalEvent.dataTransfer;
      var that = this,
        dataTransfer = e.dataTransfer,
        data = {};
      if (dataTransfer && dataTransfer.files && dataTransfer.files.length) {
        e.preventDefault();
        this._getDroppedFiles(dataTransfer).always(function (files) {
          data.files = files;
          if (that._trigger(
              'drop',
              $.Event('drop', {delegatedEvent: e}),
              data
            ) !== false) {
            that._onAdd(e, data);
          }
        });
      }
    },

    _onDragOver: getDragHandler('dragover'),

    _onDragEnter: getDragHandler('dragenter'),

    _onDragLeave: getDragHandler('dragleave'),

    _initEventHandlers: function () {
      if (this._isXHRUpload(this.options)) {
        this._on(this.options.dropZone, {
          dragover: this._onDragOver,
          drop: this._onDrop,
          // event.preventDefault() on dragenter is required for IE10+:
          dragenter: this._onDragEnter,
          // dragleave is not required, but added for completeness:
          dragleave: this._onDragLeave
        });
        this._on(this.options.pasteZone, {
          paste: this._onPaste
        });
      }
      if ($.support.fileInput) {
        this._on(this.options.fileInput, {
          change: this._onChange
        });
      }
    },

    _destroyEventHandlers: function () {
      this._off(this.options.dropZone, 'dragenter dragleave dragover drop');
      this._off(this.options.pasteZone, 'paste');
      this._off(this.options.fileInput, 'change');
    },

    _setOption: function (key, value) {
      var reinit = $.inArray(key, this._specialOptions) !== -1;
      if (reinit) {
        this._destroyEventHandlers();
      }
      this._super(key, value);
      if (reinit) {
        this._initSpecialOptions();
        this._initEventHandlers();
      }
    },

    _initSpecialOptions: function () {
      var options = this.options;
      if (options.fileInput === undefined) {
        options.fileInput = this.element.is('input[type="file"]') ?
          this.element : this.element.find('input[type="file"]');
      } else if (!(options.fileInput instanceof $)) {
        options.fileInput = $(options.fileInput);
      }
      if (!(options.dropZone instanceof $)) {
        options.dropZone = $(options.dropZone);
      }
      if (!(options.pasteZone instanceof $)) {
        options.pasteZone = $(options.pasteZone);
      }
    },

    _getRegExp: function (str) {
      var parts = str.split('/'),
        modifiers = parts.pop();
      parts.shift();
      return new RegExp(parts.join('/'), modifiers);
    },

    _isRegExpOption: function (key, value) {
      return key !== 'url' && $.type(value) === 'string' &&
        /^\/.*\/[igm]{0,3}$/.test(value);
    },

    _initDataAttributes: function () {
      var that = this,
        options = this.options,
        data = this.element.data();
      // Initialize options set via HTML5 data-attributes:
      $.each(
        this.element[0].attributes,
        function (index, attr) {
          var key = attr.name.toLowerCase(),
            value;
          if (/^data-/.test(key)) {
            // Convert hyphen-ated key to camelCase:
            key = key.slice(5).replace(/-[a-z]/g, function (str) {
              return str.charAt(1).toUpperCase();
            });
            value = data[key];
            if (that._isRegExpOption(key, value)) {
              value = that._getRegExp(value);
            }
            options[key] = value;
          }
        }
      );
    },

    _create: function () {
      this._initDataAttributes();
      this._initSpecialOptions();
      this._slots = [];
      this._sequence = this._getXHRPromise(true);
      this._sending = this._active = 0;
      this._initProgressObject(this);
      this._initEventHandlers();
    },

    // This method is exposed to the widget API and allows to query
    // the number of active uploads:
    active: function () {
      return this._active;
    },

    // This method is exposed to the widget API and allows to query
    // the widget upload progress.
    // It returns an object with loaded, total and bitrate properties
    // for the running uploads:
    progress: function () {
      return this._progress;
    },

    // This method is exposed to the widget API and allows adding files
    // using the fileupload API. The data parameter accepts an object which
    // must have a files property and can contain additional options:
    // .fileupload('add', {files: filesList});
    add: function (data) {
      var that = this;
      if (!data || this.options.disabled) {
        return;
      }
      if (data.fileInput && !data.files) {
        this._getFileInputFiles(data.fileInput).always(function (files) {
          data.files = files;
          that._onAdd(null, data);
        });
      } else {
        data.files = $.makeArray(data.files);
        this._onAdd(null, data);
      }
    },

    // This method is exposed to the widget API and allows sending files
    // using the fileupload API. The data parameter accepts an object which
    // must have a files or fileInput property and can contain additional options:
    // .fileupload('send', {files: filesList});
    // The method returns a Promise object for the file upload call.
    send: function (data) {
      if (data && !this.options.disabled) {
        if (data.fileInput && !data.files) {
          var that = this,
            dfd = $.Deferred(),
            promise = dfd.promise(),
            jqXHR,
            aborted;
          promise.abort = function () {
            aborted = true;
            if (jqXHR) {
              return jqXHR.abort();
            }
            dfd.reject(null, 'abort', 'abort');
            return promise;
          };
          this._getFileInputFiles(data.fileInput).always(
            function (files) {
              if (aborted) {
                return;
              }
              if (!files.length) {
                dfd.reject();
                return;
              }
              data.files = files;
              jqXHR = that._onSend(null, data);
              jqXHR.then(
                function (result, textStatus, jqXHR) {
                  dfd.resolve(result, textStatus, jqXHR);
                },
                function (jqXHR, textStatus, errorThrown) {
                  dfd.reject(jqXHR, textStatus, errorThrown);
                }
              );
            }
          );
          return this._enhancePromise(promise);
        }
        data.files = $.makeArray(data.files);
        if (data.files.length) {
          return this._onSend(null, data);
        }
      }
      return this._getXHRPromise(false, data && data.context);
    }

  });

}));

/*global self, document, DOMException */

/*! @source http://purl.eligrey.com/github/classList.js/blob/master/classList.js */

// Full polyfill for browsers with no classList support
if (!("classList" in document.createElement("_"))) {
  (function (view) {

    "use strict";

    if (!('Element' in view)) return;

    var
      classListProp = "classList"
      , protoProp = "prototype"
      , elemCtrProto = view.Element[protoProp]
      , objCtr = Object
      , strTrim = String[protoProp].trim || function () {
        return this.replace(/^\s+|\s+$/g, "");
      }
      , arrIndexOf = Array[protoProp].indexOf || function (item) {
        var
          i = 0
          , len = this.length
        ;
        for (; i < len; i++) {
          if (i in this && this[i] === item) {
            return i;
          }
        }
        return -1;
      }
      // Vendors: please allow content code to instantiate DOMExceptions
      , DOMEx = function (type, message) {
        this.name = type;
        this.code = DOMException[type];
        this.message = message;
      }
      , checkTokenAndGetIndex = function (classList, token) {
        if (token === "") {
          throw new DOMEx(
            "SYNTAX_ERR"
            , "An invalid or illegal string was specified"
          );
        }
        if (/\s/.test(token)) {
          throw new DOMEx(
            "INVALID_CHARACTER_ERR"
            , "String contains an invalid character"
          );
        }
        return arrIndexOf.call(classList, token);
      }
      , ClassList = function (elem) {
        var
          trimmedClasses = strTrim.call(elem.getAttribute("class") || "")
          , classes = trimmedClasses ? trimmedClasses.split(/\s+/) : []
          , i = 0
          , len = classes.length
        ;
        for (; i < len; i++) {
          this.push(classes[i]);
        }
        this._updateClassName = function () {
          elem.setAttribute("class", this.toString());
        };
      }
      , classListProto = ClassList[protoProp] = []
      , classListGetter = function () {
        return new ClassList(this);
      }
    ;
    // Most DOMException implementations don't allow calling DOMException's toString()
    // on non-DOMExceptions. Error's toString() is sufficient here.
    DOMEx[protoProp] = Error[protoProp];
    classListProto.item = function (i) {
      return this[i] || null;
    };
    classListProto.contains = function (token) {
      token += "";
      return checkTokenAndGetIndex(this, token) !== -1;
    };
    classListProto.add = function () {
      var
        tokens = arguments
        , i = 0
        , l = tokens.length
        , token
        , updated = false
      ;
      do {
        token = tokens[i] + "";
        if (checkTokenAndGetIndex(this, token) === -1) {
          this.push(token);
          updated = true;
        }
      }
      while (++i < l);

      if (updated) {
        this._updateClassName();
      }
    };
    classListProto.remove = function () {
      var
        tokens = arguments
        , i = 0
        , l = tokens.length
        , token
        , updated = false
        , index
      ;
      do {
        token = tokens[i] + "";
        index = checkTokenAndGetIndex(this, token);
        while (index !== -1) {
          this.splice(index, 1);
          updated = true;
          index = checkTokenAndGetIndex(this, token);
        }
      }
      while (++i < l);

      if (updated) {
        this._updateClassName();
      }
    };
    classListProto.toggle = function (token, force) {
      token += "";

      var
        result = this.contains(token)
        , method = result ?
        force !== true && "remove"
        :
        force !== false && "add"
      ;

      if (method) {
        this[method](token);
      }

      if (force === true || force === false) {
        return force;
      } else {
        return !result;
      }
    };
    classListProto.toString = function () {
      return this.join(" ");
    };

    if (objCtr.defineProperty) {
      var classListPropDesc = {
        get: classListGetter
        , enumerable: true
        , configurable: true
      };
      try {
        objCtr.defineProperty(elemCtrProto, classListProp, classListPropDesc);
      } catch (ex) { // IE 8 doesn't support enumerable:true
        if (ex.number === -0x7FF5EC54) {
          classListPropDesc.enumerable = false;
          objCtr.defineProperty(elemCtrProto, classListProp, classListPropDesc);
        }
      }
    } else if (objCtr[protoProp].__defineGetter__) {
      elemCtrProto.__defineGetter__(classListProp, classListGetter);
    }

  }(self));
}

/* Blob.js
 * A Blob implementation.
 * 2014-07-24
 *
 * By Eli Grey, http://eligrey.com
 * By Devin Samarin, https://github.com/dsamarin
 * License: X11/MIT
 *   See https://github.com/eligrey/Blob.js/blob/master/LICENSE.md
 */

/*global self, unescape */
/*jslint bitwise: true, regexp: true, confusion: true, es5: true, vars: true, white: true,
  plusplus: true */

/*! @source http://purl.eligrey.com/github/Blob.js/blob/master/Blob.js */

(function (view) {
  "use strict";

  view.URL = view.URL || view.webkitURL;

  if (view.Blob && view.URL) {
    try {
      new Blob;
      return;
    } catch (e) {}
  }

  // Internally we use a BlobBuilder implementation to base Blob off of
  // in order to support older browsers that only have BlobBuilder
  var BlobBuilder = view.BlobBuilder || view.WebKitBlobBuilder || view.MozBlobBuilder || (function(view) {
    var
      get_class = function(object) {
        return Object.prototype.toString.call(object).match(/^\[object\s(.*)\]$/)[1];
      }
      , FakeBlobBuilder = function BlobBuilder() {
        this.data = [];
      }
      , FakeBlob = function Blob(data, type, encoding) {
        this.data = data;
        this.size = data.length;
        this.type = type;
        this.encoding = encoding;
      }
      , FBB_proto = FakeBlobBuilder.prototype
      , FB_proto = FakeBlob.prototype
      , FileReaderSync = view.FileReaderSync
      , FileException = function(type) {
        this.code = this[this.name = type];
      }
      , file_ex_codes = (
        "NOT_FOUND_ERR SECURITY_ERR ABORT_ERR NOT_READABLE_ERR ENCODING_ERR "
        + "NO_MODIFICATION_ALLOWED_ERR INVALID_STATE_ERR SYNTAX_ERR"
      ).split(" ")
      , file_ex_code = file_ex_codes.length
      , real_URL = view.URL || view.webkitURL || view
      , real_create_object_URL = real_URL.createObjectURL
      , real_revoke_object_URL = real_URL.revokeObjectURL
      , URL = real_URL
      , btoa = view.btoa
      , atob = view.atob

      , ArrayBuffer = view.ArrayBuffer
      , Uint8Array = view.Uint8Array

      , origin = /^[\w-]+:\/*\[?[\w\.:-]+\]?(?::[0-9]+)?/
    ;
    FakeBlob.fake = FB_proto.fake = true;
    while (file_ex_code--) {
      FileException.prototype[file_ex_codes[file_ex_code]] = file_ex_code + 1;
    }
    // Polyfill URL
    if (!real_URL.createObjectURL) {
      URL = view.URL = function(uri) {
        var
          uri_info = document.createElementNS("http://www.w3.org/1999/xhtml", "a")
          , uri_origin
        ;
        uri_info.href = uri;
        if (!("origin" in uri_info)) {
          if (uri_info.protocol.toLowerCase() === "data:") {
            uri_info.origin = null;
          } else {
            uri_origin = uri.match(origin);
            uri_info.origin = uri_origin && uri_origin[1];
          }
        }
        return uri_info;
      };
    }
    URL.createObjectURL = function(blob) {
      var
        type = blob.type
        , data_URI_header
      ;
      if (type === null) {
        type = "application/octet-stream";
      }
      if (blob instanceof FakeBlob) {
        data_URI_header = "data:" + type;
        if (blob.encoding === "base64") {
          return data_URI_header + ";base64," + blob.data;
        } else if (blob.encoding === "URI") {
          return data_URI_header + "," + decodeURIComponent(blob.data);
        } if (btoa) {
          return data_URI_header + ";base64," + btoa(blob.data);
        } else {
          return data_URI_header + "," + encodeURIComponent(blob.data);
        }
      } else if (real_create_object_URL) {
        return real_create_object_URL.call(real_URL, blob);
      }
    };
    URL.revokeObjectURL = function(object_URL) {
      if (object_URL.substring(0, 5) !== "data:" && real_revoke_object_URL) {
        real_revoke_object_URL.call(real_URL, object_URL);
      }
    };
    FBB_proto.append = function(data/*, endings*/) {
      var bb = this.data;
      // decode data to a binary string
      if (Uint8Array && (data instanceof ArrayBuffer || data instanceof Uint8Array)) {
        var
          str = ""
          , buf = new Uint8Array(data)
          , i = 0
          , buf_len = buf.length
        ;
        for (; i < buf_len; i++) {
          str += String.fromCharCode(buf[i]);
        }
        bb.push(str);
      } else if (get_class(data) === "Blob" || get_class(data) === "File") {
        if (FileReaderSync) {
          var fr = new FileReaderSync;
          bb.push(fr.readAsBinaryString(data));
        } else {
          // async FileReader won't work as BlobBuilder is sync
          throw new FileException("NOT_READABLE_ERR");
        }
      } else if (data instanceof FakeBlob) {
        if (data.encoding === "base64" && atob) {
          bb.push(atob(data.data));
        } else if (data.encoding === "URI") {
          bb.push(decodeURIComponent(data.data));
        } else if (data.encoding === "raw") {
          bb.push(data.data);
        }
      } else {
        if (typeof data !== "string") {
          data += ""; // convert unsupported types to strings
        }
        // decode UTF-16 to binary string
        bb.push(unescape(encodeURIComponent(data)));
      }
    };
    FBB_proto.getBlob = function(type) {
      if (!arguments.length) {
        type = null;
      }
      return new FakeBlob(this.data.join(""), type, "raw");
    };
    FBB_proto.toString = function() {
      return "[object BlobBuilder]";
    };
    FB_proto.slice = function(start, end, type) {
      var args = arguments.length;
      if (args < 3) {
        type = null;
      }
      return new FakeBlob(
        this.data.slice(start, args > 1 ? end : this.data.length)
        , type
        , this.encoding
      );
    };
    FB_proto.toString = function() {
      return "[object Blob]";
    };
    FB_proto.close = function() {
      this.size = 0;
      delete this.data;
    };
    return FakeBlobBuilder;
  }(view));

  view.Blob = function(blobParts, options) {
    var type = options ? (options.type || "") : "";
    var builder = new BlobBuilder();
    if (blobParts) {
      for (var i = 0, len = blobParts.length; i < len; i++) {
        if (Uint8Array && blobParts[i] instanceof Uint8Array) {
          builder.append(blobParts[i].buffer);
        }
        else {
          builder.append(blobParts[i]);
        }
      }
    }
    var blob = builder.getBlob(type);
    if (!blob.slice && blob.webkitSlice) {
      blob.slice = blob.webkitSlice;
    }
    return blob;
  };

  var getPrototypeOf = Object.getPrototypeOf || function(object) {
    return object.__proto__;
  };
  view.Blob.prototype = getPrototypeOf(new view.Blob());
}(typeof self !== "undefined" && self || typeof window !== "undefined" && window || this.content || this));

(function (root, factory) {
  'use strict';
  if (typeof module === 'object') {
    module.exports = factory;
  } else if (typeof define === 'function' && define.amd) {
    define(function () {
      return factory;
    });
  } else {
    root.MediumEditor = factory;
  }
}(this, function () {

  'use strict';

  function MediumEditor(elements, options) {
    'use strict';
    return this.init(elements, options);
  }

  MediumEditor.extensions = {};
  /*jshint unused: true */
  (function (window) {
    'use strict';

    function copyInto(overwrite, dest) {
      var prop,
        sources = Array.prototype.slice.call(arguments, 2);
      dest = dest || {};
      for (var i = 0; i < sources.length; i++) {
        var source = sources[i];
        if (source) {
          for (prop in source) {
            if (source.hasOwnProperty(prop) &&
              typeof source[prop] !== 'undefined' &&
              (overwrite || dest.hasOwnProperty(prop) === false)) {
              dest[prop] = source[prop];
            }
          }
        }
      }
      return dest;
    }

    // https://developer.mozilla.org/en-US/docs/Web/API/Node/contains
    // Some browsers (including phantom) don't return true for Node.contains(child)
    // if child is a text node.  Detect these cases here and use a fallback
    // for calls to Util.isDescendant()
    var nodeContainsWorksWithTextNodes = false;
    try {
      var testParent = document.createElement('div'),
        testText = document.createTextNode(' ');
      testParent.appendChild(testText);
      nodeContainsWorksWithTextNodes = testParent.contains(testText);
    } catch (exc) {}

    var Util = {

      // http://stackoverflow.com/questions/17907445/how-to-detect-ie11#comment30165888_17907562
      // by rg89
      isIE: ((navigator.appName === 'Microsoft Internet Explorer') || ((navigator.appName === 'Netscape') && (new RegExp('Trident/.*rv:([0-9]{1,}[.0-9]{0,})').exec(navigator.userAgent) !== null))),

      // http://stackoverflow.com/a/11752084/569101
      isMac: (window.navigator.platform.toUpperCase().indexOf('MAC') >= 0),

      // https://github.com/jashkenas/underscore
      keyCode: {
        BACKSPACE: 8,
        TAB: 9,
        ENTER: 13,
        ESCAPE: 27,
        SPACE: 32,
        DELETE: 46,
        K: 75, // K keycode, and not k
        M: 77
      },

      /**
       * Returns true if it's metaKey on Mac, or ctrlKey on non-Mac.
       * See #591
       */
      isMetaCtrlKey: function (event) {
        if ((Util.isMac && event.metaKey) || (!Util.isMac && event.ctrlKey)) {
          return true;
        }

        return false;
      },

      /**
       * Returns true if the key associated to the event is inside keys array
       *
       * @see : https://github.com/jquery/jquery/blob/0705be475092aede1eddae01319ec931fb9c65fc/src/event.js#L473-L484
       * @see : http://stackoverflow.com/q/4471582/569101
       */
      isKey: function (event, keys) {
        var keyCode = Util.getKeyCode(event);

        // it's not an array let's just compare strings!
        if (false === Array.isArray(keys)) {
          return keyCode === keys;
        }

        if (-1 === keys.indexOf(keyCode)) {
          return false;
        }

        return true;
      },

      getKeyCode: function (event) {
        var keyCode = event.which;

        // getting the key code from event
        if (null === keyCode) {
          keyCode = event.charCode !== null ? event.charCode : event.keyCode;
        }

        return keyCode;
      },

      blockContainerElementNames: [
        // elements our editor generates
        'p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'blockquote', 'pre', 'ul', 'li', 'ol',
        // all other known block elements
        'address', 'article', 'aside', 'audio', 'canvas', 'dd', 'dl', 'dt', 'fieldset',
        'figcaption', 'figure', 'footer', 'form', 'header', 'hgroup', 'main', 'nav',
        'noscript', 'output', 'section', 'video',
        'table', 'thead', 'tbody', 'tfoot', 'tr', 'th', 'td'
      ],

      emptyElementNames: ['br', 'col', 'colgroup', 'hr', 'img', 'input', 'source', 'wbr'],

      extend: function extend(/* dest, source1, source2, ...*/) {
        var args = [true].concat(Array.prototype.slice.call(arguments));
        return copyInto.apply(this, args);
      },

      defaults: function defaults(/*dest, source1, source2, ...*/) {
        var args = [false].concat(Array.prototype.slice.call(arguments));
        return copyInto.apply(this, args);
      },

      /*
         * Create a link around the provided text nodes which must be adjacent to each other and all be
         * descendants of the same closest block container. If the preconditions are not met, unexpected
         * behavior will result.
         */
      createLink: function (document, textNodes, href, target) {
        var anchor = document.createElement('a');
        Util.moveTextRangeIntoElement(textNodes[0], textNodes[textNodes.length - 1], anchor);
        anchor.setAttribute('href', href);
        if (target) {
          anchor.setAttribute('target', target);
        }
        return anchor;
      },

      /*
         * Given the provided match in the format {start: 1, end: 2} where start and end are indices into the
         * textContent of the provided element argument, modify the DOM inside element to ensure that the text
         * identified by the provided match can be returned as text nodes that contain exactly that text, without
         * any additional text at the beginning or end of the returned array of adjacent text nodes.
         *
         * The only DOM manipulation performed by this function is splitting the text nodes, non-text nodes are
         * not affected in any way.
         */
      findOrCreateMatchingTextNodes: function (document, element, match) {
        var treeWalker = document.createTreeWalker(element, NodeFilter.SHOW_TEXT, null, false),
          matchedNodes = [],
          currentTextIndex = 0,
          startReached = false,
          currentNode = null,
          newNode = null;

        while ((currentNode = treeWalker.nextNode()) !== null) {
          if (!startReached && match.start < (currentTextIndex + currentNode.nodeValue.length)) {
            startReached = true;
            newNode = Util.splitStartNodeIfNeeded(currentNode, match.start, currentTextIndex);
          }
          if (startReached) {
            Util.splitEndNodeIfNeeded(currentNode, newNode, match.end, currentTextIndex);
          }
          if (startReached && currentTextIndex === match.end) {
            break; // Found the node(s) corresponding to the link. Break out and move on to the next.
          } else if (startReached && currentTextIndex > (match.end + 1)) {
            throw new Error('PerformLinking overshot the target!'); // should never happen...
          }

          if (startReached) {
            matchedNodes.push(newNode || currentNode);
          }

          currentTextIndex += currentNode.nodeValue.length;
          if (newNode !== null) {
            currentTextIndex += newNode.nodeValue.length;
            // Skip the newNode as we'll already have pushed it to the matches
            treeWalker.nextNode();
          }
          newNode = null;
        }
        return matchedNodes;
      },

      /*
         * Given the provided text node and text coordinates, split the text node if needed to make it align
         * precisely with the coordinates.
         *
         * This function is intended to be called from Util.findOrCreateMatchingTextNodes.
         */
      splitStartNodeIfNeeded: function (currentNode, matchStartIndex, currentTextIndex) {
        if (matchStartIndex !== currentTextIndex) {
          return currentNode.splitText(matchStartIndex - currentTextIndex);
        }
        return null;
      },

      /*
         * Given the provided text node and text coordinates, split the text node if needed to make it align
         * precisely with the coordinates. The newNode argument should from the result of Util.splitStartNodeIfNeeded,
         * if that function has been called on the same currentNode.
         *
         * This function is intended to be called from Util.findOrCreateMatchingTextNodes.
         */
      splitEndNodeIfNeeded: function (currentNode, newNode, matchEndIndex, currentTextIndex) {
        var textIndexOfEndOfFarthestNode,
          endSplitPoint;
        textIndexOfEndOfFarthestNode = currentTextIndex + (newNode || currentNode).nodeValue.length +
          (newNode ? currentNode.nodeValue.length : 0) -
          1;
        endSplitPoint = (newNode || currentNode).nodeValue.length -
          (textIndexOfEndOfFarthestNode + 1 - matchEndIndex);
        if (textIndexOfEndOfFarthestNode >= matchEndIndex &&
          currentTextIndex !== textIndexOfEndOfFarthestNode &&
          endSplitPoint !== 0) {
          (newNode || currentNode).splitText(endSplitPoint);
        }
      },

      /*
        * Take an element, and break up all of its text content into unique pieces such that:
         * 1) All text content of the elements are in separate blocks. No piece of text content should span
         *    across multiple blocks. This means no element return by this function should have
         *    any blocks as children.
         * 2) The union of the textcontent of all of the elements returned here covers all
         *    of the text within the element.
         *
         *
         * EXAMPLE:
         * In the event that we have something like:
         *
         * <blockquote>
         *   <p>Some Text</p>
         *   <ol>
         *     <li>List Item 1</li>
         *     <li>List Item 2</li>
         *   </ol>
         * </blockquote>
         *
         * This function would return these elements as an array:
         *   [ <p>Some Text</p>, <li>List Item 1</li>, <li>List Item 2</li> ]
         *
         * Since the <blockquote> and <ol> elements contain blocks within them they are not returned.
         * Since the <p> and <li>'s don't contain block elements and cover all the text content of the
         * <blockquote> container, they are the elements returned.
         */
      splitByBlockElements: function (element) {
        var toRet = [],
          blockElementQuery = MediumEditor.util.blockContainerElementNames.join(',');

        if (element.nodeType === 3 || element.querySelectorAll(blockElementQuery).length === 0) {
          return [element];
        }

        for (var i = 0; i < element.childNodes.length; i++) {
          var child = element.childNodes[i];
          if (child.nodeType === 3) {
            toRet.push(child);
          } else {
            var blockElements = child.querySelectorAll(blockElementQuery);
            if (blockElements.length === 0) {
              toRet.push(child);
            } else {
              toRet = toRet.concat(MediumEditor.util.splitByBlockElements(child));
            }
          }
        }

        return toRet;
      },

      // Find the next node in the DOM tree that represents any text that is being
      // displayed directly next to the targetNode (passed as an argument)
      // Text that appears directly next to the current node can be:
      //  - A sibling text node
      //  - A descendant of a sibling element
      //  - A sibling text node of an ancestor
      //  - A descendant of a sibling element of an ancestor
      findAdjacentTextNodeWithContent: function findAdjacentTextNodeWithContent(rootNode, targetNode, ownerDocument) {
        var pastTarget = false,
          nextNode,
          nodeIterator = ownerDocument.createNodeIterator(rootNode, NodeFilter.SHOW_TEXT, null, false);

        // Use a native NodeIterator to iterate over all the text nodes that are descendants
        // of the rootNode.  Once past the targetNode, choose the first non-empty text node
        nextNode = nodeIterator.nextNode();
        while (nextNode) {
          if (nextNode === targetNode) {
            pastTarget = true;
          } else if (pastTarget) {
            if (nextNode.nodeType === 3 && nextNode.nodeValue && nextNode.nodeValue.trim().length > 0) {
              break;
            }
          }
          nextNode = nodeIterator.nextNode();
        }

        return nextNode;
      },

      isDescendant: function isDescendant(parent, child, checkEquality) {
        if (!parent || !child) {
          return false;
        }
        if (parent === child) {
          return !!checkEquality;
        }
        // If parent is not an element, it can't have any descendants
        if (parent.nodeType !== 1) {
          return false;
        }
        if (nodeContainsWorksWithTextNodes || child.nodeType !== 3) {
          return parent.contains(child);
        }
        var node = child.parentNode;
        while (node !== null) {
          if (node === parent) {
            return true;
          }
          node = node.parentNode;
        }
        return false;
      },

      // https://github.com/jashkenas/underscore
      isElement: function isElement(obj) {
        return !!(obj && obj.nodeType === 1);
      },

      // https://github.com/jashkenas/underscore
      throttle: function (func, wait) {
        var THROTTLE_INTERVAL = 50,
          context,
          args,
          result,
          timeout = null,
          previous = 0,
          later = function () {
            previous = Date.now();
            timeout = null;
            result = func.apply(context, args);
            if (!timeout) {
              context = args = null;
            }
          };

        if (!wait && wait !== 0) {
          wait = THROTTLE_INTERVAL;
        }

        return function () {
          var now = Date.now(),
            remaining = wait - (now - previous);

          context = this;
          args = arguments;
          if (remaining <= 0 || remaining > wait) {
            if (timeout) {
              clearTimeout(timeout);
              timeout = null;
            }
            previous = now;
            result = func.apply(context, args);
            if (!timeout) {
              context = args = null;
            }
          } else if (!timeout) {
            timeout = setTimeout(later, remaining);
          }
          return result;
        };
      },

      traverseUp: function (current, testElementFunction) {
        if (!current) {
          return false;
        }

        do {
          if (current.nodeType === 1) {
            if (testElementFunction(current)) {
              return current;
            }
            // do not traverse upwards past the nearest containing editor
            if (Util.isMediumEditorElement(current)) {
              return false;
            }
          }

          current = current.parentNode;
        } while (current);

        return false;
      },

      htmlEntities: function (str) {
        // converts special characters (like <) into their escaped/encoded values (like &lt;).
        // This allows you to show to display the string without the browser reading it as HTML.
        return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
      },

      // http://stackoverflow.com/questions/6690752/insert-html-at-caret-in-a-contenteditable-div
      insertHTMLCommand: function (doc, html) {
        var selection, range, el, fragment, node, lastNode, toReplace;

        if (doc.queryCommandSupported('insertHTML')) {
          try {
            return doc.execCommand('insertHTML', false, html);
          } catch (ignore) {}
        }

        selection = doc.getSelection();
        if (selection.rangeCount) {
          range = selection.getRangeAt(0);
          toReplace = range.commonAncestorContainer;

          // https://github.com/yabwe/medium-editor/issues/748
          // If the selection is an empty editor element, create a temporary text node inside of the editor
          // and select it so that we don't delete the editor element
          if (Util.isMediumEditorElement(toReplace) && !toReplace.firstChild) {
            range.selectNode(toReplace.appendChild(doc.createTextNode('')));
          } else if ((toReplace.nodeType === 3 && range.startOffset === 0 && range.endOffset === toReplace.nodeValue.length) ||
            (toReplace.nodeType !== 3 && toReplace.innerHTML === range.toString())) {
            // Ensure range covers maximum amount of nodes as possible
            // By moving up the DOM and selecting ancestors whose only child is the range
            while (!Util.isMediumEditorElement(toReplace) &&
            toReplace.parentNode &&
            toReplace.parentNode.childNodes.length === 1 &&
            !Util.isMediumEditorElement(toReplace.parentNode)) {
              toReplace = toReplace.parentNode;
            }
            range.selectNode(toReplace);
          }
          range.deleteContents();

          el = doc.createElement('div');
          el.innerHTML = html;
          fragment = doc.createDocumentFragment();
          while (el.firstChild) {
            node = el.firstChild;
            lastNode = fragment.appendChild(node);
          }
          range.insertNode(fragment);

          // Preserve the selection:
          if (lastNode) {
            range = range.cloneRange();
            range.setStartAfter(lastNode);
            range.collapse(true);
            selection.removeAllRanges();
            selection.addRange(range);
          }
        }
      },

      execFormatBlock: function (doc, tagName) {
        // Get the top level block element that contains the selection
        var blockContainer = Util.getTopBlockContainer(MediumEditor.selection.getSelectionStart(doc));

        // Special handling for blockquote
        if (tagName === 'blockquote') {
          if (blockContainer) {
            var childNodes = Array.prototype.slice.call(blockContainer.childNodes);
            // Check if the blockquote has a block element as a child (nested blocks)
            if (childNodes.some(function (childNode) {
                return Util.isBlockContainer(childNode);
              })) {
              // FF handles blockquote differently on formatBlock
              // allowing nesting, we need to use outdent
              // https://developer.mozilla.org/en-US/docs/Rich-Text_Editing_in_Mozilla
              return doc.execCommand('outdent', false, null);
            }
          }

          // When IE blockquote needs to be called as indent
          // http://stackoverflow.com/questions/1816223/rich-text-editor-with-blockquote-function/1821777#1821777
          if (Util.isIE) {
            return doc.execCommand('indent', false, tagName);
          }
        }

        // If the blockContainer is already the element type being passed in
        // treat it as 'undo' formatting and just convert it to a <p>
        if (blockContainer && tagName === blockContainer.nodeName.toLowerCase()) {
          tagName = 'p';
        }

        // When IE we need to add <> to heading elements
        // http://stackoverflow.com/questions/10741831/execcommand-formatblock-headings-in-ie
        if (Util.isIE) {
          tagName = '<' + tagName + '>';
        }
        return doc.execCommand('formatBlock', false, tagName);
      },

      /**
       * Set target to blank on the given el element
       *
       * TODO: not sure if this should be here
       *
       * When creating a link (using core -> createLink) the selection returned by Firefox will be the parent of the created link
       * instead of the created link itself (as it is for Chrome for example), so we retrieve all "a" children to grab the good one by
       * using `anchorUrl` to ensure that we are adding target="_blank" on the good one.
       * This isn't a bulletproof solution anyway ..
       */
      setTargetBlank: function (el, anchorUrl) {
        var i, url = anchorUrl || false;
        if (el.nodeName.toLowerCase() === 'a') {
          el.target = '_blank';
        } else {
          el = el.getElementsByTagName('a');

          for (i = 0; i < el.length; i += 1) {
            if (false === url || url === el[i].attributes.href.value) {
              el[i].target = '_blank';
            }
          }
        }
      },

      addClassToAnchors: function (el, buttonClass) {
        var classes = buttonClass.split(' '),
          i,
          j;
        if (el.nodeName.toLowerCase() === 'a') {
          for (j = 0; j < classes.length; j += 1) {
            el.classList.add(classes[j]);
          }
        } else {
          el = el.getElementsByTagName('a');
          for (i = 0; i < el.length; i += 1) {
            for (j = 0; j < classes.length; j += 1) {
              el[i].classList.add(classes[j]);
            }
          }
        }
      },

      isListItem: function (node) {
        if (!node) {
          return false;
        }
        if (node.nodeName.toLowerCase() === 'li') {
          return true;
        }

        var parentNode = node.parentNode,
          tagName = parentNode.nodeName.toLowerCase();
        while (tagName === 'li' || (!Util.isBlockContainer(parentNode) && tagName !== 'div')) {
          if (tagName === 'li') {
            return true;
          }
          parentNode = parentNode.parentNode;
          if (parentNode) {
            tagName = parentNode.nodeName.toLowerCase();
          } else {
            return false;
          }
        }
        return false;
      },

      cleanListDOM: function (ownerDocument, element) {
        if (element.nodeName.toLowerCase() !== 'li') {
          return;
        }

        var list = element.parentElement;

        if (list.parentElement.nodeName.toLowerCase() === 'p') { // yes we need to clean up
          Util.unwrap(list.parentElement, ownerDocument);

          // move cursor at the end of the text inside the list
          // for some unknown reason, the cursor is moved to end of the "visual" line
          MediumEditor.selection.moveCursor(ownerDocument, element.firstChild, element.firstChild.textContent.length);
        }
      },

      /* splitDOMTree
         *
         * Given a root element some descendant element, split the root element
         * into its own element containing the descendant element and all elements
         * on the left or right side of the descendant ('right' is default)
         *
         * example:
         *
         *         <div>
         *      /    |   \
         *  <span> <span> <span>
         *   / \    / \    / \
         *  1   2  3   4  5   6
         *
         *  If I wanted to split this tree given the <div> as the root and "4" as the leaf
         *  the result would be (the prime ' marks indicates nodes that are created as clones):
         *
         *   SPLITTING OFF 'RIGHT' TREE       SPLITTING OFF 'LEFT' TREE
         *
         *     <div>            <div>'              <div>'      <div>
         *      / \              / \                 / \          |
         * <span> <span>   <span>' <span>       <span> <span>   <span>
         *   / \    |        |      / \           /\     /\       /\
         *  1   2   3        4     5   6         1  2   3  4     5  6
         *
         *  The above example represents splitting off the 'right' or 'left' part of a tree, where
         *  the <div>' would be returned as an element not appended to the DOM, and the <div>
         *  would remain in place where it was
         *
        */
      splitOffDOMTree: function (rootNode, leafNode, splitLeft) {
        var splitOnNode = leafNode,
          createdNode = null,
          splitRight = !splitLeft;

        // loop until we hit the root
        while (splitOnNode !== rootNode) {
          var currParent = splitOnNode.parentNode,
            newParent = currParent.cloneNode(false),
            targetNode = (splitRight ? splitOnNode : currParent.firstChild),
            appendLast;

          // Create a new parent element which is a clone of the current parent
          if (createdNode) {
            if (splitRight) {
              // If we're splitting right, add previous created element before siblings
              newParent.appendChild(createdNode);
            } else {
              // If we're splitting left, add previous created element last
              appendLast = createdNode;
            }
          }
          createdNode = newParent;

          while (targetNode) {
            var sibling = targetNode.nextSibling;
            // Special handling for the 'splitNode'
            if (targetNode === splitOnNode) {
              if (!targetNode.hasChildNodes()) {
                targetNode.parentNode.removeChild(targetNode);
              } else {
                // For the node we're splitting on, if it has children, we need to clone it
                // and not just move it
                targetNode = targetNode.cloneNode(false);
              }
              // If the resulting split node has content, add it
              if (targetNode.textContent) {
                createdNode.appendChild(targetNode);
              }

              targetNode = (splitRight ? sibling : null);
            } else {
              // For general case, just remove the element and only
              // add it to the split tree if it contains something
              targetNode.parentNode.removeChild(targetNode);
              if (targetNode.hasChildNodes() || targetNode.textContent) {
                createdNode.appendChild(targetNode);
              }

              targetNode = sibling;
            }
          }

          // If we had an element we wanted to append at the end, do that now
          if (appendLast) {
            createdNode.appendChild(appendLast);
          }

          splitOnNode = currParent;
        }

        return createdNode;
      },

      moveTextRangeIntoElement: function (startNode, endNode, newElement) {
        if (!startNode || !endNode) {
          return false;
        }

        var rootNode = Util.findCommonRoot(startNode, endNode);
        if (!rootNode) {
          return false;
        }

        if (endNode === startNode) {
          var temp = startNode.parentNode,
            sibling = startNode.nextSibling;
          temp.removeChild(startNode);
          newElement.appendChild(startNode);
          if (sibling) {
            temp.insertBefore(newElement, sibling);
          } else {
            temp.appendChild(newElement);
          }
          return newElement.hasChildNodes();
        }

        // create rootChildren array which includes all the children
        // we care about
        var rootChildren = [],
          firstChild,
          lastChild,
          nextNode;
        for (var i = 0; i < rootNode.childNodes.length; i++) {
          nextNode = rootNode.childNodes[i];
          if (!firstChild) {
            if (Util.isDescendant(nextNode, startNode, true)) {
              firstChild = nextNode;
            }
          } else {
            if (Util.isDescendant(nextNode, endNode, true)) {
              lastChild = nextNode;
              break;
            } else {
              rootChildren.push(nextNode);
            }
          }
        }

        var afterLast = lastChild.nextSibling,
          fragment = rootNode.ownerDocument.createDocumentFragment();

        // build up fragment on startNode side of tree
        if (firstChild === startNode) {
          firstChild.parentNode.removeChild(firstChild);
          fragment.appendChild(firstChild);
        } else {
          fragment.appendChild(Util.splitOffDOMTree(firstChild, startNode));
        }

        // add any elements between firstChild & lastChild
        rootChildren.forEach(function (element) {
          element.parentNode.removeChild(element);
          fragment.appendChild(element);
        });

        // build up fragment on endNode side of the tree
        if (lastChild === endNode) {
          lastChild.parentNode.removeChild(lastChild);
          fragment.appendChild(lastChild);
        } else {
          fragment.appendChild(Util.splitOffDOMTree(lastChild, endNode, true));
        }

        // Add fragment into passed in element
        newElement.appendChild(fragment);

        if (lastChild.parentNode === rootNode) {
          // If last child is in the root, insert newElement in front of it
          rootNode.insertBefore(newElement, lastChild);
        } else if (afterLast) {
          // If last child was removed, but it had a sibling, insert in front of it
          rootNode.insertBefore(newElement, afterLast);
        } else {
          // lastChild was removed and was the last actual element just append
          rootNode.appendChild(newElement);
        }

        return newElement.hasChildNodes();
      },

      /* based on http://stackoverflow.com/a/6183069 */
      depthOfNode: function (inNode) {
        var theDepth = 0,
          node = inNode;
        while (node.parentNode !== null) {
          node = node.parentNode;
          theDepth++;
        }
        return theDepth;
      },

      findCommonRoot: function (inNode1, inNode2) {
        var depth1 = Util.depthOfNode(inNode1),
          depth2 = Util.depthOfNode(inNode2),
          node1 = inNode1,
          node2 = inNode2;

        while (depth1 !== depth2) {
          if (depth1 > depth2) {
            node1 = node1.parentNode;
            depth1 -= 1;
          } else {
            node2 = node2.parentNode;
            depth2 -= 1;
          }
        }

        while (node1 !== node2) {
          node1 = node1.parentNode;
          node2 = node2.parentNode;
        }

        return node1;
      },
      /* END - based on http://stackoverflow.com/a/6183069 */

      isElementAtBeginningOfBlock: function (node) {
        var textVal,
          sibling;
        while (!Util.isBlockContainer(node) && !Util.isMediumEditorElement(node)) {
          sibling = node;
          while (sibling = sibling.previousSibling) {
            textVal = sibling.nodeType === 3 ? sibling.nodeValue : sibling.textContent;
            if (textVal.length > 0) {
              return false;
            }
          }
          node = node.parentNode;
        }
        return true;
      },

      isMediumEditorElement: function (element) {
        return element && element.getAttribute && !!element.getAttribute('data-medium-editor-element');
      },

      getContainerEditorElement: function (element) {
        return Util.traverseUp(element, function (node) {
          return Util.isMediumEditorElement(node);
        });
      },

      isBlockContainer: function (element) {
        return element && element.nodeType !== 3 && Util.blockContainerElementNames.indexOf(element.nodeName.toLowerCase()) !== -1;
      },

      getClosestBlockContainer: function (node) {
        return Util.traverseUp(node, function (node) {
          return Util.isBlockContainer(node);
        });
      },

      getTopBlockContainer: function (element) {
        var topBlock = element;
        Util.traverseUp(element, function (el) {
          if (Util.isBlockContainer(el)) {
            topBlock = el;
          }
          return false;
        });
        return topBlock;
      },

      getFirstSelectableLeafNode: function (element) {
        while (element && element.firstChild) {
          element = element.firstChild;
        }

        // We don't want to set the selection to an element that can't have children, this messes up Gecko.
        element = Util.traverseUp(element, function (el) {
          return Util.emptyElementNames.indexOf(el.nodeName.toLowerCase()) === -1;
        });
        // Selecting at the beginning of a table doesn't work in PhantomJS.
        if (element.nodeName.toLowerCase() === 'table') {
          var firstCell = element.querySelector('th, td');
          if (firstCell) {
            element = firstCell;
          }
        }
        return element;
      },

      getFirstTextNode: function (element) {
        if (element.nodeType === 3) {
          return element;
        }

        for (var i = 0; i < element.childNodes.length; i++) {
          var textNode = Util.getFirstTextNode(element.childNodes[i]);
          if (textNode !== null) {
            return textNode;
          }
        }
        return null;
      },

      ensureUrlHasProtocol: function (url) {
        if (url.indexOf('://') === -1) {
          return 'http://' + url;
        }
        return url;
      },

      warn: function () {
        if (window.console !== undefined && typeof window.console.warn === 'function') {
          window.console.warn.apply(window.console, arguments);
        }
      },

      deprecated: function (oldName, newName, version) {
        // simple deprecation warning mechanism.
        var m = oldName + ' is deprecated, please use ' + newName + ' instead.';
        if (version) {
          m += ' Will be removed in ' + version;
        }
        Util.warn(m);
      },

      deprecatedMethod: function (oldName, newName, args, version) {
        // run the replacement and warn when someone calls a deprecated method
        Util.deprecated(oldName, newName, version);
        if (typeof this[newName] === 'function') {
          this[newName].apply(this, args);
        }
      },

      cleanupAttrs: function (el, attrs) {
        attrs.forEach(function (attr) {
          el.removeAttribute(attr);
        });
      },

      cleanupTags: function (el, tags) {
        tags.forEach(function (tag) {
          if (el.nodeName.toLowerCase() === tag) {
            el.parentNode.removeChild(el);
          }
        });
      },

      // get the closest parent
      getClosestTag: function (el, tag) {
        return Util.traverseUp(el, function (element) {
          return element.nodeName.toLowerCase() === tag.toLowerCase();
        });
      },

      unwrap: function (el, doc) {
        var fragment = doc.createDocumentFragment(),
          nodes = Array.prototype.slice.call(el.childNodes);

        // cast nodeList to array since appending child
        // to a different node will alter length of el.childNodes
        for (var i = 0; i < nodes.length; i++) {
          fragment.appendChild(nodes[i]);
        }

        if (fragment.childNodes.length) {
          el.parentNode.replaceChild(fragment, el);
        } else {
          el.parentNode.removeChild(el);
        }
      }
    };

    MediumEditor.util = Util;
  }(window));

  (function () {
    'use strict';

    var Extension = function (options) {
      MediumEditor.util.extend(this, options);
    };

    Extension.extend = function (protoProps) {
      // magic extender thinger. mostly borrowed from backbone/goog.inherits
      // place this function on some thing you want extend-able.
      //
      // example:
      //
      //      function Thing(args){
      //          this.options = args;
      //      }
      //
      //      Thing.prototype = { foo: "bar" };
      //      Thing.extend = extenderify;
      //
      //      var ThingTwo = Thing.extend({ foo: "baz" });
      //
      //      var thingOne = new Thing(); // foo === "bar"
      //      var thingTwo = new ThingTwo(); // foo === "baz"
      //
      //      which seems like some simply shallow copy nonsense
      //      at first, but a lot more is going on there.
      //
      //      passing a `constructor` to the extend props
      //      will cause the instance to instantiate through that
      //      instead of the parent's constructor.

      var parent = this,
        child;

      // The constructor function for the new subclass is either defined by you
      // (the "constructor" property in your `extend` definition), or defaulted
      // by us to simply call the parent's constructor.

      if (protoProps && protoProps.hasOwnProperty('constructor')) {
        child = protoProps.constructor;
      } else {
        child = function () {
          return parent.apply(this, arguments);
        };
      }

      // das statics (.extend comes over, so your subclass can have subclasses too)
      MediumEditor.util.extend(child, parent);

      // Set the prototype chain to inherit from `parent`, without calling
      // `parent`'s constructor function.
      var Surrogate = function () {
        this.constructor = child;
      };
      Surrogate.prototype = parent.prototype;
      child.prototype = new Surrogate();

      if (protoProps) {
        MediumEditor.util.extend(child.prototype, protoProps);
      }

      // todo: $super?

      return child;
    };

    Extension.prototype = {
      /* init: [function]
         *
         * Called by MediumEditor during initialization.
         * The .base property will already have been set to
         * current instance of MediumEditor when this is called.
         * All helper methods will exist as well
         */
      init: function () {},

      /* base: [MediumEditor instance]
         *
         * If not overriden, this will be set to the current instance
         * of MediumEditor, before the init method is called
         */
      base: undefined,

      /* name: [string]
         *
         * 'name' of the extension, used for retrieving the extension.
         * If not set, MediumEditor will set this to be the key
         * used when passing the extension into MediumEditor via the
         * 'extensions' option
         */
      name: undefined,

      /* checkState: [function (node)]
         *
         * If implemented, this function will be called one or more times
         * the state of the editor & toolbar are updated.
         * When the state is updated, the editor does the following:
         *
         * 1) Find the parent node containing the current selection
         * 2) Call checkState on the extension, passing the node as an argument
         * 3) Get the parent node of the previous node
         * 4) Repeat steps #2 and #3 until we move outside the parent contenteditable
         */
      checkState: undefined,

      /* destroy: [function ()]
         *
         * This method should remove any created html, custom event handlers
         * or any other cleanup tasks that should be performed.
         * If implemented, this function will be called when MediumEditor's
         * destroy method has been called.
         */
      destroy: undefined,

      /* As alternatives to checkState, these functions provide a more structured
         * path to updating the state of an extension (usually a button) whenever
         * the state of the editor & toolbar are updated.
         */

      /* queryCommandState: [function ()]
         *
         * If implemented, this function will be called once on each extension
         * when the state of the editor/toolbar is being updated.
         *
         * If this function returns a non-null value, the extension will
         * be ignored as the code climbs the dom tree.
         *
         * If this function returns true, and the setActive() function is defined
         * setActive() will be called
         */
      queryCommandState: undefined,

      /* isActive: [function ()]
         *
         * If implemented, this function will be called when MediumEditor
         * has determined that this extension is 'active' for the current selection.
         * This may be called when the editor & toolbar are being updated,
         * but only if queryCommandState() or isAlreadyApplied() functions
         * are implemented, and when called, return true.
         */
      isActive: undefined,

      /* isAlreadyApplied: [function (node)]
         *
         * If implemented, this function is similar to checkState() in
         * that it will be called repeatedly as MediumEditor moves up
         * the DOM to update the editor & toolbar after a state change.
         *
         * NOTE: This function will NOT be called if checkState() has
         * been implemented. This function will NOT be called if
         * queryCommandState() is implemented and returns a non-null
         * value when called
         */
      isAlreadyApplied: undefined,

      /* setActive: [function ()]
         *
         * If implemented, this function is called when MediumEditor knows
         * that this extension is currently enabled.  Currently, this
         * function is called when updating the editor & toolbar, and
         * only if queryCommandState() or isAlreadyApplied(node) return
         * true when called
         */
      setActive: undefined,

      /* setInactive: [function ()]
         *
         * If implemented, this function is called when MediumEditor knows
         * that this extension is currently disabled.  Curently, this
         * is called at the beginning of each state change for
         * the editor & toolbar. After calling this, MediumEditor
         * will attempt to update the extension, either via checkState()
         * or the combination of queryCommandState(), isAlreadyApplied(node),
         * isActive(), and setActive()
         */
      setInactive: undefined,

      /************************ Helpers ************************
       * The following are helpers that are either set by MediumEditor
       * during initialization, or are helper methods which either
       * route calls to the MediumEditor instance or provide common
       * functionality for all extensions
       *********************************************************/

      /* window: [Window]
         *
         * If not overriden, this will be set to the window object
         * to be used by MediumEditor and its extensions.  This is
         * passed via the 'contentWindow' option to MediumEditor
         * and is the global 'window' object by default
         */
      'window': undefined,

      /* document: [Document]
         *
         * If not overriden, this will be set to the document object
         * to be used by MediumEditor and its extensions. This is
         * passed via the 'ownerDocument' optin to MediumEditor
         * and is the global 'document' object by default
         */
      'document': undefined,

      /* getEditorElements: [function ()]
         *
         * Helper function which returns an array containing
         * all the contenteditable elements for this instance
         * of MediumEditor
         */
      getEditorElements: function () {
        return this.base.elements;
      },

      /* getEditorId: [function ()]
         *
         * Helper function which returns a unique identifier
         * for this instance of MediumEditor
         */
      getEditorId: function () {
        return this.base.id;
      },

      /* getEditorOptions: [function (option)]
         *
         * Helper function which returns the value of an option
         * used to initialize this instance of MediumEditor
         */
      getEditorOption: function (option) {
        return this.base.options[option];
      }
    };

    /* List of method names to add to the prototype of Extension
     * Each of these methods will be defined as helpers that
     * just call directly into the MediumEditor instance.
     *
     * example for 'on' method:
     * Extension.prototype.on = function () {
     *     return this.base.on.apply(this.base, arguments);
     * }
     */
    [
      // general helpers
      'execAction',

      // event handling
      'on',
      'off',
      'subscribe',
      'trigger'

    ].forEach(function (helper) {
      Extension.prototype[helper] = function () {
        return this.base[helper].apply(this.base, arguments);
      };
    });

    MediumEditor.Extension = Extension;
  })();

  (function () {
    'use strict';

    function filterOnlyParentElements(node) {
      if (MediumEditor.util.isBlockContainer(node)) {
        return NodeFilter.FILTER_ACCEPT;
      } else {
        return NodeFilter.FILTER_SKIP;
      }
    }

    var Selection = {
      findMatchingSelectionParent: function (testElementFunction, contentWindow) {
        var selection = contentWindow.getSelection(),
          range,
          current;

        if (selection.rangeCount === 0) {
          return false;
        }

        range = selection.getRangeAt(0);
        current = range.commonAncestorContainer;

        return MediumEditor.util.traverseUp(current, testElementFunction);
      },

      getSelectionElement: function (contentWindow) {
        return this.findMatchingSelectionParent(function (el) {
          return MediumEditor.util.isMediumEditorElement(el);
        }, contentWindow);
      },

      // http://stackoverflow.com/questions/17678843/cant-restore-selection-after-html-modify-even-if-its-the-same-html
      // Tim Down
      exportSelection: function (root, doc) {
        if (!root) {
          return null;
        }

        var selectionState = null,
          selection = doc.getSelection();

        if (selection.rangeCount > 0) {
          var range = selection.getRangeAt(0),
            preSelectionRange = range.cloneRange(),
            start;

          preSelectionRange.selectNodeContents(root);
          preSelectionRange.setEnd(range.startContainer, range.startOffset);
          start = preSelectionRange.toString().length;

          selectionState = {
            start: start,
            end: start + range.toString().length
          };
          // If start = 0 there may still be an empty paragraph before it, but we don't care.
          if (start !== 0) {
            var emptyBlocksIndex = this.getIndexRelativeToAdjacentEmptyBlocks(doc, root, range.startContainer, range.startOffset);
            if (emptyBlocksIndex !== -1) {
              selectionState.emptyBlocksIndex = emptyBlocksIndex;
            }
          }
        }

        return selectionState;
      },

      // http://stackoverflow.com/questions/17678843/cant-restore-selection-after-html-modify-even-if-its-the-same-html
      // Tim Down
      //
      // {object} selectionState - the selection to import
      // {DOMElement} root - the root element the selection is being restored inside of
      // {Document} doc - the document to use for managing selection
      // {boolean} [favorLaterSelectionAnchor] - defaults to false. If true, import the cursor immediately
      //      subsequent to an anchor tag if it would otherwise be placed right at the trailing edge inside the
      //      anchor. This cursor positioning, even though visually equivalent to the user, can affect behavior
      //      in MS IE.
      importSelection: function (selectionState, root, doc, favorLaterSelectionAnchor) {
        if (!selectionState || !root) {
          return;
        }

        var range = doc.createRange();
        range.setStart(root, 0);
        range.collapse(true);

        var node = root,
          nodeStack = [],
          charIndex = 0,
          foundStart = false,
          stop = false,
          nextCharIndex;

        while (!stop && node) {
          if (node.nodeType === 3) {
            nextCharIndex = charIndex + node.length;
            if (!foundStart && selectionState.start >= charIndex && selectionState.start <= nextCharIndex) {
              range.setStart(node, selectionState.start - charIndex);
              foundStart = true;
            }
            if (foundStart && selectionState.end >= charIndex && selectionState.end <= nextCharIndex) {
              range.setEnd(node, selectionState.end - charIndex);
              stop = true;
            }
            charIndex = nextCharIndex;
          } else {
            var i = node.childNodes.length - 1;
            while (i >= 0) {
              nodeStack.push(node.childNodes[i]);
              i -= 1;
            }
          }
          if (!stop) {
            node = nodeStack.pop();
          }
        }

        if (typeof selectionState.emptyBlocksIndex !== 'undefined') {
          range = this.importSelectionMoveCursorPastBlocks(doc, root, selectionState.emptyBlocksIndex, range);
        }

        // If the selection is right at the ending edge of a link, put it outside the anchor tag instead of inside.
        if (favorLaterSelectionAnchor) {
          range = this.importSelectionMoveCursorPastAnchor(selectionState, range);
        }

        var sel = doc.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
      },

      // Utility method called from importSelection only
      importSelectionMoveCursorPastAnchor: function (selectionState, range) {
        var nodeInsideAnchorTagFunction = function (node) {
          return node.nodeName.toLowerCase() === 'a';
        };
        if (selectionState.start === selectionState.end &&
          range.startContainer.nodeType === 3 &&
          range.startOffset === range.startContainer.nodeValue.length &&
          MediumEditor.util.traverseUp(range.startContainer, nodeInsideAnchorTagFunction)) {
          var prevNode = range.startContainer,
            currentNode = range.startContainer.parentNode;
          while (currentNode !== null && currentNode.nodeName.toLowerCase() !== 'a') {
            if (currentNode.childNodes[currentNode.childNodes.length - 1] !== prevNode) {
              currentNode = null;
            } else {
              prevNode = currentNode;
              currentNode = currentNode.parentNode;
            }
          }
          if (currentNode !== null && currentNode.nodeName.toLowerCase() === 'a') {
            var currentNodeIndex = null;
            for (var i = 0; currentNodeIndex === null && i < currentNode.parentNode.childNodes.length; i++) {
              if (currentNode.parentNode.childNodes[i] === currentNode) {
                currentNodeIndex = i;
              }
            }
            range.setStart(currentNode.parentNode, currentNodeIndex + 1);
            range.collapse(true);
          }
        }
        return range;
      },

      // Uses the emptyBlocksIndex calculated by getIndexRelativeToAdjacentEmptyBlocks
      // to move the cursor back to the start of the correct paragraph
      importSelectionMoveCursorPastBlocks: function (doc, root, index, range) {
        var treeWalker = doc.createTreeWalker(root, NodeFilter.SHOW_ELEMENT, filterOnlyParentElements, false),
          startContainer = range.startContainer,
          startBlock,
          targetNode,
          currIndex = 0;
        index = index || 1; // If index is 0, we still want to move to the next block

        // Chrome counts newlines and spaces that separate block elements as actual elements.
        // If the selection is inside one of these text nodes, and it has a previous sibling
        // which is a block element, we want the treewalker to start at the previous sibling
        // and NOT at the parent of the textnode
        if (startContainer.nodeType === 3 && MediumEditor.util.isBlockContainer(startContainer.previousSibling)) {
          startBlock = startContainer.previousSibling;
        } else {
          startBlock = MediumEditor.util.getClosestBlockContainer(startContainer);
        }

        // Skip over empty blocks until we hit the block we want the selection to be in
        while (treeWalker.nextNode()) {
          if (!targetNode) {
            // Loop through all blocks until we hit the starting block element
            if (startBlock === treeWalker.currentNode) {
              targetNode = treeWalker.currentNode;
            }
          } else {
            targetNode = treeWalker.currentNode;
            currIndex++;
            // We hit the target index, bail
            if (currIndex === index) {
              break;
            }
            // If we find a non-empty block, ignore the emptyBlocksIndex and just put selection here
            if (targetNode.textContent.length > 0) {
              break;
            }
          }
        }

        // We're selecting a high-level block node, so make sure the cursor gets moved into the deepest
        // element at the beginning of the block
        range.setStart(MediumEditor.util.getFirstSelectableLeafNode(targetNode), 0);

        return range;
      },

      // Returns -1 unless the cursor is at the beginning of a paragraph/block
      // If the paragraph/block is preceeded by empty paragraphs/block (with no text)
      // it will return the number of empty paragraphs before the cursor.
      // Otherwise, it will return 0, which indicates the cursor is at the beginning
      // of a paragraph/block, and not at the end of the paragraph/block before it
      getIndexRelativeToAdjacentEmptyBlocks: function (doc, root, cursorContainer, cursorOffset) {
        // If there is text in front of the cursor, that means there isn't only empty blocks before it
        if (cursorContainer.textContent.length > 0 && cursorOffset > 0) {
          return -1;
        }

        // Check if the block that contains the cursor has any other text in front of the cursor
        var node = cursorContainer;
        if (node.nodeType !== 3) {
          node = cursorContainer.childNodes[cursorOffset];
        }
        if (node && !MediumEditor.util.isElementAtBeginningOfBlock(node)) {
          return -1;
        }

        // Walk over block elements, counting number of empty blocks between last piece of text
        // and the block the cursor is in
        var closestBlock = MediumEditor.util.getClosestBlockContainer(cursorContainer),
          treeWalker = doc.createTreeWalker(root, NodeFilter.SHOW_ELEMENT, filterOnlyParentElements, false),
          emptyBlocksCount = 0;
        while (treeWalker.nextNode()) {
          var blockIsEmpty = treeWalker.currentNode.textContent === '';
          if (blockIsEmpty || emptyBlocksCount > 0) {
            emptyBlocksCount += 1;
          }
          if (treeWalker.currentNode === closestBlock) {
            return emptyBlocksCount;
          }
          if (!blockIsEmpty) {
            emptyBlocksCount = 0;
          }
        }

        return emptyBlocksCount;
      },

      selectionInContentEditableFalse: function (contentWindow) {
        // determine if the current selection is exclusively inside
        // a contenteditable="false", though treat the case of an
        // explicit contenteditable="true" inside a "false" as false.
        var sawtrue,
          sawfalse = this.findMatchingSelectionParent(function (el) {
            var ce = el && el.getAttribute('contenteditable');
            if (ce === 'true') {
              sawtrue = true;
            }
            return el.nodeName !== '#text' && ce === 'false';
          }, contentWindow);

        return !sawtrue && sawfalse;
      },

      // http://stackoverflow.com/questions/4176923/html-of-selected-text
      // by Tim Down
      getSelectionHtml: function getSelectionHtml(doc) {
        var i,
          html = '',
          sel = doc.getSelection(),
          len,
          container;
        if (sel.rangeCount) {
          container = doc.createElement('div');
          for (i = 0, len = sel.rangeCount; i < len; i += 1) {
            container.appendChild(sel.getRangeAt(i).cloneContents());
          }
          html = container.innerHTML;
        }
        return html;
      },

      /**
       *  Find the caret position within an element irrespective of any inline tags it may contain.
       *
       *  @param {DOMElement} An element containing the cursor to find offsets relative to.
       *  @param {Range} A Range representing cursor position. Will window.getSelection if none is passed.
       *  @return {Object} 'left' and 'right' attributes contain offsets from begining and end of Element
       */
      getCaretOffsets: function getCaretOffsets(element, range) {
        var preCaretRange, postCaretRange;

        if (!range) {
          range = window.getSelection().getRangeAt(0);
        }

        preCaretRange = range.cloneRange();
        postCaretRange = range.cloneRange();

        preCaretRange.selectNodeContents(element);
        preCaretRange.setEnd(range.endContainer, range.endOffset);

        postCaretRange.selectNodeContents(element);
        postCaretRange.setStart(range.endContainer, range.endOffset);

        return {
          left: preCaretRange.toString().length,
          right: postCaretRange.toString().length
        };
      },

      // http://stackoverflow.com/questions/15867542/range-object-get-selection-parent-node-chrome-vs-firefox
      rangeSelectsSingleNode: function (range) {
        var startNode = range.startContainer;
        return startNode === range.endContainer &&
          startNode.hasChildNodes() &&
          range.endOffset === range.startOffset + 1;
      },

      getSelectedParentElement: function (range) {
        if (!range) {
          return null;
        }

        // Selection encompasses a single element
        if (this.rangeSelectsSingleNode(range) && range.startContainer.childNodes[range.startOffset].nodeType !== 3) {
          return range.startContainer.childNodes[range.startOffset];
        }

        // Selection range starts inside a text node, so get its parent
        if (range.startContainer.nodeType === 3) {
          return range.startContainer.parentNode;
        }

        // Selection starts inside an element
        return range.startContainer;
      },

      getSelectedElements: function (doc) {
        var selection = doc.getSelection(),
          range,
          toRet,
          currNode;

        if (!selection.rangeCount || selection.isCollapsed || !selection.getRangeAt(0).commonAncestorContainer) {
          return [];
        }

        range = selection.getRangeAt(0);

        if (range.commonAncestorContainer.nodeType === 3) {
          toRet = [];
          currNode = range.commonAncestorContainer;
          while (currNode.parentNode && currNode.parentNode.childNodes.length === 1) {
            toRet.push(currNode.parentNode);
            currNode = currNode.parentNode;
          }

          return toRet;
        }

        return [].filter.call(range.commonAncestorContainer.getElementsByTagName('*'), function (el) {
          return (typeof selection.containsNode === 'function') ? selection.containsNode(el, true) : true;
        });
      },

      selectNode: function (node, doc) {
        var range = doc.createRange(),
          sel = doc.getSelection();

        range.selectNodeContents(node);
        sel.removeAllRanges();
        sel.addRange(range);
      },

      select: function (doc, startNode, startOffset, endNode, endOffset) {
        doc.getSelection().removeAllRanges();
        var range = doc.createRange();
        range.setStart(startNode, startOffset);
        if (endNode) {
          range.setEnd(endNode, endOffset);
        } else {
          range.collapse(true);
        }
        doc.getSelection().addRange(range);
        return range;
      },

      /**
       * Move cursor to the given node with the given offset.
       *
       * @param  {DomDocument} doc     Current document
       * @param  {DomElement}  node    Element where to jump
       * @param  {integer}     offset  Where in the element should we jump, 0 by default
       */
      moveCursor: function (doc, node, offset) {
        this.select(doc, node, offset);
      },

      getSelectionRange: function (ownerDocument) {
        var selection = ownerDocument.getSelection();
        if (selection.rangeCount === 0) {
          return null;
        }
        return selection.getRangeAt(0);
      },

      // http://stackoverflow.com/questions/1197401/how-can-i-get-the-element-the-caret-is-in-with-javascript-when-using-contentedi
      // by You
      getSelectionStart: function (ownerDocument) {
        var node = ownerDocument.getSelection().anchorNode,
          startNode = (node && node.nodeType === 3 ? node.parentNode : node);

        return startNode;
      }
    };

    MediumEditor.selection = Selection;
  }());

  (function () {
    'use strict';

    var Events = function (instance) {
      this.base = instance;
      this.options = this.base.options;
      this.events = [];
      this.disabledEvents = {};
      this.customEvents = {};
      this.listeners = {};
    };

    Events.prototype = {
      InputEventOnContenteditableSupported: !MediumEditor.util.isIE,

      // Helpers for event handling

      attachDOMEvent: function (target, event, listener, useCapture) {
        target.addEventListener(event, listener, useCapture);
        this.events.push([target, event, listener, useCapture]);
      },

      detachDOMEvent: function (target, event, listener, useCapture) {
        var index = this.indexOfListener(target, event, listener, useCapture),
          e;
        if (index !== -1) {
          e = this.events.splice(index, 1)[0];
          e[0].removeEventListener(e[1], e[2], e[3]);
        }
      },

      indexOfListener: function (target, event, listener, useCapture) {
        var i, n, item;
        for (i = 0, n = this.events.length; i < n; i = i + 1) {
          item = this.events[i];
          if (item[0] === target && item[1] === event && item[2] === listener && item[3] === useCapture) {
            return i;
          }
        }
        return -1;
      },

      detachAllDOMEvents: function () {
        var e = this.events.pop();
        while (e) {
          e[0].removeEventListener(e[1], e[2], e[3]);
          e = this.events.pop();
        }
      },

      enableCustomEvent: function (event) {
        if (this.disabledEvents[event] !== undefined) {
          delete this.disabledEvents[event];
        }
      },

      disableCustomEvent: function (event) {
        this.disabledEvents[event] = true;
      },

      // custom events
      attachCustomEvent: function (event, listener) {
        this.setupListener(event);
        if (!this.customEvents[event]) {
          this.customEvents[event] = [];
        }
        this.customEvents[event].push(listener);
      },

      detachCustomEvent: function (event, listener) {
        var index = this.indexOfCustomListener(event, listener);
        if (index !== -1) {
          this.customEvents[event].splice(index, 1);
          // TODO: If array is empty, should detach internal listeners via destroyListener()
        }
      },

      indexOfCustomListener: function (event, listener) {
        if (!this.customEvents[event] || !this.customEvents[event].length) {
          return -1;
        }

        return this.customEvents[event].indexOf(listener);
      },

      detachAllCustomEvents: function () {
        this.customEvents = {};
        // TODO: Should detach internal listeners here via destroyListener()
      },

      triggerCustomEvent: function (name, data, editable) {
        if (this.customEvents[name] && !this.disabledEvents[name]) {
          this.customEvents[name].forEach(function (listener) {
            listener(data, editable);
          });
        }
      },

      // Cleaning up

      destroy: function () {
        this.detachAllDOMEvents();
        this.detachAllCustomEvents();
        this.detachExecCommand();

        if (this.base.elements) {
          this.base.elements.forEach(function (element) {
            element.removeAttribute('data-medium-focused');
          });
        }
      },

      // Listening to calls to document.execCommand

      // Attach a listener to be notified when document.execCommand is called
      attachToExecCommand: function () {
        if (this.execCommandListener) {
          return;
        }

        // Store an instance of the listener so:
        // 1) We only attach to execCommand once
        // 2) We can remove the listener later
        this.execCommandListener = function (execInfo) {
          this.handleDocumentExecCommand(execInfo);
        }.bind(this);

        // Ensure that execCommand has been wrapped correctly
        this.wrapExecCommand();

        // Add listener to list of execCommand listeners
        this.options.ownerDocument.execCommand.listeners.push(this.execCommandListener);
      },

      // Remove our listener for calls to document.execCommand
      detachExecCommand: function () {
        var doc = this.options.ownerDocument;
        if (!this.execCommandListener || !doc.execCommand.listeners) {
          return;
        }

        // Find the index of this listener in the array of listeners so it can be removed
        var index = doc.execCommand.listeners.indexOf(this.execCommandListener);
        if (index !== -1) {
          doc.execCommand.listeners.splice(index, 1);
        }

        // If the list of listeners is now empty, put execCommand back to its original state
        if (!doc.execCommand.listeners.length) {
          this.unwrapExecCommand();
        }
      },

      // Wrap document.execCommand in a custom method so we can listen to calls to it
      wrapExecCommand: function () {
        var doc = this.options.ownerDocument;

        // Ensure all instance of MediumEditor only wrap execCommand once
        if (doc.execCommand.listeners) {
          return;
        }

        // Create a wrapper method for execCommand which will:
        // 1) Call document.execCommand with the correct arguments
        // 2) Loop through any listeners and notify them that execCommand was called
        //    passing extra info on the call
        // 3) Return the result
        var wrapper = function (aCommandName, aShowDefaultUI, aValueArgument) {
          var result = doc.execCommand.orig.apply(this, arguments);

          if (!doc.execCommand.listeners) {
            return result;
          }

          var args = Array.prototype.slice.call(arguments);
          doc.execCommand.listeners.forEach(function (listener) {
            listener({
              command: aCommandName,
              value: aValueArgument,
              args: args,
              result: result
            });
          });

          return result;
        };

        // Store a reference to the original execCommand
        wrapper.orig = doc.execCommand;

        // Attach an array for storing listeners
        wrapper.listeners = [];

        // Overwrite execCommand
        doc.execCommand = wrapper;
      },

      // Revert document.execCommand back to its original self
      unwrapExecCommand: function () {
        var doc = this.options.ownerDocument;
        if (!doc.execCommand.orig) {
          return;
        }

        // Use the reference to the original execCommand to revert back
        doc.execCommand = doc.execCommand.orig;
      },

      // Listening to browser events to emit events medium-editor cares about
      setupListener: function (name) {
        if (this.listeners[name]) {
          return;
        }

        switch (name) {
          case 'externalInteraction':
            // Detecting when user has interacted with elements outside of MediumEditor
            this.attachDOMEvent(this.options.ownerDocument.body, 'mousedown', this.handleBodyMousedown.bind(this), true);
            this.attachDOMEvent(this.options.ownerDocument.body, 'click', this.handleBodyClick.bind(this), true);
            this.attachDOMEvent(this.options.ownerDocument.body, 'focus', this.handleBodyFocus.bind(this), true);
            break;
          case 'blur':
            // Detecting when focus is lost
            this.setupListener('externalInteraction');
            break;
          case 'focus':
            // Detecting when focus moves into some part of MediumEditor
            this.setupListener('externalInteraction');
            break;
          case 'editableInput':
            // setup cache for knowing when the content has changed
            this.contentCache = [];
            this.base.elements.forEach(function (element) {
              this.contentCache[element.getAttribute('medium-editor-index')] = element.innerHTML;

              // Attach to the 'oninput' event, handled correctly by most browsers
              if (this.InputEventOnContenteditableSupported) {
                this.attachDOMEvent(element, 'input', this.handleInput.bind(this));
              }
            }.bind(this));

            // For browsers which don't support the input event on contenteditable (IE)
            // we'll attach to 'selectionchange' on the document and 'keypress' on the editables
            if (!this.InputEventOnContenteditableSupported) {
              this.setupListener('editableKeypress');
              this.keypressUpdateInput = true;
              this.attachDOMEvent(document, 'selectionchange', this.handleDocumentSelectionChange.bind(this));
              // Listen to calls to execCommand
              this.attachToExecCommand();
            }
            break;
          case 'editableClick':
            // Detecting click in the contenteditables
            this.attachToEachElement('click', this.handleClick);
            break;
          case 'editableBlur':
            // Detecting blur in the contenteditables
            this.attachToEachElement('blur', this.handleBlur);
            break;
          case 'editableKeypress':
            // Detecting keypress in the contenteditables
            this.attachToEachElement('keypress', this.handleKeypress);
            break;
          case 'editableKeyup':
            // Detecting keyup in the contenteditables
            this.attachToEachElement('keyup', this.handleKeyup);
            break;
          case 'editableKeydown':
            // Detecting keydown on the contenteditables
            this.attachToEachElement('keydown', this.handleKeydown);
            break;
          case 'editableKeydownSpace':
            // Detecting keydown for SPACE on the contenteditables
            this.setupListener('editableKeydown');
            break;
          case 'editableKeydownEnter':
            // Detecting keydown for ENTER on the contenteditables
            this.setupListener('editableKeydown');
            break;
          case 'editableKeydownTab':
            // Detecting keydown for TAB on the contenteditable
            this.setupListener('editableKeydown');
            break;
          case 'editableKeydownDelete':
            // Detecting keydown for DELETE/BACKSPACE on the contenteditables
            this.setupListener('editableKeydown');
            break;
          case 'editableMouseover':
            // Detecting mouseover on the contenteditables
            this.attachToEachElement('mouseover', this.handleMouseover);
            break;
          case 'editableDrag':
            // Detecting dragover and dragleave on the contenteditables
            this.attachToEachElement('dragover', this.handleDragging);
            this.attachToEachElement('dragleave', this.handleDragging);
            break;
          case 'editableDrop':
            // Detecting drop on the contenteditables
            this.attachToEachElement('drop', this.handleDrop);
            break;
          case 'editablePaste':
            // Detecting paste on the contenteditables
            this.attachToEachElement('paste', this.handlePaste);
            break;
        }
        this.listeners[name] = true;
      },

      attachToEachElement: function (name, handler) {
        this.base.elements.forEach(function (element) {
          this.attachDOMEvent(element, name, handler.bind(this));
        }, this);
      },

      focusElement: function (element) {
        element.focus();
        this.updateFocus(element, { target: element, type: 'focus' });
      },

      updateFocus: function (target, eventObj) {
        var toolbar = this.base.getExtensionByName('toolbar'),
          toolbarEl = toolbar ? toolbar.getToolbarElement() : null,
          anchorPreview = this.base.getExtensionByName('anchor-preview'),
          previewEl = (anchorPreview && anchorPreview.getPreviewElement) ? anchorPreview.getPreviewElement() : null,
          hadFocus = this.base.getFocusedElement(),
          toFocus;

        // For clicks, we need to know if the mousedown that caused the click happened inside the existing focused element.
        // If so, we don't want to focus another element
        if (hadFocus &&
          eventObj.type === 'click' &&
          this.lastMousedownTarget &&
          (MediumEditor.util.isDescendant(hadFocus, this.lastMousedownTarget, true) ||
            MediumEditor.util.isDescendant(toolbarEl, this.lastMousedownTarget, true) ||
            MediumEditor.util.isDescendant(previewEl, this.lastMousedownTarget, true))) {
          toFocus = hadFocus;
        }

        if (!toFocus) {
          this.base.elements.some(function (element) {
            // If the target is part of an editor element, this is the element getting focus
            if (!toFocus && (MediumEditor.util.isDescendant(element, target, true))) {
              toFocus = element;
            }

            // bail if we found an element that's getting focus
            return !!toFocus;
          }, this);
        }

        // Check if the target is external (not part of the editor, toolbar, or anchorpreview)
        var externalEvent = !MediumEditor.util.isDescendant(hadFocus, target, true) &&
          !MediumEditor.util.isDescendant(toolbarEl, target, true) &&
          !MediumEditor.util.isDescendant(previewEl, target, true);

        if (toFocus !== hadFocus) {
          // If element has focus, and focus is going outside of editor
          // Don't blur focused element if clicking on editor, toolbar, or anchorpreview
          if (hadFocus && externalEvent) {
            // Trigger blur on the editable that has lost focus
            hadFocus.removeAttribute('data-medium-focused');
            this.triggerCustomEvent('blur', eventObj, hadFocus);
          }

          // If focus is going into an editor element
          if (toFocus) {
            // Trigger focus on the editable that now has focus
            toFocus.setAttribute('data-medium-focused', true);
            this.triggerCustomEvent('focus', eventObj, toFocus);
          }
        }

        if (externalEvent) {
          this.triggerCustomEvent('externalInteraction', eventObj);
        }
      },

      updateInput: function (target, eventObj) {
        if (!this.contentCache) {
          return;
        }
        // An event triggered which signifies that the user may have changed someting
        // Look in our cache of input for the contenteditables to see if something changed
        var index = target.getAttribute('medium-editor-index');
        if (target.innerHTML !== this.contentCache[index]) {
          // The content has changed since the last time we checked, fire the event
          this.triggerCustomEvent('editableInput', eventObj, target);
        }
        this.contentCache[index] = target.innerHTML;
      },

      handleDocumentSelectionChange: function (event) {
        // When selectionchange fires, target and current target are set
        // to document, since this is where the event is handled
        // However, currentTarget will have an 'activeElement' property
        // which will point to whatever element has focus.
        if (event.currentTarget && event.currentTarget.activeElement) {
          var activeElement = event.currentTarget.activeElement,
            currentTarget;
          // We can look at the 'activeElement' to determine if the selectionchange has
          // happened within a contenteditable owned by this instance of MediumEditor
          this.base.elements.some(function (element) {
            if (MediumEditor.util.isDescendant(element, activeElement, true)) {
              currentTarget = element;
              return true;
            }
            return false;
          }, this);

          // We know selectionchange fired within one of our contenteditables
          if (currentTarget) {
            this.updateInput(currentTarget, { target: activeElement, currentTarget: currentTarget });
          }
        }
      },

      handleDocumentExecCommand: function () {
        // document.execCommand has been called
        // If one of our contenteditables currently has focus, we should
        // attempt to trigger the 'editableInput' event
        var target = this.base.getFocusedElement();
        if (target) {
          this.updateInput(target, { target: target, currentTarget: target });
        }
      },

      handleBodyClick: function (event) {
        this.updateFocus(event.target, event);
      },

      handleBodyFocus: function (event) {
        this.updateFocus(event.target, event);
      },

      handleBodyMousedown: function (event) {
        this.lastMousedownTarget = event.target;
      },

      handleInput: function (event) {
        this.updateInput(event.currentTarget, event);
      },

      handleClick: function (event) {
        this.triggerCustomEvent('editableClick', event, event.currentTarget);
      },

      handleBlur: function (event) {
        this.triggerCustomEvent('editableBlur', event, event.currentTarget);
      },

      handleKeypress: function (event) {
        this.triggerCustomEvent('editableKeypress', event, event.currentTarget);

        // If we're doing manual detection of the editableInput event we need
        // to check for input changes during 'keypress'
        if (this.keypressUpdateInput) {
          var eventObj = { target: event.target, currentTarget: event.currentTarget };

          // In IE, we need to let the rest of the event stack complete before we detect
          // changes to input, so using setTimeout here
          setTimeout(function () {
            this.updateInput(eventObj.currentTarget, eventObj);
          }.bind(this), 0);
        }
      },

      handleKeyup: function (event) {
        this.triggerCustomEvent('editableKeyup', event, event.currentTarget);
      },

      handleMouseover: function (event) {
        this.triggerCustomEvent('editableMouseover', event, event.currentTarget);
      },

      handleDragging: function (event) {
        this.triggerCustomEvent('editableDrag', event, event.currentTarget);
      },

      handleDrop: function (event) {
        this.triggerCustomEvent('editableDrop', event, event.currentTarget);
      },

      handlePaste: function (event) {
        this.triggerCustomEvent('editablePaste', event, event.currentTarget);
      },

      handleKeydown: function (event) {

        this.triggerCustomEvent('editableKeydown', event, event.currentTarget);

        if (MediumEditor.util.isKey(event, MediumEditor.util.keyCode.SPACE)) {
          return this.triggerCustomEvent('editableKeydownSpace', event, event.currentTarget);
        }

        if (MediumEditor.util.isKey(event, MediumEditor.util.keyCode.ENTER) || (event.ctrlKey && MediumEditor.util.isKey(event, MediumEditor.util.keyCode.M))) {
          return this.triggerCustomEvent('editableKeydownEnter', event, event.currentTarget);
        }

        if (MediumEditor.util.isKey(event, MediumEditor.util.keyCode.TAB)) {
          return this.triggerCustomEvent('editableKeydownTab', event, event.currentTarget);
        }

        if (MediumEditor.util.isKey(event, [MediumEditor.util.keyCode.DELETE, MediumEditor.util.keyCode.BACKSPACE])) {
          return this.triggerCustomEvent('editableKeydownDelete', event, event.currentTarget);
        }
      }
    };

    MediumEditor.Events = Events;
  }());

  (function () {
    'use strict';

    var Button = MediumEditor.Extension.extend({

      /* Button Options */

      /* action: [string]
         * The action argument to pass to MediumEditor.execAction()
         * when the button is clicked
         */
      action: undefined,

      /* aria: [string]
         * The value to add as the aria-label attribute of the button
         * element displayed in the toolbar.
         * This is also used as the tooltip for the button
         */
      aria: undefined,

      /* tagNames: [Array]
         * NOTE: This is not used if useQueryState is set to true.
         *
         * Array of element tag names that would indicate that this
         * button has already been applied. If this action has already
         * been applied, the button will be displayed as 'active' in the toolbar
         *
         * Example:
         * For 'bold', if the text is ever within a <b> or <strong>
         * tag that indicates the text is already bold. So the array
         * of tagNames for bold would be: ['b', 'strong']
         */
      tagNames: undefined,

      /* style: [Object]
         * NOTE: This is not used if useQueryState is set to true.
         *
         * A pair of css property & value(s) that indicate that this
         * button has already been applied. If this action has already
         * been applied, the button will be displayed as 'active' in the toolbar
         * Properties of the object:
         *   prop [String]: name of the css property
         *   value [String]: value(s) of the css property
         *                   multiple values can be separated by a '|'
         *
         * Example:
         * For 'bold', if the text is ever within an element with a 'font-weight'
         * style property set to '700' or 'bold', that indicates the text
         * is already bold.  So the style object for bold would be:
         * { prop: 'font-weight', value: '700|bold' }
         */
      style: undefined,

      /* useQueryState: [boolean]
         * Enables/disables whether this button should use the built-in
         * document.queryCommandState() method to determine whether
         * the action has already been applied.  If the action has already
         * been applied, the button will be displayed as 'active' in the toolbar
         *
         * Example:
         * For 'bold', if this is set to true, the code will call:
         * document.queryCommandState('bold') which will return true if the
         * browser thinks the text is already bold, and false otherwise
         */
      useQueryState: undefined,

      /* contentDefault: [string]
         * Default innerHTML to put inside the button
         */
      contentDefault: undefined,

      /* contentFA: [string]
         * The innerHTML to use for the content of the button
         * if the `buttonLabels` option for MediumEditor is set to 'fontawesome'
         */
      contentFA: undefined,

      /* classList: [Array]
         * An array of classNames (strings) to be added to the button
         */
      classList: undefined,

      /* attrs: [object]
         * A set of key-value pairs to add to the button as custom attributes
         */
      attrs: undefined,

      // The button constructor can optionally accept the name of a built-in button
      // (ie 'bold', 'italic', etc.)
      // When the name of a button is passed, it will initialize itself with the
      // configuration for that button
      constructor: function (options) {
        if (Button.isBuiltInButton(options)) {
          MediumEditor.Extension.call(this, this.defaults[options]);
        } else {
          MediumEditor.Extension.call(this, options);
        }
      },

      init: function () {
        MediumEditor.Extension.prototype.init.apply(this, arguments);

        this.button = this.createButton();
        this.on(this.button, 'click', this.handleClick.bind(this));
      },

      /* getButton: [function ()]
         *
         * If implemented, this function will be called when
         * the toolbar is being created.  The DOM Element returned
         * by this function will be appended to the toolbar along
         * with any other buttons.
         */
      getButton: function () {
        return this.button;
      },

      getAction: function () {
        return (typeof this.action === 'function') ? this.action(this.base.options) : this.action;
      },

      getAria: function () {
        return (typeof this.aria === 'function') ? this.aria(this.base.options) : this.aria;
      },

      getTagNames: function () {
        return (typeof this.tagNames === 'function') ? this.tagNames(this.base.options) : this.tagNames;
      },

      createButton: function () {
        var button = this.document.createElement('button'),
          content = this.contentDefault,
          ariaLabel = this.getAria(),
          buttonLabels = this.getEditorOption('buttonLabels');
        // Add class names
        button.classList.add('medium-editor-action');
        button.classList.add('medium-editor-action-' + this.name);
        if (this.classList) {
          this.classList.forEach(function (className) {
            button.classList.add(className);
          });
        }

        // Add attributes
        button.setAttribute('data-action', this.getAction());
        if (ariaLabel) {
          button.setAttribute('title', ariaLabel);
          button.setAttribute('aria-label', ariaLabel);
        }
        if (this.attrs) {
          Object.keys(this.attrs).forEach(function (attr) {
            button.setAttribute(attr, this.attrs[attr]);
          }, this);
        }

        if (buttonLabels === 'fontawesome' && this.contentFA) {
          content = this.contentFA;
        }
        button.innerHTML = content;
        return button;
      },

      handleClick: function (event) {
        event.preventDefault();
        event.stopPropagation();

        var action = this.getAction();

        if (action) {
          this.execAction(action);
        }
      },

      isActive: function () {
        return this.button.classList.contains(this.getEditorOption('activeButtonClass'));
      },

      setInactive: function () {
        this.button.classList.remove(this.getEditorOption('activeButtonClass'));
        delete this.knownState;
      },

      setActive: function () {
        this.button.classList.add(this.getEditorOption('activeButtonClass'));
        delete this.knownState;
      },

      queryCommandState: function () {
        var queryState = null;
        if (this.useQueryState) {
          queryState = this.base.queryCommandState(this.getAction());
        }
        return queryState;
      },

      isAlreadyApplied: function (node) {
        var isMatch = false,
          tagNames = this.getTagNames(),
          styleVals,
          computedStyle;

        if (this.knownState === false || this.knownState === true) {
          return this.knownState;
        }

        if (tagNames && tagNames.length > 0) {
          isMatch = tagNames.indexOf(node.nodeName.toLowerCase()) !== -1;
        }

        if (!isMatch && this.style) {
          styleVals = this.style.value.split('|');
          computedStyle = this.window.getComputedStyle(node, null).getPropertyValue(this.style.prop);
          styleVals.forEach(function (val) {
            if (!this.knownState) {
              isMatch = (computedStyle.indexOf(val) !== -1);
              // text-decoration is not inherited by default
              // so if the computed style for text-decoration doesn't match
              // don't write to knownState so we can fallback to other checks
              if (isMatch || this.style.prop !== 'text-decoration') {
                this.knownState = isMatch;
              }
            }
          }, this);
        }

        return isMatch;
      }
    });

    Button.isBuiltInButton = function (name) {
      return (typeof name === 'string') && MediumEditor.extensions.button.prototype.defaults.hasOwnProperty(name);
    };

    MediumEditor.extensions.button = Button;
  }());

  (function () {
    'use strict';

    /* MediumEditor.extensions.button.defaults: [Object]
     * Set of default config options for all of the built-in MediumEditor buttons
     */
    MediumEditor.extensions.button.prototype.defaults = {
      'bold': {
        name: 'bold',
        action: 'bold',
        aria: 'bold',
        tagNames: ['b', 'strong'],
        style: {
          prop: 'font-weight',
          value: '700|bold'
        },
        useQueryState: true,
        contentDefault: '<b>B</b>',
        contentFA: '<i class="fa fa-bold"></i>'
      },
      'italic': {
        name: 'italic',
        action: 'italic',
        aria: 'italic',
        tagNames: ['i', 'em'],
        style: {
          prop: 'font-style',
          value: 'italic'
        },
        useQueryState: true,
        contentDefault: '<b><i>I</i></b>',
        contentFA: '<i class="fa fa-italic"></i>'
      },
      'underline': {
        name: 'underline',
        action: 'underline',
        aria: 'underline',
        tagNames: ['u'],
        style: {
          prop: 'text-decoration',
          value: 'underline'
        },
        useQueryState: true,
        contentDefault: '<b><u>U</u></b>',
        contentFA: '<i class="fa fa-underline"></i>'
      },
      'strikethrough': {
        name: 'strikethrough',
        action: 'strikethrough',
        aria: 'strike through',
        tagNames: ['strike'],
        style: {
          prop: 'text-decoration',
          value: 'line-through'
        },
        useQueryState: true,
        contentDefault: '<s>A</s>',
        contentFA: '<i class="fa fa-strikethrough"></i>'
      },
      'superscript': {
        name: 'superscript',
        action: 'superscript',
        aria: 'superscript',
        tagNames: ['sup'],
        /* firefox doesn't behave the way we want it to, so we CAN'T use queryCommandState for superscript
               https://github.com/guardian/scribe/blob/master/BROWSERINCONSISTENCIES.md#documentquerycommandstate */
        // useQueryState: true
        contentDefault: '<b>x<sup>1</sup></b>',
        contentFA: '<i class="fa fa-superscript"></i>'
      },
      'subscript': {
        name: 'subscript',
        action: 'subscript',
        aria: 'subscript',
        tagNames: ['sub'],
        /* firefox doesn't behave the way we want it to, so we CAN'T use queryCommandState for subscript
               https://github.com/guardian/scribe/blob/master/BROWSERINCONSISTENCIES.md#documentquerycommandstate */
        // useQueryState: true
        contentDefault: '<b>x<sub>1</sub></b>',
        contentFA: '<i class="fa fa-subscript"></i>'
      },
      'image': {
        name: 'image',
        action: 'image',
        aria: 'image',
        tagNames: ['img'],
        contentDefault: '<b>image</b>',
        contentFA: '<i class="fa fa-picture-o"></i>'
      },
      'orderedlist': {
        name: 'orderedlist',
        action: 'insertorderedlist',
        aria: 'ordered list',
        tagNames: ['ol'],
        useQueryState: true,
        contentDefault: '<b>1.</b>',
        contentFA: '<i class="fa fa-list-ol"></i>'
      },
      'unorderedlist': {
        name: 'unorderedlist',
        action: 'insertunorderedlist',
        aria: 'unordered list',
        tagNames: ['ul'],
        useQueryState: true,
        contentDefault: '<b>&bull;</b>',
        contentFA: '<i class="fa fa-list-ul"></i>'
      },
      'indent': {
        name: 'indent',
        action: 'indent',
        aria: 'indent',
        tagNames: [],
        contentDefault: '<b>&rarr;</b>',
        contentFA: '<i class="fa fa-indent"></i>'
      },
      'outdent': {
        name: 'outdent',
        action: 'outdent',
        aria: 'outdent',
        tagNames: [],
        contentDefault: '<b>&larr;</b>',
        contentFA: '<i class="fa fa-outdent"></i>'
      },
      'justifyCenter': {
        name: 'justifyCenter',
        action: 'justifyCenter',
        aria: 'center justify',
        tagNames: [],
        style: {
          prop: 'text-align',
          value: 'center'
        },
        contentDefault: '<b>C</b>',
        contentFA: '<i class="fa fa-align-center"></i>'
      },
      'justifyFull': {
        name: 'justifyFull',
        action: 'justifyFull',
        aria: 'full justify',
        tagNames: [],
        style: {
          prop: 'text-align',
          value: 'justify'
        },
        contentDefault: '<b>J</b>',
        contentFA: '<i class="fa fa-align-justify"></i>'
      },
      'justifyLeft': {
        name: 'justifyLeft',
        action: 'justifyLeft',
        aria: 'left justify',
        tagNames: [],
        style: {
          prop: 'text-align',
          value: 'left'
        },
        contentDefault: '<b>L</b>',
        contentFA: '<i class="fa fa-align-left"></i>'
      },
      'justifyRight': {
        name: 'justifyRight',
        action: 'justifyRight',
        aria: 'right justify',
        tagNames: [],
        style: {
          prop: 'text-align',
          value: 'right'
        },
        contentDefault: '<b>R</b>',
        contentFA: '<i class="fa fa-align-right"></i>'
      },
      // Known inline elements that are not removed, or not removed consistantly across browsers:
      // <span>, <label>, <br>
      'removeFormat': {
        name: 'removeFormat',
        aria: 'remove formatting',
        action: 'removeFormat',
        contentDefault: '<b>X</b>',
        contentFA: '<i class="fa fa-eraser"></i>'
      },

      /***** Buttons for appending block elements (append-<element> action) *****/

      'quote': {
        name: 'quote',
        action: 'append-blockquote',
        aria: 'blockquote',
        tagNames: ['blockquote'],
        contentDefault: '<b>&ldquo;</b>',
        contentFA: '<i class="fa fa-quote-right"></i>'
      },
      'pre': {
        name: 'pre',
        action: 'append-pre',
        aria: 'preformatted text',
        tagNames: ['pre'],
        contentDefault: '<b>0101</b>',
        contentFA: '<i class="fa fa-code fa-lg"></i>'
      },
      'h1': {
        name: 'h1',
        action: 'append-h1',
        aria: 'header type one',
        tagNames: ['h1'],
        contentDefault: '<b>H1</b>',
        contentFA: '<i class="fa fa-header"><sup>1</sup>'
      },
      'h2': {
        name: 'h2',
        action: 'append-h2',
        aria: 'header type two',
        tagNames: ['h2'],
        contentDefault: '<b>H2</b>',
        contentFA: '<i class="fa fa-header"><sup>2</sup>'
      },
      'h3': {
        name: 'h3',
        action: 'append-h3',
        aria: 'header type three',
        tagNames: ['h3'],
        contentDefault: '<b>H3</b>',
        contentFA: '<i class="fa fa-header"><sup>3</sup>'
      },
      'h4': {
        name: 'h4',
        action: 'append-h4',
        aria: 'header type four',
        tagNames: ['h4'],
        contentDefault: '<b>H4</b>',
        contentFA: '<i class="fa fa-header"><sup>4</sup>'
      },
      'h5': {
        name: 'h5',
        action: 'append-h5',
        aria: 'header type five',
        tagNames: ['h5'],
        contentDefault: '<b>H5</b>',
        contentFA: '<i class="fa fa-header"><sup>5</sup>'
      },
      'h6': {
        name: 'h6',
        action: 'append-h6',
        aria: 'header type six',
        tagNames: ['h6'],
        contentDefault: '<b>H6</b>',
        contentFA: '<i class="fa fa-header"><sup>6</sup>'
      }
    };

  })();
  (function () {
    'use strict';

    /* Base functionality for an extension which will display
     * a 'form' inside the toolbar
     */
    var FormExtension = MediumEditor.extensions.button.extend({

      init: function () {
        MediumEditor.extensions.button.prototype.init.apply(this, arguments);
      },

      // default labels for the form buttons
      formSaveLabel: '&#10003;',
      formCloseLabel: '&times;',

      /* hasForm: [boolean]
         *
         * Setting this to true will cause getForm() to be called
         * when the toolbar is created, so the form can be appended
         * inside the toolbar container
         */
      hasForm: true,

      /* getForm: [function ()]
         *
         * When hasForm is true, this function must be implemented
         * and return a DOM Element which will be appended to
         * the toolbar container. The form should start hidden, and
         * the extension can choose when to hide/show it
         */
      getForm: function () {},

      /* isDisplayed: [function ()]
         *
         * This function should return true/false reflecting
         * whether the form is currently displayed
         */
      isDisplayed: function () {},

      /* hideForm: [function ()]
         *
         * This function should hide the form element inside
         * the toolbar container
         */
      hideForm: function () {},

      /************************ Helpers ************************
       * The following are helpers that are either set by MediumEditor
       * during initialization, or are helper methods which either
       * route calls to the MediumEditor instance or provide common
       * functionality for all form extensions
       *********************************************************/

      /* showToolbarDefaultActions: [function ()]
         *
         * Helper method which will turn back the toolbar after canceling
         * the customized form
         */
      showToolbarDefaultActions: function () {
        var toolbar = this.base.getExtensionByName('toolbar');
        if (toolbar) {
          toolbar.showToolbarDefaultActions();
        }
      },

      /* hideToolbarDefaultActions: [function ()]
         *
         * Helper function which will hide the default contents of the
         * toolbar, but leave the toolbar container in the same state
         * to allow a form to display its custom contents inside the toolbar
         */
      hideToolbarDefaultActions: function () {
        var toolbar = this.base.getExtensionByName('toolbar');
        if (toolbar) {
          toolbar.hideToolbarDefaultActions();
        }
      },

      /* setToolbarPosition: [function ()]
         *
         * Helper function which will update the size and position
         * of the toolbar based on the toolbar content and the current
         * position of the user's selection
         */
      setToolbarPosition: function () {
        var toolbar = this.base.getExtensionByName('toolbar');
        if (toolbar) {
          toolbar.setToolbarPosition();
        }
      }
    });

    MediumEditor.extensions.form = FormExtension;
  })();
  (function () {
    'use strict';

    var AnchorForm = MediumEditor.extensions.form.extend({
      /* Anchor Form Options */

      /* customClassOption: [string]  (previously options.anchorButton + options.anchorButtonClass)
         * Custom class name the user can optionally have added to their created links (ie 'button').
         * If passed as a non-empty string, a checkbox will be displayed allowing the user to choose
         * whether to have the class added to the created link or not.
         */
      customClassOption: null,

      /* customClassOptionText: [string]
         * text to be shown in the checkbox when the __customClassOption__ is being used.
         */
      customClassOptionText: 'Button',

      /* linkValidation: [boolean]  (previously options.checkLinkFormat)
         * enables/disables check for common URL protocols on anchor links.
         */
      linkValidation: false,

      /* placeholderText: [string]  (previously options.anchorInputPlaceholder)
         * text to be shown as placeholder of the anchor input.
         */
      placeholderText: 'Paste or type a link',

      /* targetCheckbox: [boolean]  (previously options.anchorTarget)
         * enables/disables displaying a "Open in new window" checkbox, which when checked
         * changes the `target` attribute of the created link.
         */
      targetCheckbox: false,

      /* targetCheckboxText: [string]  (previously options.anchorInputCheckboxLabel)
         * text to be shown in the checkbox enabled via the __targetCheckbox__ option.
         */
      targetCheckboxText: 'Open in new window',

      // Options for the Button base class
      name: 'anchor',
      action: 'createLink',
      aria: 'link',
      tagNames: ['a'],
      contentDefault: '<b>#</b>',
      contentFA: '<i class="fa fa-link"></i>',

      init: function () {
        MediumEditor.extensions.form.prototype.init.apply(this, arguments);

        this.subscribe('editableKeydown', this.handleKeydown.bind(this));
      },

      // Called when the button the toolbar is clicked
      // Overrides ButtonExtension.handleClick
      handleClick: function (event) {
        event.preventDefault();
        event.stopPropagation();

        var range = MediumEditor.selection.getSelectionRange(this.document);

        if (range.startContainer.nodeName.toLowerCase() === 'a' ||
          range.endContainer.nodeName.toLowerCase() === 'a' ||
          MediumEditor.util.getClosestTag(MediumEditor.selection.getSelectedParentElement(range), 'a')) {
          return this.execAction('unlink');
        }

        if (!this.isDisplayed()) {
          this.showForm();
        }

        return false;
      },

      // Called when user hits the defined shortcut (CTRL / COMMAND + K)
      handleKeydown: function (event) {
        if (MediumEditor.util.isKey(event, MediumEditor.util.keyCode.K) && MediumEditor.util.isMetaCtrlKey(event) && !event.shiftKey) {
          this.handleClick(event);
        }
      },

      // Called by medium-editor to append form to the toolbar
      getForm: function () {
        if (!this.form) {
          this.form = this.createForm();
        }
        return this.form;
      },

      getTemplate: function () {
        var template = [
          '<input type="text" class="medium-editor-toolbar-input" placeholder="', this.placeholderText, '">'
        ];

        template.push(
          '<a href="#" class="medium-editor-toolbar-save">',
          this.getEditorOption('buttonLabels') === 'fontawesome' ? '<i class="fa fa-check"></i>' : this.formSaveLabel,
          '</a>'
        );

        template.push('<a href="#" class="medium-editor-toolbar-close">',
          this.getEditorOption('buttonLabels') === 'fontawesome' ? '<i class="fa fa-times"></i>' : this.formCloseLabel,
          '</a>');

        // both of these options are slightly moot with the ability to
        // override the various form buildup/serialize functions.

        if (this.targetCheckbox) {
          // fixme: ideally, this targetCheckboxText would be a formLabel too,
          // figure out how to deprecate? also consider `fa-` icon default implcations.
          template.push(
            '<div class="medium-editor-toolbar-form-row">',
            '<input type="checkbox" class="medium-editor-toolbar-anchor-target">',
            '<label>',
            this.targetCheckboxText,
            '</label>',
            '</div>'
          );
        }

        if (this.customClassOption) {
          // fixme: expose this `Button` text as a formLabel property, too
          // and provide similar access to a `fa-` icon default.
          template.push(
            '<div class="medium-editor-toolbar-form-row">',
            '<input type="checkbox" class="medium-editor-toolbar-anchor-button">',
            '<label>',
            this.customClassOptionText,
            '</label>',
            '</div>'
          );
        }

        return template.join('');

      },

      // Used by medium-editor when the default toolbar is to be displayed
      isDisplayed: function () {
        return this.getForm().style.display === 'block';
      },

      hideForm: function () {
        this.getForm().style.display = 'none';
        this.getInput().value = '';
      },

      showForm: function (opts) {
        var input = this.getInput(),
          targetCheckbox = this.getAnchorTargetCheckbox(),
          buttonCheckbox = this.getAnchorButtonCheckbox();

        opts = opts || { url: '' };
        // TODO: This is for backwards compatability
        // We don't need to support the 'string' argument in 6.0.0
        if (typeof opts === 'string') {
          opts = {
            url: opts
          };
        }

        this.base.saveSelection();
        this.hideToolbarDefaultActions();
        this.getForm().style.display = 'block';
        this.setToolbarPosition();

        input.value = opts.url;
        input.focus();

        // If we have a target checkbox, we want it to be checked/unchecked
        // based on whether the existing link has target=_blank
        if (targetCheckbox) {
          targetCheckbox.checked = opts.target === '_blank';
        }

        // If we have a custom class checkbox, we want it to be checked/unchecked
        // based on whether an existing link already has the class
        if (buttonCheckbox) {
          var classList = opts.buttonClass ? opts.buttonClass.split(' ') : [];
          buttonCheckbox.checked = (classList.indexOf(this.customClassOption) !== -1);
        }
      },

      // Called by core when tearing down medium-editor (destroy)
      destroy: function () {
        if (!this.form) {
          return false;
        }

        if (this.form.parentNode) {
          this.form.parentNode.removeChild(this.form);
        }

        delete this.form;
      },

      // core methods

      getFormOpts: function () {
        // no notion of private functions? wanted `_getFormOpts`
        var targetCheckbox = this.getAnchorTargetCheckbox(),
          buttonCheckbox = this.getAnchorButtonCheckbox(),
          opts = {
            url: this.getInput().value.trim()
          };

        if (this.linkValidation) {
          opts.url = this.checkLinkFormat(opts.url);
        }

        opts.target = '_self';
        if (targetCheckbox && targetCheckbox.checked) {
          opts.target = '_blank';
        }

        if (buttonCheckbox && buttonCheckbox.checked) {
          opts.buttonClass = this.customClassOption;
        }

        return opts;
      },

      doFormSave: function () {
        var opts = this.getFormOpts();
        this.completeFormSave(opts);
      },

      completeFormSave: function (opts) {
        this.base.restoreSelection();
        this.execAction(this.action, opts);
        this.base.checkSelection();
      },

      checkLinkFormat: function (value) {
        var re = /^(https?|ftps?|rtmpt?):\/\/|mailto:/;
        return (re.test(value) ? '' : 'http://') + value;
      },

      doFormCancel: function () {
        this.base.restoreSelection();
        this.base.checkSelection();
      },

      // form creation and event handling
      attachFormEvents: function (form) {
        var close = form.querySelector('.medium-editor-toolbar-close'),
          save = form.querySelector('.medium-editor-toolbar-save'),
          input = form.querySelector('.medium-editor-toolbar-input');

        // Handle clicks on the form itself
        this.on(form, 'click', this.handleFormClick.bind(this));

        // Handle typing in the textbox
        this.on(input, 'keyup', this.handleTextboxKeyup.bind(this));

        // Handle close button clicks
        this.on(close, 'click', this.handleCloseClick.bind(this));

        // Handle save button clicks (capture)
        this.on(save, 'click', this.handleSaveClick.bind(this), true);

      },

      createForm: function () {
        var doc = this.document,
          form = doc.createElement('div');

        // Anchor Form (div)
        form.className = 'medium-editor-toolbar-form';
        form.id = 'medium-editor-toolbar-form-anchor-' + this.getEditorId();
        form.innerHTML = this.getTemplate();
        this.attachFormEvents(form);

        return form;
      },

      getInput: function () {
        return this.getForm().querySelector('input.medium-editor-toolbar-input');
      },

      getAnchorTargetCheckbox: function () {
        return this.getForm().querySelector('.medium-editor-toolbar-anchor-target');
      },

      getAnchorButtonCheckbox: function () {
        return this.getForm().querySelector('.medium-editor-toolbar-anchor-button');
      },

      handleTextboxKeyup: function (event) {
        // For ENTER -> create the anchor
        if (event.keyCode === MediumEditor.util.keyCode.ENTER) {
          event.preventDefault();
          this.doFormSave();
          return;
        }

        // For ESCAPE -> close the form
        if (event.keyCode === MediumEditor.util.keyCode.ESCAPE) {
          event.preventDefault();
          this.doFormCancel();
        }
      },

      handleFormClick: function (event) {
        // make sure not to hide form when clicking inside the form
        event.stopPropagation();
      },

      handleSaveClick: function (event) {
        // Clicking Save -> create the anchor
        event.preventDefault();
        this.doFormSave();
      },

      handleCloseClick: function (event) {
        // Click Close -> close the form
        event.preventDefault();
        this.doFormCancel();
      }
    });

    MediumEditor.extensions.anchor = AnchorForm;
  }());

  (function () {
    'use strict';

    var AnchorPreview = MediumEditor.Extension.extend({
      name: 'anchor-preview',

      // Anchor Preview Options

      /* hideDelay: [number]  (previously options.anchorPreviewHideDelay)
         * time in milliseconds to show the anchor tag preview after the mouse has left the anchor tag.
         */
      hideDelay: 500,

      /* previewValueSelector: [string]
         * the default selector to locate where to put the activeAnchor value in the preview
         */
      previewValueSelector: 'a',

      /* showWhenToolbarIsVisible: [boolean]
         * determines whether the anchor tag preview shows up when the toolbar is visible
         */
      showWhenToolbarIsVisible: false,

      init: function () {
        this.anchorPreview = this.createPreview();

        this.getEditorOption('elementsContainer').appendChild(this.anchorPreview);

        this.attachToEditables();
      },

      getPreviewElement: function () {
        return this.anchorPreview;
      },

      createPreview: function () {
        var el = this.document.createElement('div');

        el.id = 'medium-editor-anchor-preview-' + this.getEditorId();
        el.className = 'medium-editor-anchor-preview';
        el.innerHTML = this.getTemplate();

        this.on(el, 'click', this.handleClick.bind(this));

        return el;
      },

      getTemplate: function () {
        return '<div class="medium-editor-toolbar-anchor-preview" id="medium-editor-toolbar-anchor-preview">' +
          '    <a class="medium-editor-toolbar-anchor-preview-inner"></a>' +
          '</div>';
      },

      destroy: function () {
        if (this.anchorPreview) {
          if (this.anchorPreview.parentNode) {
            this.anchorPreview.parentNode.removeChild(this.anchorPreview);
          }
          delete this.anchorPreview;
        }
      },

      hidePreview: function () {
        this.anchorPreview.classList.remove('medium-editor-anchor-preview-active');
        this.activeAnchor = null;
      },

      showPreview: function (anchorEl) {
        if (this.anchorPreview.classList.contains('medium-editor-anchor-preview-active') ||
          anchorEl.getAttribute('data-disable-preview')) {
          return true;
        }

        if (this.previewValueSelector) {
          this.anchorPreview.querySelector(this.previewValueSelector).textContent = anchorEl.attributes.href.value;
          this.anchorPreview.querySelector(this.previewValueSelector).href = anchorEl.attributes.href.value;
        }

        this.anchorPreview.classList.add('medium-toolbar-arrow-over');
        this.anchorPreview.classList.remove('medium-toolbar-arrow-under');

        if (!this.anchorPreview.classList.contains('medium-editor-anchor-preview-active')) {
          this.anchorPreview.classList.add('medium-editor-anchor-preview-active');
        }

        this.activeAnchor = anchorEl;

        this.positionPreview();
        this.attachPreviewHandlers();

        return this;
      },

      positionPreview: function (activeAnchor) {
        activeAnchor = activeAnchor || this.activeAnchor;
        var buttonHeight = this.anchorPreview.offsetHeight,
          boundary = activeAnchor.getBoundingClientRect(),
          middleBoundary = (boundary.left + boundary.right) / 2,
          diffLeft = this.diffLeft,
          diffTop = this.diffTop,
          halfOffsetWidth,
          defaultLeft;

        halfOffsetWidth = this.anchorPreview.offsetWidth / 2;
        var toolbarExtension = this.base.getExtensionByName('toolbar');
        if (toolbarExtension) {
          diffLeft = toolbarExtension.diffLeft;
          diffTop = toolbarExtension.diffTop;
        }
        defaultLeft = diffLeft - halfOffsetWidth;

        this.anchorPreview.style.top = Math.round(buttonHeight + boundary.bottom - diffTop + this.window.pageYOffset - this.anchorPreview.offsetHeight) + 'px';
        if (middleBoundary < halfOffsetWidth) {
          this.anchorPreview.style.left = defaultLeft + halfOffsetWidth + 'px';
        } else if ((this.window.innerWidth - middleBoundary) < halfOffsetWidth) {
          this.anchorPreview.style.left = this.window.innerWidth + defaultLeft - halfOffsetWidth + 'px';
        } else {
          this.anchorPreview.style.left = defaultLeft + middleBoundary + 'px';
        }
      },

      attachToEditables: function () {
        this.subscribe('editableMouseover', this.handleEditableMouseover.bind(this));
      },

      handleClick: function (event) {
        var anchorExtension = this.base.getExtensionByName('anchor'),
          activeAnchor = this.activeAnchor;

        if (anchorExtension && activeAnchor) {
          event.preventDefault();

          this.base.selectElement(this.activeAnchor);

          // Using setTimeout + delay because:
          // We may actually be displaying the anchor form, which should be controlled by delay
          this.base.delay(function () {
            if (activeAnchor) {
              var opts = {
                url: activeAnchor.attributes.href.value,
                target: activeAnchor.getAttribute('target'),
                buttonClass: activeAnchor.getAttribute('class')
              };
              anchorExtension.showForm(opts);
              activeAnchor = null;
            }
          }.bind(this));
        }

        this.hidePreview();
      },

      handleAnchorMouseout: function () {
        this.anchorToPreview = null;
        this.off(this.activeAnchor, 'mouseout', this.instanceHandleAnchorMouseout);
        this.instanceHandleAnchorMouseout = null;
      },

      handleEditableMouseover: function (event) {
        var target = MediumEditor.util.getClosestTag(event.target, 'a');

        if (false === target) {
          return;
        }

        // Detect empty href attributes
        // The browser will make href="" or href="#top"
        // into absolute urls when accessed as event.target.href, so check the html
        if (!/href=["']\S+["']/.test(target.outerHTML) || /href=["']#\S+["']/.test(target.outerHTML)) {
          return true;
        }

        // only show when toolbar is not present
        var toolbar = this.base.getExtensionByName('toolbar');
        if (!this.showWhenToolbarIsVisible && toolbar && toolbar.isDisplayed && toolbar.isDisplayed()) {
          return true;
        }

        // detach handler for other anchor in case we hovered multiple anchors quickly
        if (this.activeAnchor && this.activeAnchor !== target) {
          this.detachPreviewHandlers();
        }

        this.anchorToPreview = target;

        this.instanceHandleAnchorMouseout = this.handleAnchorMouseout.bind(this);
        this.on(this.anchorToPreview, 'mouseout', this.instanceHandleAnchorMouseout);
        // Using setTimeout + delay because:
        // - We're going to show the anchor preview according to the configured delay
        //   if the mouse has not left the anchor tag in that time
        this.base.delay(function () {
          if (this.anchorToPreview) {
            this.showPreview(this.anchorToPreview);
          }
        }.bind(this));
      },

      handlePreviewMouseover: function () {
        this.lastOver = (new Date()).getTime();
        this.hovering = true;
      },

      handlePreviewMouseout: function (event) {
        if (!event.relatedTarget || !/anchor-preview/.test(event.relatedTarget.className)) {
          this.hovering = false;
        }
      },

      updatePreview: function () {
        if (this.hovering) {
          return true;
        }
        var durr = (new Date()).getTime() - this.lastOver;
        if (durr > this.hideDelay) {
          // hide the preview 1/2 second after mouse leaves the link
          this.detachPreviewHandlers();
        }
      },

      detachPreviewHandlers: function () {
        // cleanup
        clearInterval(this.intervalTimer);
        if (this.instanceHandlePreviewMouseover) {
          this.off(this.anchorPreview, 'mouseover', this.instanceHandlePreviewMouseover);
          this.off(this.anchorPreview, 'mouseout', this.instanceHandlePreviewMouseout);
          if (this.activeAnchor) {
            this.off(this.activeAnchor, 'mouseover', this.instanceHandlePreviewMouseover);
            this.off(this.activeAnchor, 'mouseout', this.instanceHandlePreviewMouseout);
          }
        }

        this.hidePreview();

        this.hovering = this.instanceHandlePreviewMouseover = this.instanceHandlePreviewMouseout = null;
      },

      // TODO: break up method and extract out handlers
      attachPreviewHandlers: function () {
        this.lastOver = (new Date()).getTime();
        this.hovering = true;

        this.instanceHandlePreviewMouseover = this.handlePreviewMouseover.bind(this);
        this.instanceHandlePreviewMouseout = this.handlePreviewMouseout.bind(this);

        this.intervalTimer = setInterval(this.updatePreview.bind(this), 200);

        this.on(this.anchorPreview, 'mouseover', this.instanceHandlePreviewMouseover);
        this.on(this.anchorPreview, 'mouseout', this.instanceHandlePreviewMouseout);
        this.on(this.activeAnchor, 'mouseover', this.instanceHandlePreviewMouseover);
        this.on(this.activeAnchor, 'mouseout', this.instanceHandlePreviewMouseout);
      }
    });

    MediumEditor.extensions.anchorPreview = AnchorPreview;
  }());

  (function () {
    'use strict';

    var WHITESPACE_CHARS,
      KNOWN_TLDS_FRAGMENT,
      LINK_REGEXP_TEXT,
      KNOWN_TLDS_REGEXP;

    WHITESPACE_CHARS = [' ', '\t', '\n', '\r', '\u00A0', '\u2000', '\u2001', '\u2002', '\u2003',
      '\u2028', '\u2029'];
    KNOWN_TLDS_FRAGMENT = 'com|net|org|edu|gov|mil|aero|asia|biz|cat|coop|info|int|jobs|mobi|museum|name|post|pro|tel|travel|' +
      'xxx|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|' +
      'bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cs|cu|cv|cx|cy|cz|dd|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|' +
      'fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|' +
      'is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|' +
      'mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|' +
      'pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|ja|sk|sl|sm|sn|so|sr|ss|st|su|sv|sx|sy|sz|tc|td|tf|tg|th|' +
      'tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw';

    LINK_REGEXP_TEXT =
      '(' +
      // Version of Gruber URL Regexp optimized for JS: http://stackoverflow.com/a/17733640
      '((?:(https?://|ftps?://|nntp://)|www\\d{0,3}[.]|[a-z0-9.\\-]+[.](' + KNOWN_TLDS_FRAGMENT + ')\\\/)\\S+(?:[^\\s`!\\[\\]{};:\'\".,?\u00AB\u00BB\u201C\u201D\u2018\u2019]))' +
      // Addition to above Regexp to support bare domains/one level subdomains with common non-i18n TLDs and without www prefix:
      ')|(([a-z0-9\\-]+\\.)?[a-z0-9\\-]+\\.(' + KNOWN_TLDS_FRAGMENT + '))';

    KNOWN_TLDS_REGEXP = new RegExp('^(' + KNOWN_TLDS_FRAGMENT + ')$', 'i');

    function nodeIsNotInsideAnchorTag(node) {
      return !MediumEditor.util.getClosestTag(node, 'a');
    }

    var AutoLink = MediumEditor.Extension.extend({
      init: function () {
        MediumEditor.Extension.prototype.init.apply(this, arguments);

        this.disableEventHandling = false;
        this.subscribe('editableKeypress', this.onKeypress.bind(this));
        this.subscribe('editableBlur', this.onBlur.bind(this));
        // MS IE has it's own auto-URL detect feature but ours is better in some ways. Be consistent.
        this.document.execCommand('AutoUrlDetect', false, false);
      },

      destroy: function () {
        // Turn AutoUrlDetect back on
        if (this.document.queryCommandSupported('AutoUrlDetect')) {
          this.document.execCommand('AutoUrlDetect', false, true);
        }
      },

      onBlur: function (blurEvent, editable) {
        this.performLinking(editable);
      },

      onKeypress: function (keyPressEvent) {
        if (this.disableEventHandling) {
          return;
        }

        if (MediumEditor.util.isKey(keyPressEvent, [MediumEditor.util.keyCode.SPACE, MediumEditor.util.keyCode.ENTER])) {
          clearTimeout(this.performLinkingTimeout);
          // Saving/restoring the selection in the middle of a keypress doesn't work well...
          this.performLinkingTimeout = setTimeout(function () {
            try {
              var sel = this.base.exportSelection();
              if (this.performLinking(keyPressEvent.target)) {
                // pass true for favorLaterSelectionAnchor - this is needed for links at the end of a
                // paragraph in MS IE, or MS IE causes the link to be deleted right after adding it.
                this.base.importSelection(sel, true);
              }
            } catch (e) {
              if (window.console) {
                window.console.error('Failed to perform linking', e);
              }
              this.disableEventHandling = true;
            }
          }.bind(this), 0);
        }
      },

      performLinking: function (contenteditable) {
        /*
            Perform linking on blockElement basis, blockElements are HTML elements with text content and without
            child element.

            Example:
            - HTML content
            <blockquote>
              <p>link.</p>
              <p>my</p>
            </blockquote>

            - blockElements
            [<p>link.</p>, <p>my</p>]

            otherwise the detection can wrongly find the end of one paragraph and the beginning of another paragraph
            to constitute a link, such as a paragraph ending "link." and the next paragraph beginning with "my" is
            interpreted into "link.my" and the code tries to create a link across blockElements - which doesn't work
            and is terrible.
            (Medium deletes the spaces/returns between P tags so the textContent ends up without paragraph spacing)
            */
        var blockElements = MediumEditor.util.splitByBlockElements(contenteditable),
          documentModified = false;
        if (blockElements.length === 0) {
          blockElements = [contenteditable];
        }
        for (var i = 0; i < blockElements.length; i++) {
          documentModified = this.removeObsoleteAutoLinkSpans(blockElements[i]) || documentModified;
          documentModified = this.performLinkingWithinElement(blockElements[i]) || documentModified;
        }
        return documentModified;
      },

      removeObsoleteAutoLinkSpans: function (element) {
        if (!element || element.nodeType === 3) {
          return false;
        }

        var spans = element.querySelectorAll('span[data-auto-link="true"]'),
          documentModified = false;

        for (var i = 0; i < spans.length; i++) {
          var textContent = spans[i].textContent;
          if (textContent.indexOf('://') === -1) {
            textContent = MediumEditor.util.ensureUrlHasProtocol(textContent);
          }
          if (spans[i].getAttribute('data-href') !== textContent && nodeIsNotInsideAnchorTag(spans[i])) {
            documentModified = true;
            var trimmedTextContent = textContent.replace(/\s+$/, '');
            if (spans[i].getAttribute('data-href') === trimmedTextContent) {
              var charactersTrimmed = textContent.length - trimmedTextContent.length,
                subtree = MediumEditor.util.splitOffDOMTree(spans[i], this.splitTextBeforeEnd(spans[i], charactersTrimmed));
              spans[i].parentNode.insertBefore(subtree, spans[i].nextSibling);
            } else {
              // Some editing has happened to the span, so just remove it entirely. The user can put it back
              // around just the href content if they need to prevent it from linking
              MediumEditor.util.unwrap(spans[i], this.document);
            }
          }
        }
        return documentModified;
      },

      splitTextBeforeEnd: function (element, characterCount) {
        var treeWalker = this.document.createTreeWalker(element, NodeFilter.SHOW_TEXT, null, false),
          lastChildNotExhausted = true;

        // Start the tree walker at the last descendant of the span
        while (lastChildNotExhausted) {
          lastChildNotExhausted = treeWalker.lastChild() !== null;
        }

        var currentNode,
          currentNodeValue,
          previousNode;
        while (characterCount > 0 && previousNode !== null) {
          currentNode = treeWalker.currentNode;
          currentNodeValue = currentNode.nodeValue;
          if (currentNodeValue.length > characterCount) {
            previousNode = currentNode.splitText(currentNodeValue.length - characterCount);
            characterCount = 0;
          } else {
            previousNode = treeWalker.previousNode();
            characterCount -= currentNodeValue.length;
          }
        }
        return previousNode;
      },

      performLinkingWithinElement: function (element) {
        var matches = this.findLinkableText(element),
          linkCreated = false;

        for (var matchIndex = 0; matchIndex < matches.length; matchIndex++) {
          var matchingTextNodes = MediumEditor.util.findOrCreateMatchingTextNodes(this.document, element,
            matches[matchIndex]);
          if (this.shouldNotLink(matchingTextNodes)) {
            continue;
          }
          this.createAutoLink(matchingTextNodes, matches[matchIndex].href);
        }
        return linkCreated;
      },

      shouldNotLink: function (textNodes) {
        var shouldNotLink = false;
        for (var i = 0; i < textNodes.length && shouldNotLink === false; i++) {
          // Do not link if the text node is either inside an anchor or inside span[data-auto-link]
          shouldNotLink = !!MediumEditor.util.traverseUp(textNodes[i], function (node) {
            return node.nodeName.toLowerCase() === 'a' ||
              (node.getAttribute && node.getAttribute('data-auto-link') === 'true');
          });
        }
        return shouldNotLink;
      },

      findLinkableText: function (contenteditable) {
        var linkRegExp = new RegExp(LINK_REGEXP_TEXT, 'gi'),
          textContent = contenteditable.textContent,
          match = null,
          matches = [];

        while ((match = linkRegExp.exec(textContent)) !== null) {
          var matchOk = true,
            matchEnd = match.index + match[0].length;
          // If the regexp detected something as a link that has text immediately preceding/following it, bail out.
          matchOk = (match.index === 0 || WHITESPACE_CHARS.indexOf(textContent[match.index - 1]) !== -1) &&
            (matchEnd === textContent.length || WHITESPACE_CHARS.indexOf(textContent[matchEnd]) !== -1);
          // If the regexp detected a bare domain that doesn't use one of our expected TLDs, bail out.
          matchOk = matchOk && (match[0].indexOf('/') !== -1 ||
            KNOWN_TLDS_REGEXP.test(match[0].split('.').pop().split('?').shift()));

          if (matchOk) {
            matches.push({
              href: match[0],
              start: match.index,
              end: matchEnd
            });
          }
        }
        return matches;
      },

      createAutoLink: function (textNodes, href) {
        href = MediumEditor.util.ensureUrlHasProtocol(href);
        var anchor = MediumEditor.util.createLink(this.document, textNodes, href, this.getEditorOption('targetBlank') ? '_blank' : null),
          span = this.document.createElement('span');
        span.setAttribute('data-auto-link', 'true');
        span.setAttribute('data-href', href);
        anchor.insertBefore(span, anchor.firstChild);
        while (anchor.childNodes.length > 1) {
          span.appendChild(anchor.childNodes[1]);
        }
      }

    });

    MediumEditor.extensions.autoLink = AutoLink;
  }());

  (function () {
    'use strict';

    var CLASS_DRAG_OVER = 'medium-editor-dragover';

    function clearClassNames(element) {
      var editable = MediumEditor.util.getContainerEditorElement(element),
        existing = Array.prototype.slice.call(editable.parentElement.querySelectorAll('.' + CLASS_DRAG_OVER));

      existing.forEach(function (el) {
        el.classList.remove(CLASS_DRAG_OVER);
      });
    }

    var FileDragging = MediumEditor.Extension.extend({
      name: 'fileDragging',

      allowedTypes: ['image'],

      init: function () {
        MediumEditor.Extension.prototype.init.apply(this, arguments);

        this.subscribe('editableDrag', this.handleDrag.bind(this));
        this.subscribe('editableDrop', this.handleDrop.bind(this));
      },

      handleDrag: function (event) {
        event.preventDefault();
        event.dataTransfer.dropEffect = 'copy';

        var target = event.target.classList ? event.target : event.target.parentElement;

        // Ensure the class gets removed from anything that had it before
        clearClassNames(target);

        if (event.type === 'dragover') {
          target.classList.add(CLASS_DRAG_OVER);
        }
      },

      handleDrop: function (event) {
        // Prevent file from opening in the current window
        event.preventDefault();
        event.stopPropagation();

        // IE9 does not support the File API, so prevent file from opening in the window
        // but also don't try to actually get the file
        if (event.dataTransfer.files) {
          Array.prototype.slice.call(event.dataTransfer.files).forEach(function (file) {
            if (this.isAllowedFile(file)) {
              if (file.type.match('image')) {
                this.insertImageFile(file);
              }
            }
          }, this);
        }

        // Make sure we remove our class from everything
        clearClassNames(event.target);
      },

      isAllowedFile: function (file) {
        return this.allowedTypes.some(function (fileType) {
          return !!file.type.match(fileType);
        });
      },

      insertImageFile: function (file) {
        var fileReader = new FileReader();
        fileReader.readAsDataURL(file);

        var id = 'medium-img-' + (+new Date());
        MediumEditor.util.insertHTMLCommand(this.document, '<img class="medium-editor-image-loading" id="' + id + '" />');

        fileReader.onload = function () {
          var img = this.document.getElementById(id);
          if (img) {
            img.removeAttribute('id');
            img.removeAttribute('class');
            img.src = fileReader.result;
          }
        }.bind(this);
      }
    });

    MediumEditor.extensions.fileDragging = FileDragging;
  }());

  (function () {
    'use strict';

    var KeyboardCommands = MediumEditor.Extension.extend({
      name: 'keyboard-commands',

      /* KeyboardCommands Options */

      /* commands: [Array]
         * Array of objects describing each command and the combination of keys that will trigger it
         * Required for each object:
         *   command [String] (argument passed to editor.execAction())
         *   key [String] (keyboard character that triggers this command)
         *   meta [boolean] (whether the ctrl/meta key has to be active or inactive)
         *   shift [boolean] (whether the shift key has to be active or inactive)
         *   alt [boolean] (whether the alt key has to be active or inactive)
         */
      commands: [
        {
          command: 'bold',
          key: 'B',
          meta: true,
          shift: false,
          alt: false
        },
        {
          command: 'italic',
          key: 'I',
          meta: true,
          shift: false,
          alt: false
        },
        {
          command: 'underline',
          key: 'U',
          meta: true,
          shift: false,
          alt: false
        }
      ],

      init: function () {
        MediumEditor.Extension.prototype.init.apply(this, arguments);

        this.subscribe('editableKeydown', this.handleKeydown.bind(this));
        this.keys = {};
        this.commands.forEach(function (command) {
          var keyCode = command.key.charCodeAt(0);
          if (!this.keys[keyCode]) {
            this.keys[keyCode] = [];
          }
          this.keys[keyCode].push(command);
        }, this);
      },

      handleKeydown: function (event) {
        var keyCode = MediumEditor.util.getKeyCode(event);
        if (!this.keys[keyCode]) {
          return;
        }

        var isMeta = MediumEditor.util.isMetaCtrlKey(event),
          isShift = !!event.shiftKey,
          isAlt = !!event.altKey;

        this.keys[keyCode].forEach(function (data) {
          if (data.meta === isMeta &&
            data.shift === isShift &&
            (data.alt === isAlt ||
              undefined === data.alt)) { // TODO deprecated: remove check for undefined === data.alt when jumping to 6.0.0
            event.preventDefault();
            event.stopPropagation();

            // command can be false so the shortcut is just disabled
            if (false !== data.command) {
              this.execAction(data.command);
            }
          }
        }, this);
      }
    });

    MediumEditor.extensions.keyboardCommands = KeyboardCommands;
  }());

  (function () {
    'use strict';

    var FontNameForm = MediumEditor.extensions.form.extend({

      name: 'fontname',
      action: 'fontName',
      aria: 'change font name',
      contentDefault: '&#xB1;', // ±
      contentFA: '<i class="fa fa-font"></i>',

      fonts: ['', 'Arial', 'Verdana', 'Times New Roman'],

      init: function () {
        MediumEditor.extensions.form.prototype.init.apply(this, arguments);
      },

      // Called when the button the toolbar is clicked
      // Overrides ButtonExtension.handleClick
      handleClick: function (event) {
        event.preventDefault();
        event.stopPropagation();

        if (!this.isDisplayed()) {
          // Get FontName of current selection (convert to string since IE returns this as number)
          var fontName = this.document.queryCommandValue('fontName') + '';
          this.showForm(fontName);
        }

        return false;
      },

      // Called by medium-editor to append form to the toolbar
      getForm: function () {
        if (!this.form) {
          this.form = this.createForm();
        }
        return this.form;
      },

      // Used by medium-editor when the default toolbar is to be displayed
      isDisplayed: function () {
        return this.getForm().style.display === 'block';
      },

      hideForm: function () {
        this.getForm().style.display = 'none';
        this.getSelect().value = '';
      },

      showForm: function (fontName) {
        var select = this.getSelect();

        this.base.saveSelection();
        this.hideToolbarDefaultActions();
        this.getForm().style.display = 'block';
        this.setToolbarPosition();

        select.value = fontName || '';
        select.focus();
      },

      // Called by core when tearing down medium-editor (destroy)
      destroy: function () {
        if (!this.form) {
          return false;
        }

        if (this.form.parentNode) {
          this.form.parentNode.removeChild(this.form);
        }

        delete this.form;
      },

      // core methods

      doFormSave: function () {
        this.base.restoreSelection();
        this.base.checkSelection();
      },

      doFormCancel: function () {
        this.base.restoreSelection();
        this.clearFontName();
        this.base.checkSelection();
      },

      // form creation and event handling
      createForm: function () {
        var doc = this.document,
          form = doc.createElement('div'),
          select = doc.createElement('select'),
          close = doc.createElement('a'),
          save = doc.createElement('a'),
          option;

        // Font Name Form (div)
        form.className = 'medium-editor-toolbar-form';
        form.id = 'medium-editor-toolbar-form-fontname-' + this.getEditorId();

        // Handle clicks on the form itself
        this.on(form, 'click', this.handleFormClick.bind(this));

        // Add font names
        for (var i = 0; i<this.fonts.length; i++) {
          option = doc.createElement('option');
          option.innerHTML = this.fonts[i];
          option.value = this.fonts[i];
          select.appendChild(option);
        }

        select.className = 'medium-editor-toolbar-select';
        form.appendChild(select);

        // Handle typing in the textbox
        this.on(select, 'change', this.handleFontChange.bind(this));

        // Add save buton
        save.setAttribute('href', '#');
        save.className = 'medium-editor-toobar-save';
        save.innerHTML = this.getEditorOption('buttonLabels') === 'fontawesome' ?
          '<i class="fa fa-check"></i>' :
          '&#10003;';
        form.appendChild(save);

        // Handle save button clicks (capture)
        this.on(save, 'click', this.handleSaveClick.bind(this), true);

        // Add close button
        close.setAttribute('href', '#');
        close.className = 'medium-editor-toobar-close';
        close.innerHTML = this.getEditorOption('buttonLabels') === 'fontawesome' ?
          '<i class="fa fa-times"></i>' :
          '&times;';
        form.appendChild(close);

        // Handle close button clicks
        this.on(close, 'click', this.handleCloseClick.bind(this));

        return form;
      },

      getSelect: function () {
        return this.getForm().querySelector('select.medium-editor-toolbar-select');
      },

      clearFontName: function () {
        MediumEditor.selection.getSelectedElements(this.document).forEach(function (el) {
          if (el.nodeName.toLowerCase() === 'font' && el.hasAttribute('face')) {
            el.removeAttribute('face');
          }
        });
      },

      handleFontChange: function () {
        var font = this.getSelect().value;
        if (font === '') {
          this.clearFontName();
        } else {
          this.execAction('fontName', { name: font });
        }
      },

      handleFormClick: function (event) {
        // make sure not to hide form when clicking inside the form
        event.stopPropagation();
      },

      handleSaveClick: function (event) {
        // Clicking Save -> create the font size
        event.preventDefault();
        this.doFormSave();
      },

      handleCloseClick: function (event) {
        // Click Close -> close the form
        event.preventDefault();
        this.doFormCancel();
      }
    });

    MediumEditor.extensions.fontName = FontNameForm;
  }());

  (function () {
    'use strict';

    var FontSizeForm = MediumEditor.extensions.form.extend({

      name: 'fontsize',
      action: 'fontSize',
      aria: 'increase/decrease font size',
      contentDefault: '&#xB1;', // ±
      contentFA: '<i class="fa fa-text-height"></i>',

      init: function () {
        MediumEditor.extensions.form.prototype.init.apply(this, arguments);
      },

      // Called when the button the toolbar is clicked
      // Overrides ButtonExtension.handleClick
      handleClick: function (event) {
        event.preventDefault();
        event.stopPropagation();

        if (!this.isDisplayed()) {
          // Get fontsize of current selection (convert to string since IE returns this as number)
          var fontSize = this.document.queryCommandValue('fontSize') + '';
          this.showForm(fontSize);
        }

        return false;
      },

      // Called by medium-editor to append form to the toolbar
      getForm: function () {
        if (!this.form) {
          this.form = this.createForm();
        }
        return this.form;
      },

      // Used by medium-editor when the default toolbar is to be displayed
      isDisplayed: function () {
        return this.getForm().style.display === 'block';
      },

      hideForm: function () {
        this.getForm().style.display = 'none';
        this.getInput().value = '';
      },

      showForm: function (fontSize) {
        var input = this.getInput();

        this.base.saveSelection();
        this.hideToolbarDefaultActions();
        this.getForm().style.display = 'block';
        this.setToolbarPosition();

        input.value = fontSize || '';
        input.focus();
      },

      // Called by core when tearing down medium-editor (destroy)
      destroy: function () {
        if (!this.form) {
          return false;
        }

        if (this.form.parentNode) {
          this.form.parentNode.removeChild(this.form);
        }

        delete this.form;
      },

      // core methods

      doFormSave: function () {
        this.base.restoreSelection();
        this.base.checkSelection();
      },

      doFormCancel: function () {
        this.base.restoreSelection();
        this.clearFontSize();
        this.base.checkSelection();
      },

      // form creation and event handling
      createForm: function () {
        var doc = this.document,
          form = doc.createElement('div'),
          input = doc.createElement('input'),
          close = doc.createElement('a'),
          save = doc.createElement('a');

        // Font Size Form (div)
        form.className = 'medium-editor-toolbar-form';
        form.id = 'medium-editor-toolbar-form-fontsize-' + this.getEditorId();

        // Handle clicks on the form itself
        this.on(form, 'click', this.handleFormClick.bind(this));

        // Add font size slider
        input.setAttribute('type', 'range');
        input.setAttribute('min', '1');
        input.setAttribute('max', '7');
        input.className = 'medium-editor-toolbar-input';
        form.appendChild(input);

        // Handle typing in the textbox
        this.on(input, 'change', this.handleSliderChange.bind(this));

        // Add save buton
        save.setAttribute('href', '#');
        save.className = 'medium-editor-toobar-save';
        save.innerHTML = this.getEditorOption('buttonLabels') === 'fontawesome' ?
          '<i class="fa fa-check"></i>' :
          '&#10003;';
        form.appendChild(save);

        // Handle save button clicks (capture)
        this.on(save, 'click', this.handleSaveClick.bind(this), true);

        // Add close button
        close.setAttribute('href', '#');
        close.className = 'medium-editor-toobar-close';
        close.innerHTML = this.getEditorOption('buttonLabels') === 'fontawesome' ?
          '<i class="fa fa-times"></i>' :
          '&times;';
        form.appendChild(close);

        // Handle close button clicks
        this.on(close, 'click', this.handleCloseClick.bind(this));

        return form;
      },

      getInput: function () {
        return this.getForm().querySelector('input.medium-editor-toolbar-input');
      },

      clearFontSize: function () {
        MediumEditor.selection.getSelectedElements(this.document).forEach(function (el) {
          if (el.nodeName.toLowerCase() === 'font' && el.hasAttribute('size')) {
            el.removeAttribute('size');
          }
        });
      },

      handleSliderChange: function () {
        var size = this.getInput().value;
        if (size === '4') {
          this.clearFontSize();
        } else {
          this.execAction('fontSize', { size: size });
        }
      },

      handleFormClick: function (event) {
        // make sure not to hide form when clicking inside the form
        event.stopPropagation();
      },

      handleSaveClick: function (event) {
        // Clicking Save -> create the font size
        event.preventDefault();
        this.doFormSave();
      },

      handleCloseClick: function (event) {
        // Click Close -> close the form
        event.preventDefault();
        this.doFormCancel();
      }
    });

    MediumEditor.extensions.fontSize = FontSizeForm;
  }());
  (function () {
    'use strict';
    /*jslint regexp: true*/
    /*
        jslint does not allow character negation, because the negation
        will not match any unicode characters. In the regexes in this
        block, negation is used specifically to match the end of an html
        tag, and in fact unicode characters *should* be allowed.
    */
    function createReplacements() {
      return [
        // replace two bogus tags that begin pastes from google docs
        [new RegExp(/<[^>]*docs-internal-guid[^>]*>/gi), ''],
        [new RegExp(/<\/b>(<br[^>]*>)?$/gi), ''],

        // un-html spaces and newlines inserted by OS X
        [new RegExp(/<span class="Apple-converted-space">\s+<\/span>/g), ' '],
        [new RegExp(/<br class="Apple-interchange-newline">/g), '<br>'],

        // replace google docs italics+bold with a span to be replaced once the html is inserted
        [new RegExp(/<span[^>]*(font-style:italic;font-weight:bold|font-weight:bold;font-style:italic)[^>]*>/gi), '<span class="replace-with italic bold">'],

        // replace google docs italics with a span to be replaced once the html is inserted
        [new RegExp(/<span[^>]*font-style:italic[^>]*>/gi), '<span class="replace-with italic">'],

        //[replace google docs bolds with a span to be replaced once the html is inserted
        [new RegExp(/<span[^>]*font-weight:bold[^>]*>/gi), '<span class="replace-with bold">'],

        // replace manually entered b/i/a tags with real ones
        [new RegExp(/&lt;(\/?)(i|b|a)&gt;/gi), '<$1$2>'],

        // replace manually a tags with real ones, converting smart-quotes from google docs
        [new RegExp(/&lt;a(?:(?!href).)+href=(?:&quot;|&rdquo;|&ldquo;|"|“|”)(((?!&quot;|&rdquo;|&ldquo;|"|“|”).)*)(?:&quot;|&rdquo;|&ldquo;|"|“|”)(?:(?!&gt;).)*&gt;/gi), '<a href="$1">'],

        // Newlines between paragraphs in html have no syntactic value,
        // but then have a tendency to accidentally become additional paragraphs down the line
        [new RegExp(/<\/p>\n+/gi), '</p>'],
        [new RegExp(/\n+<p/gi), '<p'],

        // Microsoft Word makes these odd tags, like <o:p></o:p>
        [new RegExp(/<\/?o:[a-z]*>/gi), ''],

        // cleanup comments added by Chrome when pasting html
        ['<!--EndFragment-->', ''],
        ['<!--StartFragment-->', '']
      ];
    }
    /*jslint regexp: false*/

    var PasteHandler = MediumEditor.Extension.extend({
      /* Paste Options */

      /* forcePlainText: [boolean]
         * Forces pasting as plain text.
         */
      forcePlainText: true,

      /* cleanPastedHTML: [boolean]
         * cleans pasted content from different sources, like google docs etc.
         */
      cleanPastedHTML: false,

      /* cleanReplacements: [Array]
         * custom pairs (2 element arrays) of RegExp and replacement text to use during paste when
         * __forcePlainText__ or __cleanPastedHTML__ are `true` OR when calling `cleanPaste(text)` helper method.
         */
      cleanReplacements: [],

      /* cleanAttrs:: [Array]
         * list of element attributes to remove during paste when __cleanPastedHTML__ is `true` or when
         * calling `cleanPaste(text)` or `pasteHTML(html, options)` helper methods.
         */
      cleanAttrs: ['class', 'style', 'dir'],

      /* cleanTags: [Array]
         * list of element tag names to remove during paste when __cleanPastedHTML__ is `true` or when
         * calling `cleanPaste(text)` or `pasteHTML(html, options)` helper methods.
         */
      cleanTags: ['meta'],

      init: function () {
        MediumEditor.Extension.prototype.init.apply(this, arguments);

        if (this.forcePlainText || this.cleanPastedHTML) {
          this.subscribe('editablePaste', this.handlePaste.bind(this));
        }
      },

      handlePaste: function (event, element) {
        var paragraphs,
          html = '',
          p,
          dataFormatHTML = 'text/html',
          dataFormatPlain = 'text/plain',
          pastedHTML,
          pastedPlain;

        if (this.window.clipboardData && event.clipboardData === undefined) {
          event.clipboardData = this.window.clipboardData;
          // If window.clipboardData exists, but event.clipboardData doesn't exist,
          // we're probably in IE. IE only has two possibilities for clipboard
          // data format: 'Text' and 'URL'.
          //
          // Of the two, we want 'Text':
          dataFormatHTML = 'Text';
          dataFormatPlain = 'Text';
        }

        if (event.clipboardData &&
          event.clipboardData.getData &&
          !event.defaultPrevented) {
          event.preventDefault();

          pastedHTML = event.clipboardData.getData(dataFormatHTML);
          pastedPlain = event.clipboardData.getData(dataFormatPlain);

          if (this.cleanPastedHTML && pastedHTML) {
            return this.cleanPaste(pastedHTML);
          }

          if (!(this.getEditorOption('disableReturn') || element.getAttribute('data-disable-return'))) {
            paragraphs = pastedPlain.split(/[\r\n]+/g);
            // If there are no \r\n in data, don't wrap in <p>
            if (paragraphs.length > 1) {
              for (p = 0; p < paragraphs.length; p += 1) {
                if (paragraphs[p] !== '') {
                  html += '<p>' + MediumEditor.util.htmlEntities(paragraphs[p]) + '</p>';
                }
              }
            } else {
              html = MediumEditor.util.htmlEntities(paragraphs[0]);
            }
          } else {
            html = MediumEditor.util.htmlEntities(pastedPlain);
          }
          MediumEditor.util.insertHTMLCommand(this.document, html);
        }
      },

      cleanPaste: function (text) {
        var i, elList, tmp, workEl,
          multiline = /<p|<br|<div/.test(text),
          replacements = createReplacements().concat(this.cleanReplacements || []);

        for (i = 0; i < replacements.length; i += 1) {
          text = text.replace(replacements[i][0], replacements[i][1]);
        }

        if (!multiline) {
          return this.pasteHTML(text);
        }

        // create a temporary div to cleanup block elements
        tmp = this.document.createElement('div');

        // double br's aren't converted to p tags, but we want paragraphs.
        tmp.innerHTML = '<p>' + text.split('<br><br>').join('</p><p>') + '</p>';

        // block element cleanup
        elList = tmp.querySelectorAll('a,p,div,br');
        for (i = 0; i < elList.length; i += 1) {
          workEl = elList[i];

          // Microsoft Word replaces some spaces with newlines.
          // While newlines between block elements are meaningless, newlines within
          // elements are sometimes actually spaces.
          workEl.innerHTML = workEl.innerHTML.replace(/\n/gi, ' ');

          switch (workEl.nodeName.toLowerCase()) {
            case 'p':
            case 'div':
              this.filterCommonBlocks(workEl);
              break;
            case 'br':
              this.filterLineBreak(workEl);
              break;
          }
        }

        this.pasteHTML(tmp.innerHTML);
      },

      pasteHTML: function (html, options) {
        options = MediumEditor.util.defaults({}, options, {
          cleanAttrs: this.cleanAttrs,
          cleanTags: this.cleanTags
        });

        var elList, workEl, i, fragmentBody, pasteBlock = this.document.createDocumentFragment();

        pasteBlock.appendChild(this.document.createElement('body'));

        fragmentBody = pasteBlock.querySelector('body');
        fragmentBody.innerHTML = html;

        this.cleanupSpans(fragmentBody);

        elList = fragmentBody.querySelectorAll('*');
        for (i = 0; i < elList.length; i += 1) {
          workEl = elList[i];

          if ('a' === workEl.nodeName.toLowerCase() && this.getEditorOption('targetBlank')) {
            MediumEditor.util.setTargetBlank(workEl);
          }

          MediumEditor.util.cleanupAttrs(workEl, options.cleanAttrs);
          MediumEditor.util.cleanupTags(workEl, options.cleanTags);
        }

        MediumEditor.util.insertHTMLCommand(this.document, fragmentBody.innerHTML.replace(/&nbsp;/g, ' '));
      },

      isCommonBlock: function (el) {
        return (el && (el.nodeName.toLowerCase() === 'p' || el.nodeName.toLowerCase() === 'div'));
      },

      filterCommonBlocks: function (el) {
        if (/^\s*$/.test(el.textContent) && el.parentNode) {
          el.parentNode.removeChild(el);
        }
      },

      filterLineBreak: function (el) {
        if (this.isCommonBlock(el.previousElementSibling)) {
          // remove stray br's following common block elements
          this.removeWithParent(el);
        } else if (this.isCommonBlock(el.parentNode) && (el.parentNode.firstChild === el || el.parentNode.lastChild === el)) {
          // remove br's just inside open or close tags of a div/p
          this.removeWithParent(el);
        } else if (el.parentNode && el.parentNode.childElementCount === 1 && el.parentNode.textContent === '') {
          // and br's that are the only child of elements other than div/p
          this.removeWithParent(el);
        }
      },

      // remove an element, including its parent, if it is the only element within its parent
      removeWithParent: function (el) {
        if (el && el.parentNode) {
          if (el.parentNode.parentNode && el.parentNode.childElementCount === 1) {
            el.parentNode.parentNode.removeChild(el.parentNode);
          } else {
            el.parentNode.removeChild(el);
          }
        }
      },

      cleanupSpans: function (containerEl) {
        var i,
          el,
          newEl,
          spans = containerEl.querySelectorAll('.replace-with'),
          isCEF = function (el) {
            return (el && el.nodeName !== '#text' && el.getAttribute('contenteditable') === 'false');
          };

        for (i = 0; i < spans.length; i += 1) {
          el = spans[i];
          newEl = this.document.createElement(el.classList.contains('bold') ? 'b' : 'i');

          if (el.classList.contains('bold') && el.classList.contains('italic')) {
            // add an i tag as well if this has both italics and bold
            newEl.innerHTML = '<i>' + el.innerHTML + '</i>';
          } else {
            newEl.innerHTML = el.innerHTML;
          }
          el.parentNode.replaceChild(newEl, el);
        }

        spans = containerEl.querySelectorAll('span');
        for (i = 0; i < spans.length; i += 1) {
          el = spans[i];

          // bail if span is in contenteditable = false
          if (MediumEditor.util.traverseUp(el, isCEF)) {
            return false;
          }

          // remove empty spans, replace others with their contents
          MediumEditor.util.unwrap(el, this.document);
        }
      }
    });

    MediumEditor.extensions.paste = PasteHandler;
  }());

  (function () {
    'use strict';

    var Placeholder = MediumEditor.Extension.extend({
      name: 'placeholder',

      /* Placeholder Options */

      /* text: [string]
         * Text to display in the placeholder
         */
      text: 'Type your text',

      /* hideOnClick: [boolean]
         * Should we hide the placeholder on click (true) or when user starts typing (false)
         */
      hideOnClick: true,

      init: function () {
        MediumEditor.Extension.prototype.init.apply(this, arguments);

        this.initPlaceholders();
        this.attachEventHandlers();
      },

      initPlaceholders: function () {
        this.getEditorElements().forEach(function (el) {
          if (!el.getAttribute('data-placeholder')) {
            el.setAttribute('data-placeholder', this.text);
          }
          this.updatePlaceholder(el);
        }, this);
      },

      destroy: function () {
        this.getEditorElements().forEach(function (el) {
          if (el.getAttribute('data-placeholder') === this.text) {
            el.removeAttribute('data-placeholder');
          }
        }, this);
      },

      showPlaceholder: function (el) {
        if (el) {
          el.classList.add('medium-editor-placeholder');
        }
      },

      hidePlaceholder: function (el) {
        if (el) {
          el.classList.remove('medium-editor-placeholder');
        }
      },

      updatePlaceholder: function (el, dontShow) {
        // If the element has content, hide the placeholder
        if (el.querySelector('img, blockquote, ul, ol') || (el.textContent.replace(/^\s+|\s+$/g, '') !== '')) {
          return this.hidePlaceholder(el);
        }

        if (!dontShow) {
          this.showPlaceholder(el);
        }
      },

      attachEventHandlers: function () {
        if (this.hideOnClick) {
          // For the 'hideOnClick' option, the placeholder should always be hidden on focus
          this.subscribe('focus', this.handleFocus.bind(this));
        }

        // If the editor has content, it should always hide the placeholder
        this.subscribe('editableInput', this.handleInput.bind(this));

        // When the editor loses focus, check if the placeholder should be visible
        this.subscribe('blur', this.handleBlur.bind(this));
      },

      handleInput: function (event, element) {
        // If the placeholder should be hidden on focus and the
        // element has focus, don't show the placeholder
        var dontShow = this.hideOnClick && (element === this.base.getFocusedElement());

        // Editor's content has changed, check if the placeholder should be hidden
        this.updatePlaceholder(element, dontShow);
      },

      handleFocus: function (event, element) {
        // Editor has focus, hide the placeholder
        this.hidePlaceholder(element);
      },

      handleBlur: function (event, element) {
        // Editor has lost focus, check if the placeholder should be shown
        this.updatePlaceholder(element);
      }
    });

    MediumEditor.extensions.placeholder = Placeholder;
  }());

  (function () {
    'use strict';

    var Toolbar = MediumEditor.Extension.extend({
      name: 'toolbar',

      /* Toolbar Options */

      /* align: ['left'|'center'|'right']
         * When the __static__ option is true, this aligns the static toolbar
         * relative to the medium-editor element.
         */
      align: 'center',

      /* allowMultiParagraphSelection: [boolean]
         * enables/disables whether the toolbar should be displayed when
         * selecting multiple paragraphs/block elements
         */
      allowMultiParagraphSelection: true,

      /* buttons: [Array]
         * the names of the set of buttons to display on the toolbar.
         */
      buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],

      /* diffLeft: [Number]
         * value in pixels to be added to the X axis positioning of the toolbar.
         */
      diffLeft: 0,

      /* diffTop: [Number]
         * value in pixels to be added to the Y axis positioning of the toolbar.
         */
      diffTop: -10,

      /* firstButtonClass: [string]
         * CSS class added to the first button in the toolbar.
         */
      firstButtonClass: 'medium-editor-button-first',

      /* lastButtonClass: [string]
         * CSS class added to the last button in the toolbar.
         */
      lastButtonClass: 'medium-editor-button-last',

      /* standardizeSelectionStart: [boolean]
         * enables/disables standardizing how the beginning of a range is decided
         * between browsers whenever the selected text is analyzed for updating toolbar buttons status.
         */
      standardizeSelectionStart: false,

      /* static: [boolean]
         * enable/disable the toolbar always displaying in the same location
         * relative to the medium-editor element.
         */
      static: false,

      /* sticky: [boolean]
         * When the __static__ option is true, this enables/disables the toolbar
         * "sticking" to the viewport and staying visible on the screen while
         * the page scrolls.
         */
      sticky: false,

      /* updateOnEmptySelection: [boolean]
         * When the __static__ option is true, this enables/disables updating
         * the state of the toolbar buttons even when the selection is collapsed
         * (there is no selection, just a cursor).
         */
      updateOnEmptySelection: false,

      /* relativeContainer: [node]
         * appending the toolbar to a given node instead of body
         */
      relativeContainer: null,

      init: function () {
        MediumEditor.Extension.prototype.init.apply(this, arguments);

        this.initThrottledMethods();

        if (!this.relativeContainer) {
          this.getEditorOption('elementsContainer').appendChild(this.getToolbarElement());
        } else {
          this.relativeContainer.appendChild(this.getToolbarElement());
        }
      },

      // Helper method to execute method for every extension, but ignoring the toolbar extension
      forEachExtension: function (iterator, context) {
        return this.base.extensions.forEach(function (command) {
          if (command === this) {
            return;
          }
          return iterator.apply(context || this, arguments);
        }, this);
      },

      // Toolbar creation/deletion

      createToolbar: function () {
        var toolbar = this.document.createElement('div');

        toolbar.id = 'medium-editor-toolbar-' + this.getEditorId();
        toolbar.className = 'medium-editor-toolbar';

        if (this.static) {
          toolbar.className += ' static-toolbar';
        } else if (this.relativeContainer) {
          toolbar.className += ' medium-editor-relative-toolbar';
        } else {
          toolbar.className += ' medium-editor-stalker-toolbar';
        }

        toolbar.appendChild(this.createToolbarButtons());

        // Add any forms that extensions may have
        this.forEachExtension(function (extension) {
          if (extension.hasForm) {
            toolbar.appendChild(extension.getForm());
          }
        });

        this.attachEventHandlers();

        return toolbar;
      },

      createToolbarButtons: function () {
        var ul = this.document.createElement('ul'),
          li,
          btn,
          buttons,
          extension,
          buttonName,
          buttonOpts;

        ul.id = 'medium-editor-toolbar-actions' + this.getEditorId();
        ul.className = 'medium-editor-toolbar-actions';
        ul.style.display = 'block';

        this.buttons.forEach(function (button) {
          if (typeof button === 'string') {
            buttonName = button;
            buttonOpts = null;
          } else {
            buttonName = button.name;
            buttonOpts = button;
          }

          // If the button already exists as an extension, it'll be returned
          // othwerise it'll create the default built-in button
          extension = this.base.addBuiltInExtension(buttonName, buttonOpts);

          if (extension && typeof extension.getButton === 'function') {
            btn = extension.getButton(this.base);
            li = this.document.createElement('li');
            if (MediumEditor.util.isElement(btn)) {
              li.appendChild(btn);
            } else {
              li.innerHTML = btn;
            }
            ul.appendChild(li);
          }
        }, this);

        buttons = ul.querySelectorAll('button');
        if (buttons.length > 0) {
          buttons[0].classList.add(this.firstButtonClass);
          buttons[buttons.length - 1].classList.add(this.lastButtonClass);
        }

        return ul;
      },

      destroy: function () {
        if (this.toolbar) {
          if (this.toolbar.parentNode) {
            this.toolbar.parentNode.removeChild(this.toolbar);
          }
          delete this.toolbar;
        }
      },

      // Toolbar accessors

      getToolbarElement: function () {
        if (!this.toolbar) {
          this.toolbar = this.createToolbar();
        }

        return this.toolbar;
      },

      getToolbarActionsElement: function () {
        return this.getToolbarElement().querySelector('.medium-editor-toolbar-actions');
      },

      // Toolbar event handlers

      initThrottledMethods: function () {
        // throttledPositionToolbar is throttled because:
        // - It will be called when the browser is resizing, which can fire many times very quickly
        // - For some event (like resize) a slight lag in UI responsiveness is OK and provides performance benefits
        this.throttledPositionToolbar = MediumEditor.util.throttle(function () {
          if (this.base.isActive) {
            this.positionToolbarIfShown();
          }
        }.bind(this));
      },

      attachEventHandlers: function () {
        // MediumEditor custom events for when user beings and ends interaction with a contenteditable and its elements
        this.subscribe('blur', this.handleBlur.bind(this));
        this.subscribe('focus', this.handleFocus.bind(this));

        // Updating the state of the toolbar as things change
        this.subscribe('editableClick', this.handleEditableClick.bind(this));
        this.subscribe('editableKeyup', this.handleEditableKeyup.bind(this));

        // Handle mouseup on document for updating the selection in the toolbar
        this.on(this.document.documentElement, 'mouseup', this.handleDocumentMouseup.bind(this));

        // Add a scroll event for sticky toolbar
        if (this.static && this.sticky) {
          // On scroll (capture), re-position the toolbar
          this.on(this.window, 'scroll', this.handleWindowScroll.bind(this), true);
        }

        // On resize, re-position the toolbar
        this.on(this.window, 'resize', this.handleWindowResize.bind(this));
      },

      handleWindowScroll: function () {
        this.positionToolbarIfShown();
      },

      handleWindowResize: function () {
        this.throttledPositionToolbar();
      },

      handleDocumentMouseup: function (event) {
        // Do not trigger checkState when mouseup fires over the toolbar
        if (event &&
          event.target &&
          MediumEditor.util.isDescendant(this.getToolbarElement(), event.target)) {
          return false;
        }
        this.checkState();
      },

      handleEditableClick: function () {
        // Delay the call to checkState to handle bug where selection is empty
        // immediately after clicking inside a pre-existing selection
        setTimeout(function () {
          this.checkState();
        }.bind(this), 0);
      },

      handleEditableKeyup: function () {
        this.checkState();
      },

      handleBlur: function () {
        // Kill any previously delayed calls to hide the toolbar
        clearTimeout(this.hideTimeout);

        // Blur may fire even if we have a selection, so we want to prevent any delayed showToolbar
        // calls from happening in this specific case
        clearTimeout(this.delayShowTimeout);

        // Delay the call to hideToolbar to handle bug with multiple editors on the page at once
        this.hideTimeout = setTimeout(function () {
          this.hideToolbar();
        }.bind(this), 1);
      },

      handleFocus: function () {
        this.checkState();
      },

      // Hiding/showing toolbar

      isDisplayed: function () {
        return this.getToolbarElement().classList.contains('medium-editor-toolbar-active');
      },

      showToolbar: function () {
        clearTimeout(this.hideTimeout);
        if (!this.isDisplayed()) {
          this.getToolbarElement().classList.add('medium-editor-toolbar-active');
          this.trigger('showToolbar', {}, this.base.getFocusedElement());
        }
      },

      hideToolbar: function () {
        if (this.isDisplayed()) {
          this.getToolbarElement().classList.remove('medium-editor-toolbar-active');
          this.trigger('hideToolbar', {}, this.base.getFocusedElement());
        }
      },

      isToolbarDefaultActionsDisplayed: function () {
        return this.getToolbarActionsElement().style.display === 'block';
      },

      hideToolbarDefaultActions: function () {
        if (this.isToolbarDefaultActionsDisplayed()) {
          this.getToolbarActionsElement().style.display = 'none';
        }
      },

      showToolbarDefaultActions: function () {
        this.hideExtensionForms();

        if (!this.isToolbarDefaultActionsDisplayed()) {
          this.getToolbarActionsElement().style.display = 'block';
        }

        // Using setTimeout + options.delay because:
        // We will actually be displaying the toolbar, which should be controlled by options.delay
        this.delayShowTimeout = this.base.delay(function () {
          this.showToolbar();
        }.bind(this));
      },

      hideExtensionForms: function () {
        // Hide all extension forms
        this.forEachExtension(function (extension) {
          if (extension.hasForm && extension.isDisplayed()) {
            extension.hideForm();
          }
        });
      },

      // Responding to changes in user selection

      // Checks for existance of multiple block elements in the current selection
      multipleBlockElementsSelected: function () {
        var regexEmptyHTMLTags = /<[^\/>][^>]*><\/[^>]+>/gim, // http://stackoverflow.com/questions/3129738/remove-empty-tags-using-regex
          regexBlockElements = new RegExp('<(' + MediumEditor.util.blockContainerElementNames.join('|') + ')[^>]*>', 'g'),
          selectionHTML = MediumEditor.selection.getSelectionHtml(this.document).replace(regexEmptyHTMLTags, ''), // Filter out empty blocks from selection
          hasMultiParagraphs = selectionHTML.match(regexBlockElements); // Find how many block elements are within the html

        return !!hasMultiParagraphs && hasMultiParagraphs.length > 1;
      },

      modifySelection: function () {
        var selection = this.window.getSelection(),
          selectionRange = selection.getRangeAt(0);

        /*
            * In firefox, there are cases (ie doubleclick of a word) where the selectionRange start
            * will be at the very end of an element.  In other browsers, the selectionRange start
            * would instead be at the very beginning of an element that actually has content.
            * example:
            *   <span>foo</span><span>bar</span>
            *
            * If the text 'bar' is selected, most browsers will have the selectionRange start at the beginning
            * of the 'bar' span.  However, there are cases where firefox will have the selectionRange start
            * at the end of the 'foo' span.  The contenteditable behavior will be ok, but if there are any
            * properties on the 'bar' span, they won't be reflected accurately in the toolbar
            * (ie 'Bold' button wouldn't be active)
            *
            * So, for cases where the selectionRange start is at the end of an element/node, find the next
            * adjacent text node that actually has content in it, and move the selectionRange start there.
            */
        if (this.standardizeSelectionStart &&
          selectionRange.startContainer.nodeValue &&
          (selectionRange.startOffset === selectionRange.startContainer.nodeValue.length)) {
          var adjacentNode = MediumEditor.util.findAdjacentTextNodeWithContent(MediumEditor.selection.getSelectionElement(this.window), selectionRange.startContainer, this.document);
          if (adjacentNode) {
            var offset = 0;
            while (adjacentNode.nodeValue.substr(offset, 1).trim().length === 0) {
              offset = offset + 1;
            }
            selectionRange = MediumEditor.selection.select(this.document, adjacentNode, offset,
              selectionRange.endContainer, selectionRange.endOffset);
          }
        }
      },

      checkState: function () {
        if (this.base.preventSelectionUpdates) {
          return;
        }

        // If no editable has focus OR selection is inside contenteditable = false
        // hide toolbar
        if (!this.base.getFocusedElement() ||
          MediumEditor.selection.selectionInContentEditableFalse(this.window)) {
          return this.hideToolbar();
        }

        // If there's no selection element, selection element doesn't belong to this editor
        // or toolbar is disabled for this selection element
        // hide toolbar
        var selectionElement = MediumEditor.selection.getSelectionElement(this.window);
        if (!selectionElement ||
          this.getEditorElements().indexOf(selectionElement) === -1 ||
          selectionElement.getAttribute('data-disable-toolbar')) {
          return this.hideToolbar();
        }

        // Now we know there's a focused editable with a selection

        // If the updateOnEmptySelection option is true, show the toolbar
        if (this.updateOnEmptySelection && this.static) {
          return this.showAndUpdateToolbar();
        }

        // If we don't have a 'valid' selection -> hide toolbar
        if (this.window.getSelection().toString().trim() === '' ||
          (this.allowMultiParagraphSelection === false && this.multipleBlockElementsSelected())) {
          return this.hideToolbar();
        }

        this.showAndUpdateToolbar();
      },

      // Updating the toolbar

      showAndUpdateToolbar: function () {
        this.modifySelection();
        this.setToolbarButtonStates();
        this.trigger('positionToolbar', {}, this.base.getFocusedElement());
        this.showToolbarDefaultActions();
        this.setToolbarPosition();
      },

      setToolbarButtonStates: function () {
        this.forEachExtension(function (extension) {
          if (typeof extension.isActive === 'function' &&
            typeof extension.setInactive === 'function') {
            extension.setInactive();
          }
        });

        this.checkActiveButtons();
      },

      checkActiveButtons: function () {
        var manualStateChecks = [],
          queryState = null,
          selectionRange = MediumEditor.selection.getSelectionRange(this.document),
          parentNode,
          updateExtensionState = function (extension) {
            if (typeof extension.checkState === 'function') {
              extension.checkState(parentNode);
            } else if (typeof extension.isActive === 'function' &&
              typeof extension.isAlreadyApplied === 'function' &&
              typeof extension.setActive === 'function') {
              if (!extension.isActive() && extension.isAlreadyApplied(parentNode)) {
                extension.setActive();
              }
            }
          };

        if (!selectionRange) {
          return;
        }

        // Loop through all extensions
        this.forEachExtension(function (extension) {
          // For those extensions where we can use document.queryCommandState(), do so
          if (typeof extension.queryCommandState === 'function') {
            queryState = extension.queryCommandState();
            // If queryCommandState returns a valid value, we can trust the browser
            // and don't need to do our manual checks
            if (queryState !== null) {
              if (queryState && typeof extension.setActive === 'function') {
                extension.setActive();
              }
              return;
            }
          }
          // We can't use queryCommandState for this extension, so add to manualStateChecks
          manualStateChecks.push(extension);
        });

        parentNode = MediumEditor.selection.getSelectedParentElement(selectionRange);

        // Make sure the selection parent isn't outside of the contenteditable
        if (!this.getEditorElements().some(function (element) {
            return MediumEditor.util.isDescendant(element, parentNode, true);
          })) {
          return;
        }

        // Climb up the DOM and do manual checks for whether a certain extension is currently enabled for this node
        while (parentNode) {
          manualStateChecks.forEach(updateExtensionState);

          // we can abort the search upwards if we leave the contentEditable element
          if (MediumEditor.util.isMediumEditorElement(parentNode)) {
            break;
          }
          parentNode = parentNode.parentNode;
        }
      },

      // Positioning toolbar

      positionToolbarIfShown: function () {
        if (this.isDisplayed()) {
          this.setToolbarPosition();
        }
      },

      setToolbarPosition: function () {
        var container = this.base.getFocusedElement(),
          selection = this.window.getSelection(),
          anchorPreview;

        // If there isn't a valid selection, bail
        if (!container) {
          return this;
        }

        if (this.static && !this.relativeContainer) {
          this.showToolbar();
          this.positionStaticToolbar(container);
        } else if (!selection.isCollapsed) {
          this.showToolbar();

          // we don't need any absolute positioning if relativeContainer is set
          if (!this.relativeContainer) {
            this.positionToolbar(selection);
          }
        }

        anchorPreview = this.base.getExtensionByName('anchor-preview');

        if (anchorPreview && typeof anchorPreview.hidePreview === 'function') {
          anchorPreview.hidePreview();
        }
      },

      positionStaticToolbar: function (container) {
        // position the toolbar at left 0, so we can get the real width of the toolbar
        this.getToolbarElement().style.left = '0';

        // document.documentElement for IE 9
        var scrollTop = (this.document.documentElement && this.document.documentElement.scrollTop) || this.document.body.scrollTop,
          windowWidth = this.window.innerWidth,
          toolbarElement = this.getToolbarElement(),
          containerRect = container.getBoundingClientRect(),
          containerTop = containerRect.top + scrollTop,
          containerCenter = (containerRect.left + (containerRect.width / 2)),
          toolbarHeight = toolbarElement.offsetHeight,
          toolbarWidth = toolbarElement.offsetWidth,
          halfOffsetWidth = toolbarWidth / 2,
          targetLeft;

        if (this.sticky) {
          // If it's beyond the height of the editor, position it at the bottom of the editor
          if (scrollTop > (containerTop + container.offsetHeight - toolbarHeight)) {
            toolbarElement.style.top = (containerTop + container.offsetHeight - toolbarHeight) + 'px';
            toolbarElement.classList.remove('medium-editor-sticky-toolbar');

            // Stick the toolbar to the top of the window
          } else if (scrollTop > (containerTop - toolbarHeight)) {
            toolbarElement.classList.add('medium-editor-sticky-toolbar');
            toolbarElement.style.top = '0px';

            // Normal static toolbar position
          } else {
            toolbarElement.classList.remove('medium-editor-sticky-toolbar');
            toolbarElement.style.top = containerTop - toolbarHeight + 'px';
          }
        } else {
          toolbarElement.style.top = containerTop - toolbarHeight + 'px';
        }

        switch (this.align) {
          case 'left':
            targetLeft = containerRect.left;
            break;

          case 'right':
            targetLeft = containerRect.right - toolbarWidth;
            break;

          case 'center':
            targetLeft = containerCenter - halfOffsetWidth;
            break;
        }

        if (targetLeft < 0) {
          targetLeft = 0;
        } else if ((targetLeft + toolbarWidth) > windowWidth) {
          targetLeft = (windowWidth - Math.ceil(toolbarWidth) - 1);
        }

        toolbarElement.style.left = targetLeft + 'px';
      },

      positionToolbar: function (selection) {
        // position the toolbar at left 0, so we can get the real width of the toolbar
        this.getToolbarElement().style.left = '0';

        var windowWidth = this.window.innerWidth,
          range = selection.getRangeAt(0),
          boundary = range.getBoundingClientRect(),
          middleBoundary = (boundary.left + boundary.right) / 2,
          toolbarElement = this.getToolbarElement(),
          toolbarHeight = toolbarElement.offsetHeight,
          toolbarWidth = toolbarElement.offsetWidth,
          halfOffsetWidth = toolbarWidth / 2,
          buttonHeight = 50,
          defaultLeft = this.diffLeft - halfOffsetWidth;

        if (boundary.top < buttonHeight) {
          toolbarElement.classList.add('medium-toolbar-arrow-over');
          toolbarElement.classList.remove('medium-toolbar-arrow-under');
          toolbarElement.style.top = buttonHeight + boundary.bottom - this.diffTop + this.window.pageYOffset - toolbarHeight + 'px';
        } else {
          toolbarElement.classList.add('medium-toolbar-arrow-under');
          toolbarElement.classList.remove('medium-toolbar-arrow-over');
          toolbarElement.style.top = boundary.top + this.diffTop + this.window.pageYOffset - toolbarHeight + 'px';
        }

        if (middleBoundary < halfOffsetWidth) {
          toolbarElement.style.left = defaultLeft + halfOffsetWidth + 'px';
        } else if ((windowWidth - middleBoundary) < halfOffsetWidth) {
          toolbarElement.style.left = windowWidth + defaultLeft - halfOffsetWidth + 'px';
        } else {
          toolbarElement.style.left = defaultLeft + middleBoundary + 'px';
        }
      }
    });

    MediumEditor.extensions.toolbar = Toolbar;
  }());

  (function () {
    'use strict';

    var ImageDragging = MediumEditor.Extension.extend({
      init: function () {
        MediumEditor.Extension.prototype.init.apply(this, arguments);

        this.subscribe('editableDrag', this.handleDrag.bind(this));
        this.subscribe('editableDrop', this.handleDrop.bind(this));
      },

      handleDrag: function (event) {
        var className = 'medium-editor-dragover';
        event.preventDefault();
        event.dataTransfer.dropEffect = 'copy';

        if (event.type === 'dragover') {
          event.target.classList.add(className);
        } else if (event.type === 'dragleave') {
          event.target.classList.remove(className);
        }
      },

      handleDrop: function (event) {
        var className = 'medium-editor-dragover',
          files;
        event.preventDefault();
        event.stopPropagation();

        // IE9 does not support the File API, so prevent file from opening in a new window
        // but also don't try to actually get the file
        if (event.dataTransfer.files) {
          files = Array.prototype.slice.call(event.dataTransfer.files, 0);
          files.some(function (file) {
            if (file.type.match('image')) {
              var fileReader, id;
              fileReader = new FileReader();
              fileReader.readAsDataURL(file);

              id = 'medium-img-' + (+new Date());
              MediumEditor.util.insertHTMLCommand(this.document, '<img class="medium-editor-image-loading" id="' + id + '" />');

              fileReader.onload = function () {
                var img = this.document.getElementById(id);
                if (img) {
                  img.removeAttribute('id');
                  img.removeAttribute('class');
                  img.src = fileReader.result;
                }
              }.bind(this);
            }
          }.bind(this));
        }
        event.target.classList.remove(className);
      }
    });

    MediumEditor.extensions.imageDragging = ImageDragging;
  }());

  (function () {
    'use strict';

    // Event handlers that shouldn't be exposed externally

    function handleDisableExtraSpaces(event) {
      var node = MediumEditor.selection.getSelectionStart(this.options.ownerDocument),
        textContent = node.textContent,
        caretPositions = MediumEditor.selection.getCaretOffsets(node);

      if ((textContent[caretPositions.left - 1] === undefined) || (textContent[caretPositions.left - 1] === ' ') || (textContent[caretPositions.left] === undefined)) {
        event.preventDefault();
      }
    }

    function handleDisabledEnterKeydown(event, element) {
      if (this.options.disableReturn || element.getAttribute('data-disable-return')) {
        event.preventDefault();
      } else if (this.options.disableDoubleReturn || element.getAttribute('data-disable-double-return')) {
        var node = MediumEditor.selection.getSelectionStart(this.options.ownerDocument);

        // if current text selection is empty OR previous sibling text is empty OR it is not a list
        if ((node && node.textContent.trim() === '' && node.nodeName.toLowerCase() !== 'li') ||
          (node.previousElementSibling && node.previousElementSibling.nodeName.toLowerCase() !== 'br' &&
            node.previousElementSibling.textContent.trim() === '')) {
          event.preventDefault();
        }
      }
    }

    function handleTabKeydown(event) {
      // Override tab only for pre nodes
      var node = MediumEditor.selection.getSelectionStart(this.options.ownerDocument),
        tag = node && node.nodeName.toLowerCase();

      if (tag === 'pre') {
        event.preventDefault();
        MediumEditor.util.insertHTMLCommand(this.options.ownerDocument, '    ');
      }

      // Tab to indent list structures!
      if (MediumEditor.util.isListItem(node)) {
        event.preventDefault();

        // If Shift is down, outdent, otherwise indent
        if (event.shiftKey) {
          this.options.ownerDocument.execCommand('outdent', false, null);
        } else {
          this.options.ownerDocument.execCommand('indent', false, null);
        }
      }
    }

    function handleBlockDeleteKeydowns(event) {
      var p, node = MediumEditor.selection.getSelectionStart(this.options.ownerDocument),
        tagName = node.nodeName.toLowerCase(),
        isEmpty = /^(\s+|<br\/?>)?$/i,
        isHeader = /h\d/i;

      if (MediumEditor.util.isKey(event, [MediumEditor.util.keyCode.BACKSPACE, MediumEditor.util.keyCode.ENTER]) &&
        // has a preceeding sibling
        node.previousElementSibling &&
        // in a header
        isHeader.test(tagName) &&
        // at the very end of the block
        MediumEditor.selection.getCaretOffsets(node).left === 0) {
        if (MediumEditor.util.isKey(event, MediumEditor.util.keyCode.BACKSPACE) && isEmpty.test(node.previousElementSibling.innerHTML)) {
          // backspacing the begining of a header into an empty previous element will
          // change the tagName of the current node to prevent one
          // instead delete previous node and cancel the event.
          node.previousElementSibling.parentNode.removeChild(node.previousElementSibling);
          event.preventDefault();
        } else if (MediumEditor.util.isKey(event, MediumEditor.util.keyCode.ENTER)) {
          // hitting return in the begining of a header will create empty header elements before the current one
          // instead, make "<p><br></p>" element, which are what happens if you hit return in an empty paragraph
          p = this.options.ownerDocument.createElement('p');
          p.innerHTML = '<br>';
          node.previousElementSibling.parentNode.insertBefore(p, node);
          event.preventDefault();
        }
      } else if (MediumEditor.util.isKey(event, MediumEditor.util.keyCode.DELETE) &&
        // between two sibling elements
        node.nextElementSibling &&
        node.previousElementSibling &&
        // not in a header
        !isHeader.test(tagName) &&
        // in an empty tag
        isEmpty.test(node.innerHTML) &&
        // when the next tag *is* a header
        isHeader.test(node.nextElementSibling.nodeName.toLowerCase())) {
        // hitting delete in an empty element preceding a header, ex:
        //  <p>[CURSOR]</p><h1>Header</h1>
        // Will cause the h1 to become a paragraph.
        // Instead, delete the paragraph node and move the cursor to the begining of the h1

        // remove node and move cursor to start of header
        MediumEditor.selection.moveCursor(this.options.ownerDocument, node.nextElementSibling);

        node.previousElementSibling.parentNode.removeChild(node);

        event.preventDefault();
      } else if (MediumEditor.util.isKey(event, MediumEditor.util.keyCode.BACKSPACE) &&
        tagName === 'li' &&
        // hitting backspace inside an empty li
        isEmpty.test(node.innerHTML) &&
        // is first element (no preceeding siblings)
        !node.previousElementSibling &&
        // parent also does not have a sibling
        !node.parentElement.previousElementSibling &&
        // is not the only li in a list
        node.nextElementSibling &&
        node.nextElementSibling.nodeName.toLowerCase() === 'li') {
        // backspacing in an empty first list element in the first list (with more elements) ex:
        //  <ul><li>[CURSOR]</li><li>List Item 2</li></ul>
        // will remove the first <li> but add some extra element before (varies based on browser)
        // Instead, this will:
        // 1) remove the list element
        // 2) create a paragraph before the list
        // 3) move the cursor into the paragraph

        // create a paragraph before the list
        p = this.options.ownerDocument.createElement('p');
        p.innerHTML = '<br>';
        node.parentElement.parentElement.insertBefore(p, node.parentElement);

        // move the cursor into the new paragraph
        MediumEditor.selection.moveCursor(this.options.ownerDocument, p);

        // remove the list element
        node.parentElement.removeChild(node);

        event.preventDefault();
      }
    }

    function handleKeyup(event) {
      var node = MediumEditor.selection.getSelectionStart(this.options.ownerDocument),
        tagName;

      if (!node) {
        return;
      }

      if (MediumEditor.util.isMediumEditorElement(node) && node.children.length === 0) {
        this.options.ownerDocument.execCommand('formatBlock', false, 'p');
      }

      if (MediumEditor.util.isKey(event, MediumEditor.util.keyCode.ENTER) && !MediumEditor.util.isListItem(node)) {
        tagName = node.nodeName.toLowerCase();
        // For anchor tags, unlink
        if (tagName === 'a') {
          this.options.ownerDocument.execCommand('unlink', false, null);
        } else if (!event.shiftKey && !event.ctrlKey) {
          // only format block if this is not a header tag
          if (!/h\d/.test(tagName)) {
            this.options.ownerDocument.execCommand('formatBlock', false, 'p');
          }
        }
      }
    }

    // Internal helper methods which shouldn't be exposed externally

    function addToEditors(win) {
      if (!win._mediumEditors) {
        // To avoid breaking users who are assuming that the unique id on
        // medium-editor elements will start at 1, inserting a 'null' in the
        // array so the unique-id can always map to the index of the editor instance
        win._mediumEditors = [null];
      }

      // If this already has a unique id, re-use it
      if (!this.id) {
        this.id = win._mediumEditors.length;
      }

      win._mediumEditors[this.id] = this;
    }

    function removeFromEditors(win) {
      if (!win._mediumEditors || !win._mediumEditors[this.id]) {
        return;
      }

      /* Setting the instance to null in the array instead of deleting it allows:
         * 1) Each instance to preserve its own unique-id, even after being destroyed
         *    and initialized again
         * 2) The unique-id to always correspond to an index in the array of medium-editor
         *    instances. Thus, we will be able to look at a contenteditable, and determine
         *    which instance it belongs to, by indexing into the global array.
         */
      win._mediumEditors[this.id] = null;
    }

    function createElementsArray(selector) {
      if (!selector) {
        selector = [];
      }
      // If string, use as query selector
      if (typeof selector === 'string') {
        selector = this.options.ownerDocument.querySelectorAll(selector);
      }
      // If element, put into array
      if (MediumEditor.util.isElement(selector)) {
        selector = [selector];
      }
      // Convert NodeList (or other array like object) into an array
      var elements = Array.prototype.slice.apply(selector);

      // Loop through elements and convert textarea's into divs
      this.elements = [];
      elements.forEach(function (element, index) {
        if (element.nodeName.toLowerCase() === 'textarea') {
          this.elements.push(createContentEditable.call(this, element, index));
        } else {
          this.elements.push(element);
        }
      }, this);
    }

    function setExtensionDefaults(extension, defaults) {
      Object.keys(defaults).forEach(function (prop) {
        if (extension[prop] === undefined) {
          extension[prop] = defaults[prop];
        }
      });
      return extension;
    }

    function initExtension(extension, name, instance) {
      var extensionDefaults = {
        'window': instance.options.contentWindow,
        'document': instance.options.ownerDocument,
        'base': instance
      };

      // Add default options into the extension
      extension = setExtensionDefaults(extension, extensionDefaults);

      // Call init on the extension
      if (typeof extension.init === 'function') {
        extension.init();
      }

      // Set extension name (if not already set)
      if (!extension.name) {
        extension.name = name;
      }
      return extension;
    }

    function isToolbarEnabled() {
      // If any of the elements don't have the toolbar disabled
      // We need a toolbar
      if (this.elements.every(function (element) {
          return !!element.getAttribute('data-disable-toolbar');
        })) {
        return false;
      }

      return this.options.toolbar !== false;
    }

    function isAnchorPreviewEnabled() {
      // If toolbar is disabled, don't add
      if (!isToolbarEnabled.call(this)) {
        return false;
      }

      return this.options.anchorPreview !== false;
    }

    function isPlaceholderEnabled() {
      return this.options.placeholder !== false;
    }

    function isAutoLinkEnabled() {
      return this.options.autoLink !== false;
    }

    function isImageDraggingEnabled() {
      return this.options.imageDragging !== false;
    }

    function isKeyboardCommandsEnabled() {
      return this.options.keyboardCommands !== false;
    }

    function shouldUseFileDraggingExtension() {
      // Since the file-dragging extension replaces the image-dragging extension,
      // we need to check if the user passed an overrided image-dragging extension.
      // If they have, to avoid breaking users, we won't use file-dragging extension.
      return !this.options.extensions['imageDragging'];
    }

    function createContentEditable(textarea, id) {
      var div = this.options.ownerDocument.createElement('div'),
        now = Date.now(),
        uniqueId = 'medium-editor-' + now + '-' + id,
        atts = textarea.attributes;

      // Some browsers can move pretty fast, since we're using a timestamp
      // to make a unique-id, ensure that the id is actually unique on the page
      while (this.options.ownerDocument.getElementById(uniqueId)) {
        now++;
        uniqueId = 'medium-editor-' + now + '-' + id;
      }

      div.className = textarea.className;
      div.id = uniqueId;
      div.innerHTML = textarea.value;

      textarea.setAttribute('medium-editor-textarea-id', uniqueId);

      // re-create all attributes from the textearea to the new created div
      for (var i = 0, n = atts.length; i < n; i++) {
        // do not re-create existing attributes
        if (!div.hasAttribute(atts[i].nodeName)) {
          div.setAttribute(atts[i].nodeName, atts[i].nodeValue);
        }
      }

      textarea.classList.add('medium-editor-hidden');
      textarea.parentNode.insertBefore(
        div,
        textarea
      );

      return div;
    }

    function initElements() {
      this.elements.forEach(function (element, index) {
        if (!this.options.disableEditing && !element.getAttribute('data-disable-editing')) {
          element.setAttribute('contentEditable', true);
          element.setAttribute('spellcheck', this.options.spellcheck);
        }
        element.setAttribute('data-medium-editor-element', true);
        element.setAttribute('role', 'textbox');
        element.setAttribute('aria-multiline', true);
        element.setAttribute('medium-editor-index', index);

        if (element.hasAttribute('medium-editor-textarea-id')) {
          this.on(element, 'input', function (event) {
            var target = event.target,
              textarea = target.parentNode.querySelector('textarea[medium-editor-textarea-id="' + target.getAttribute('medium-editor-textarea-id') + '"]');
            if (textarea) {
              textarea.value = this.serialize()[target.id].value;
            }
          }.bind(this));
        }
      }, this);
    }

    function attachHandlers() {
      var i;

      // attach to tabs
      this.subscribe('editableKeydownTab', handleTabKeydown.bind(this));

      // Bind keys which can create or destroy a block element: backspace, delete, return
      this.subscribe('editableKeydownDelete', handleBlockDeleteKeydowns.bind(this));
      this.subscribe('editableKeydownEnter', handleBlockDeleteKeydowns.bind(this));

      // Bind double space event
      if (this.options.disableExtraSpaces) {
        this.subscribe('editableKeydownSpace', handleDisableExtraSpaces.bind(this));
      }

      // disabling return or double return
      if (this.options.disableReturn || this.options.disableDoubleReturn) {
        this.subscribe('editableKeydownEnter', handleDisabledEnterKeydown.bind(this));
      } else {
        for (i = 0; i < this.elements.length; i += 1) {
          if (this.elements[i].getAttribute('data-disable-return') || this.elements[i].getAttribute('data-disable-double-return')) {
            this.subscribe('editableKeydownEnter', handleDisabledEnterKeydown.bind(this));
            break;
          }
        }
      }

      // if we're not disabling return, add a handler to help handle cleanup
      // for certain cases when enter is pressed
      if (!this.options.disableReturn) {
        this.elements.forEach(function (element) {
          if (!element.getAttribute('data-disable-return')) {
            this.on(element, 'keyup', handleKeyup.bind(this));
          }
        }, this);
      }
    }

    function initExtensions() {

      this.extensions = [];

      // Passed in extensions
      Object.keys(this.options.extensions).forEach(function (name) {
        // Always save the toolbar extension for last
        if (name !== 'toolbar' && this.options.extensions[name]) {
          this.extensions.push(initExtension(this.options.extensions[name], name, this));
        }
      }, this);

      // 4 Cases for imageDragging + fileDragging extensons:
      //
      // 1. ImageDragging ON + No Custom Image Dragging Extension:
      //    * Use fileDragging extension (default options)
      // 2. ImageDragging OFF + No Custom Image Dragging Extension:
      //    * Use fileDragging extension w/ images turned off
      // 3. ImageDragging ON + Custom Image Dragging Extension:
      //    * Don't use fileDragging (could interfere with custom image dragging extension)
      // 4. ImageDragging OFF + Custom Image Dragging:
      //    * Don't use fileDragging (could interfere with custom image dragging extension)
      if (shouldUseFileDraggingExtension.call(this)) {
        var opts = this.options.fileDragging;
        if (!opts) {
          opts = {};

          // Image is in the 'allowedTypes' list by default.
          // If imageDragging is off override the 'allowedTypes' list with an empty one
          if (!isImageDraggingEnabled.call(this)) {
            opts.allowedTypes = [];
          }
        }
        this.addBuiltInExtension('fileDragging', opts);
      }

      // Built-in extensions
      var builtIns = {
        paste: true,
        'anchor-preview': isAnchorPreviewEnabled.call(this),
        autoLink: isAutoLinkEnabled.call(this),
        keyboardCommands: isKeyboardCommandsEnabled.call(this),
        placeholder: isPlaceholderEnabled.call(this)
      };
      Object.keys(builtIns).forEach(function (name) {
        if (builtIns[name]) {
          this.addBuiltInExtension(name);
        }
      }, this);

      // Users can pass in a custom toolbar extension
      // so check for that first and if it's not present
      // just create the default toolbar
      var toolbarExtension = this.options.extensions['toolbar'];
      if (!toolbarExtension && isToolbarEnabled.call(this)) {
        // Backwards compatability
        var toolbarOptions = MediumEditor.util.extend({}, this.options.toolbar, {
          allowMultiParagraphSelection: this.options.allowMultiParagraphSelection // deprecated
        });
        toolbarExtension = new MediumEditor.extensions.toolbar(toolbarOptions);
      }

      // If the toolbar is not disabled, so we actually have an extension
      // initialize it and add it to the extensions array
      if (toolbarExtension) {
        this.extensions.push(initExtension(toolbarExtension, 'toolbar', this));
      }
    }

    function mergeOptions(defaults, options) {
      var deprecatedProperties = [
        ['allowMultiParagraphSelection', 'toolbar.allowMultiParagraphSelection']
      ];
      // warn about using deprecated properties
      if (options) {
        deprecatedProperties.forEach(function (pair) {
          if (options.hasOwnProperty(pair[0]) && options[pair[0]] !== undefined) {
            MediumEditor.util.deprecated(pair[0], pair[1], 'v6.0.0');
          }
        });
      }

      return MediumEditor.util.defaults({}, options, defaults);
    }

    function execActionInternal(action, opts) {
      /*jslint regexp: true*/
      var appendAction = /^append-(.+)$/gi,
        justifyAction = /justify([A-Za-z]*)$/g, /* Detecting if is justifyCenter|Right|Left */
        match;
      /*jslint regexp: false*/

      // Actions starting with 'append-' should attempt to format a block of text ('formatBlock') using a specific
      // type of block element (ie append-blockquote, append-h1, append-pre, etc.)
      match = appendAction.exec(action);
      if (match) {
        return MediumEditor.util.execFormatBlock(this.options.ownerDocument, match[1]);
      }

      if (action === 'fontSize') {
        return this.options.ownerDocument.execCommand('fontSize', false, opts.size);
      }

      if (action === 'fontName') {
        return this.options.ownerDocument.execCommand('fontName', false, opts.name);
      }

      if (action === 'createLink') {
        return this.createLink(opts);
      }

      if (action === 'image') {
        return this.options.ownerDocument.execCommand('insertImage', false, this.options.contentWindow.getSelection());
      }

      /* Issue: https://github.com/yabwe/medium-editor/issues/595
         * If the action is to justify the text */
      if (justifyAction.exec(action)) {
        var result = this.options.ownerDocument.execCommand(action, false, null),
          parentNode = MediumEditor.selection.getSelectedParentElement(MediumEditor.selection.getSelectionRange(this.options.ownerDocument));
        if (parentNode) {
          cleanupJustifyDivFragments.call(this, MediumEditor.util.getTopBlockContainer(parentNode));
        }

        return result;
      }

      return this.options.ownerDocument.execCommand(action, false, null);
    }

    /* If we've just justified text within a container block
     * Chrome may have removed <br> elements and instead wrapped lines in <div> elements
     * with a text-align property.  If so, we want to fix this
     */
    function cleanupJustifyDivFragments(blockContainer) {
      if (!blockContainer) {
        return;
      }

      var textAlign,
        childDivs = Array.prototype.slice.call(blockContainer.childNodes).filter(function (element) {
          var isDiv = element.nodeName.toLowerCase() === 'div';
          if (isDiv && !textAlign) {
            textAlign = element.style.textAlign;
          }
          return isDiv;
        });

      /* If we found child <div> elements with text-align style attributes
         * we should fix this by:
         *
         * 1) Unwrapping each <div> which has a text-align style
         * 2) Insert a <br> element after each set of 'unwrapped' div children
         * 3) Set the text-align style of the parent block element
         */
      if (childDivs.length) {
        // Since we're mucking with the HTML, preserve selection
        this.saveSelection();
        childDivs.forEach(function (div) {
          if (div.style.textAlign === textAlign) {
            var lastChild = div.lastChild;
            if (lastChild) {
              // Instead of a div, extract the child elements and add a <br>
              MediumEditor.util.unwrap(div, this.options.ownerDocument);
              var br = this.options.ownerDocument.createElement('BR');
              lastChild.parentNode.insertBefore(br, lastChild.nextSibling);
            }
          }
        }, this);
        blockContainer.style.textAlign = textAlign;
        // We're done, so restore selection
        this.restoreSelection();
      }
    }

    MediumEditor.prototype = {
      // NOT DOCUMENTED - exposed for backwards compatability
      init: function (elements, options) {
        this.options = mergeOptions.call(this, this.defaults, options);
        this.origElements = elements;

        if (!this.options.elementsContainer) {
          this.options.elementsContainer = this.options.ownerDocument.body;
        }

        return this.setup();
      },

      setup: function () {
        if (this.isActive) {
          return;
        }

        createElementsArray.call(this, this.origElements);

        if (this.elements.length === 0) {
          return;
        }

        this.isActive = true;
        addToEditors.call(this, this.options.contentWindow);

        this.events = new MediumEditor.Events(this);

        // Call initialization helpers
        initElements.call(this);
        initExtensions.call(this);
        attachHandlers.call(this);
      },

      destroy: function () {
        if (!this.isActive) {
          return;
        }

        this.isActive = false;

        this.extensions.forEach(function (extension) {
          if (typeof extension.destroy === 'function') {
            extension.destroy();
          }
        }, this);

        this.events.destroy();

        this.elements.forEach(function (element) {
          // Reset elements content, fix for issue where after editor destroyed the red underlines on spelling errors are left
          if (this.options.spellcheck) {
            element.innerHTML = element.innerHTML;
          }

          // cleanup extra added attributes
          element.removeAttribute('contentEditable');
          element.removeAttribute('spellcheck');
          element.removeAttribute('data-medium-editor-element');
          element.removeAttribute('role');
          element.removeAttribute('aria-multiline');
          element.removeAttribute('medium-editor-index');

          // Remove any elements created for textareas
          if (element.hasAttribute('medium-editor-textarea-id')) {
            var textarea = element.parentNode.querySelector('textarea[medium-editor-textarea-id="' + element.getAttribute('medium-editor-textarea-id') + '"]');
            if (textarea) {
              // Un-hide the textarea
              textarea.classList.remove('medium-editor-hidden');
            }
            if (element.parentNode) {
              element.parentNode.removeChild(element);
            }
          }
        }, this);
        this.elements = [];

        removeFromEditors.call(this, this.options.contentWindow);
      },

      on: function (target, event, listener, useCapture) {
        this.events.attachDOMEvent(target, event, listener, useCapture);
      },

      off: function (target, event, listener, useCapture) {
        this.events.detachDOMEvent(target, event, listener, useCapture);
      },

      subscribe: function (event, listener) {
        this.events.attachCustomEvent(event, listener);
      },

      unsubscribe: function (event, listener) {
        this.events.detachCustomEvent(event, listener);
      },

      trigger: function (name, data, editable) {
        this.events.triggerCustomEvent(name, data, editable);
      },

      delay: function (fn) {
        var self = this;
        return setTimeout(function () {
          if (self.isActive) {
            fn();
          }
        }, this.options.delay);
      },

      serialize: function () {
        var i,
          elementid,
          content = {};
        for (i = 0; i < this.elements.length; i += 1) {
          elementid = (this.elements[i].id !== '') ? this.elements[i].id : 'element-' + i;
          content[elementid] = {
            value: this.elements[i].innerHTML.trim()
          };
        }
        return content;
      },

      getExtensionByName: function (name) {
        var extension;
        if (this.extensions && this.extensions.length) {
          this.extensions.some(function (ext) {
            if (ext.name === name) {
              extension = ext;
              return true;
            }
            return false;
          });
        }
        return extension;
      },

      /**
       * NOT DOCUMENTED - exposed as a helper for other extensions to use
       */
      addBuiltInExtension: function (name, opts) {
        var extension = this.getExtensionByName(name),
          merged;
        if (extension) {
          return extension;
        }

        switch (name) {
          case 'anchor':
            merged = MediumEditor.util.extend({}, this.options.anchor, opts);
            extension = new MediumEditor.extensions.anchor(merged);
            break;
          case 'anchor-preview':
            extension = new MediumEditor.extensions.anchorPreview(this.options.anchorPreview);
            break;
          case 'autoLink':
            extension = new MediumEditor.extensions.autoLink();
            break;
          case 'fileDragging':
            extension = new MediumEditor.extensions.fileDragging(opts);
            break;
          case 'fontname':
            extension = new MediumEditor.extensions.fontName(this.options.fontName);
            break;
          case 'fontsize':
            extension = new MediumEditor.extensions.fontSize(opts);
            break;
          case 'keyboardCommands':
            extension = new MediumEditor.extensions.keyboardCommands(this.options.keyboardCommands);
            break;
          case 'paste':
            extension = new MediumEditor.extensions.paste(this.options.paste);
            break;
          case 'placeholder':
            extension = new MediumEditor.extensions.placeholder(this.options.placeholder);
            break;
          default:
            // All of the built-in buttons for MediumEditor are extensions
            // so check to see if the extension we're creating is a built-in button
            if (MediumEditor.extensions.button.isBuiltInButton(name)) {
              if (opts) {
                merged = MediumEditor.util.defaults({}, opts, MediumEditor.extensions.button.prototype.defaults[name]);
                extension = new MediumEditor.extensions.button(merged);
              } else {
                extension = new MediumEditor.extensions.button(name);
              }
            }
        }

        if (extension) {
          this.extensions.push(initExtension(extension, name, this));
        }

        return extension;
      },

      stopSelectionUpdates: function () {
        this.preventSelectionUpdates = true;
      },

      startSelectionUpdates: function () {
        this.preventSelectionUpdates = false;
      },

      checkSelection: function () {
        var toolbar = this.getExtensionByName('toolbar');
        if (toolbar) {
          toolbar.checkState();
        }
        return this;
      },

      // Wrapper around document.queryCommandState for checking whether an action has already
      // been applied to the current selection
      queryCommandState: function (action) {
        var fullAction = /^full-(.+)$/gi,
          match,
          queryState = null;

        // Actions starting with 'full-' need to be modified since this is a medium-editor concept
        match = fullAction.exec(action);
        if (match) {
          action = match[1];
        }

        try {
          queryState = this.options.ownerDocument.queryCommandState(action);
        } catch (exc) {
          queryState = null;
        }

        return queryState;
      },

      execAction: function (action, opts) {
        /*jslint regexp: true*/
        var fullAction = /^full-(.+)$/gi,
          match,
          result;
        /*jslint regexp: false*/

        // Actions starting with 'full-' should be applied to to the entire contents of the editable element
        // (ie full-bold, full-append-pre, etc.)
        match = fullAction.exec(action);
        if (match) {
          // Store the current selection to be restored after applying the action
          this.saveSelection();
          // Select all of the contents before calling the action
          this.selectAllContents();
          result = execActionInternal.call(this, match[1], opts);
          // Restore the previous selection
          this.restoreSelection();
        } else {
          result = execActionInternal.call(this, action, opts);
        }

        // do some DOM clean-up for known browser issues after the action
        if (action === 'insertunorderedlist' || action === 'insertorderedlist') {
          MediumEditor.util.cleanListDOM(this.options.ownerDocument, this.getSelectedParentElement());
        }

        this.checkSelection();
        return result;
      },

      getSelectedParentElement: function (range) {
        if (range === undefined) {
          range = this.options.contentWindow.getSelection().getRangeAt(0);
        }
        return MediumEditor.selection.getSelectedParentElement(range);
      },

      selectAllContents: function () {
        var currNode = MediumEditor.selection.getSelectionElement(this.options.contentWindow);

        if (currNode) {
          // Move to the lowest descendant node that still selects all of the contents
          while (currNode.children.length === 1) {
            currNode = currNode.children[0];
          }

          this.selectElement(currNode);
        }
      },

      selectElement: function (element) {
        MediumEditor.selection.selectNode(element, this.options.ownerDocument);

        var selElement = MediumEditor.selection.getSelectionElement(this.options.contentWindow);
        if (selElement) {
          this.events.focusElement(selElement);
        }
      },

      getFocusedElement: function () {
        var focused;
        this.elements.some(function (element) {
          // Find the element that has focus
          if (!focused && element.getAttribute('data-medium-focused')) {
            focused = element;
          }

          // bail if we found the element that had focus
          return !!focused;
        }, this);

        return focused;
      },

      // Export the state of the selection in respect to one of this
      // instance of MediumEditor's elements
      exportSelection: function () {
        var selectionElement = MediumEditor.selection.getSelectionElement(this.options.contentWindow),
          editableElementIndex = this.elements.indexOf(selectionElement),
          selectionState = null;

        if (editableElementIndex >= 0) {
          selectionState = MediumEditor.selection.exportSelection(selectionElement, this.options.ownerDocument);
        }

        if (selectionState !== null && editableElementIndex !== 0) {
          selectionState.editableElementIndex = editableElementIndex;
        }

        return selectionState;
      },

      saveSelection: function () {
        this.selectionState = this.exportSelection();
      },

      // Restore a selection based on a selectionState returned by a call
      // to MediumEditor.exportSelection
      importSelection: function (selectionState, favorLaterSelectionAnchor) {
        if (!selectionState) {
          return;
        }

        var editableElement = this.elements[selectionState.editableElementIndex || 0];
        MediumEditor.selection.importSelection(selectionState, editableElement, this.options.ownerDocument, favorLaterSelectionAnchor);
      },

      restoreSelection: function () {
        this.importSelection(this.selectionState);
      },

      createLink: function (opts) {
        var currentEditor = MediumEditor.selection.getSelectionElement(this.options.contentWindow),
          customEvent = {};

        // Make sure the selection is within an element this editor is tracking
        if (this.elements.indexOf(currentEditor) === -1) {
          return;
        }

        try {
          this.events.disableCustomEvent('editableInput');
          if (opts.url && opts.url.trim().length > 0) {
            var currentSelection = this.options.contentWindow.getSelection();
            if (currentSelection) {
              var currRange = currentSelection.getRangeAt(0),
                commonAncestorContainer = currRange.commonAncestorContainer,
                exportedSelection,
                startContainerParentElement,
                endContainerParentElement,
                textNodes;

              // If the selection is contained within a single text node
              // and the selection starts at the beginning of the text node,
              // MSIE still says the startContainer is the parent of the text node.
              // If the selection is contained within a single text node, we
              // want to just use the default browser 'createLink', so we need
              // to account for this case and adjust the commonAncestorContainer accordingly
              if (currRange.endContainer.nodeType === 3 &&
                currRange.startContainer.nodeType !== 3 &&
                currRange.startOffset === 0 &&
                currRange.startContainer.firstChild === currRange.endContainer) {
                commonAncestorContainer = currRange.endContainer;
              }

              startContainerParentElement = MediumEditor.util.getClosestBlockContainer(currRange.startContainer);
              endContainerParentElement = MediumEditor.util.getClosestBlockContainer(currRange.endContainer);

              // If the selection is not contained within a single text node
              // but the selection is contained within the same block element
              // we want to make sure we create a single link, and not multiple links
              // which can happen with the built in browser functionality
              if (commonAncestorContainer.nodeType !== 3 && startContainerParentElement === endContainerParentElement) {
                var parentElement = (startContainerParentElement || currentEditor),
                  fragment = this.options.ownerDocument.createDocumentFragment();

                // since we are going to create a link from an extracted text,
                // be sure that if we are updating a link, we won't let an empty link behind (see #754)
                // (Workaroung for Chrome)
                this.execAction('unlink');

                exportedSelection = this.exportSelection();
                fragment.appendChild(parentElement.cloneNode(true));

                if (currentEditor === parentElement) {
                  // We have to avoid the editor itself being wiped out when it's the only block element,
                  // as our reference inside this.elements gets detached from the page when insertHTML runs.
                  // If we just use [parentElement, 0] and [parentElement, parentElement.childNodes.length]
                  // as the range boundaries, this happens whenever parentElement === currentEditor.
                  // The tradeoff to this workaround is that a orphaned tag can sometimes be left behind at
                  // the end of the editor's content.
                  // In Gecko:
                  // as an empty <strong></strong> if parentElement.lastChild is a <strong> tag.
                  // In WebKit:
                  // an invented <br /> tag at the end in the same situation
                  MediumEditor.selection.select(
                    this.options.ownerDocument,
                    parentElement.firstChild,
                    0,
                    parentElement.lastChild,
                    parentElement.lastChild.nodeType === 3 ?
                      parentElement.lastChild.nodeValue.length : parentElement.lastChild.childNodes.length
                  );
                } else {
                  MediumEditor.selection.select(
                    this.options.ownerDocument,
                    parentElement,
                    0,
                    parentElement,
                    parentElement.childNodes.length
                  );
                }

                var modifiedExportedSelection = this.exportSelection();

                textNodes = MediumEditor.util.findOrCreateMatchingTextNodes(
                  this.options.ownerDocument,
                  fragment,
                  {
                    start: exportedSelection.start - modifiedExportedSelection.start,
                    end: exportedSelection.end - modifiedExportedSelection.start,
                    editableElementIndex: exportedSelection.editableElementIndex
                  }
                );
                // If textNodes are not present, when changing link on images
                // ex: <a><img src="http://image.test.com"></a>, change fragment to currRange.startContainer
                // and set textNodes array to [imageElement, imageElement]
                if (textNodes.length === 0) {
                  fragment = this.options.ownerDocument.createDocumentFragment();
                  fragment.appendChild(commonAncestorContainer.cloneNode(true));
                  textNodes = [fragment.firstChild.firstChild, fragment.firstChild.lastChild];
                }

                // Creates the link in the document fragment
                MediumEditor.util.createLink(this.options.ownerDocument, textNodes, opts.url.trim());

                // Chrome trims the leading whitespaces when inserting HTML, which messes up restoring the selection.
                var leadingWhitespacesCount = (fragment.firstChild.innerHTML.match(/^\s+/) || [''])[0].length;

                // Now move the created link back into the original document in a way to preserve undo/redo history
                MediumEditor.util.insertHTMLCommand(this.options.ownerDocument, fragment.firstChild.innerHTML.replace(/^\s+/, ''));
                exportedSelection.start -= leadingWhitespacesCount;
                exportedSelection.end -= leadingWhitespacesCount;

                this.importSelection(exportedSelection);
              } else {
                this.options.ownerDocument.execCommand('createLink', false, opts.url);
              }

              if (this.options.targetBlank || opts.target === '_blank') {
                MediumEditor.util.setTargetBlank(MediumEditor.selection.getSelectionStart(this.options.ownerDocument), opts.url);
              }

              if (opts.buttonClass) {
                MediumEditor.util.addClassToAnchors(MediumEditor.selection.getSelectionStart(this.options.ownerDocument), opts.buttonClass);
              }
            }
          }
          // Fire input event for backwards compatibility if anyone was listening directly to the DOM input event
          if (this.options.targetBlank || opts.target === '_blank' || opts.buttonClass) {
            customEvent = this.options.ownerDocument.createEvent('HTMLEvents');
            customEvent.initEvent('input', true, true, this.options.contentWindow);
            for (var i = 0; i < this.elements.length; i += 1) {
              this.elements[i].dispatchEvent(customEvent);
            }
          }
        } finally {
          this.events.enableCustomEvent('editableInput');
        }
        // Fire our custom editableInput event
        this.events.triggerCustomEvent('editableInput', customEvent, currentEditor);
      },

      cleanPaste: function (text) {
        this.getExtensionByName('paste').cleanPaste(text);
      },

      pasteHTML: function (html, options) {
        this.getExtensionByName('paste').pasteHTML(html, options);
      },

      setContent: function (html, index) {
        index = index || 0;

        if (this.elements[index]) {
          var target = this.elements[index];
          target.innerHTML = html;
          this.events.updateInput(target, { target: target, currentTarget: target });
        }
      }
    };
  }());

  (function () {
    // summary: The default options hash used by the Editor

    MediumEditor.prototype.defaults = {
      activeButtonClass: 'medium-editor-button-active',
      buttonLabels: false,
      delay: 0,
      disableReturn: false,
      disableDoubleReturn: false,
      disableExtraSpaces: false,
      disableEditing: false,
      autoLink: false,
      elementsContainer: false,
      contentWindow: window,
      ownerDocument: document,
      targetBlank: false,
      extensions: {},
      spellcheck: true
    };
  })();

  MediumEditor.parseVersionString = function (release) {
    var split = release.split('-'),
      version = split[0].split('.'),
      preRelease = (split.length > 1) ? split[1] : '';
    return {
      major: parseInt(version[0], 10),
      minor: parseInt(version[1], 10),
      revision: parseInt(version[2], 10),
      preRelease: preRelease,
      toString: function () {
        return [version[0], version[1], version[2]].join('.') + (preRelease ? '-' + preRelease : '');
      }
    };
  };

  MediumEditor.version = MediumEditor.parseVersionString.call(this, ({
    // grunt-bump looks for this:
    'version': '5.10.0'
  }).version);

  return MediumEditor;
}()));

/*!

 handlebars v4.0.5

Copyright (C) 2011-2015 by Yehuda Katz

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

@license
*/
!function(a,b){"object"==typeof exports&&"object"==typeof module?module.exports=b():"function"==typeof define&&define.amd?define([],b):"object"==typeof exports?exports.Handlebars=b():a.Handlebars=b()}(this,function(){return function(a){function b(d){if(c[d])return c[d].exports;var e=c[d]={exports:{},id:d,loaded:!1};return a[d].call(e.exports,e,e.exports,b),e.loaded=!0,e.exports}var c={};return b.m=a,b.c=c,b.p="",b(0)}([function(a,b,c){"use strict";function d(){var a=new h.HandlebarsEnvironment;return n.extend(a,h),a.SafeString=j["default"],a.Exception=l["default"],a.Utils=n,a.escapeExpression=n.escapeExpression,a.VM=p,a.template=function(b){return p.template(b,a)},a}var e=c(1)["default"],f=c(2)["default"];b.__esModule=!0;var g=c(3),h=e(g),i=c(17),j=f(i),k=c(5),l=f(k),m=c(4),n=e(m),o=c(18),p=e(o),q=c(19),r=f(q),s=d();s.create=d,r["default"](s),s["default"]=s,b["default"]=s,a.exports=b["default"]},function(a,b){"use strict";b["default"]=function(a){if(a&&a.__esModule)return a;var b={};if(null!=a)for(var c in a)Object.prototype.hasOwnProperty.call(a,c)&&(b[c]=a[c]);return b["default"]=a,b},b.__esModule=!0},function(a,b){"use strict";b["default"]=function(a){return a&&a.__esModule?a:{"default":a}},b.__esModule=!0},function(a,b,c){"use strict";function d(a,b,c){this.helpers=a||{},this.partials=b||{},this.decorators=c||{},i.registerDefaultHelpers(this),j.registerDefaultDecorators(this)}var e=c(2)["default"];b.__esModule=!0,b.HandlebarsEnvironment=d;var f=c(4),g=c(5),h=e(g),i=c(6),j=c(14),k=c(16),l=e(k),m="4.0.5";b.VERSION=m;var n=7;b.COMPILER_REVISION=n;var o={1:"<= 1.0.rc.2",2:"== 1.0.0-rc.3",3:"== 1.0.0-rc.4",4:"== 1.x.x",5:"== 2.0.0-alpha.x",6:">= 2.0.0-beta.1",7:">= 4.0.0"};b.REVISION_CHANGES=o;var p="[object Object]";d.prototype={constructor:d,logger:l["default"],log:l["default"].log,registerHelper:function(a,b){if(f.toString.call(a)===p){if(b)throw new h["default"]("Arg not supported with multiple helpers");f.extend(this.helpers,a)}else this.helpers[a]=b},unregisterHelper:function(a){delete this.helpers[a]},registerPartial:function(a,b){if(f.toString.call(a)===p)f.extend(this.partials,a);else{if("undefined"==typeof b)throw new h["default"]('Attempting to register a partial called "'+a+'" as undefined');this.partials[a]=b}},unregisterPartial:function(a){delete this.partials[a]},registerDecorator:function(a,b){if(f.toString.call(a)===p){if(b)throw new h["default"]("Arg not supported with multiple decorators");f.extend(this.decorators,a)}else this.decorators[a]=b},unregisterDecorator:function(a){delete this.decorators[a]}};var q=l["default"].log;b.log=q,b.createFrame=f.createFrame,b.logger=l["default"]},function(a,b){"use strict";function c(a){return k[a]}function d(a){for(var b=1;b<arguments.length;b++)for(var c in arguments[b])Object.prototype.hasOwnProperty.call(arguments[b],c)&&(a[c]=arguments[b][c]);return a}function e(a,b){for(var c=0,d=a.length;d>c;c++)if(a[c]===b)return c;return-1}function f(a){if("string"!=typeof a){if(a&&a.toHTML)return a.toHTML();if(null==a)return"";if(!a)return a+"";a=""+a}return m.test(a)?a.replace(l,c):a}function g(a){return a||0===a?p(a)&&0===a.length?!0:!1:!0}function h(a){var b=d({},a);return b._parent=a,b}function i(a,b){return a.path=b,a}function j(a,b){return(a?a+".":"")+b}b.__esModule=!0,b.extend=d,b.indexOf=e,b.escapeExpression=f,b.isEmpty=g,b.createFrame=h,b.blockParams=i,b.appendContextPath=j;var k={"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#x27;","`":"&#x60;","=":"&#x3D;"},l=/[&<>"'`=]/g,m=/[&<>"'`=]/,n=Object.prototype.toString;b.toString=n;var o=function(a){return"function"==typeof a};o(/x/)&&(b.isFunction=o=function(a){return"function"==typeof a&&"[object Function]"===n.call(a)}),b.isFunction=o;var p=Array.isArray||function(a){return a&&"object"==typeof a?"[object Array]"===n.call(a):!1};b.isArray=p},function(a,b){"use strict";function c(a,b){var e=b&&b.loc,f=void 0,g=void 0;e&&(f=e.start.line,g=e.start.column,a+=" - "+f+":"+g);for(var h=Error.prototype.constructor.call(this,a),i=0;i<d.length;i++)this[d[i]]=h[d[i]];Error.captureStackTrace&&Error.captureStackTrace(this,c),e&&(this.lineNumber=f,this.column=g)}b.__esModule=!0;var d=["description","fileName","lineNumber","message","name","number","stack"];c.prototype=new Error,b["default"]=c,a.exports=b["default"]},function(a,b,c){"use strict";function d(a){g["default"](a),i["default"](a),k["default"](a),m["default"](a),o["default"](a),q["default"](a),s["default"](a)}var e=c(2)["default"];b.__esModule=!0,b.registerDefaultHelpers=d;var f=c(7),g=e(f),h=c(8),i=e(h),j=c(9),k=e(j),l=c(10),m=e(l),n=c(11),o=e(n),p=c(12),q=e(p),r=c(13),s=e(r)},function(a,b,c){"use strict";b.__esModule=!0;var d=c(4);b["default"]=function(a){a.registerHelper("blockHelperMissing",function(b,c){var e=c.inverse,f=c.fn;if(b===!0)return f(this);if(b===!1||null==b)return e(this);if(d.isArray(b))return b.length>0?(c.ids&&(c.ids=[c.name]),a.helpers.each(b,c)):e(this);if(c.data&&c.ids){var g=d.createFrame(c.data);g.contextPath=d.appendContextPath(c.data.contextPath,c.name),c={data:g}}return f(b,c)})},a.exports=b["default"]},function(a,b,c){"use strict";var d=c(2)["default"];b.__esModule=!0;var e=c(4),f=c(5),g=d(f);b["default"]=function(a){a.registerHelper("each",function(a,b){function c(b,c,f){j&&(j.key=b,j.index=c,j.first=0===c,j.last=!!f,k&&(j.contextPath=k+b)),i+=d(a[b],{data:j,blockParams:e.blockParams([a[b],b],[k+b,null])})}if(!b)throw new g["default"]("Must pass iterator to #each");var d=b.fn,f=b.inverse,h=0,i="",j=void 0,k=void 0;if(b.data&&b.ids&&(k=e.appendContextPath(b.data.contextPath,b.ids[0])+"."),e.isFunction(a)&&(a=a.call(this)),b.data&&(j=e.createFrame(b.data)),a&&"object"==typeof a)if(e.isArray(a))for(var l=a.length;l>h;h++)h in a&&c(h,h,h===a.length-1);else{var m=void 0;for(var n in a)a.hasOwnProperty(n)&&(void 0!==m&&c(m,h-1),m=n,h++);void 0!==m&&c(m,h-1,!0)}return 0===h&&(i=f(this)),i})},a.exports=b["default"]},function(a,b,c){"use strict";var d=c(2)["default"];b.__esModule=!0;var e=c(5),f=d(e);b["default"]=function(a){a.registerHelper("helperMissing",function(){if(1!==arguments.length)throw new f["default"]('Missing helper: "'+arguments[arguments.length-1].name+'"')})},a.exports=b["default"]},function(a,b,c){"use strict";b.__esModule=!0;var d=c(4);b["default"]=function(a){a.registerHelper("if",function(a,b){return d.isFunction(a)&&(a=a.call(this)),!b.hash.includeZero&&!a||d.isEmpty(a)?b.inverse(this):b.fn(this)}),a.registerHelper("unless",function(b,c){return a.helpers["if"].call(this,b,{fn:c.inverse,inverse:c.fn,hash:c.hash})})},a.exports=b["default"]},function(a,b){"use strict";b.__esModule=!0,b["default"]=function(a){a.registerHelper("log",function(){for(var b=[void 0],c=arguments[arguments.length-1],d=0;d<arguments.length-1;d++)b.push(arguments[d]);var e=1;null!=c.hash.level?e=c.hash.level:c.data&&null!=c.data.level&&(e=c.data.level),b[0]=e,a.log.apply(a,b)})},a.exports=b["default"]},function(a,b){"use strict";b.__esModule=!0,b["default"]=function(a){a.registerHelper("lookup",function(a,b){return a&&a[b]})},a.exports=b["default"]},function(a,b,c){"use strict";b.__esModule=!0;var d=c(4);b["default"]=function(a){a.registerHelper("with",function(a,b){d.isFunction(a)&&(a=a.call(this));var c=b.fn;if(d.isEmpty(a))return b.inverse(this);var e=b.data;return b.data&&b.ids&&(e=d.createFrame(b.data),e.contextPath=d.appendContextPath(b.data.contextPath,b.ids[0])),c(a,{data:e,blockParams:d.blockParams([a],[e&&e.contextPath])})})},a.exports=b["default"]},function(a,b,c){"use strict";function d(a){g["default"](a)}var e=c(2)["default"];b.__esModule=!0,b.registerDefaultDecorators=d;var f=c(15),g=e(f)},function(a,b,c){"use strict";b.__esModule=!0;var d=c(4);b["default"]=function(a){a.registerDecorator("inline",function(a,b,c,e){var f=a;return b.partials||(b.partials={},f=function(e,f){var g=c.partials;c.partials=d.extend({},g,b.partials);var h=a(e,f);return c.partials=g,h}),b.partials[e.args[0]]=e.fn,f})},a.exports=b["default"]},function(a,b,c){"use strict";b.__esModule=!0;var d=c(4),e={methodMap:["debug","info","warn","error"],level:"info",lookupLevel:function(a){if("string"==typeof a){var b=d.indexOf(e.methodMap,a.toLowerCase());a=b>=0?b:parseInt(a,10)}return a},log:function(a){if(a=e.lookupLevel(a),"undefined"!=typeof console&&e.lookupLevel(e.level)<=a){var b=e.methodMap[a];console[b]||(b="log");for(var c=arguments.length,d=Array(c>1?c-1:0),f=1;c>f;f++)d[f-1]=arguments[f];console[b].apply(console,d)}}};b["default"]=e,a.exports=b["default"]},function(a,b){"use strict";function c(a){this.string=a}b.__esModule=!0,c.prototype.toString=c.prototype.toHTML=function(){return""+this.string},b["default"]=c,a.exports=b["default"]},function(a,b,c){"use strict";function d(a){var b=a&&a[0]||1,c=r.COMPILER_REVISION;if(b!==c){if(c>b){var d=r.REVISION_CHANGES[c],e=r.REVISION_CHANGES[b];throw new q["default"]("Template was precompiled with an older version of Handlebars than the current runtime. Please update your precompiler to a newer version ("+d+") or downgrade your runtime to an older version ("+e+").")}throw new q["default"]("Template was precompiled with a newer version of Handlebars than the current runtime. Please update your runtime to a newer version ("+a[1]+").")}}function e(a,b){function c(c,d,e){e.hash&&(d=o.extend({},d,e.hash),e.ids&&(e.ids[0]=!0)),c=b.VM.resolvePartial.call(this,c,d,e);var f=b.VM.invokePartial.call(this,c,d,e);if(null==f&&b.compile&&(e.partials[e.name]=b.compile(c,a.compilerOptions,b),f=e.partials[e.name](d,e)),null!=f){if(e.indent){for(var g=f.split("\n"),h=0,i=g.length;i>h&&(g[h]||h+1!==i);h++)g[h]=e.indent+g[h];f=g.join("\n")}return f}throw new q["default"]("The partial "+e.name+" could not be compiled when running in runtime-only mode")}function d(b){function c(b){return""+a.main(e,b,e.helpers,e.partials,g,i,h)}var f=arguments.length<=1||void 0===arguments[1]?{}:arguments[1],g=f.data;d._setup(f),!f.partial&&a.useData&&(g=j(b,g));var h=void 0,i=a.useBlockParams?[]:void 0;return a.useDepths&&(h=f.depths?b!==f.depths[0]?[b].concat(f.depths):f.depths:[b]),(c=k(a.main,c,e,f.depths||[],g,i))(b,f)}if(!b)throw new q["default"]("No environment passed to template");if(!a||!a.main)throw new q["default"]("Unknown template object: "+typeof a);a.main.decorator=a.main_d,b.VM.checkRevision(a.compiler);var e={strict:function(a,b){if(!(b in a))throw new q["default"]('"'+b+'" not defined in '+a);return a[b]},lookup:function(a,b){for(var c=a.length,d=0;c>d;d++)if(a[d]&&null!=a[d][b])return a[d][b]},lambda:function(a,b){return"function"==typeof a?a.call(b):a},escapeExpression:o.escapeExpression,invokePartial:c,fn:function(b){var c=a[b];return c.decorator=a[b+"_d"],c},programs:[],program:function(a,b,c,d,e){var g=this.programs[a],h=this.fn(a);return b||e||d||c?g=f(this,a,h,b,c,d,e):g||(g=this.programs[a]=f(this,a,h)),g},data:function(a,b){for(;a&&b--;)a=a._parent;return a},merge:function(a,b){var c=a||b;return a&&b&&a!==b&&(c=o.extend({},b,a)),c},noop:b.VM.noop,compilerInfo:a.compiler};return d.isTop=!0,d._setup=function(c){c.partial?(e.helpers=c.helpers,e.partials=c.partials,e.decorators=c.decorators):(e.helpers=e.merge(c.helpers,b.helpers),a.usePartial&&(e.partials=e.merge(c.partials,b.partials)),(a.usePartial||a.useDecorators)&&(e.decorators=e.merge(c.decorators,b.decorators)))},d._child=function(b,c,d,g){if(a.useBlockParams&&!d)throw new q["default"]("must pass block params");if(a.useDepths&&!g)throw new q["default"]("must pass parent depths");return f(e,b,a[b],c,0,d,g)},d}function f(a,b,c,d,e,f,g){function h(b){var e=arguments.length<=1||void 0===arguments[1]?{}:arguments[1],h=g;return g&&b!==g[0]&&(h=[b].concat(g)),c(a,b,a.helpers,a.partials,e.data||d,f&&[e.blockParams].concat(f),h)}return h=k(c,h,a,g,d,f),h.program=b,h.depth=g?g.length:0,h.blockParams=e||0,h}function g(a,b,c){return a?a.call||c.name||(c.name=a,a=c.partials[a]):a="@partial-block"===c.name?c.data["partial-block"]:c.partials[c.name],a}function h(a,b,c){c.partial=!0,c.ids&&(c.data.contextPath=c.ids[0]||c.data.contextPath);var d=void 0;if(c.fn&&c.fn!==i&&(c.data=r.createFrame(c.data),d=c.data["partial-block"]=c.fn,d.partials&&(c.partials=o.extend({},c.partials,d.partials))),void 0===a&&d&&(a=d),void 0===a)throw new q["default"]("The partial "+c.name+" could not be found");return a instanceof Function?a(b,c):void 0}function i(){return""}function j(a,b){return b&&"root"in b||(b=b?r.createFrame(b):{},b.root=a),b}function k(a,b,c,d,e,f){if(a.decorator){var g={};b=a.decorator(b,g,c,d&&d[0],e,f,d),o.extend(b,g)}return b}var l=c(1)["default"],m=c(2)["default"];b.__esModule=!0,b.checkRevision=d,b.template=e,b.wrapProgram=f,b.resolvePartial=g,b.invokePartial=h,b.noop=i;var n=c(4),o=l(n),p=c(5),q=m(p),r=c(3)},function(a,b){(function(c){"use strict";b.__esModule=!0,b["default"]=function(a){var b="undefined"!=typeof c?c:window,d=b.Handlebars;a.noConflict=function(){return b.Handlebars===a&&(b.Handlebars=d),a}},a.exports=b["default"]}).call(b,function(){return this}())}])});


!function(d,B,m,f){function v(a,b){var c=Math.max(0,a[0]-b[0],b[0]-a[1]),e=Math.max(0,a[2]-b[1],b[1]-a[3]);return c+e}function w(a,b,c,e){var k=a.length;e=e?"offset":"position";for(c=c||0;k--;){var g=a[k].el?a[k].el:d(a[k]),l=g[e]();l.left+=parseInt(g.css("margin-left"),10);l.top+=parseInt(g.css("margin-top"),10);b[k]=[l.left-c,l.left+g.outerWidth()+c,l.top-c,l.top+g.outerHeight()+c]}}function p(a,b){var c=b.offset();return{left:a.left-c.left,top:a.top-c.top}}function x(a,b,c){b=[b.left,b.top];c=
  c&&[c.left,c.top];for(var e,k=a.length,d=[];k--;)e=a[k],d[k]=[k,v(e,b),c&&v(e,c)];return d=d.sort(function(a,b){return b[1]-a[1]||b[2]-a[2]||b[0]-a[0]})}function q(a){this.options=d.extend({},n,a);this.containers=[];this.options.rootGroup||(this.scrollProxy=d.proxy(this.scroll,this),this.dragProxy=d.proxy(this.drag,this),this.dropProxy=d.proxy(this.drop,this),this.placeholder=d(this.options.placeholder),a.isValidTarget||(this.options.isValidTarget=f))}function t(a,b){this.el=a;this.options=d.extend({},
  z,b);this.group=q.get(this.options);this.rootGroup=this.options.rootGroup||this.group;this.handle=this.rootGroup.options.handle||this.rootGroup.options.itemSelector;var c=this.rootGroup.options.itemPath;this.target=c?this.el.find(c):this.el;this.target.on(r.start,this.handle,d.proxy(this.dragInit,this));this.options.drop&&this.group.containers.push(this)}var r,z={drag:!0,drop:!0,exclude:"",nested:!0,vertical:!0},n={afterMove:function(a,b,c){},containerPath:"",containerSelector:"ol, ul",distance:0,
  delay:0,handle:"",itemPath:"",itemSelector:"li",bodyClass:"dragging",draggedClass:"dragged",isValidTarget:function(a,b){return!0},onCancel:function(a,b,c,e){},onDrag:function(a,b,c,e){a.css(b)},onDragStart:function(a,b,c,e){a.css({height:a.outerHeight(),width:a.outerWidth()});a.addClass(b.group.options.draggedClass);d("body").addClass(b.group.options.bodyClass)},onDrop:function(a,b,c,e){a.removeClass(b.group.options.draggedClass).removeAttr("style");d("body").removeClass(b.group.options.bodyClass)},
  onMousedown:function(a,b,c){if(!c.target.nodeName.match(/^(input|select|textarea)$/i))return c.preventDefault(),!0},placeholderClass:"placeholder",placeholder:'<li class="placeholder"></li>',pullPlaceholder:!0,serialize:function(a,b,c){a=d.extend({},a.data());if(c)return[b];b[0]&&(a.children=b);delete a.subContainers;delete a.sortable;return a},tolerance:0},s={},y=0,A={left:0,top:0,bottom:0,right:0};r={start:"touchstart.sortable mousedown.sortable",drop:"touchend.sortable touchcancel.sortable mouseup.sortable",
  drag:"touchmove.sortable mousemove.sortable",scroll:"scroll.sortable"};q.get=function(a){s[a.group]||(a.group===f&&(a.group=y++),s[a.group]=new q(a));return s[a.group]};q.prototype={dragInit:function(a,b){this.$document=d(b.el[0].ownerDocument);var c=d(a.target).closest(this.options.itemSelector);c.length&&(this.item=c,this.itemContainer=b,!this.item.is(this.options.exclude)&&this.options.onMousedown(this.item,n.onMousedown,a)&&(this.setPointer(a),this.toggleListeners("on"),this.setupDelayTimer(),
  this.dragInitDone=!0))},drag:function(a){if(!this.dragging){if(!this.distanceMet(a)||!this.delayMet)return;this.options.onDragStart(this.item,this.itemContainer,n.onDragStart,a);this.item.before(this.placeholder);this.dragging=!0}this.setPointer(a);this.options.onDrag(this.item,p(this.pointer,this.item.offsetParent()),n.onDrag,a);a=this.getPointer(a);var b=this.sameResultBox,c=this.options.tolerance;(!b||b.top-c>a.top||b.bottom+c<a.top||b.left-c>a.left||b.right+c<a.left)&&!this.searchValidTarget()&&
(this.placeholder.detach(),this.lastAppendedItem=f)},drop:function(a){this.toggleListeners("off");this.dragInitDone=!1;if(this.dragging){if(this.placeholder.closest("html")[0])this.placeholder.before(this.item).detach();else this.options.onCancel(this.item,this.itemContainer,n.onCancel,a);this.options.onDrop(this.item,this.getContainer(this.item),n.onDrop,a);this.clearDimensions();this.clearOffsetParent();this.lastAppendedItem=this.sameResultBox=f;this.dragging=!1}},searchValidTarget:function(a,b){a||
(a=this.relativePointer||this.pointer,b=this.lastRelativePointer||this.lastPointer);for(var c=x(this.getContainerDimensions(),a,b),e=c.length;e--;){var d=c[e][0];if(!c[e][1]||this.options.pullPlaceholder)if(d=this.containers[d],!d.disabled){if(!this.$getOffsetParent()){var g=d.getItemOffsetParent();a=p(a,g);b=p(b,g)}if(d.searchValidTarget(a,b))return!0}}this.sameResultBox&&(this.sameResultBox=f)},movePlaceholder:function(a,b,c,e){var d=this.lastAppendedItem;if(e||!d||d[0]!==b[0])b[c](this.placeholder),
  this.lastAppendedItem=b,this.sameResultBox=e,this.options.afterMove(this.placeholder,a,b)},getContainerDimensions:function(){this.containerDimensions||w(this.containers,this.containerDimensions=[],this.options.tolerance,!this.$getOffsetParent());return this.containerDimensions},getContainer:function(a){return a.closest(this.options.containerSelector).data(m)},$getOffsetParent:function(){if(this.offsetParent===f){var a=this.containers.length-1,b=this.containers[a].getItemOffsetParent();if(!this.options.rootGroup)for(;a--;)if(b[0]!=
  this.containers[a].getItemOffsetParent()[0]){b=!1;break}this.offsetParent=b}return this.offsetParent},setPointer:function(a){a=this.getPointer(a);if(this.$getOffsetParent()){var b=p(a,this.$getOffsetParent());this.lastRelativePointer=this.relativePointer;this.relativePointer=b}this.lastPointer=this.pointer;this.pointer=a},distanceMet:function(a){a=this.getPointer(a);return Math.max(Math.abs(this.pointer.left-a.left),Math.abs(this.pointer.top-a.top))>=this.options.distance},getPointer:function(a){var b=
  a.originalEvent||a.originalEvent.touches&&a.originalEvent.touches[0];return{left:a.pageX||b.pageX,top:a.pageY||b.pageY}},setupDelayTimer:function(){var a=this;this.delayMet=!this.options.delay;this.delayMet||(clearTimeout(this._mouseDelayTimer),this._mouseDelayTimer=setTimeout(function(){a.delayMet=!0},this.options.delay))},scroll:function(a){this.clearDimensions();this.clearOffsetParent()},toggleListeners:function(a){var b=this;d.each(["drag","drop","scroll"],function(c,e){b.$document[a](r[e],b[e+
"Proxy"])})},clearOffsetParent:function(){this.offsetParent=f},clearDimensions:function(){this.traverse(function(a){a._clearDimensions()})},traverse:function(a){a(this);for(var b=this.containers.length;b--;)this.containers[b].traverse(a)},_clearDimensions:function(){this.containerDimensions=f},_destroy:function(){s[this.options.group]=f}};t.prototype={dragInit:function(a){var b=this.rootGroup;!this.disabled&&!b.dragInitDone&&this.options.drag&&this.isValidDrag(a)&&b.dragInit(a,this)},isValidDrag:function(a){return 1==
  a.which||"touchstart"==a.type&&1==a.originalEvent.touches.length},searchValidTarget:function(a,b){var c=x(this.getItemDimensions(),a,b),e=c.length,d=this.rootGroup,g=!d.options.isValidTarget||d.options.isValidTarget(d.item,this);if(!e&&g)return d.movePlaceholder(this,this.target,"append"),!0;for(;e--;)if(d=c[e][0],!c[e][1]&&this.hasChildGroup(d)){if(this.getContainerGroup(d).searchValidTarget(a,b))return!0}else if(g)return this.movePlaceholder(d,a),!0},movePlaceholder:function(a,b){var c=d(this.items[a]),
  e=this.itemDimensions[a],k="after",g=c.outerWidth(),f=c.outerHeight(),h=c.offset(),h={left:h.left,right:h.left+g,top:h.top,bottom:h.top+f};this.options.vertical?b.top<=(e[2]+e[3])/2?(k="before",h.bottom-=f/2):h.top+=f/2:b.left<=(e[0]+e[1])/2?(k="before",h.right-=g/2):h.left+=g/2;this.hasChildGroup(a)&&(h=A);this.rootGroup.movePlaceholder(this,c,k,h)},getItemDimensions:function(){this.itemDimensions||(this.items=this.$getChildren(this.el,"item").filter(":not(."+this.group.options.placeholderClass+
  ", ."+this.group.options.draggedClass+")").get(),w(this.items,this.itemDimensions=[],this.options.tolerance));return this.itemDimensions},getItemOffsetParent:function(){var a=this.el;return"relative"===a.css("position")||"absolute"===a.css("position")||"fixed"===a.css("position")?a:a.offsetParent()},hasChildGroup:function(a){return this.options.nested&&this.getContainerGroup(a)},getContainerGroup:function(a){var b=d.data(this.items[a],"subContainers");if(b===f){var c=this.$getChildren(this.items[a],
  "container"),b=!1;c[0]&&(b=d.extend({},this.options,{rootGroup:this.rootGroup,group:y++}),b=c[m](b).data(m).group);d.data(this.items[a],"subContainers",b)}return b},$getChildren:function(a,b){var c=this.rootGroup.options,e=c[b+"Path"],c=c[b+"Selector"];a=d(a);e&&(a=a.find(e));return a.children(c)},_serialize:function(a,b){var c=this,e=this.$getChildren(a,b?"item":"container").not(this.options.exclude).map(function(){return c._serialize(d(this),!b)}).get();return this.rootGroup.options.serialize(a,
  e,b)},traverse:function(a){d.each(this.items||[],function(b){(b=d.data(this,"subContainers"))&&b.traverse(a)});a(this)},_clearDimensions:function(){this.itemDimensions=f},_destroy:function(){var a=this;this.target.off(r.start,this.handle);this.el.removeData(m);this.options.drop&&(this.group.containers=d.grep(this.group.containers,function(b){return b!=a}));d.each(this.items||[],function(){d.removeData(this,"subContainers")})}};var u={enable:function(){this.traverse(function(a){a.disabled=!1})},disable:function(){this.traverse(function(a){a.disabled=
  !0})},serialize:function(){return this._serialize(this.el,!0)},refresh:function(){this.traverse(function(a){a._clearDimensions()})},destroy:function(){this.traverse(function(a){a._destroy()})}};d.extend(t.prototype,u);d.fn[m]=function(a){var b=Array.prototype.slice.call(arguments,1);return this.map(function(){var c=d(this),e=c.data(m);if(e&&u[a])return u[a].apply(e,b)||this;e||a!==f&&"object"!==typeof a||c.data(m,new t(c,a));return this})}}(jQuery,window,"sortable");

/*!
* jQuery Cycle2; version: 2.1.6 build: 20141007
* http://jquery.malsup.com/cycle2/
* Copyright (c) 2014 M. Alsup; Dual licensed: MIT/GPL
*/
!function(a){"use strict";function b(a){return(a||"").toLowerCase()}var c="2.1.6";a.fn.cycle=function(c){var d;return 0!==this.length||a.isReady?this.each(function(){var d,e,f,g,h=a(this),i=a.fn.cycle.log;if(!h.data("cycle.opts")){(h.data("cycle-log")===!1||c&&c.log===!1||e&&e.log===!1)&&(i=a.noop),i("--c2 init--"),d=h.data();for(var j in d)d.hasOwnProperty(j)&&/^cycle[A-Z]+/.test(j)&&(g=d[j],f=j.match(/^cycle(.*)/)[1].replace(/^[A-Z]/,b),i(f+":",g,"("+typeof g+")"),d[f]=g);e=a.extend({},a.fn.cycle.defaults,d,c||{}),e.timeoutId=0,e.paused=e.paused||!1,e.container=h,e._maxZ=e.maxZ,e.API=a.extend({_container:h},a.fn.cycle.API),e.API.log=i,e.API.trigger=function(a,b){return e.container.trigger(a,b),e.API},h.data("cycle.opts",e),h.data("cycle.API",e.API),e.API.trigger("cycle-bootstrap",[e,e.API]),e.API.addInitialSlides(),e.API.preInitSlideshow(),e.slides.length&&e.API.initSlideshow()}}):(d={s:this.selector,c:this.context},a.fn.cycle.log("requeuing slideshow (dom not ready)"),a(function(){a(d.s,d.c).cycle(c)}),this)},a.fn.cycle.API={opts:function(){return this._container.data("cycle.opts")},addInitialSlides:function(){var b=this.opts(),c=b.slides;b.slideCount=0,b.slides=a(),c=c.jquery?c:b.container.find(c),b.random&&c.sort(function(){return Math.random()-.5}),b.API.add(c)},preInitSlideshow:function(){var b=this.opts();b.API.trigger("cycle-pre-initialize",[b]);var c=a.fn.cycle.transitions[b.fx];c&&a.isFunction(c.preInit)&&c.preInit(b),b._preInitialized=!0},postInitSlideshow:function(){var b=this.opts();b.API.trigger("cycle-post-initialize",[b]);var c=a.fn.cycle.transitions[b.fx];c&&a.isFunction(c.postInit)&&c.postInit(b)},initSlideshow:function(){var b,c=this.opts(),d=c.container;c.API.calcFirstSlide(),"static"==c.container.css("position")&&c.container.css("position","relative"),a(c.slides[c.currSlide]).css({opacity:1,display:"block",visibility:"visible"}),c.API.stackSlides(c.slides[c.currSlide],c.slides[c.nextSlide],!c.reverse),c.pauseOnHover&&(c.pauseOnHover!==!0&&(d=a(c.pauseOnHover)),d.hover(function(){c.API.pause(!0)},function(){c.API.resume(!0)})),c.timeout&&(b=c.API.getSlideOpts(c.currSlide),c.API.queueTransition(b,b.timeout+c.delay)),c._initialized=!0,c.API.updateView(!0),c.API.trigger("cycle-initialized",[c]),c.API.postInitSlideshow()},pause:function(b){var c=this.opts(),d=c.API.getSlideOpts(),e=c.hoverPaused||c.paused;b?c.hoverPaused=!0:c.paused=!0,e||(c.container.addClass("cycle-paused"),c.API.trigger("cycle-paused",[c]).log("cycle-paused"),d.timeout&&(clearTimeout(c.timeoutId),c.timeoutId=0,c._remainingTimeout-=a.now()-c._lastQueue,(c._remainingTimeout<0||isNaN(c._remainingTimeout))&&(c._remainingTimeout=void 0)))},resume:function(a){var b=this.opts(),c=!b.hoverPaused&&!b.paused;a?b.hoverPaused=!1:b.paused=!1,c||(b.container.removeClass("cycle-paused"),0===b.slides.filter(":animated").length&&b.API.queueTransition(b.API.getSlideOpts(),b._remainingTimeout),b.API.trigger("cycle-resumed",[b,b._remainingTimeout]).log("cycle-resumed"))},add:function(b,c){var d,e=this.opts(),f=e.slideCount,g=!1;"string"==a.type(b)&&(b=a.trim(b)),a(b).each(function(){var b,d=a(this);c?e.container.prepend(d):e.container.append(d),e.slideCount++,b=e.API.buildSlideOpts(d),e.slides=c?a(d).add(e.slides):e.slides.add(d),e.API.initSlide(b,d,--e._maxZ),d.data("cycle.opts",b),e.API.trigger("cycle-slide-added",[e,b,d])}),e.API.updateView(!0),g=e._preInitialized&&2>f&&e.slideCount>=1,g&&(e._initialized?e.timeout&&(d=e.slides.length,e.nextSlide=e.reverse?d-1:1,e.timeoutId||e.API.queueTransition(e)):e.API.initSlideshow())},calcFirstSlide:function(){var a,b=this.opts();a=parseInt(b.startingSlide||0,10),(a>=b.slides.length||0>a)&&(a=0),b.currSlide=a,b.reverse?(b.nextSlide=a-1,b.nextSlide<0&&(b.nextSlide=b.slides.length-1)):(b.nextSlide=a+1,b.nextSlide==b.slides.length&&(b.nextSlide=0))},calcNextSlide:function(){var a,b=this.opts();b.reverse?(a=b.nextSlide-1<0,b.nextSlide=a?b.slideCount-1:b.nextSlide-1,b.currSlide=a?0:b.nextSlide+1):(a=b.nextSlide+1==b.slides.length,b.nextSlide=a?0:b.nextSlide+1,b.currSlide=a?b.slides.length-1:b.nextSlide-1)},calcTx:function(b,c){var d,e=b;return e._tempFx?d=a.fn.cycle.transitions[e._tempFx]:c&&e.manualFx&&(d=a.fn.cycle.transitions[e.manualFx]),d||(d=a.fn.cycle.transitions[e.fx]),e._tempFx=null,this.opts()._tempFx=null,d||(d=a.fn.cycle.transitions.fade,e.API.log('Transition "'+e.fx+'" not found.  Using fade.')),d},prepareTx:function(a,b){var c,d,e,f,g,h=this.opts();return h.slideCount<2?void(h.timeoutId=0):(!a||h.busy&&!h.manualTrump||(h.API.stopTransition(),h.busy=!1,clearTimeout(h.timeoutId),h.timeoutId=0),void(h.busy||(0!==h.timeoutId||a)&&(d=h.slides[h.currSlide],e=h.slides[h.nextSlide],f=h.API.getSlideOpts(h.nextSlide),g=h.API.calcTx(f,a),h._tx=g,a&&void 0!==f.manualSpeed&&(f.speed=f.manualSpeed),h.nextSlide!=h.currSlide&&(a||!h.paused&&!h.hoverPaused&&h.timeout)?(h.API.trigger("cycle-before",[f,d,e,b]),g.before&&g.before(f,d,e,b),c=function(){h.busy=!1,h.container.data("cycle.opts")&&(g.after&&g.after(f,d,e,b),h.API.trigger("cycle-after",[f,d,e,b]),h.API.queueTransition(f),h.API.updateView(!0))},h.busy=!0,g.transition?g.transition(f,d,e,b,c):h.API.doTransition(f,d,e,b,c),h.API.calcNextSlide(),h.API.updateView()):h.API.queueTransition(f))))},doTransition:function(b,c,d,e,f){var g=b,h=a(c),i=a(d),j=function(){i.animate(g.animIn||{opacity:1},g.speed,g.easeIn||g.easing,f)};i.css(g.cssBefore||{}),h.animate(g.animOut||{},g.speed,g.easeOut||g.easing,function(){h.css(g.cssAfter||{}),g.sync||j()}),g.sync&&j()},queueTransition:function(b,c){var d=this.opts(),e=void 0!==c?c:b.timeout;return 0===d.nextSlide&&0===--d.loop?(d.API.log("terminating; loop=0"),d.timeout=0,e?setTimeout(function(){d.API.trigger("cycle-finished",[d])},e):d.API.trigger("cycle-finished",[d]),void(d.nextSlide=d.currSlide)):void 0!==d.continueAuto&&(d.continueAuto===!1||a.isFunction(d.continueAuto)&&d.continueAuto()===!1)?(d.API.log("terminating automatic transitions"),d.timeout=0,void(d.timeoutId&&clearTimeout(d.timeoutId))):void(e&&(d._lastQueue=a.now(),void 0===c&&(d._remainingTimeout=b.timeout),d.paused||d.hoverPaused||(d.timeoutId=setTimeout(function(){d.API.prepareTx(!1,!d.reverse)},e))))},stopTransition:function(){var a=this.opts();a.slides.filter(":animated").length&&(a.slides.stop(!1,!0),a.API.trigger("cycle-transition-stopped",[a])),a._tx&&a._tx.stopTransition&&a._tx.stopTransition(a)},advanceSlide:function(a){var b=this.opts();return clearTimeout(b.timeoutId),b.timeoutId=0,b.nextSlide=b.currSlide+a,b.nextSlide<0?b.nextSlide=b.slides.length-1:b.nextSlide>=b.slides.length&&(b.nextSlide=0),b.API.prepareTx(!0,a>=0),!1},buildSlideOpts:function(c){var d,e,f=this.opts(),g=c.data()||{};for(var h in g)g.hasOwnProperty(h)&&/^cycle[A-Z]+/.test(h)&&(d=g[h],e=h.match(/^cycle(.*)/)[1].replace(/^[A-Z]/,b),f.API.log("["+(f.slideCount-1)+"]",e+":",d,"("+typeof d+")"),g[e]=d);g=a.extend({},a.fn.cycle.defaults,f,g),g.slideNum=f.slideCount;try{delete g.API,delete g.slideCount,delete g.currSlide,delete g.nextSlide,delete g.slides}catch(i){}return g},getSlideOpts:function(b){var c=this.opts();void 0===b&&(b=c.currSlide);var d=c.slides[b],e=a(d).data("cycle.opts");return a.extend({},c,e)},initSlide:function(b,c,d){var e=this.opts();c.css(b.slideCss||{}),d>0&&c.css("zIndex",d),isNaN(b.speed)&&(b.speed=a.fx.speeds[b.speed]||a.fx.speeds._default),b.sync||(b.speed=b.speed/2),c.addClass(e.slideClass)},updateView:function(a,b){var c=this.opts();if(c._initialized){var d=c.API.getSlideOpts(),e=c.slides[c.currSlide];!a&&b!==!0&&(c.API.trigger("cycle-update-view-before",[c,d,e]),c.updateView<0)||(c.slideActiveClass&&c.slides.removeClass(c.slideActiveClass).eq(c.currSlide).addClass(c.slideActiveClass),a&&c.hideNonActive&&c.slides.filter(":not(."+c.slideActiveClass+")").css("visibility","hidden"),0===c.updateView&&setTimeout(function(){c.API.trigger("cycle-update-view",[c,d,e,a])},d.speed/(c.sync?2:1)),0!==c.updateView&&c.API.trigger("cycle-update-view",[c,d,e,a]),a&&c.API.trigger("cycle-update-view-after",[c,d,e]))}},getComponent:function(b){var c=this.opts(),d=c[b];return"string"==typeof d?/^\s*[\>|\+|~]/.test(d)?c.container.find(d):a(d):d.jquery?d:a(d)},stackSlides:function(b,c,d){var e=this.opts();b||(b=e.slides[e.currSlide],c=e.slides[e.nextSlide],d=!e.reverse),a(b).css("zIndex",e.maxZ);var f,g=e.maxZ-2,h=e.slideCount;if(d){for(f=e.currSlide+1;h>f;f++)a(e.slides[f]).css("zIndex",g--);for(f=0;f<e.currSlide;f++)a(e.slides[f]).css("zIndex",g--)}else{for(f=e.currSlide-1;f>=0;f--)a(e.slides[f]).css("zIndex",g--);for(f=h-1;f>e.currSlide;f--)a(e.slides[f]).css("zIndex",g--)}a(c).css("zIndex",e.maxZ-1)},getSlideIndex:function(a){return this.opts().slides.index(a)}},a.fn.cycle.log=function(){window.console&&console.log&&console.log("[cycle2] "+Array.prototype.join.call(arguments," "))},a.fn.cycle.version=function(){return"Cycle2: "+c},a.fn.cycle.transitions={custom:{},none:{before:function(a,b,c,d){a.API.stackSlides(c,b,d),a.cssBefore={opacity:1,visibility:"visible",display:"block"}}},fade:{before:function(b,c,d,e){var f=b.API.getSlideOpts(b.nextSlide).slideCss||{};b.API.stackSlides(c,d,e),b.cssBefore=a.extend(f,{opacity:0,visibility:"visible",display:"block"}),b.animIn={opacity:1},b.animOut={opacity:0}}},fadeout:{before:function(b,c,d,e){var f=b.API.getSlideOpts(b.nextSlide).slideCss||{};b.API.stackSlides(c,d,e),b.cssBefore=a.extend(f,{opacity:1,visibility:"visible",display:"block"}),b.animOut={opacity:0}}},scrollHorz:{before:function(a,b,c,d){a.API.stackSlides(b,c,d);var e=a.container.css("overflow","hidden").width();a.cssBefore={left:d?e:-e,top:0,opacity:1,visibility:"visible",display:"block"},a.cssAfter={zIndex:a._maxZ-2,left:0},a.animIn={left:0},a.animOut={left:d?-e:e}}}},a.fn.cycle.defaults={allowWrap:!0,autoSelector:".cycle-slideshow[data-cycle-auto-init!=false]",delay:0,easing:null,fx:"fade",hideNonActive:!0,loop:0,manualFx:void 0,manualSpeed:void 0,manualTrump:!0,maxZ:100,pauseOnHover:!1,reverse:!1,slideActiveClass:"cycle-slide-active",slideClass:"cycle-slide",slideCss:{position:"absolute",top:0,left:0},slides:"> img",speed:500,startingSlide:0,sync:!0,timeout:4e3,updateView:0},a(document).ready(function(){a(a.fn.cycle.defaults.autoSelector).cycle()})}(jQuery),/*! Cycle2 autoheight plugin; Copyright (c) M.Alsup, 2012; version: 20130913 */
  function(a){"use strict";function b(b,d){var e,f,g,h=d.autoHeight;if("container"==h)f=a(d.slides[d.currSlide]).outerHeight(),d.container.height(f);else if(d._autoHeightRatio)d.container.height(d.container.width()/d._autoHeightRatio);else if("calc"===h||"number"==a.type(h)&&h>=0){if(g="calc"===h?c(b,d):h>=d.slides.length?0:h,g==d._sentinelIndex)return;d._sentinelIndex=g,d._sentinel&&d._sentinel.remove(),e=a(d.slides[g].cloneNode(!0)),e.removeAttr("id name rel").find("[id],[name],[rel]").removeAttr("id name rel"),e.css({position:"static",visibility:"hidden",display:"block"}).prependTo(d.container).addClass("cycle-sentinel cycle-slide").removeClass("cycle-slide-active"),e.find("*").css("visibility","hidden"),d._sentinel=e}}function c(b,c){var d=0,e=-1;return c.slides.each(function(b){var c=a(this).height();c>e&&(e=c,d=b)}),d}function d(b,c,d,e){var f=a(e).outerHeight();c.container.animate({height:f},c.autoHeightSpeed,c.autoHeightEasing)}function e(c,f){f._autoHeightOnResize&&(a(window).off("resize orientationchange",f._autoHeightOnResize),f._autoHeightOnResize=null),f.container.off("cycle-slide-added cycle-slide-removed",b),f.container.off("cycle-destroyed",e),f.container.off("cycle-before",d),f._sentinel&&(f._sentinel.remove(),f._sentinel=null)}a.extend(a.fn.cycle.defaults,{autoHeight:0,autoHeightSpeed:250,autoHeightEasing:null}),a(document).on("cycle-initialized",function(c,f){function g(){b(c,f)}var h,i=f.autoHeight,j=a.type(i),k=null;("string"===j||"number"===j)&&(f.container.on("cycle-slide-added cycle-slide-removed",b),f.container.on("cycle-destroyed",e),"container"==i?f.container.on("cycle-before",d):"string"===j&&/\d+\:\d+/.test(i)&&(h=i.match(/(\d+)\:(\d+)/),h=h[1]/h[2],f._autoHeightRatio=h),"number"!==j&&(f._autoHeightOnResize=function(){clearTimeout(k),k=setTimeout(g,50)},a(window).on("resize orientationchange",f._autoHeightOnResize)),setTimeout(g,30))})}(jQuery),/*! caption plugin for Cycle2;  version: 20130306 */
  function(a){"use strict";a.extend(a.fn.cycle.defaults,{caption:"> .cycle-caption",captionTemplate:"{{slideNum}} / {{slideCount}}",overlay:"> .cycle-overlay",overlayTemplate:"<div>{{title}}</div><div>{{desc}}</div>",captionModule:"caption"}),a(document).on("cycle-update-view",function(b,c,d,e){if("caption"===c.captionModule){a.each(["caption","overlay"],function(){var a=this,b=d[a+"Template"],f=c.API.getComponent(a);f.length&&b?(f.html(c.API.tmpl(b,d,c,e)),f.show()):f.hide()})}}),a(document).on("cycle-destroyed",function(b,c){var d;a.each(["caption","overlay"],function(){var a=this,b=c[a+"Template"];c[a]&&b&&(d=c.API.getComponent("caption"),d.empty())})})}(jQuery),/*! command plugin for Cycle2;  version: 20140415 */
  function(a){"use strict";var b=a.fn.cycle;a.fn.cycle=function(c){var d,e,f,g=a.makeArray(arguments);return"number"==a.type(c)?this.cycle("goto",c):"string"==a.type(c)?this.each(function(){var h;return d=c,f=a(this).data("cycle.opts"),void 0===f?void b.log('slideshow must be initialized before sending commands; "'+d+'" ignored'):(d="goto"==d?"jump":d,e=f.API[d],a.isFunction(e)?(h=a.makeArray(g),h.shift(),e.apply(f.API,h)):void b.log("unknown command: ",d))}):b.apply(this,arguments)},a.extend(a.fn.cycle,b),a.extend(b.API,{next:function(){var a=this.opts();if(!a.busy||a.manualTrump){var b=a.reverse?-1:1;a.allowWrap===!1&&a.currSlide+b>=a.slideCount||(a.API.advanceSlide(b),a.API.trigger("cycle-next",[a]).log("cycle-next"))}},prev:function(){var a=this.opts();if(!a.busy||a.manualTrump){var b=a.reverse?1:-1;a.allowWrap===!1&&a.currSlide+b<0||(a.API.advanceSlide(b),a.API.trigger("cycle-prev",[a]).log("cycle-prev"))}},destroy:function(){this.stop();var b=this.opts(),c=a.isFunction(a._data)?a._data:a.noop;clearTimeout(b.timeoutId),b.timeoutId=0,b.API.stop(),b.API.trigger("cycle-destroyed",[b]).log("cycle-destroyed"),b.container.removeData(),c(b.container[0],"parsedAttrs",!1),b.retainStylesOnDestroy||(b.container.removeAttr("style"),b.slides.removeAttr("style"),b.slides.removeClass(b.slideActiveClass)),b.slides.each(function(){var d=a(this);d.removeData(),d.removeClass(b.slideClass),c(this,"parsedAttrs",!1)})},jump:function(a,b){var c,d=this.opts();if(!d.busy||d.manualTrump){var e=parseInt(a,10);if(isNaN(e)||0>e||e>=d.slides.length)return void d.API.log("goto: invalid slide index: "+e);if(e==d.currSlide)return void d.API.log("goto: skipping, already on slide",e);d.nextSlide=e,clearTimeout(d.timeoutId),d.timeoutId=0,d.API.log("goto: ",e," (zero-index)"),c=d.currSlide<d.nextSlide,d._tempFx=b,d.API.prepareTx(!0,c)}},stop:function(){var b=this.opts(),c=b.container;clearTimeout(b.timeoutId),b.timeoutId=0,b.API.stopTransition(),b.pauseOnHover&&(b.pauseOnHover!==!0&&(c=a(b.pauseOnHover)),c.off("mouseenter mouseleave")),b.API.trigger("cycle-stopped",[b]).log("cycle-stopped")},reinit:function(){var a=this.opts();a.API.destroy(),a.container.cycle()},remove:function(b){for(var c,d,e=this.opts(),f=[],g=1,h=0;h<e.slides.length;h++)c=e.slides[h],h==b?d=c:(f.push(c),a(c).data("cycle.opts").slideNum=g,g++);d&&(e.slides=a(f),e.slideCount--,a(d).remove(),b==e.currSlide?e.API.advanceSlide(1):b<e.currSlide?e.currSlide--:e.currSlide++,e.API.trigger("cycle-slide-removed",[e,b,d]).log("cycle-slide-removed"),e.API.updateView())}}),a(document).on("click.cycle","[data-cycle-cmd]",function(b){b.preventDefault();var c=a(this),d=c.data("cycle-cmd"),e=c.data("cycle-context")||".cycle-slideshow";a(e).cycle(d,c.data("cycle-arg"))})}(jQuery),/*! hash plugin for Cycle2;  version: 20130905 */
  function(a){"use strict";function b(b,c){var d;return b._hashFence?void(b._hashFence=!1):(d=window.location.hash.substring(1),void b.slides.each(function(e){if(a(this).data("cycle-hash")==d){if(c===!0)b.startingSlide=e;else{var f=b.currSlide<e;b.nextSlide=e,b.API.prepareTx(!0,f)}return!1}}))}a(document).on("cycle-pre-initialize",function(c,d){b(d,!0),d._onHashChange=function(){b(d,!1)},a(window).on("hashchange",d._onHashChange)}),a(document).on("cycle-update-view",function(a,b,c){c.hash&&"#"+c.hash!=window.location.hash&&(b._hashFence=!0,window.location.hash=c.hash)}),a(document).on("cycle-destroyed",function(b,c){c._onHashChange&&a(window).off("hashchange",c._onHashChange)})}(jQuery),/*! loader plugin for Cycle2;  version: 20131121 */
  function(a){"use strict";a.extend(a.fn.cycle.defaults,{loader:!1}),a(document).on("cycle-bootstrap",function(b,c){function d(b,d){function f(b){var f;"wait"==c.loader?(h.push(b),0===j&&(h.sort(g),e.apply(c.API,[h,d]),c.container.removeClass("cycle-loading"))):(f=a(c.slides[c.currSlide]),e.apply(c.API,[b,d]),f.show(),c.container.removeClass("cycle-loading"))}function g(a,b){return a.data("index")-b.data("index")}var h=[];if("string"==a.type(b))b=a.trim(b);else if("array"===a.type(b))for(var i=0;i<b.length;i++)b[i]=a(b[i])[0];b=a(b);var j=b.length;j&&(b.css("visibility","hidden").appendTo("body").each(function(b){function g(){0===--i&&(--j,f(k))}var i=0,k=a(this),l=k.is("img")?k:k.find("img");return k.data("index",b),l=l.filter(":not(.cycle-loader-ignore)").filter(':not([src=""])'),l.length?(i=l.length,void l.each(function(){this.complete?g():a(this).load(function(){g()}).on("error",function(){0===--i&&(c.API.log("slide skipped; img not loaded:",this.src),0===--j&&"wait"==c.loader&&e.apply(c.API,[h,d]))})})):(--j,void h.push(k))}),j&&c.container.addClass("cycle-loading"))}var e;c.loader&&(e=c.API.add,c.API.add=d)})}(jQuery),/*! pager plugin for Cycle2;  version: 20140415 */
  function(a){"use strict";function b(b,c,d){var e,f=b.API.getComponent("pager");f.each(function(){var f=a(this);if(c.pagerTemplate){var g=b.API.tmpl(c.pagerTemplate,c,b,d[0]);e=a(g).appendTo(f)}else e=f.children().eq(b.slideCount-1);e.on(b.pagerEvent,function(a){b.pagerEventBubble||a.preventDefault(),b.API.page(f,a.currentTarget)})})}function c(a,b){var c=this.opts();if(!c.busy||c.manualTrump){var d=a.children().index(b),e=d,f=c.currSlide<e;c.currSlide!=e&&(c.nextSlide=e,c._tempFx=c.pagerFx,c.API.prepareTx(!0,f),c.API.trigger("cycle-pager-activated",[c,a,b]))}}a.extend(a.fn.cycle.defaults,{pager:"> .cycle-pager",pagerActiveClass:"cycle-pager-active",pagerEvent:"click.cycle",pagerEventBubble:void 0,pagerTemplate:"<span>&bull;</span>"}),a(document).on("cycle-bootstrap",function(a,c,d){d.buildPagerLink=b}),a(document).on("cycle-slide-added",function(a,b,d,e){b.pager&&(b.API.buildPagerLink(b,d,e),b.API.page=c)}),a(document).on("cycle-slide-removed",function(b,c,d){if(c.pager){var e=c.API.getComponent("pager");e.each(function(){var b=a(this);a(b.children()[d]).remove()})}}),a(document).on("cycle-update-view",function(b,c){var d;c.pager&&(d=c.API.getComponent("pager"),d.each(function(){a(this).children().removeClass(c.pagerActiveClass).eq(c.currSlide).addClass(c.pagerActiveClass)}))}),a(document).on("cycle-destroyed",function(a,b){var c=b.API.getComponent("pager");c&&(c.children().off(b.pagerEvent),b.pagerTemplate&&c.empty())})}(jQuery),/*! prevnext plugin for Cycle2;  version: 20140408 */
  function(a){"use strict";a.extend(a.fn.cycle.defaults,{next:"> .cycle-next",nextEvent:"click.cycle",disabledClass:"disabled",prev:"> .cycle-prev",prevEvent:"click.cycle",swipe:!1}),a(document).on("cycle-initialized",function(a,b){if(b.API.getComponent("next").on(b.nextEvent,function(a){a.preventDefault(),b.API.next()}),b.API.getComponent("prev").on(b.prevEvent,function(a){a.preventDefault(),b.API.prev()}),b.swipe){var c=b.swipeVert?"swipeUp.cycle":"swipeLeft.cycle swipeleft.cycle",d=b.swipeVert?"swipeDown.cycle":"swipeRight.cycle swiperight.cycle";b.container.on(c,function(){b._tempFx=b.swipeFx,b.API.next()}),b.container.on(d,function(){b._tempFx=b.swipeFx,b.API.prev()})}}),a(document).on("cycle-update-view",function(a,b){if(!b.allowWrap){var c=b.disabledClass,d=b.API.getComponent("next"),e=b.API.getComponent("prev"),f=b._prevBoundry||0,g=void 0!==b._nextBoundry?b._nextBoundry:b.slideCount-1;b.currSlide==g?d.addClass(c).prop("disabled",!0):d.removeClass(c).prop("disabled",!1),b.currSlide===f?e.addClass(c).prop("disabled",!0):e.removeClass(c).prop("disabled",!1)}}),a(document).on("cycle-destroyed",function(a,b){b.API.getComponent("prev").off(b.nextEvent),b.API.getComponent("next").off(b.prevEvent),b.container.off("swipeleft.cycle swiperight.cycle swipeLeft.cycle swipeRight.cycle swipeUp.cycle swipeDown.cycle")})}(jQuery),/*! progressive loader plugin for Cycle2;  version: 20130315 */
  function(a){"use strict";a.extend(a.fn.cycle.defaults,{progressive:!1}),a(document).on("cycle-pre-initialize",function(b,c){if(c.progressive){var d,e,f=c.API,g=f.next,h=f.prev,i=f.prepareTx,j=a.type(c.progressive);if("array"==j)d=c.progressive;else if(a.isFunction(c.progressive))d=c.progressive(c);else if("string"==j){if(e=a(c.progressive),d=a.trim(e.html()),!d)return;if(/^(\[)/.test(d))try{d=a.parseJSON(d)}catch(k){return void f.log("error parsing progressive slides",k)}else d=d.split(new RegExp(e.data("cycle-split")||"\n")),d[d.length-1]||d.pop()}i&&(f.prepareTx=function(a,b){var e,f;return a||0===d.length?void i.apply(c.API,[a,b]):void(b&&c.currSlide==c.slideCount-1?(f=d[0],d=d.slice(1),c.container.one("cycle-slide-added",function(a,b){setTimeout(function(){b.API.advanceSlide(1)},50)}),c.API.add(f)):b||0!==c.currSlide?i.apply(c.API,[a,b]):(e=d.length-1,f=d[e],d=d.slice(0,e),c.container.one("cycle-slide-added",function(a,b){setTimeout(function(){b.currSlide=1,b.API.advanceSlide(-1)},50)}),c.API.add(f,!0)))}),g&&(f.next=function(){var a=this.opts();if(d.length&&a.currSlide==a.slideCount-1){var b=d[0];d=d.slice(1),a.container.one("cycle-slide-added",function(a,b){g.apply(b.API),b.container.removeClass("cycle-loading")}),a.container.addClass("cycle-loading"),a.API.add(b)}else g.apply(a.API)}),h&&(f.prev=function(){var a=this.opts();if(d.length&&0===a.currSlide){var b=d.length-1,c=d[b];d=d.slice(0,b),a.container.one("cycle-slide-added",function(a,b){b.currSlide=1,b.API.advanceSlide(-1),b.container.removeClass("cycle-loading")}),a.container.addClass("cycle-loading"),a.API.add(c,!0)}else h.apply(a.API)})}})}(jQuery),/*! tmpl plugin for Cycle2;  version: 20121227 */
  function(a){"use strict";a.extend(a.fn.cycle.defaults,{tmplRegex:"{{((.)?.*?)}}"}),a.extend(a.fn.cycle.API,{tmpl:function(b,c){var d=new RegExp(c.tmplRegex||a.fn.cycle.defaults.tmplRegex,"g"),e=a.makeArray(arguments);return e.shift(),b.replace(d,function(b,c){var d,f,g,h,i=c.split(".");for(d=0;d<e.length;d++)if(g=e[d]){if(i.length>1)for(h=g,f=0;f<i.length;f++)g=h,h=h[i[f]]||c;else h=g[c];if(a.isFunction(h))return h.apply(g,e);if(void 0!==h&&null!==h&&h!=c)return h}return c})}})}(jQuery);
//# sourceMappingURL=jquery.cycle2.js.map

/* Plugin for Cycle2; Copyright (c) 2012 M. Alsup; v20141007 */
!function(a){"use strict";a.extend(a.fn.cycle.defaults,{centerHorz:!1,centerVert:!1}),a(document).on("cycle-pre-initialize",function(b,c){function d(){clearTimeout(i),i=setTimeout(g,50)}function e(){clearTimeout(i),clearTimeout(j),a(window).off("resize orientationchange",d)}function f(){c.slides.each(h)}function g(){h.apply(c.container.find("."+c.slideActiveClass)),clearTimeout(j),j=setTimeout(f,50)}function h(){var b=a(this),d=c.container.width(),e=c.container.height(),f=b.outerWidth(),g=b.outerHeight();f&&(c.centerHorz&&d>=f&&b.css("marginLeft",(d-f)/2),c.centerVert&&e>=g&&b.css("marginTop",(e-g)/2))}if(c.centerHorz||c.centerVert){var i,j;a(window).on("resize orientationchange load",d),c.container.on("cycle-destroyed",e),c.container.on("cycle-initialized cycle-slide-added cycle-slide-removed",function(){d()}),g()}})}(jQuery);


/*!
 * medium-editor-insert-plugin v2.2.3 - jQuery insert plugin for MediumEditor
 *
 * https://github.com/orthes/medium-editor-insert-plugin
 *
 * Copyright (c) 2014 Pavel Linkesch (http://linkesch.sk)
 * Released under the MIT license
 */

this.MediumInsert=this.MediumInsert||{},this.MediumInsert.Templates=this.MediumInsert.Templates||{},this.MediumInsert.Templates["src/js/templates/core-buttons.hbs"]=Handlebars.template({1:function(a,b,c,d,e){var f,g,h=null!=b?b:{},i=c.helperMissing,j="function";return'            <li><a data-addon="'+a.escapeExpression((g=null!=(g=c.key||e&&e.key)?g:i,typeof g===j?g.call(h,{name:"key",hash:{},data:e}):g))+'" data-action="add" class="medium-insert-action">'+(null!=(g=null!=(g=c.label||(null!=b?b.label:b))?g:i,f=typeof g===j?g.call(h,{name:"label",hash:{},data:e}):g)?f:"")+"</a></li>\n"},compiler:[7,">= 4.0.0"],main:function(a,b,c,d,e){var f;return'<div class="medium-insert-buttons" contenteditable="false" style="display: none">\n    <a class="medium-insert-buttons-show">+</a>\n    <ul class="medium-insert-buttons-addons" style="display: none">\n'+(null!=(f=c.each.call(null!=b?b:{},null!=b?b.addons:b,{name:"each",hash:{},fn:a.program(1,e,0),inverse:a.noop,data:e}))?f:"")+"    </ul>\n</div>\n"},useData:!0}),this.MediumInsert.Templates["src/js/templates/core-caption.hbs"]=Handlebars.template({compiler:[7,">= 4.0.0"],main:function(a,b,c,d,e){var f;return'<figcaption contenteditable="true" class="medium-insert-caption-placeholder" data-placeholder="'+a.escapeExpression((f=null!=(f=c.placeholder||(null!=b?b.placeholder:b))?f:c.helperMissing,"function"==typeof f?f.call(null!=b?b:{},{name:"placeholder",hash:{},data:e}):f))+'"></figcaption>'},useData:!0}),this.MediumInsert.Templates["src/js/templates/core-empty-line.hbs"]=Handlebars.template({compiler:[7,">= 4.0.0"],main:function(a,b,c,d,e){return"<p><br></p>\n"},useData:!0}),this.MediumInsert.Templates["src/js/templates/embeds-toolbar.hbs"]=Handlebars.template({1:function(a,b,c,d,e){var f;return'    <div class="medium-insert-embeds-toolbar medium-editor-toolbar medium-toolbar-arrow-under medium-editor-toolbar-active">\n        <ul class="medium-editor-toolbar-actions clearfix">\n'+(null!=(f=c.each.call(null!=b?b:{},null!=b?b.styles:b,{name:"each",hash:{},fn:a.program(2,e,0),inverse:a.noop,data:e}))?f:"")+"        </ul>\n    </div>\n"},2:function(a,b,c,d,e){var f;return null!=(f=c["if"].call(null!=b?b:{},null!=b?b.label:b,{name:"if",hash:{},fn:a.program(3,e,0),inverse:a.noop,data:e}))?f:""},3:function(a,b,c,d,e){var f,g,h=null!=b?b:{},i=c.helperMissing,j="function";return'                    <li>\n                        <button class="medium-editor-action" data-action="'+a.escapeExpression((g=null!=(g=c.key||e&&e.key)?g:i,typeof g===j?g.call(h,{name:"key",hash:{},data:e}):g))+'">'+(null!=(g=null!=(g=c.label||(null!=b?b.label:b))?g:i,f=typeof g===j?g.call(h,{name:"label",hash:{},data:e}):g)?f:"")+"</button>\n                    </li>\n"},5:function(a,b,c,d,e){var f;return'    <div class="medium-insert-embeds-toolbar2 medium-editor-toolbar medium-editor-toolbar-active">\n        <ul class="medium-editor-toolbar-actions clearfix">\n'+(null!=(f=c.each.call(null!=b?b:{},null!=b?b.actions:b,{name:"each",hash:{},fn:a.program(2,e,0),inverse:a.noop,data:e}))?f:"")+"        </ul>\n    </div>\n"},compiler:[7,">= 4.0.0"],main:function(a,b,c,d,e){var f,g=null!=b?b:{};return(null!=(f=c["if"].call(g,null!=b?b.styles:b,{name:"if",hash:{},fn:a.program(1,e,0),inverse:a.noop,data:e}))?f:"")+"\n"+(null!=(f=c["if"].call(g,null!=b?b.actions:b,{name:"if",hash:{},fn:a.program(5,e,0),inverse:a.noop,data:e}))?f:"")},useData:!0}),this.MediumInsert.Templates["src/js/templates/embeds-wrapper.hbs"]=Handlebars.template({compiler:[7,">= 4.0.0"],main:function(a,b,c,d,e){var f,g;return'<div class="medium-insert-embeds" contenteditable="false">\n	<figure>\n		<div class="medium-insert-embed">\n			'+(null!=(g=null!=(g=c.html||(null!=b?b.html:b))?g:c.helperMissing,f="function"==typeof g?g.call(null!=b?b:{},{name:"html",hash:{},data:e}):g)?f:"")+'\n		</div>\n	</figure>\n	<div class="medium-insert-embeds-overlay"></div>\n</div>'},useData:!0}),this.MediumInsert.Templates["src/js/templates/images-fileupload.hbs"]=Handlebars.template({compiler:[7,">= 4.0.0"],main:function(a,b,c,d,e){return'<input type="file" multiple>'},useData:!0}),this.MediumInsert.Templates["src/js/templates/images-image.hbs"]=Handlebars.template({1:function(a,b,c,d,e){return'        <div class="medium-insert-images-progress"></div>\n'},compiler:[7,">= 4.0.0"],main:function(a,b,c,d,e){var f,g,h=null!=b?b:{};return'<figure contenteditable="false">\n    <img src="'+a.escapeExpression((g=null!=(g=c.img||(null!=b?b.img:b))?g:c.helperMissing,"function"==typeof g?g.call(h,{name:"img",hash:{},data:e}):g))+'" alt="" />\n'+(null!=(f=c["if"].call(h,null!=b?b.progress:b,{name:"if",hash:{},fn:a.program(1,e,0),inverse:a.noop,data:e}))?f:"")+"</figure>\n"},useData:!0}),this.MediumInsert.Templates["src/js/templates/images-progressbar.hbs"]=Handlebars.template({compiler:[7,">= 4.0.0"],main:function(a,b,c,d,e){return'<progress min="0" max="100" value="0">0</progress>'},useData:!0}),this.MediumInsert.Templates["src/js/templates/images-toolbar.hbs"]=Handlebars.template({1:function(a,b,c,d,e){var f;return null!=(f=c["if"].call(null!=b?b:{},null!=b?b.label:b,{name:"if",hash:{},fn:a.program(2,e,0),inverse:a.noop,data:e}))?f:""},2:function(a,b,c,d,e){var f,g,h=null!=b?b:{},i=c.helperMissing,j="function";return'                <li>\n                    <button class="medium-editor-action" data-action="'+a.escapeExpression((g=null!=(g=c.key||e&&e.key)?g:i,typeof g===j?g.call(h,{name:"key",hash:{},data:e}):g))+'">'+(null!=(g=null!=(g=c.label||(null!=b?b.label:b))?g:i,f=typeof g===j?g.call(h,{name:"label",hash:{},data:e}):g)?f:"")+"</button>\n                </li>\n"},4:function(a,b,c,d,e){var f;return'	<div class="medium-insert-images-toolbar2 medium-editor-toolbar medium-editor-toolbar-active">\n		<ul class="medium-editor-toolbar-actions clearfix">\n'+(null!=(f=c.each.call(null!=b?b:{},null!=b?b.actions:b,{name:"each",hash:{},fn:a.program(5,e,0),inverse:a.noop,data:e}))?f:"")+"    	</ul>\n    </div>\n"},5:function(a,b,c,d,e){var f;return null!=(f=c["if"].call(null!=b?b:{},null!=b?b.label:b,{name:"if",hash:{},fn:a.program(6,e,0),inverse:a.noop,data:e}))?f:""},6:function(a,b,c,d,e){var f,g,h=null!=b?b:{},i=c.helperMissing,j="function";return'        	        <li>\n        	            <button class="medium-editor-action" data-action="'+a.escapeExpression((g=null!=(g=c.key||e&&e.key)?g:i,typeof g===j?g.call(h,{name:"key",hash:{},data:e}):g))+'">'+(null!=(g=null!=(g=c.label||(null!=b?b.label:b))?g:i,f=typeof g===j?g.call(h,{name:"label",hash:{},data:e}):g)?f:"")+"</button>\n        	        </li>\n"},compiler:[7,">= 4.0.0"],main:function(a,b,c,d,e){var f,g=null!=b?b:{};return'<div class="medium-insert-images-toolbar medium-editor-toolbar medium-toolbar-arrow-under medium-editor-toolbar-active">\n    <ul class="medium-editor-toolbar-actions clearfix">\n'+(null!=(f=c.each.call(g,null!=b?b.styles:b,{name:"each",hash:{},fn:a.program(1,e,0),inverse:a.noop,data:e}))?f:"")+"    </ul>\n</div>\n\n"+(null!=(f=c["if"].call(g,null!=b?b.actions:b,{name:"if",hash:{},fn:a.program(4,e,0),inverse:a.noop,data:e}))?f:"")},useData:!0}),function(a,b,c,d){"use strict";function e(a){return a.charAt(0).toUpperCase()+a.slice(1)}function f(c,e){var f;this.el=c,this.$el=a(c),this.templates=b.MediumInsert.Templates,e&&(f=e.editor,e.editor=null),this.options=a.extend(!0,{},h,e),this.options.editor=f,this._defaults=h,this._name=g,this.options&&this.options.editor&&(this.options.editor._serialize=this.options.editor.serialize,this.options.editor._destroy=this.options.editor.destroy,this.options.editor._setup=this.options.editor.setup,this.options.editor._hideInsertButtons=this.hideButtons,this.options.editor.serialize=this.editorSerialize,this.options.editor.destroy=this.editorDestroy,this.options.editor.setup=this.editorSetup,this.options.editor.getExtensionByName("placeholder")!==d&&(this.options.editor.getExtensionByName("placeholder").updatePlaceholder=this.editorUpdatePlaceholder))}var g="mediumInsert",h={editor:null,enabled:!0,addons:{images:!0,embeds:!0}};f.prototype.init=function(){this.$el.addClass("medium-editor-insert-plugin"),("object"!=typeof this.options.addons||0===Object.keys(this.options.addons).length)&&this.disable(),this.initAddons(),this.clean(),this.events()},f.prototype.events=function(){var c=this;this.$el.on("dragover drop",function(a){a.preventDefault()}).on("keyup click",a.proxy(this,"toggleButtons")).on("selectstart mousedown",".medium-insert, .medium-insert-buttons",a.proxy(this,"disableSelection")).on("click",".medium-insert-buttons-show",a.proxy(this,"toggleAddons")).on("click",".medium-insert-action",a.proxy(this,"addonAction")).on("paste",".medium-insert-caption-placeholder",function(b){a.proxy(c,"removeCaptionPlaceholder")(a(b.target))}),a(b).on("resize",a.proxy(this,"positionButtons",null))},f.prototype.getEditor=function(){return this.options.editor},f.prototype.editorSerialize=function(){var b=this._serialize();return a.each(b,function(c){var d=a("<div />").html(b[c].value);d.find(".medium-insert-buttons").remove(),d.find("[data-embed-code]").each(function(){var b=a(this);b.html(b.attr("data-embed-code"))}),b[c].value=d.html()}),b},f.prototype.editorDestroy=function(){a.each(this.elements,function(b,c){a(c).data("plugin_"+g).disable()}),this._destroy()},f.prototype.editorSetup=function(){this._setup(),a.each(this.elements,function(b,c){a(c).data("plugin_"+g).enable()})},f.prototype.editorUpdatePlaceholder=function(b,c){var d=a(b).children().not(".medium-insert-buttons").contents();c||1!==d.length||"br"!==d[0].nodeName.toLowerCase()?this.hidePlaceholder(b):(this.showPlaceholder(b),this.base._hideInsertButtons(a(b)))},f.prototype.triggerInput=function(){this.getEditor()&&this.getEditor().trigger("editableInput",null,this.el)},f.prototype.deselect=function(){c.getSelection().removeAllRanges()},f.prototype.disable=function(){this.options.enabled=!1,this.$el.find(".medium-insert-buttons").addClass("hide")},f.prototype.enable=function(){this.options.enabled=!0,this.$el.find(".medium-insert-buttons").removeClass("hide")},f.prototype.disableSelection=function(b){var c=a(b.target);(c.is("img")===!1||c.hasClass("medium-insert-buttons-show"))&&b.preventDefault()},f.prototype.initAddons=function(){var b=this;this.options.addons&&0!==this.options.addons.length&&a.each(this.options.addons,function(a,c){var d=g+e(a);return c===!1?void delete b.options.addons[a]:(b.$el[d](c),void(b.options.addons[a]=b.$el.data("plugin_"+d).options))})},f.prototype.clean=function(){var b,c,d,e=this;this.options.enabled!==!1&&(0===this.$el.children().length&&this.$el.html(this.templates["src/js/templates/core-empty-line.hbs"]().trim()),d=this.$el.contents().filter(function(){return"#text"===this.nodeName&&""!==a.trim(a(this).text())||"br"===this.nodeName.toLowerCase()}),d.each(function(){a(this).wrap("<p />"),e.moveCaret(a(this).parent(),a(this).text().length)}),this.addButtons(),b=this.$el.find(".medium-insert-buttons"),c=b.prev(),c.attr("class")&&c.attr("class").match(/medium\-insert(?!\-active)/)&&b.before(this.templates["src/js/templates/core-empty-line.hbs"]().trim()))},f.prototype.getButtons=function(){return this.options.enabled!==!1?this.templates["src/js/templates/core-buttons.hbs"]({addons:this.options.addons}).trim():void 0},f.prototype.addButtons=function(){0===this.$el.find(".medium-insert-buttons").length&&this.$el.append(this.getButtons())},f.prototype.toggleButtons=function(c){var d,e,f,g,h=a(c.target),i=b.getSelection(),j=this;this.options.enabled!==!1&&(i&&0!==i.rangeCount?(d=i.getRangeAt(0),e=a(d.commonAncestorContainer)):e=h,e.hasClass("medium-editor-insert-plugin")&&(e=e.find("p:first")),f=e.is("p")?e:e.closest("p"),this.clean(),h.hasClass("medium-editor-placeholder")===!1&&0===h.closest(".medium-insert-buttons").length&&0===e.closest(".medium-insert-buttons").length&&(this.$el.find(".medium-insert-active").removeClass("medium-insert-active"),a.each(this.options.addons,function(a){return h.closest(".medium-insert-"+a).length&&(e=h),e.closest(".medium-insert-"+a).length?(f=e.closest(".medium-insert-"+a),void(g=a)):void 0}),f.length&&(""===f.text().trim()&&!g||"images"===g)?(f.addClass("medium-insert-active"),setTimeout(function(){j.positionButtons(g),j.showButtons(g)},g?100:0)):this.hideButtons()))},f.prototype.showButtons=function(a){var b=this.$el.find(".medium-insert-buttons");b.show(),b.find("li").show(),a&&(b.find("li").hide(),b.find('a[data-addon="'+a+'"]').parent().show())},f.prototype.hideButtons=function(a){a=a||this.$el,a.find(".medium-insert-buttons").hide(),a.find(".medium-insert-buttons-addons").hide(),a.find(".medium-insert-buttons-show").removeClass("medium-insert-buttons-rotate")},f.prototype.positionButtons=function(a){var b,c,d=this.$el.find(".medium-insert-buttons"),e=this.$el.find(".medium-insert-active"),f=e.find("figure:first").length?e.find("figure:first"):e;e.length&&(b=e.position().left-parseInt(d.find(".medium-insert-buttons-addons").css("left"),10)-parseInt(d.find(".medium-insert-buttons-addons a:first").css("margin-left"),10),b=0>b?e.position().left:b,c=e.position().top+parseInt(e.css("margin-top"),10),a&&(e.position().left!==f.position().left&&(b=f.position().left),c+=e.height()+15),d.css({left:b,top:c}))},f.prototype.toggleAddons=function(){this.$el.find(".medium-insert-buttons-addons").fadeToggle(),this.$el.find(".medium-insert-buttons-show").toggleClass("medium-insert-buttons-rotate")},f.prototype.hideAddons=function(){this.$el.find(".medium-insert-buttons-addons").hide(),this.$el.find(".medium-insert-buttons-show").removeClass("medium-insert-buttons-rotate")},f.prototype.addonAction=function(b){var c=a(b.target).is("a")?a(b.target):a(b.target).closest("a"),d=c.data("addon"),f=c.data("action");this.$el.data("plugin_"+g+e(d))[f]()},f.prototype.moveCaret=function(a,d){var e,f,g;if(d=d||0,e=c.createRange(),f=b.getSelection(),g=a.get(0),!g.childNodes.length){var h=c.createTextNode(" ");g.appendChild(h)}e.setStart(g.childNodes[0],d),e.collapse(!0),f.removeAllRanges(),f.addRange(e)},f.prototype.addCaption=function(a,b){var c=a.find("figcaption");0===c.length&&a.append(this.templates["src/js/templates/core-caption.hbs"]({placeholder:b}))},f.prototype.removeCaptions=function(b){var c=this.$el.find("figcaption");b&&(c=c.not(b)),c.each(function(){(a(this).hasClass("medium-insert-caption-placeholder")||""===a(this).text().trim())&&a(this).remove()})},f.prototype.removeCaptionPlaceholder=function(a){var b=a.is("figcaption")?a:a.find("figcaption");b.length&&b.removeClass("medium-insert-caption-placeholder").removeAttr("data-placeholder")},a.fn[g]=function(b){return this.each(function(){var c,d=this;a(d).is("textarea")&&(c=a(d).attr("medium-editor-textarea-id"),d=a(d).siblings('[medium-editor-textarea-id="'+c+'"]').get(0)),a.data(d,"plugin_"+g)?"string"==typeof b&&a.data(d,"plugin_"+g)[b]&&a.data(d,"plugin_"+g)[b]():(a.data(d,"plugin_"+g,new f(d,b)),a.data(d,"plugin_"+g).init())})}}(jQuery,window,document),function(a,b,c,d){"use strict";function e(c,d){this.el=c,this.$el=a(c),this.templates=b.MediumInsert.Templates,this.core=this.$el.data("plugin_"+f),this.options=a.extend(!0,{},h,d),this._defaults=h,this._name=f,this.core.getEditor()&&(this.core.getEditor()._serializePreEmbeds=this.core.getEditor().serialize,this.core.getEditor().serialize=this.editorSerialize),this.init()}var f="mediumInsert",g="Embeds",h={label:'<span class="fa fa-youtube-play"></span>',placeholder:"Paste a YouTube, Vimeo, Facebook, Twitter or Instagram link and press Enter",oembedProxy:"http://medium.iframe.ly/api/oembed?iframe=1",captions:!0,captionPlaceholder:"Type caption (optional)",styles:{wide:{label:'<span class="fa fa-align-justify"></span>'},left:{label:'<span class="fa fa-align-left"></span>'},right:{label:'<span class="fa fa-align-right"></span>'}},actions:{remove:{label:'<span class="fa fa-times"></span>',clicked:function(){var b=a.Event("keydown");b.which=8,a(c).trigger(b)}}}};e.prototype.init=function(){var b=this.$el.find(".medium-insert-embeds");b.attr("contenteditable",!1),b.each(function(){0===a(this).find(".medium-insert-embeds-overlay").length&&a(this).append(a("<div />").addClass("medium-insert-embeds-overlay"))}),this.events(),this.backwardsCompatibility()},e.prototype.events=function(){a(c).on("click",a.proxy(this,"unselectEmbed")).on("keydown",a.proxy(this,"removeEmbed")).on("click",".medium-insert-embeds-toolbar .medium-editor-action",a.proxy(this,"toolbarAction")).on("click",".medium-insert-embeds-toolbar2 .medium-editor-action",a.proxy(this,"toolbar2Action")),this.$el.on("keyup click paste",a.proxy(this,"togglePlaceholder")).on("keydown",a.proxy(this,"processLink")).on("click",".medium-insert-embeds-overlay",a.proxy(this,"selectEmbed")).on("contextmenu",".medium-insert-embeds-placeholder",a.proxy(this,"fixRightClickOnPlaceholder"))},e.prototype.backwardsCompatibility=function(){var b=this;this.$el.find(".mediumInsert-embeds").removeClass("mediumInsert-embeds").addClass("medium-insert-embeds"),this.$el.find(".medium-insert-embeds").each(function(){0===a(this).find(".medium-insert-embed").length&&(a(this).after(b.templates["src/js/templates/embeds-wrapper.hbs"]({html:a(this).html()})),a(this).remove())})},e.prototype.editorSerialize=function(){var b=this._serializePreEmbeds();return a.each(b,function(c){var d=a("<div />").html(b[c].value);d.find(".medium-insert-embeds").removeAttr("contenteditable"),d.find(".medium-insert-embeds-overlay").remove(),b[c].value=d.html()}),b},e.prototype.add=function(){var a=this.$el.find(".medium-insert-active");a.html(this.templates["src/js/templates/core-empty-line.hbs"]().trim()),a.is("p")&&(a.replaceWith('<div class="medium-insert-active">'+a.html()+"</div>"),a=this.$el.find(".medium-insert-active"),this.core.moveCaret(a)),a.addClass("medium-insert-embeds medium-insert-embeds-input medium-insert-embeds-active"),this.togglePlaceholder({target:a.get(0)}),a.click(),this.core.hideButtons()},e.prototype.togglePlaceholder=function(c){var d,e,f,g=a(c.target),h=b.getSelection();h&&0!==h.rangeCount&&(d=h.getRangeAt(0),e=a(d.commonAncestorContainer),e.hasClass("medium-insert-embeds-active")?g=e:e.closest(".medium-insert-embeds-active").length&&(g=e.closest(".medium-insert-embeds-active")),g.hasClass("medium-insert-embeds-active")?(f=g.text().trim(),""===f&&g.hasClass("medium-insert-embeds-placeholder")===!1?g.addClass("medium-insert-embeds-placeholder").attr("data-placeholder",this.options.placeholder):""!==f&&g.hasClass("medium-insert-embeds-placeholder")&&g.removeClass("medium-insert-embeds-placeholder").removeAttr("data-placeholder")):this.$el.find(".medium-insert-embeds-active").remove())},e.prototype.fixRightClickOnPlaceholder=function(b){this.core.moveCaret(a(b.target))},e.prototype.processLink=function(a){var b,c=this.$el.find(".medium-insert-embeds-active");if(c.length)return b=c.text().trim(),""===b&&-1!==[8,46,13].indexOf(a.which)?void c.remove():void(13===a.which&&(a.preventDefault(),a.stopPropagation(),this.options.oembedProxy?this.oembed(b):this.parseUrl(b)))},e.prototype.oembed=function(c){var d=this;a.support.cors=!0,a.ajax({crossDomain:!0,cache:!1,url:this.options.oembedProxy,dataType:"json",data:{url:c},success:function(b){var e=b&&b.html;if(b&&!e&&"photo"===b.type&&b.url&&(e='<img src="'+b.url+'" alt="">'),!e)return void a.proxy(d,"convertBadEmbed",c)();if(e&&e.indexOf("</script>")>-1){var f=a("<div>").attr("data-embed-code",e).html(e);e=a("<div>").append(f).html()}a.proxy(d,"embed",e)()},error:function(e,f,g){var h=function(){try{return JSON.parse(e.responseText)}catch(a){}}();"undefined"!=typeof b.console?b.console.log(h&&h.error||e.status||g.message):b.alert("Error requesting media from "+d.options.oembedProxy+" to insert: "+g+" (response status: "+e.status+")"),a.proxy(d,"convertBadEmbed",c)()}})},e.prototype.parseUrl=function(b){var c;return new RegExp(["youtube","youtu.be","vimeo","instagram"].join("|")).test(b)?(c=b.replace(/\n?/g,"").replace(/^((http(s)?:\/\/)?(www\.)?(youtube\.com|youtu\.be)\/(watch\?v=|v\/)?)([a-zA-Z0-9\-_]+)(.*)?$/,'<div class="video video-youtube"><iframe width="420" height="315" src="//www.youtube.com/embed/$7" frameborder="0" allowfullscreen></iframe></div>').replace(/^https?:\/\/vimeo\.com(\/.+)?\/([0-9]+)$/,'<div class="video video-vimeo"><iframe src="//player.vimeo.com/video/$2" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>').replace(/^https?:\/\/instagram\.com\/p\/(.+)\/?$/,'<span class="instagram"><iframe src="//instagram.com/p/$1/embed/" width="612" height="710" frameborder="0" scrolling="no" allowtransparency="true"></iframe></span>'),void this.embed(/<("[^"]*"|'[^']*'|[^'">])*>/.test(c)?c:!1)):(a.proxy(this,"convertBadEmbed",b)(),!1)},e.prototype.embed=function(a){var b=this.$el.find(".medium-insert-embeds-active");return a?(b.after(this.templates["src/js/templates/embeds-wrapper.hbs"]({html:a})),b.remove(),this.core.triggerInput(),-1!==a.indexOf("facebook")&&"undefined"!=typeof FB&&setTimeout(function(){FB.XFBML.parse()},2e3),void 0):(alert("Incorrect URL format specified"),!1)},e.prototype.convertBadEmbed=function(b){var c,d,e,f=this.templates["src/js/templates/core-empty-line.hbs"]().trim();c=this.$el.find(".medium-insert-embeds-active"),e=a(f),c.before(e),c.remove(),e.html(b),d=a(f),e.after(d),this.core.triggerInput(),this.core.moveCaret(c)},e.prototype.selectEmbed=function(b){if(this.core.options.enabled){var c=a(b.target).hasClass("medium-insert-embeds")?a(b.target):a(b.target).closest(".medium-insert-embeds"),d=this;c.addClass("medium-insert-embeds-selected"),setTimeout(function(){d.addToolbar(),d.options.captions&&d.core.addCaption(c.find("figure"),d.options.captionPlaceholder)},50)}},e.prototype.unselectEmbed=function(b){var c=a(b.target).hasClass("medium-insert-embeds")?a(b.target):a(b.target).closest(".medium-insert-embeds"),d=this.$el.find(".medium-insert-embeds-selected");return c.hasClass("medium-insert-embeds-selected")?(d.not(c).removeClass("medium-insert-embeds-selected"),a(".medium-insert-embeds-toolbar, .medium-insert-embeds-toolbar2").remove(),this.core.removeCaptions(c.find("figcaption")),void((a(b.target).is(".medium-insert-caption-placeholder")||a(b.target).is("figcaption"))&&(c.removeClass("medium-insert-embeds-selected"),this.core.removeCaptionPlaceholder(c.find("figure"))))):(d.removeClass("medium-insert-embeds-selected"),a(".medium-insert-embeds-toolbar, .medium-insert-embeds-toolbar2").remove(),void(a(b.target).is(".medium-insert-caption-placeholder")?this.core.removeCaptionPlaceholder(c.find("figure")):a(b.target).is("figcaption")===!1&&this.core.removeCaptions()))},e.prototype.removeEmbed=function(b){var c,d;(8===b.which||46===b.which)&&(c=this.$el.find(".medium-insert-embeds-selected"),c.length&&(b.preventDefault(),a(".medium-insert-embeds-toolbar, .medium-insert-embeds-toolbar2").remove(),d=a(this.templates["src/js/templates/core-empty-line.hbs"]().trim()),c.before(d),c.remove(),this.core.hideAddons(),this.core.moveCaret(d),this.core.triggerInput()))},e.prototype.addToolbar=function(){var b,c,d,e=this.$el.find(".medium-insert-embeds-selected"),f=!1;if(0!==e.length){var g=this.core.getEditor(),h=g.options.elementsContainer||"body";a(h).append(this.templates["src/js/templates/embeds-toolbar.hbs"]({styles:this.options.styles,actions:this.options.actions}).trim()),b=a(".medium-insert-embeds-toolbar"),c=a(".medium-insert-embeds-toolbar2"),d=e.offset().top-b.height()-8-2-5,0>d&&(d=0),b.css({top:d,left:e.offset().left+e.width()/2-b.width()/2}).show(),c.css({top:e.offset().top+2,left:e.offset().left+e.width()-c.width()-4}).show(),b.find("button").each(function(){e.hasClass("medium-insert-embeds-"+a(this).data("action"))&&(a(this).addClass("medium-editor-button-active"),f=!0)}),f===!1&&b.find("button").first().addClass("medium-editor-button-active")}},e.prototype.toolbarAction=function(b){var c=a(b.target).is("button")?a(b.target):a(b.target).closest("button"),d=c.closest("li"),e=d.closest("ul"),f=e.find("li"),g=this.$el.find(".medium-insert-embeds-selected"),h=this;c.addClass("medium-editor-button-active"),d.siblings().find(".medium-editor-button-active").removeClass("medium-editor-button-active"),f.find("button").each(function(){var b="medium-insert-embeds-"+a(this).data("action");a(this).hasClass("medium-editor-button-active")?(g.addClass(b),h.options.styles[a(this).data("action")].added&&h.options.styles[a(this).data("action")].added(g)):(g.removeClass(b),h.options.styles[a(this).data("action")].removed&&h.options.styles[a(this).data("action")].removed(g))}),this.core.triggerInput()},e.prototype.toolbar2Action=function(b){var c=a(b.target).is("button")?a(b.target):a(b.target).closest("button"),d=this.options.actions[c.data("action")].clicked;d&&d(this.$el.find(".medium-insert-embeds-selected")),this.core.triggerInput()},a.fn[f+g]=function(b){return this.each(function(){a.data(this,"plugin_"+f+g)||a.data(this,"plugin_"+f+g,new e(this,b))})}}(jQuery,window,document),function(a,b,c,d,e){"use strict";function f(c,d){this.el=c,this.$el=a(c),this.$currentImage=null,this.templates=b.MediumInsert.Templates,this.core=this.$el.data("plugin_"+g),this.options=a.extend(!0,{},i,d),this._defaults=i,this._name=g,this.options.preview&&!b.FileReader&&(this.options.preview=!1),this.core.getEditor()&&(this.core.getEditor()._serializePreImages=this.core.getEditor().serialize,this.core.getEditor().serialize=this.editorSerialize),this.init()}var g="mediumInsert",h="Images",i={label:'<span class="fa fa-camera"></span>',deleteMethod:"POST",deleteScript:"delete.php",preview:!0,captions:!0,captionPlaceholder:"Type caption for image (optional)",autoGrid:3,fileUploadOptions:{url:"upload.php",acceptFileTypes:/(\.|\/)(gif|jpe?g|png)$/i},fileDeleteOptions:{},styles:{wide:{label:'<span class="fa fa-align-justify"></span>'},left:{label:'<span class="fa fa-align-left"></span>'},right:{label:'<span class="fa fa-align-right"></span>'},grid:{label:'<span class="fa fa-th"></span>'}},actions:{remove:{label:'<span class="fa fa-times"></span>',clicked:function(){var b=a.Event("keydown");b.which=8,a(c).trigger(b)}}},sorting:function(){var b=this;a(".medium-insert-images").sortable({group:"medium-insert-images",containerSelector:".medium-insert-images",itemSelector:"figure",placeholder:'<figure class="placeholder">',handle:"img",nested:!1,vertical:!1,afterMove:function(){b.core.triggerInput()}})},messages:{acceptFileTypesError:"This file is not in a supported format: ",maxFileSizeError:"This file is too big: "}};f.prototype.init=function(){var a=this.$el.find(".medium-insert-images");a.find("figcaption").attr("contenteditable",!0),a.find("figure").attr("contenteditable",!1),this.events(),this.backwardsCompatibility(),this.sorting()},f.prototype.events=function(){a(c).on("click",a.proxy(this,"unselectImage")).on("keydown",a.proxy(this,"removeImage")).on("click",".medium-insert-images-toolbar .medium-editor-action",a.proxy(this,"toolbarAction")).on("click",".medium-insert-images-toolbar2 .medium-editor-action",a.proxy(this,"toolbar2Action")),this.$el.on("click",".medium-insert-images img",a.proxy(this,"selectImage"))},f.prototype.backwardsCompatibility=function(){this.$el.find(".mediumInsert").removeClass("mediumInsert").addClass("medium-insert-images"),this.$el.find(".medium-insert-images.small").removeClass("small").addClass("medium-insert-images-left")},f.prototype.editorSerialize=function(){var b=this._serializePreImages();return a.each(b,function(c){var d=a("<div />").html(b[c].value);d.find(".medium-insert-images").find("figcaption, figure").removeAttr("contenteditable"),b[c].value=d.html()}),b},f.prototype.add=function(){var b=this,c=a(this.templates["src/js/templates/images-fileupload.hbs"]()),d={dataType:"json",add:function(c,d){a.proxy(b,"uploadAdd",c,d)()},done:function(c,d){a.proxy(b,"uploadDone",c,d)()}};(new XMLHttpRequest).upload&&(d.progress=function(c,d){a.proxy(b,"uploadProgress",c,d)()},d.progressall=function(c,d){a.proxy(b,"uploadProgressall",c,d)()}),c.fileupload(a.extend(!0,{},this.options.fileUploadOptions,d)),c.click()},f.prototype.uploadAdd=function(b,c){var d,e=this.$el.find(".medium-insert-active"),f=this,g=[],h=c.files[0],i=this.options.fileUploadOptions.acceptFileTypes,j=this.options.fileUploadOptions.maxFileSize;return i&&!i.test(h.type)?g.push(this.options.messages.acceptFileTypesError+h.name):j&&h.size>j&&g.push(this.options.messages.maxFileSizeError+h.name),g.length>0?void alert(g.join("\n")):(this.core.hideButtons(),e.is("p")&&(e.replaceWith('<div class="medium-insert-active">'+e.html()+"</div>"),e=this.$el.find(".medium-insert-active"),this.core.moveCaret(e)),e.addClass("medium-insert-images"),this.options.preview===!1&&0===e.find("progress").length&&(new XMLHttpRequest).upload&&e.append(this.templates["src/js/templates/images-progressbar.hbs"]()),void((c.autoUpload||c.autoUpload!==!1&&a(b.target).fileupload("option","autoUpload"))&&c.process().done(function(){f.options.preview?(d=new FileReader,d.onload=function(b){a.proxy(f,"showImage",b.target.result,c)()},d.readAsDataURL(c.files[0])):c.submit()})))},f.prototype.uploadProgressall=function(a,b){var c,d;this.options.preview===!1&&(c=parseInt(b.loaded/b.total*100,10),d=this.$el.find(".medium-insert-active").find("progress"),d.attr("value",c).text(c),100===c&&d.remove())},f.prototype.uploadProgress=function(a,b){var c,d;this.options.preview&&(c=100-parseInt(b.loaded/b.total*100,10),d=b.context.find(".medium-insert-images-progress"),d.css("width",c+"%"),0===c&&d.remove())},f.prototype.uploadDone=function(b,c){a.proxy(this,"showImage",c.result.files[0].url,c)(),this.core.clean(),this.sorting()},f.prototype.showImage=function(b,c){var d,e,f=this.$el.find(".medium-insert-active");return f.click(),e=this,this.options.preview&&c.context?(d=this.getDOMImage(),d.onload=function(){c.context.find("img").attr("src",d.src),this.options.uploadCompleted&&this.options.uploadCompleted(c.context,c),e.core.triggerInput()}.bind(this),d.src=b):(c.context=a(this.templates["src/js/templates/images-image.hbs"]({img:b,progress:this.options.preview})).appendTo(f),f.find("br").remove(),this.options.autoGrid&&f.find("figure").length>=this.options.autoGrid&&(a.each(this.options.styles,function(a,b){var c="medium-insert-images-"+a;f.removeClass(c),b.removed&&b.removed(f)}),f.addClass("medium-insert-images-grid"),this.options.styles.grid.added&&this.options.styles.grid.added(f)),this.options.preview?c.submit():this.options.uploadCompleted&&this.options.uploadCompleted(c.context,c)),this.core.triggerInput(),c.context},f.prototype.getDOMImage=function(){return new b.Image},f.prototype.selectImage=function(b){if(this.core.options.enabled){var c=a(b.target),d=this;this.$currentImage=c,this.$el.blur(),c.addClass("medium-insert-image-active"),c.closest(".medium-insert-images").addClass("medium-insert-active"),setTimeout(function(){d.addToolbar(),d.options.captions&&d.core.addCaption(c.closest("figure"),d.options.captionPlaceholder)},50)}},f.prototype.unselectImage=function(b){var c=a(b.target),d=this.$el.find(".medium-insert-image-active");return c.is("img")&&c.hasClass("medium-insert-image-active")?(d.not(c).removeClass("medium-insert-image-active"),a(".medium-insert-images-toolbar, .medium-insert-images-toolbar2").remove(),void this.core.removeCaptions(c)):(d.removeClass("medium-insert-image-active"),a(".medium-insert-images-toolbar, .medium-insert-images-toolbar2").remove(),c.is(".medium-insert-caption-placeholder")?this.core.removeCaptionPlaceholder(d.closest("figure")):c.is("figcaption")===!1&&this.core.removeCaptions(),void(this.$currentImage=null))},f.prototype.removeImage=function(b){var c,d,e;(8===b.which||46===b.which)&&(c=this.$el.find(".medium-insert-image-active"),c.length&&(b.preventDefault(),this.deleteFile(c.attr("src")),d=c.closest(".medium-insert-images"),c.closest("figure").remove(),a(".medium-insert-images-toolbar, .medium-insert-images-toolbar2").remove(),0===d.find("figure").length&&(e=d.next(),(e.is("p")===!1||""!==e.text())&&(e=a(this.templates["src/js/templates/core-empty-line.hbs"]().trim()),
  d.before(e)),d.remove(),this.core.hideAddons(),this.core.moveCaret(e)),this.core.triggerInput()))},f.prototype.deleteFile=function(b){if(this.options.deleteScript){var c=this.options.deleteMethod||"POST";a.ajax(a.extend(!0,{},{url:this.options.deleteScript,type:c,data:{file:b}},this.options.fileDeleteOptions))}},f.prototype.addToolbar=function(){var b,c,d,e=this.$el.find(".medium-insert-image-active"),f=e.closest(".medium-insert-images"),g=!1,h=this.core.getEditor(),i=h.options.elementsContainer||"body";a(i).append(this.templates["src/js/templates/images-toolbar.hbs"]({styles:this.options.styles,actions:this.options.actions}).trim()),b=a(".medium-insert-images-toolbar"),c=a(".medium-insert-images-toolbar2"),d=e.offset().top-b.height()-8-2-5,0>d&&(d=0),b.css({top:d,left:e.offset().left+e.width()/2-b.width()/2}).show(),c.css({top:e.offset().top+2,left:e.offset().left+e.width()-c.width()-4}).show(),b.find("button").each(function(){f.hasClass("medium-insert-images-"+a(this).data("action"))&&(a(this).addClass("medium-editor-button-active"),g=!0)}),g===!1&&b.find("button").first().addClass("medium-editor-button-active")},f.prototype.toolbarAction=function(b){if(null!==this.$currentImage){var c=a(b.target).is("button")?a(b.target):a(b.target).closest("button"),d=c.closest("li"),e=d.closest("ul"),f=e.find("li"),g=this.$el.find(".medium-insert-active"),h=this;c.addClass("medium-editor-button-active"),d.siblings().find(".medium-editor-button-active").removeClass("medium-editor-button-active"),f.find("button").each(function(){var b="medium-insert-images-"+a(this).data("action");a(this).hasClass("medium-editor-button-active")?(g.addClass(b),h.options.styles[a(this).data("action")].added&&h.options.styles[a(this).data("action")].added(g)):(g.removeClass(b),h.options.styles[a(this).data("action")].removed&&h.options.styles[a(this).data("action")].removed(g))}),this.core.hideButtons(),this.core.triggerInput()}},f.prototype.toolbar2Action=function(b){if(null!==this.$currentImage){var c=a(b.target).is("button")?a(b.target):a(b.target).closest("button"),d=this.options.actions[c.data("action")].clicked;d&&d(this.$el.find(".medium-insert-image-active")),this.core.hideButtons(),this.core.triggerInput()}},f.prototype.sorting=function(){a.proxy(this.options.sorting,this)()},a.fn[g+h]=function(b){return this.each(function(){a.data(this,"plugin_"+g+h)||a.data(this,"plugin_"+g+h,new f(this,b))})}}(jQuery,window,document,MediumEditor.util);

