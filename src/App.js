import React, {Component} from 'react';
import isElectron from 'is-electron';
const MediumEditor = window.MediumEditor;
const $ = window.$;

class App extends Component {
  componentDidMount() {
    if (isElectron()) {
      const editor = new MediumEditor('.container');

      $('.container').mediumInsert({
        editor: editor,
        addons: {
          images: {
            uploadScript: null,
            deleteScript: null,
            captionPlaceholder: 'Type caption for image',
            styles: {
              slideshow: {
                label: '<span class="fa fa-play"></span>',
                added: function ($el) {
                  $el
                    .data('cycle-center-vert', true)
                    .cycle({
                      slides: 'figure'
                    });
                },
                removed: function ($el) {
                  $el.cycle('destroy');
                }
              }
            },
            actions: null
          }
        }
      });
    }
  }
  render() {
    return (
      <div className="container"/>
    );
  }
}

export default App;
